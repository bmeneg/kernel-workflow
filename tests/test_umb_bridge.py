"""Webhook interaction tests."""
import copy
from os import environ
from time import sleep
import typing
from unittest import mock

from gitlab.exceptions import GitlabListError
from pika.exceptions import AMQPError

from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook import session
from webhook import umb_bridge
from webhook.common import get_arg_parser
from webhook.rh_metadata import Projects


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestUmbBridge(NoSocketTestCase):
    """ Test Webhook class."""

    UMB_PUBLIC = {"event": {"target": "bug",
                            "routing_key": "bug.modify",
                            "time": "2021-05-13T08:38:22",
                            "action": "modify",
                            "user": {"real_name": "Bugzilla User",
                                     "id": 12345,
                                     "login": "buguser@example.com"
                                     },
                            "bug_id": 1234567,
                            "change_set": "140589.1620895102.43339"
                            },
                  "bug": {"priority": "high",
                          "is_private": False,
                          "last_change_time": "2021-05-13T08:34:23",
                          "keywords": ["Reproducer", "Triaged"],
                          "url": "",
                          "assigned_to": {"real_name": "Bugzilla User",
                                          "id": 12345,
                                          "login": "buguser@example.com"
                                          },
                          "whiteboard": "",
                          "id": 1234567,
                          "creation_time": "2021-01-13T13:49:16",
                          "resolution": "",
                          "classification": "Red Hat",
                          "alias": [],
                          "status": {"name": "POST", "id": 26},
                          "reporter": {"real_name": "Bugzilla User",
                                       "id": 12345,
                                       "login": "omosnace@redhat.com"
                                       },
                          "summary": "A bad bug >:(",
                          "severity": "medium",
                          "flags": [],
                          "version": {"name": "8.3", "id": 5979},
                          "component": {"name": "kernel", "id": 130532},
                          "product": {"name": "Red Hat Enterprise Linux 8", "id": 370},
                          }
                  }

    UMB_PRIVATE = {"event": {"time": "2021-05-13T13:23:38",
                             "rule_id": 123,
                             "bug_id": 5454545,
                             "target": "bug",
                             "routing_key": "bug.modify",
                             "user": {"real_name": "Bugzilla User",
                                      "id": 12345,
                                      "login": "buguser@example.com"
                                      },
                             "action": "modify",
                             "change_set": "133863.1620912223.24249"
                             }
                   }

    GL_MR_URL = f'{defs.GITFORGE}/group/project/-/merge_requests/3344'
    GL_MESSAGE = {'object_kind': 'merge_request',
                  'object_attributes': {'action': 'open',
                                        'description': 'hello',
                                        'state': 'opened',
                                        'target_branch': '7.9',
                                        'url': GL_MR_URL
                                        },
                  'changes': {'description': {'previous': 'hi',
                                              'current': 'hello'
                                              }
                              },
                  'project': {'path_with_namespace': 'group/project',
                              'id': 6543}
                  }

    def setUp(self):
        """Reset session.SESSION."""
        session.SESSION = None

    @mock.patch.dict(environ, {'BUGZILLA_EMAIL': 'kwf@example.com'})
    def test_process_amqp_event(self):
        mock_send_queue = mock.Mock(send_function=None, fake_queue=False)
        headers = {'message-type': 'amqp-bridge'}

        mr_infos = [
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123'),
                bugs=frozenset([2233445])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/334'),
                bugs=frozenset([2233445])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/321'),
                bugs=frozenset([5454545])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/111'),
                bugs=frozenset([5454545])
            ),
        ]

        args = get_arg_parser('TEST').parse_args([])
        test_session = session.SessionRunner('umb_bridge', args, umb_bridge.MSG_HANDLERS)

        # Bug is not in bug_data
        mr_info_cache = umb_bridge.MRInfoCache({mr.url: mr for mr in mr_infos[:1]})
        body = copy.deepcopy(self.UMB_PRIVATE)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            event = session.create_event(test_session, headers, body)
            umb_bridge.process_amqp_event({}, test_session, event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event for unknown bug 5454545.', logs.output[-1])
            mock_send_queue.add_bug.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Bug is in bug_data
        mr_info_cache = umb_bridge.MRInfoCache({mr.url: mr for mr in mr_infos})
        body = copy.deepcopy(self.UMB_PRIVATE)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            event = session.create_event(test_session, headers, body)
            umb_bridge.process_amqp_event({}, test_session, event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            msg = 'Event for bug 5454545 by buguser@example.com relevant to these MRs:'
            self.assertIn(msg, logs.output[-1])
            self.assertIn('group/project!111', logs.output[-1])
            self.assertIn('group/project!321', logs.output[-1])
            mock_send_queue.add_bug.assert_has_calls(
                [mock.call('group/project!111'), mock.call('group/project!321')],
                any_order=True
            )
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

    def test_process_jira_event(self):
        """Adds relevant Jiras to the send_queue."""
        mock_send_queue = mock.Mock(send_function=None, fake_queue=False)
        headers = {'message-type': 'jira'}

        mr_infos = [
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123'),
                jiras=frozenset([defs.JiraKey('RHEL-123'), defs.JiraKey('RHEL-999')])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/334'),
                jiras=frozenset([defs.JiraKey('RHEL-123'), defs.JiraKey('RHEL-999')])
            )
        ]
        mr_info_cache = umb_bridge.MRInfoCache({mr.url: mr for mr in mr_infos})

        args = get_arg_parser('TEST').parse_args([])
        test_session = session.SessionRunner('umb_bridge', args, umb_bridge.MSG_HANDLERS)

        # Event is not for a kernel component issue, ignored.
        event = {'issue': {'key': 'RHEL-999', 'fields': {}}, 'user': {'name': 'not_a_bot'}}
        event['issue']['fields']['components'] = [{'name': 'systemd'}]
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event for non-kernel issue', logs.output[-1])
            mock_send_queue.add_jira.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira is not in the cache, ignored.
        event = {'issue': {'key': 'RHEL-456', 'fields': {}}, 'user': {'name': 'example_name'}}
        event['issue']['fields']['components'] = [{'name': 'kernel-rt'}]
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            self.assertIn('Ignoring event not relevant to any known MRs', logs.output[-1])
            mock_send_queue.add_jira.assert_not_called()
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

        # Jira is in the cache, queue MRs.
        event = {'issue': {'key': 'RHEL-123', 'fields': {}}, 'user': {'name': 'example_name'}}
        event['issue']['fields']['components'] = [{'name': 'kernel'}]
        test_event = session.create_event(test_session, headers, event)
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_jira_event({}, test_session, test_event,
                                          mr_info_cache=mr_info_cache,
                                          send_queue=mock_send_queue)
            msg = 'Event matches'
            self.assertIn(msg, logs.output[-1])
            self.assertEqual(mock_send_queue.add_jira.call_count, 2)
            self.assertEqual(mock_send_queue.send_function, test_session.queue.send_message)

    def test_process_gitlab_mr(self):
        headers = {'message-type': 'gitlab'}
        mr_infos = [
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123'),
                bugs=frozenset([2233445])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/3344'),
                bugs=frozenset([2233445])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/321'),
                bugs=frozenset([5454545])
            ),
            umb_bridge.MRInfo(
                url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/111'),
                bugs=frozenset([5454545])
            ),
        ]
        mr_info_cache = umb_bridge.MRInfoCache({mr.url: mr for mr in mr_infos})

        test_session = mock.Mock()

        # MR has been closed, removed it from the cache.
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            msg = copy.deepcopy(self.GL_MESSAGE)
            msg['object_attributes']['action'] = 'close'
            event = session.create_event(test_session, headers, msg)

            umb_bridge.process_gitlab_mr({}, test_session, event, mr_info_cache=mr_info_cache)
            self.assertIn('Removed <MRInfo group/project!3344', logs.output[-1])

        # MR has an unknown target branch, nothing to do.
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            msg = copy.deepcopy(self.GL_MESSAGE)
            msg['object_attributes']['target_branch'] = 'bad value'
            event = session.create_event(test_session, headers, msg)
            event.rh_branch = None

            umb_bridge.process_gitlab_mr({}, test_session, event, mr_info_cache=mr_info_cache)
            self.assertIn('Event is not associated with a recognized branch', logs.output[-1])

        # MR is added to bug_data.
        msg = copy.deepcopy(self.GL_MESSAGE)
        description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                       'Bugzilla: https://bugzilla.redhat.com/2233445')
        old_description = ('Bugzilla: https://bugzilla.redhat.com/5544332\n'
                           'Bugzilla: https://bugzilla.redhat.com/2233445')
        msg['changes']['description']['current'] = description
        msg['changes']['description']['previous'] = old_description
        msg['object_attributes']['description'] = description
        event = session.create_event(test_session, headers, msg)
        event.rh_branch = mock.Mock(fix_versions=[])
        with self.assertLogs('cki.webhook.umb_bridge', level='INFO') as logs:
            umb_bridge.process_gitlab_mr({}, test_session, event, mr_info_cache=mr_info_cache)
            self.assertIn('Adding <MRInfo group/project!3344', logs.output[-1])

    def test_SendQueue(self):
        exchange = 'cki.exchange.test'
        route = 'cki.kwf.test.notice'
        send_queue = umb_bridge.SendQueue(exchange, route)

        paths = ['group/project!123', 'group/project!234', 'group/project!345']

        # Ensure nothing happens if send_function is not set.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='WARNING') as logs:
            send_queue.add_bug(paths[0])
            self.assertIn('send_function is not set!', logs.output[-1])
            self.assertEqual(len(send_queue.data), 0)

        # Nothing to send.
        send_function = mock.Mock()
        send_queue.send_function = send_function
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('Empty.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])

        # Add an item to the queue.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='INFO') as logs:
            send_queue.add_bug(paths[0])
            self.assertIn('Adding group/project!123 for bughook.', logs.output[-1])
            send_queue.add_bug(paths[1])
            self.assertIn('Adding group/project!234 for bughook.', logs.output[-1])
            send_queue.add_bug(paths[2])
            self.assertIn('Adding group/project!345 for bughook.', logs.output[-1])
            self.assertTrue((umb_bridge.MappingType.Bug, paths[0]) in send_queue.data)
            self.assertTrue((umb_bridge.MappingType.Bug, paths[1]) in send_queue.data)
            self.assertTrue((umb_bridge.MappingType.Bug, paths[2]) in send_queue.data)

        # Nothing to send as nothing has been in the queue more than SEND_DELAY.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('Nothing older than SEND_DELAY (1) in data.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])

        # Wait over SEND_DELAY and now we could send but send_function is not set.
        send_queue.send_function = None
        sleep(umb_bridge.SEND_DELAY)
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn('send_function is not set!', logs.output[-1])
            self.assertEqual(len(send_queue.data), 3)

        # Now we can send.
        send_queue.send_function = send_function
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            send_queue.send_all()
            self.assertIn(f'Sent {paths[0]} for bughook.', logs.output[-5])
            self.assertIn(f'Sent {paths[1]} for bughook.', logs.output[-4])
            self.assertIn(f'Sent {paths[2]} for bughook.', logs.output[-3])
            self.assertIn('Empty.', logs.output[-2])
            self.assertIn('Nothing to send.', logs.output[-1])
            self.assertEqual(len(send_queue.data), 0)
            self.assertEqual(send_function.call_count, 3)
            # Confirm the expected headers and data is given to the send_function.
            expected_headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE,
                                'event_target_webhook': 'bughook'}
            for count, call in enumerate(send_function.call_args_list, start=0):
                self.assertEqual(call.kwargs['headers'], expected_headers)
                self.assertEqual(call.kwargs['data'], {'mrpath': paths[count]})

        # send_function raises an amqp exception?
        send_queue.add_bug(paths[0])
        send_function.side_effect = AMQPError('oh no!')
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='ERROR') as logs:
            exception_hit = False
            try:
                send_queue._send_message((umb_bridge.MappingType.Bug, paths[0]))
            except AMQPError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error sending message: oh no!', logs.output[-1])
        send_function.reset_mock(side_effect=True)

        # Test check_status.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            with mock.patch('sys.exit') as mock_exit:
                send_queue.add_bug(paths[0])
                send_queue.check_status()
                self.assertIn('Sender thread not running as expected.', logs.output[-2])
                self.assertIn(f'Sent {paths[0]} for bughook.', logs.output[-1])
                mock_exit.assert_called_with(1)

        # Test start.
        with self.assertLogs('cki.webhook.umb_bridge.SendQueue', level='DEBUG') as logs:
            # Test start()
            self.assertFalse(send_queue._thread.is_alive())
            send_queue.start()
            self.assertIn('Starting sender thread.', logs.output[-1])
            self.assertTrue(send_queue._thread.is_alive())

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}
    )
    def test_get_session_projects(self):
        """Returns the list of Projects matching the route keys and environment."""
        class SessionProjectsTest(typing.NamedTuple):
            route_keys: list[str]
            sandbox: bool
            expected_project_ids: typing.Tuple[int, ...]

        route_keys = ['umb.bridge.cool.route',
                      'gitlab.com.redhat.rhel.src.kernel.rhel-8.merge_request',  # id 12345
                      'gitlab.com.redhat.rhel.src.kernel.rhel-8.note',
                      'gitlab.com.redhat.rhel.src.kernel.rhel-8-sandbox.merge_request'  # id 56789
                      ]

        tests = [
            # Matches sandbox project 56789.
            SessionProjectsTest(
                route_keys=route_keys,
                sandbox=True,
                expected_project_ids=(56789,)
            ),
            # Matches non-sandbox project 12345.
            SessionProjectsTest(
                route_keys=route_keys,
                sandbox=False,
                expected_project_ids=(12345,)
            ),
        ]

        rh_projects = Projects()
        for test in tests:
            with self.subTest(**test._asdict()):
                results = umb_bridge.get_session_projects(
                    rh_projects, test.route_keys, test.sandbox
                )
                self.assertCountEqual(test.expected_project_ids, [proj.id for proj in results])

    @mock.patch('webhook.umb_bridge.SendQueue')
    @mock.patch('webhook.umb_bridge.MRInfoCache')
    @mock.patch.object(session.SessionRunner, 'consume_messages')
    def test_main(self, mock_consume_messages, mock_MrInfoCache, mock_SendQueue):
        args = ['--rabbitmq-routing-key',
                'umb.VirtualTopic.bugzilla gitlab.com.group.project.merge_request',
                '--rabbitmq-sender-exchange', 'cki.exchange.test',
                '--rabbitmq-sender-route', 'cki.route.test'
                ]

        umb_bridge.main(args)
        mock_SendQueue.assert_called_with('cki.exchange.test', 'cki.route.test', fake_queue=False)
        mock_SendQueue.return_value.start.assert_called_once()
        mock_MrInfoCache.new.assert_called_once()
        mock_consume_messages.assert_called_once_with(
            mr_info_cache=mock_MrInfoCache.new.return_value, send_queue=mock_SendQueue.return_value
        )

    def test_MappingType(self):
        """Ensure that each MappingType member value corresponds to a known webhook name in yaml."""
        rh_projects = Projects()
        self.assertTrue(len(rh_projects.webhooks) > 1)

        for mapping_type in umb_bridge.MappingType:
            with self.subTest(mapping_type=mapping_type, webhooks=rh_projects.webhooks.keys()):
                self.assertIn(mapping_type.value, rh_projects.webhooks)

    def mock_gl_mr(self, namespace, id, target_branch, bzs):
        """Return a mock gl_mr."""
        mr_spec = ['target_branch', 'web_url', 'description']
        web_url = 'https://gitlab.com/%s/-/merge_requests/%d'
        bz_url = 'https://bugzilla.redhat.com/%d'
        return mock.Mock(
            spec_set=mr_spec,
            target_branch=target_branch,
            web_url=web_url % (namespace, id),
            description='\n'.join(f'Bugzilla: {bz_url % bz_id}' for bz_id in bzs)
        )

    def test_get_project_mr_infos(self):
        """Return a list of MRInfo objects derived from the given Project."""
        class GetMRInfosTest(typing.NamedTuple):
            project: list
            mrs: dict
            expected_infos: dict[int, list[int]]

        projects = Projects()
        proj_c9s = projects.projects[24152864]

        mr123_c9s_main = self.mock_gl_mr(proj_c9s.namespace, 123, 'main', [1111, 2222])
        mr456_c9s_main = self.mock_gl_mr(proj_c9s.namespace, 456, 'main', [5555])
        mr789_c9s_bad = self.mock_gl_mr(proj_c9s.namespace, 789, 'bad', [7777])

        tests = [
            GetMRInfosTest(
                project=proj_c9s,
                mrs={123: mr123_c9s_main, 456: mr456_c9s_main, 789: mr789_c9s_bad},
                expected_infos={123: [1111, 2222], 456: [5555]}
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                mock_gl_instance = mock.Mock()
                mock_graphql = mock.Mock()
                with mock.patch('webhook.umb_bridge.get_project_mrs') as mock_get_mrs:
                    mock_get_mrs.return_value = list(test.mrs.values())
                    results = umb_bridge.get_project_mr_infos(
                        mock_gl_instance, mock_graphql, test.project
                    )
                    self.assertEqual(len(results), len(test.expected_infos))
                    while results:
                        mr_info = results.pop()
                        self.assertIn(mr_info.url.id, test.expected_infos)
                        self.assertCountEqual(mr_info.bugs, test.expected_infos[mr_info.url.id])
                    self.assertEqual(len(results), 0)

    def test_get_project_mrs(self):
        """Returns all the open MRs on the project."""
        class GetMrsTest(typing.NamedTuple):
            mr_list_side_effect: typing.Union[Exception, None]
            exception: typing.Union[Exception, None]

        tests = [
            # Working case. Not much to it...
            GetMrsTest(
                mr_list_side_effect=None,
                exception=False
            ),
            # Raises a 403 so returns an empty list.
            GetMrsTest(
                mr_list_side_effect=GitlabListError(response_code=403),
                exception=False
            ),
            # Raises some other error.
            GetMrsTest(
                mr_list_side_effect=GitlabListError(response_code=500),
                exception=True
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                mock_user = mock.Mock()
                mock_user.username = 'test_user'
                mock_instance = mock.Mock(user=mock_user)
                mock_gl_project = mock.Mock()
                mock_instance.projects.get.return_value = mock_gl_project
                if test.mr_list_side_effect:
                    mock_gl_project.mergerequests.list.side_effect = test.mr_list_side_effect
                    if test.exception:
                        with self.assertRaises(GitlabListError):
                            umb_bridge.get_project_mrs(mock_instance, 'namespace')
                    else:
                        self.assertEqual(umb_bridge.get_project_mrs(mock_instance, 'namespace'), [])
                else:
                    mock_gl_project.mergerequests.list.return_value = [1, 2, 3]
                    self.assertEqual(
                        umb_bridge.get_project_mrs(mock_instance, 'namespace'), [1, 2, 3]
                    )


class TestMRInfoCache(NoSocketTestCase):
    """Tests for the MRInfoCache thing."""

    @mock.patch('webhook.umb_bridge.get_session_projects')
    @mock.patch('webhook.umb_bridge.get_projects_mr_infos')
    def test_mrinfocache_new(self, mock_get_projects_mr_infos, mock_get_session_projects):
        """Creates a new MRInfoCache object populated with MRInfos derived from the Session."""
        mock_get_session_projects.return_value = [mock.Mock()]
        mock_mr_info = mock.Mock(
            url='https://123',
            bugs=[123, 456, 789],
            cves=['CVE-1993-53266', 'CVE-2023-42344'],
            jiras=['RHEL-4254', 'RHEL-999']
        )
        mock_get_projects_mr_infos.return_value = [mock_mr_info]
        mr_cache = umb_bridge.MRInfoCache.new(mock.Mock())
        self.assertEqual(mr_cache.mr_infos, {mock_mr_info.url: mock_mr_info})
        for bug in mock_mr_info.bugs:
            self.assertEqual(mr_cache.bugs[bug], {mock_mr_info})
        for cve in mock_mr_info.cves:
            self.assertEqual(mr_cache.cves[cve], {mock_mr_info})
        for jira in mock_mr_info.jiras:
            self.assertEqual(mr_cache.jiras[jira], {mock_mr_info})

    @mock.patch('webhook.umb_bridge.get_session_projects')
    def test_mrinfocache_new_raises(self, mock_get_session_projects):
        """Raises an error when no valid projects are derived from the session."""
        mock_get_session_projects.return_value = []
        with self.assertRaises(RuntimeError):
            umb_bridge.MRInfoCache.new(mock.Mock())
