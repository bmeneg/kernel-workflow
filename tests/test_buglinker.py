"""Webhook interaction tests."""
from copy import deepcopy
import os
import typing
from unittest import mock
from xmlrpc.client import Fault

from bugzilla import BugzillaError

from tests import fakes
from tests.no_socket_test_case import NoSocketTestCase
from webhook import buglinker
from webhook import defs
from webhook.pipelines import BridgeJob
from webhook.pipelines import PipelineType
from webhook.session import SessionRunner
from webhook.session_events import create_event


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBuglinker(NoSocketTestCase):
    """ Test Webhook class."""

    BZ_RESULTS = [{'id': 1234567,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'CVE-2020-02315 kernel: a bad one',
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': defs.EXT_TYPE_URL
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'CVE-2020-1234 kernel-rt: another one',
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': defs.EXT_TYPE_URL
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'A kernel bug?',
                   'external_bugs': []
                   }]

    def test_get_bugs(self):
        bzcon = mock.Mock()

        # No results.
        bzcon.getbugs.return_value = None
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            results = buglinker.get_bugs(bzcon, [123])
            self.assertEqual(results, None)
            self.assertIn('getbugs() returned an empty list for these bugs: [123]', logs.output[-1])

        # Results.
        bzcon.getbugs.return_value = [456]
        results = buglinker.get_bugs(bzcon, [123])
        self.assertEqual(results, bzcon.getbugs.return_value)

        # bzcon raises an exception.
        bzcon.getbugs.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.get_bugs(bzcon, ['burp'])
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error getting bugs.', logs.output[-1])

    @mock.patch('webhook.buglinker.unlink_mr_from_bzs')
    @mock.patch('webhook.buglinker.link_mr_to_bzs')
    @mock.patch('webhook.buglinker.get_bugs')
    def test_update_bugzilla(self, mock_get_bugs, mock_link, mock_unlink):
        """Check for expected calls."""
        mr_url = 'https://gitlab.com/redhat/rhel/8.y/kernel/-/merge_requests/10'
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon = mock.Mock()

        # Call add_mr_to_bz() with the expected parameters.
        bugs = {'link': [252626, 739272, 1544362],
                'unlink': [1234567]
                }
        bz0 = mock.Mock()
        bz1 = mock.Mock()
        bz2 = mock.Mock()
        mock_get_bugs.return_value = [bz0, bz1, bz2]

        buglinker.update_bugzilla(bugs, mr_url, bzcon)
        mock_get_bugs.assert_called_with(bzcon, [252626, 739272, 1544362])
        mock_link.assert_called_once_with(mock_get_bugs.return_value, ext_bz_bug_id, bzcon)
        mock_unlink.assert_called_once_with([1234567], ext_bz_bug_id, bzcon)

    def test_bz_is_linked_to_mr(self):
        """Check for expected return values."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        self.assertTrue(buglinker.bz_is_linked_to_mr(bz0, ext_bz_bug_id))
        self.assertFalse(buglinker.bz_is_linked_to_mr(bz1, ext_bz_bug_id))
        self.assertFalse(buglinker.bz_is_linked_to_mr(bz2, ext_bz_bug_id))

    def test_parse_cve_from_summary(self):
        """Check for expected return values."""
        summary = 'CVE-2020-02345 a bad problem.'
        self.assertTrue(buglinker.parse_cve_from_summary(summary))
        summary = 'kernel bug (again)'
        self.assertEqual(buglinker.parse_cve_from_summary(summary), None)
        summary = 'CVE-123-abcd kernel: the worst problem.'
        self.assertEqual(buglinker.parse_cve_from_summary(summary), None)

    @mock.patch('webhook.buglinker.get_bugs')
    def test_get_rt_cve_bugs(self, mock_get_bugs):
        bz0 = mock.Mock(id=11223344, summary='CVE-2020-12345 kernel: a problem to fix.')
        bz1 = mock.Mock(id=22334455, summary='kernel: another problem to fix.')
        bz2 = mock.Mock(id=33445566, summary='CVE-2021-4535 kernel: again a problem to fix.')
        bz3 = mock.Mock(id=44556677, summary='CVE-1996-01163 kernel-rt: how did we miss this?')
        bz4 = mock.Mock(id=55667788, summary='kernel stuff')
        bzcon = mock.Mock()

        # No CVE bugs.
        mock_get_bugs.return_value = [bz1, bz4]
        result = buglinker.get_rt_cve_bugs(bzcon, [bz1.id, bz4.id])
        self.assertEqual(result, [])
        bzcon.query.assert_not_called()

        # Three CVE bugs.
        mock_get_bugs.return_value = [bz0, bz1, bz2, bz3, bz4]
        result = buglinker.get_rt_cve_bugs(bzcon, [bz0.id, bz1.id, bz2.id, bz3.id, bz4.id])
        self.assertEqual(result, bzcon.query.return_value)
        self.assertEqual(sorted(bzcon.query.call_args.args[0]['v1']),
                         ['CVE-1996-01163', 'CVE-2020-12345', 'CVE-2021-4535'])

        # bzcon raises an exception.
        bzcon.query.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.get_rt_cve_bugs(bzcon, [bz0.id, bz1.id, bz2.id, bz3.id, bz4.id])
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error querying bugzilla.', logs.output[-1])

    @mock.patch('cki_lib.misc.is_production')
    @mock.patch('webhook.buglinker.get_rt_cve_bugs', mock.Mock(return_value=[]))
    def test_unlink_mr_from_bzs(self, mock_production):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon = mock.Mock()

        # Not production. Don't do anything.
        mock_production.return_value = False
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.unlink_mr_from_bzs([1234567], ext_bz_bug_id, bzcon)
            self.assertIn(f'Unlinking {ext_bz_bug_id} from BZ1234567.', logs.output[-1])
            bzcon.remove_external_tracker.assert_not_called()

        # Bugzilla Exception.
        mock_production.return_value = True
        bzcon.remove_external_tracker.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)

        # Known xmlrpc Fault.
        bzcon.remove_external_tracker.side_effect = Fault(faultCode=1006,
                                                          faultString='no link to remove.')
        with self.assertLogs('cki.webhook.buglinker', level='WARNING') as logs:
            exception_hit = False
            try:
                buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
            except Fault:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('xmlrpc fault 1006:', logs.output[-1])

        # Known xmlrpc Fault.
        bzcon.remove_external_tracker.side_effect = Fault(faultCode=123, faultString='oh no!')
        exception_hit = False
        try:
            buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
        except Fault:
            exception_hit = True
        self.assertTrue(exception_hit)

        # Go Go Go.
        bzcon.remove_external_tracker.reset_mock(side_effect=True)

        buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
        self.assertEqual(bzcon.remove_external_tracker.call_count, 2)

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_link_mr_to_bzs(self):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        ext_type_url = defs.EXT_TYPE_URL
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel',
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel-rt',
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel',
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        bzcon = mock.Mock()

        # Early return due to no untracked bugs
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.link_mr_to_bzs([bz0], ext_bz_bug_id, bzcon)
            self.assertIn(f'All bugs have an existing link to {ext_bz_bug_id}.', logs.output[-1])

        # bzcon raises an exception.
        bzcon.add_external_tracker.side_effect = BugzillaError('oh no!')
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
                exception_hit = False
                try:
                    buglinker.link_mr_to_bzs([bz0, bz1, bz2], ext_bz_bug_id, bzcon)
                except BugzillaError:
                    exception_hit = True
                self.assertFalse(exception_hit)
                self.assertIn(f'Problem adding tracker {ext_bz_bug_id} to BZs.', logs.output[-1])
                bzcon.add_external_tracker.assert_called_with([bz1.id, bz2.id],
                                                              ext_type_url=ext_type_url,
                                                              ext_bz_bug_id=ext_bz_bug_id)

        # Successful call to add_external_tracker()
        bzcon.add_external_tracker.side_effect = None
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            buglinker.link_mr_to_bzs([bz0, bz1, bz2], ext_bz_bug_id, bzcon)
        bzcon.add_external_tracker.assert_called_with([bz1.id, bz2.id],
                                                      ext_type_url=ext_type_url,
                                                      ext_bz_bug_id=ext_bz_bug_id)

    @mock.patch('webhook.buglinker.comment_already_posted')
    @mock.patch('webhook.buglinker.get_rt_cve_bugs')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_to_bugs(self, mock_rt_cve_bugs, mock_comment_check):
        """Generates a list of bugs and tries to post comments."""
        header = 'super header text string'
        res = mock.Mock()
        res.json.return_value = {'result': {'id': 123}, 'version': '1.1'}
        sess = mock.Mock()
        sess.post.return_value = res
        bzcon = mock.Mock()
        bzcon.get_requests_session.return_value = sess

        bz1 = mock.Mock(id=11223344)
        bz2 = mock.Mock(id=22334455)
        bz3 = mock.Mock(id=33445566)
        bz4 = mock.Mock(id=44556677)

        # No bug objects, nothing to return.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = None

            bridge_job_attrs = {
                'type': PipelineType.RHEL,
                'ds_pipeline': mock.Mock(web_url='https://yaddayadda'),
                'artifacts_text': 'artfifacts string',
                'checkout_link': 'checkout-link',
            }
            mock_bridge_job = mock.Mock(spec=list(bridge_job_attrs.keys()), **bridge_job_attrs)

            result = buglinker.post_to_bugs(bug_list, header, mock_bridge_job, bzcon)
            self.assertEqual(result, None)
            bzcon.update_bugs.assert_not_called()

        # Comment already posted to all bugs so don't return
        # anything (bugs should already be linked).
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = [bz1, bz2]
            mock_comment_check.return_value = True

            bridge_job_attrs = {
                'type': PipelineType.RHEL,
                'ds_pipeline': mock.Mock(web_url='https://yaddayadda'),
                'artifacts_text': 'artfifacts string',
                'checkout_link': 'checkout-link',
            }
            mock_bridge_job = mock.Mock(spec=list(bridge_job_attrs.keys()), **bridge_job_attrs)

            result = buglinker.post_to_bugs(bug_list, header, mock_bridge_job, bzcon)
            self.assertEqual(result, None)
            self.assertIn('This pipeline has already been posted to all relevant bugs.',
                          logs.output[-1])
            self.assertEqual(mock_comment_check.call_count, 2)
            bzcon.update_bugs.assert_not_called()

        # Succesful comment, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.reset_mock(return_value=True)
            mock_comment_check.side_effect = [False, True]

            bridge_job_attrs = {
                'type': PipelineType.RHEL,
                'ds_pipeline': mock.Mock(web_url='https://yaddayadda'),
                'artifacts_text': 'artfifacts string',
                'checkout_link': 'checkout-link',
            }
            mock_bridge_job = mock.Mock(spec=list(bridge_job_attrs.keys()), **bridge_job_attrs)

            buglinker.post_to_bugs(bug_list, header, mock_bridge_job, bzcon)
            self.assertIn('Updating these bugs [11223344] with comment', logs.output[-1])

        # Succesful comment on RT pipeline, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.reset_mock(return_value=True, side_effect=True)
            mock_comment_check.side_effect = [True, True, False, True]
            mock_rt_cve_bugs.return_value = [bz3, bz4]

            bridge_job_attrs = {
                'type': PipelineType.REALTIME,
                'ds_pipeline': mock.Mock(web_url='https://yaddayadda'),
                'artifacts_text': 'artfifacts string',
                'checkout_link': 'checkout-link',
            }
            mock_bridge_job = mock.Mock(spec=list(bridge_job_attrs.keys()), **bridge_job_attrs)

            buglinker.post_to_bugs(bug_list, header, mock_bridge_job, bzcon)
            self.assertIn('Updating these bugs [33445566] with comment', logs.output[-1])

        # Updates all the bugs but only once.
        mock_comment_check.reset_mock(side_effect=True, return_value=True)
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.reset_mock(return_value=True, side_effect=True)
            mock_comment_check.return_value = False
            mock_rt_cve_bugs.return_value = [bz3, bz4]

            bridge_job_attrs = {
                'type': PipelineType.REALTIME,
                'ds_pipeline': mock.Mock(web_url='https://yaddayadda'),
                'artifacts_text': 'artfifacts string',
                'checkout_link': 'checkout-link',
            }
            mock_bridge_job = mock.Mock(spec=list(bridge_job_attrs.keys()), **bridge_job_attrs)

            buglinker.post_to_bugs(bug_list, header, mock_bridge_job, bzcon)
            self.assertIn('Updating these bugs [11223344, 22334455, 33445566, 44556677]',
                          logs.output[-1])

    @mock.patch.dict(os.environ, {'BUGZILLA_EMAIL': 'kwf@example.com'})
    def test_comment_already_posted(self):
        checkout_link = 'checkout-link'
        pipeline_text = f'Downstream Pipeline: {checkout_link}'
        comment1 = {'creator': 'user@redhat.com', 'text': 'cool comment', 'count': 1}
        comment2 = {'creator': os.environ['BUGZILLA_EMAIL'],
                    'text': f'hey there\n{pipeline_text}\nthanks', 'count': 2}
        bug = mock.Mock()
        bug.id = 678910

        # comment2 should return True.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug.getcomments = mock.Mock(return_value=[comment1, comment2])
            self.assertTrue(buglinker.comment_already_posted(bug, checkout_link))
            self.assertIn('Excluding bug 678910 as pipeline was already posted in comment 2.',
                          logs.output[-1])

        # comment1 is False.
        bug.getcomments = mock.Mock(return_value=[comment1])
        self.assertFalse(buglinker.comment_already_posted(bug, checkout_link))

    def test_fetch_bridge_jobs(self):
        """Returns a dict of BridgeJob objects."""
        job1 = mock.Mock(spec=['name'])
        job1.name = 'c9s_merge_request'
        job2 = mock.Mock(spec=['name'])
        job2.name = 'rhel8_realtime_merge_request'
        job3 = mock.Mock(spec=['name'])
        job3.name = 'rhel8_automotive_merge_request'
        job4 = mock.Mock(spec=['name'])
        job4.name = 'ark_merge_request'
        job5 = mock.Mock(spec=['name'])
        job5.name = 'rhel7_automotive_merge_request'

        tests = [
            # ([ProjectPipelineBridge], expected Exception or None)
            ([job1, job2, job3, job4], None),
            ([job1, job2, job3, job4, job5], RuntimeError)
        ]

        for job_list, expected_exception in tests:
            with self.subTest(job_list=job_list, expected_exception=expected_exception):
                if expected_exception:
                    with self.assertRaises(expected_exception):
                        results = buglinker.fetch_bridge_jobs(job_list)
                    continue
                results = buglinker.fetch_bridge_jobs(job_list)
                self.assertEqual(len(results), len(job_list))
                for pipe_type, bridge_job in results.items():
                    self.assertIsInstance(pipe_type, PipelineType)
                    self.assertIsInstance(bridge_job, BridgeJob)

    def test_filter_bridge_jobs(self):
        """Returns a new dict of BridgeJobs with unwanted ones removed."""
        # The BridgeJob attributes the filter function uses.
        bridge_job_spec = ['type', 'builds_valid']
        bjob1 = mock.Mock(spec=bridge_job_spec, type=PipelineType.CENTOS, builds_valid=True)
        bjob2 = mock.Mock(spec=bridge_job_spec, type=PipelineType.REALTIME, builds_valid=True)
        bjob3 = mock.Mock(spec=bridge_job_spec, type=PipelineType.RHEL_COMPAT, builds_valid=True)
        bjob4 = mock.Mock(spec=bridge_job_spec, type=PipelineType.AUTOMOTIVE, builds_valid=False)
        bjob5 = mock.Mock(spec=bridge_job_spec, type=PipelineType.DEBUG, builds_valid=True)
        bjob6 = mock.Mock(spec=bridge_job_spec, type=PipelineType.CENTOS, builds_valid=False)

        tests = [
            # (branch pipeline types, input jobs list, expected_output)
            # Excludes CENTOS as we have RHEL_COMPAT.
            ([PipelineType.CENTOS, PipelineType.REALTIME, PipelineType.RHEL_COMPAT,
              PipelineType.AUTOMOTIVE, PipelineType.DEBUG],
             [bjob1, bjob2, bjob3, bjob4, bjob5], [bjob2, bjob3, bjob5]),
            # Nothing to filter.
            ([PipelineType.CENTOS, PipelineType.REALTIME, PipelineType.DEBUG],
             [bjob1, bjob2, bjob5], [bjob1, bjob2, bjob5]),
            # Filters out unexpected realtime pipeline.
            ([PipelineType.CENTOS, PipelineType.DEBUG],
             [bjob1, bjob2, bjob5], [bjob1, bjob5]),
            # Excludes RHEL_COMPAT as there is no ready CENTOS.
            ([PipelineType.CENTOS, PipelineType.REALTIME, PipelineType.RHEL_COMPAT,
              PipelineType.AUTOMOTIVE, PipelineType.DEBUG],
             [bjob2, bjob3, bjob4, bjob5, bjob6], [bjob2, bjob5]),
        ]

        for branch_pipes, input_job_list, output_job_list in tests:
            branch = mock.Mock(spec=['name', 'pipelines'], pipelines=branch_pipes)
            branch.name = 'my cool branch'
            input_dict = {job.type: job for job in input_job_list}
            expected_dict = {job.type: job for job in output_job_list}
            with self.subTest(branch=branch, input_dict=input_dict, expected_dict=expected_dict):
                result = buglinker.filter_bridge_jobs(input_dict, branch)
                self.assertCountEqual(result, expected_dict)

    @mock.patch('webhook.common.remove_labels_from_merge_request')
    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_set_targeted_testing_label(self, mock_add_label, mock_remove_label):
        """Sets the label when it is missing, removes it when not needed."""
        mock_project = mock.Mock()

        tests = [
            # (needs_label bool, MR labels list, add_label_called, remove_label_called)
            # Needs label and doesn't exist, add_label called.
            (True, [], True, False),
            # Needs label and already exists, neither label func called.
            (True, [defs.TARGETED_TESTING_LABEL], False, False),
            # Doesn't need label and has it, remove_label called.
            (False, [defs.TARGETED_TESTING_LABEL], False, True),
            # Doesn't need label and doesn't have it, meither label func called.
            (False, [], False, False)
        ]

        for needs_tt_label, mr_labels, add_label_called, remove_label_called in tests:
            with self.subTest(needs_tt_label=needs_tt_label, mr_labels=mr_labels,
                              add_label_called=add_label_called,
                              remove_label_called=remove_label_called):
                mock_add_label.reset_mock()
                mock_remove_label.reset_mock()

                mock_mr = mock.Mock(spec=['labels', 'iid'], labels=mr_labels, iid=123)
                buglinker.set_targeted_testing_label(mock_project, mock_mr, needs_tt_label)
                if add_label_called:
                    mock_add_label.assert_called_once()
                else:
                    mock_add_label.assert_not_called()
                if remove_label_called:
                    mock_remove_label.assert_called_once()
                else:
                    mock_remove_label.assert_not_called()


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestEvenHandlers(NoSocketTestCase):
    """ Test event handlers."""

    PAYLOAD_MERGE = {
        'object_kind': 'merge_request',
        'project': {'id': 1,
                    'web_url': 'https://web.url/group/project',
                    'path_with_namespace': 'group/project'
                    },
        'object_attributes': {'target_branch': 'main',
                              'iid': 2,
                              'state': 'opened',
                              'action': 'update',
                              'url': 'https://web.url/group/project/-/merge_requests/2'
                              },
        'labels': [],
        'changes': {}
    }

    PAYLOAD_PIPELINE = {
        'object_kind': 'pipeline',
        'project': {'id': 1,
                    'path_with_namespace': 'group/project'},
        'object_attributes': {'id': 22334455,
                              'status': 'success'},
        'merge_request': {'iid': 2,
                          'url': 'https://web.url/group/project/-/merge_requests/2',
                          'title': 'a test merge request'}
    }

    PAYLOAD_BUILD = {'object_kind': 'build',
                     'build_status': 'success',
                     'build_stage': 'setup',
                     'project_id': 1,
                     'pipeline_id': 22334455}

    @mock.patch('webhook.buglinker.process_mr_pipeline')
    def test_process_job_event(self, mock_process_mr_pipeline):
        """Verify process_job_event calls process_mr_pipeline when it should."""
        headers = {'message-type': 'gitlab'}
        mock_session = SessionRunner('buglinker', 'args', buglinker.HANDLERS)

        # Not a successful job
        payload = deepcopy(self.PAYLOAD_BUILD)
        event = create_event(mock_session, headers, payload)
        payload['build_status'] = 'canceled'
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_not_called()

        # Not a setup job
        payload = deepcopy(self.PAYLOAD_BUILD)
        payload['build_stage'] = 'something'
        event = create_event(mock_session, headers, payload)
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_not_called()

        # Not a job associated with an MR
        payload = deepcopy(self.PAYLOAD_BUILD)
        event = create_event(mock_session, headers, payload)
        event.gl_mr = None
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_not_called()

        # All good
        payload = deepcopy(self.PAYLOAD_BUILD)
        event = create_event(mock_session, headers, payload)
        event.gl_mr = mock.Mock()
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_called()

    @mock.patch('webhook.buglinker.remove_gitlab_link_in_issues')
    @mock.patch('webhook.buglinker.update_bugzilla')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.process_mr_pipeline')
    def test_process_mr_event(self, mock_process_mr_pipeline, mock_trybz, mock_update_bz,
                              mock_jira_unlink):
        headers = {'message-type': 'gitlab'}
        mr_url = defs.GitlabURL(self.PAYLOAD_MERGE['object_attributes']['url'])
        path_with_namespace = self.PAYLOAD_MERGE['project']['path_with_namespace']
        merge_msg = deepcopy(self.PAYLOAD_MERGE)
        merge_msg['object_attributes']['description'] = ''
        merge_request = mock.Mock(description='')
        project = mock.Mock()
        project.mergerequests.get.return_value = merge_request
        gl_instance = mock.Mock()
        gl_instance.projects.get.return_value = project

        mock_session = SessionRunner('buglinker', 'args', buglinker.HANDLERS)
        mock_session.gl_instance = gl_instance
        mock_session.rh_projects = mock.Mock()

        # MR description doesn't list any bugs.
        event = create_event(mock_session, headers, merge_msg)
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.process_mr_event({}, mock_session, event)
            self.assertIn('No description tag changes found', logs.output[-1])
            mock_process_mr_pipeline.assert_not_called()
            mock_update_bz.assert_not_called()

        mr_description = 'Bugzilla: https://bugzilla.redhat.com/12345678'
        merge_msg['object_attributes']['description'] = mr_description
        event = create_event(mock_session, headers, merge_msg)

        # No bugzilla connection.
        mock_trybz.return_value = False

        buglinker.process_mr_event({}, mock_session, event)
        mock_trybz.assert_called_once()
        mock_process_mr_pipeline.assert_not_called()
        mock_update_bz.assert_not_called()

        # One bug to link.
        mock_bz = mock.Mock()
        mock_trybz.return_value = mock_bz
        merge_msg['changes'] = {'description': {'previous': 'Bad description',
                                                'current': mr_description}}
        event = create_event(mock_session, headers, merge_msg)
        bugs = {'link': {12345678},
                'unlink': set(),
                'link_jissue': set(),
                'unlink_jissue': set()
                }

        buglinker.process_mr_event({}, mock_session, event)
        mock_update_bz.assert_called_with(bugs, mr_url, mock_bz)

        # Just removed Draft (send to pipeline processor) and one bug to link.
        merge_msg['changes']['draft'] = {'previous': True, 'current': False}
        merge_msg['object_attributes']['draft'] = False
        event = create_event(mock_session, headers, merge_msg)
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.process_mr_event({}, mock_session, event)
            self.assertIn('Sending MR event to artifact poster.', logs.output[-1])
            mock_update_bz.assert_called_with(bugs, mr_url, mock_bz)

        # MR changed to Draft state, unlink its bug, bugzilla case
        merge_msg['changes']['draft'] = {'previous': False, 'current': True}
        merge_msg['object_attributes']['draft'] = True
        event = create_event(mock_session, headers, merge_msg)
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bugs = {'link': set(),
                    'unlink': {12345678},
                    'link_jissue': set(),
                    'unlink_jissue': set()
                    }

            buglinker.process_mr_event({}, mock_session, event)
            self.assertIn('MR is closing or moved back to draft, unlinking', logs.output[-2])
            mock_update_bz.assert_called_with(bugs, mr_url, mock_bz)

        # MR changed to Draft state, unlink its bug, jira case
        mr_description = 'JIRA: https://issues.redhat.com/browse/RHEL-101'
        merge_msg['object_attributes']['description'] = mr_description
        event = create_event(mock_session, headers, merge_msg)
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bugs = {'link': set(),
                    'unlink': set(),
                    'link_jissue': set(),
                    'unlink_jissue': {'RHEL-101'}
                    }
            buglinker.process_mr_event({}, mock_session, event)
            self.assertIn('MR is closing or moved back to draft, unlinking', logs.output[-2])
            mock_jira_unlink.assert_called_with(2, path_with_namespace, bugs['unlink_jissue'])

    @mock.patch('webhook.buglinker.process_mr_pipeline')
    @mock.patch('webhook.buglinker.check_associated_mr', mock.Mock())
    def test_process_pipeline_event(self, mock_process_mr_pipeline):
        """Parses payload and calls process_mr_pipeline."""
        # Merge Request event.
        headers = {'message-type': 'gitlab'}
        merge_msg = deepcopy(self.PAYLOAD_MERGE)
        project_name = merge_msg['project']['path_with_namespace']
        mr_id = merge_msg['object_attributes']['iid']
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(123, project_name)
        mock_mr = mock_project.add_mr(mr_id)
        mock_mr.project_id = 123
        mock_mr.target_branch = 'main'

        mock_session = SessionRunner('buglinker', 'args', buglinker.HANDLERS)
        mock_session.gl_instance = mock_gl

        event = create_event(mock_session, headers, merge_msg)
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_called_once()

        # Pipeline event, no MR so nothing to do.
        mock_process_mr_pipeline.reset_mock()
        merge_msg = deepcopy(self.PAYLOAD_PIPELINE)
        merge_msg['merge_request'] = None
        event = create_event(mock_session, headers, merge_msg)
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_not_called()

        # Pipeline event with MR, calls _process_pipeline.
        mock_process_mr_pipeline.reset_mock()
        merge_msg = deepcopy(self.PAYLOAD_PIPELINE)
        event = create_event(mock_session, headers, merge_msg)
        buglinker.process_gl_event({}, mock_session, event)
        mock_process_mr_pipeline.assert_called_once()


HEADER_STR = 'a header string\n'
FOOTER_STR = 'a footer string\n'
JOB_ARTIFACT_TEXT = 'this is artifacts text'


def make_bridge_job(attrs: dict) -> mock.Mock:
    """Return a faux BridgeJob with the given properties."""
    bridge_job_spec = {
        'ds_pipeline': mock.Mock(web_url='https://url'),
        'type': None,
        'builds_valid': True,
        'artifact_text': 'boop',
        'all_sources_targeted': True,
        'artifacts_text': ''
    }
    bridge_properties = bridge_job_spec | attrs
    if bridge_properties['builds_valid'] and 'artifacts_text' not in attrs:
        bridge_properties['artifacts_text'] = JOB_ARTIFACT_TEXT
    return mock.Mock(spec=list(bridge_job_spec.keys()), **bridge_properties)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestProcessMrPipeline(NoSocketTestCase):
    """Tests for the process_mr_pipeline function."""

    bjob1 = make_bridge_job({'type': PipelineType.RAWHIDE})
    bjob2 = make_bridge_job({'type': PipelineType.REALTIME})
    bjob3 = make_bridge_job({'type': PipelineType.RHEL_COMPAT})
    bjob4 = make_bridge_job({'type': PipelineType.AUTOMOTIVE, 'builds_valid': False})
    bjob5 = make_bridge_job({'type': PipelineType.DEBUG})
    bjob6 = make_bridge_job({'type': PipelineType.CENTOS, 'builds_valid': False})
    bjob7 = make_bridge_job({'type': PipelineType.RHEL, 'builds_valid': False})
    bjob8 = make_bridge_job({'type': PipelineType.RHEL, 'all_sources_targeted': False})

    @mock.patch('webhook.buglinker.post_mr_pipelines')
    @mock.patch('webhook.buglinker.set_targeted_testing_label')
    @mock.patch('webhook.buglinker.fetch_bridge_jobs')
    def run_process_mr_pipeline_test(self, test, mock_fetch, mock_set_label, mock_post_mr):
        """Finds and processes each downstream pipeline."""
        print(f'Running process_mr_pipeline test: {test}')
        bridge_jobs = {bjob.type: bjob for bjob in test.bridges_list}

        mock_session = SessionRunner('buglinker', 'args', buglinker.HANDLERS)
        mock_session.gl_instance = mock.Mock()
        mock_branch = mock.Mock(spec_set=['name', 'pipelines'], pipelines=test.branch_pipes)
        mock_branch.name = 'my cool branch'
        mock_session.rh_projects.get_target_branch = mock.Mock(return_value=mock_branch)

        mr_desc = 'Bugzilla: https://bugzilla.redhat.com/534645' if test.has_tags else ''

        mr_spec = {
            'description': mr_desc,
            'head_pipeline': mock.Mock(),
            'project_id': 555,
            'target_branch': 'main',
            'title': 'my cool mr',
            'web_url': 'https://yaddayadda',
            'work_in_progress': False,
        }
        mock_mr = mock.Mock(spec_set=list(mr_spec.keys()), **mr_spec)
        mock_fetch.return_value = bridge_jobs

        buglinker.process_mr_pipeline(mock_session, mock_mr)

        if not test.has_tags:
            mock_fetch.assert_not_called()
            return

        # disabled until the targeted testing is switched to subsystems
        # https://gitlab.com/groups/cki-project/-/epics/90
        # mock_set_label.assert_called_once_with(mock.ANY, mock.ANY, test.needs_tt_label)
        mock_set_label.assert_not_called()

        if test.calls_post_mr_pipelines:
            mock_post_mr.assert_called_once()
        else:
            mock_post_mr.assert_not_called()

    def test_process_mr_pipeline(self):
        """Finds and processes each downstream pipeline."""
        class TestTuple(typing.NamedTuple):
            has_tags: bool
            branch_pipes: list[PipelineType]
            bridges_list: list[mock.Mock]
            needs_tt_label: bool
            calls_post_mr_pipelines: bool

        tests = [
            # Nothing in the description, nothing to do.
            TestTuple(False, [], [], True, False),
            # Two successful pipelines to post.
            TestTuple(True, [PipelineType.RAWHIDE, PipelineType.DEBUG],
                      [self.bjob1, self.bjob5], False, True),
            # No valid builds, nothing to post.
            TestTuple(True, [PipelineType.AUTOMOTIVE, PipelineType.RHEL],
                      [self.bjob4, self.bjob7], False, False),
            # A job without all_sources_targeted.
            TestTuple(True, [PipelineType.RHEL], [self.bjob8], True, True),
        ]

        for test in tests:
            self.run_process_mr_pipeline_test(test)

    @mock.patch('webhook.buglinker.find_linked_issues')
    @mock.patch('webhook.buglinker.update_testable_builds')
    @mock.patch('webhook.buglinker.fetch_issues')
    @mock.patch('webhook.buglinker.link_mr_to_bzs')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.post_to_bugs')
    def run_post_mr_pipelines_test(self, test, mock_post_bugs, mock_bz_conn, mock_link_mr,
                                   mock_fetch_issues, mock_update_builds, mock_fli):
        """Calls post_to_bugs & update_testable_builds as needed."""
        bz_ids = {123, 456} if test.has_bzs else set()
        jissues = {'RHEL-123', 'RHEL-456'} if test.has_jiras else set()
        description = mock.Mock(bugzilla=bz_ids, jissue=jissues, cves=set())
        mr_url = 'https://gitlab.com/group/project/-/merge_requests/555'

        if not test.bzcon and test.has_bzs:
            mock_bz_conn.return_value = None
            with self.assertRaises(RuntimeError):
                buglinker.post_mr_pipelines(test.bridges_list, description, mr_url,
                                            HEADER_STR, FOOTER_STR)
                mock_post_bugs.assert_not_called()
                mock_link_mr.assert_not_called()
                mock_update_builds.assert_not_called()
            return

        buglinker.post_mr_pipelines(test.bridges_list, description, mr_url,
                                    HEADER_STR, FOOTER_STR)

        if test.has_bzs:
            self.assertEqual(mock_post_bugs.call_count, len(test.bridges_list))
            if any(bjob.type is PipelineType.REALTIME for bjob in test.bridges_list):
                mock_link_mr.assert_called_once()
        else:
            mock_post_bugs.assert_not_called()
            mock_link_mr.assert_not_called()

        if test.has_jiras:
            mock_update_builds.assert_called_once()
        else:
            mock_update_builds.assert_not_called()

    def test_post_mr_pipelines(self):
        """Calls post_to_bugs & update_testable_builds as needed."""
        class TestTuple(typing.NamedTuple):
            bzcon: bool
            bridges_list: list[mock.Mock]
            has_bzs: bool
            has_jiras: bool

        tests = [
            # Posts to bugs three times and updates testable builds.
            TestTuple(True, [self.bjob1, self.bjob2, self.bjob3], True, True),
            # Posts to bugs twice.
            TestTuple(True, [self.bjob2, self.bjob3], True, False),
            # Updates testable builds.
            TestTuple(True, [self.bjob3], False, True),
            # No bugzilla connection.
            TestTuple(False, [self.bjob1], True, True)
        ]

        for test in tests:
            self.run_post_mr_pipelines_test(test)
