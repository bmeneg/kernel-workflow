"""Tests for webhook/metrics."""
import subprocess
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook import metrics
from webhook.metrics import koji

CHANNEL_DATA1 = b'''Hostname                           Enb Rdy Load/Cap  Arches          Last Update
----------------------------------------------------------------------------------------------------
arm64-025.build.eng.bos.redhat.com Y   Y    0.0/2.0  aarch64          Mon, 26 Sep 2022 14:32:03 CEST
arm64-026.build.eng.bos.redhat.com Y   Y    0.0/2.0  aarch64          Mon, 26 Sep 2022 14:32:16 CEST
ppc-061.build.eng.bos.redhat.com   Y   Y    2.0/12.0 ppc64le          Mon, 26 Sep 2022 14:32:22 CEST
s390-058.build.eng.bos.redhat.com  Y   Y    0.0/1.4  s390x            Mon, 26 Sep 2022 14:32:15 CEST
s390-062.build.eng.bos.redhat.com  Y   Y    0.0/8.0  s390x            Mon, 26 Sep 2022 14:31:59 CEST
x86-vm-07.build.eng.bos.redhat.com Y   Y    0.0/6.0  x86_64,i386      Mon, 26 Sep 2022 14:32:18 CEST
x86-vm-08.build.eng.bos.redhat.com Y   Y    3.0/5.0  x86_64,i386      Mon, 26 Sep 2022 14:32:19 CEST
x86-vm-09.build.eng.bos.redhat.com Y   Y    0.0/4.0  x86_64,i386      Mon, 26 Sep 2022 14:32:10 CEST
'''

CHANNEL_PARSED1 = [
    {'hostname': 'arm64-025.build.eng.bos.redhat.com',
     'enabled': 'Y',
     'ready': 'Y',
     'load': 0.0,
     'cap': 2.0,
     'arch': 'aarch64'},
    {'hostname': 'x86-vm-09.build.eng.bos.redhat.com',
     'enabled': 'Y',
     'ready': 'Y',
     'load': 0.0,
     'cap': 4.0,
     'arch': 'x86_64,i386'},
]


class TestGeneral(NoSocketTestCase):
    """Tests for the metrics module."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def test_all_metrics(self):
        """Test ALL_METRICS has all the expected classes."""
        all_metrics = [
            metrics.KojiMetrics,
        ]
        self.assertEqual(
            len(metrics.ALL_METRICS), len(all_metrics)
        )

        # Check all metrics from all_metrics are in metrics.ALL_METRICS
        for metric in all_metrics:
            self.assertTrue(
                any(m == metric for m in metrics.ALL_METRICS)
            )

    def test_get_enabled_metrics(self):
        """Test get_enabled_metrics detects the enabled metrics correctly."""
        metrics.METRICS_CONFIG = {}
        self.assertEqual([], metrics.get_enabled_metrics())

        metrics.METRICS_CONFIG = {'kojimetrics_enabled': True}
        self.assertEqual(
            set([metrics.KojiMetrics]),
            set(m.__class__ for m in metrics.get_enabled_metrics()),
        )

    def test_get_enabled_metrics_default_enabled(self):
        """Test get_enabled_metrics with default_enabled."""
        metrics.METRICS_CONFIG = {'default_enabled': False}
        self.assertEqual([], metrics.get_enabled_metrics())

        metrics.METRICS_CONFIG = {'default_enabled': True}
        self.assertEqual(
            set(metrics.ALL_METRICS),
            set(m.__class__ for m in metrics.get_enabled_metrics()),
        )

        metrics.METRICS_CONFIG = {
            'default_enabled': True,
            'kojimetrics_enabled': False,
        }
        self.assertEqual(
            set(metrics.ALL_METRICS) - {metrics.KojiMetrics},
            set(m.__class__ for m in metrics.get_enabled_metrics()),
        )


class TestKojiMetrics(NoSocketTestCase):
    """Test BeakerMetrics methods."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []
        self._add_run_result(['koji', '-p', 'brew', 'list-hosts', '--channel', 'rhel8-dupsign'],
                             0, CHANNEL_DATA1, '')
        self._add_run_result(['koji', '-p', 'brew', 'list-tags', '--package=kernel', 'rhel*'],
                             0, b'', b'')
        self._add_run_result(['koji', '-p', 'offline-brew', 'list-tags', '--package=kernel',
                             'rhel*'], 0, b'', b'ServerOffline')

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout, stderr) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if check and returncode:
            raise subprocess.CalledProcessError(returncode, args, output=stdout)
        return subprocess.CompletedProcess(args, returncode, stdout=stdout, stderr=stderr)

    def _add_run_result(self, args, returncode, stdout=None, stderr=None):
        self._mocked_runs.append((args, returncode, stdout, stderr))

    def test_get_channel_data(self):
        """Test channel output is parsed correctly"""

        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            data = koji.get_channel_data('brew', 'rhel8-dupsign')
            self.assertTrue(len(data) == 8)
            for entry in CHANNEL_PARSED1:
                self.assertTrue(entry in data)

    def test_is_brew_reporting_offline(self):
        """Test koji/brew is reporting offline status in stderr"""

        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            is_offline = koji.is_brew_reporting_offline('offline-brew')
            self.assertTrue(is_offline == 1)

    def test_is_channel_healthy(self):
        """Test channel is healthy check"""
        with mock.patch.object(koji, 'EXPECTED_ARCHES', ["aarch64", "x86_64"]):
            CHANNEL_PARSED1[0]['enabled'] = 'N'
            self.assertFalse(koji.is_channel_healthy('rhel8-dupsign', CHANNEL_PARSED1))

            CHANNEL_PARSED1[0]['enabled'] = 'Y'
            self.assertTrue(koji.is_channel_healthy('rhel8-dupsign', CHANNEL_PARSED1))

        # missing expected arch
        self.assertFalse(koji.is_channel_healthy('rhel8-dupsign', CHANNEL_PARSED1))

    @mock.patch.dict(koji.KOJI_CHANNELS, {"brew": ["rhel8-dupsign"]})
    @mock.patch('webhook.metrics.koji.KojiMetrics._update_metric_koji_channel_healthy')
    def test_run(self, update_metric):
        """Test KojiMetrics.run()"""
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            koji_metrics = koji.KojiMetrics()
            koji_metrics.run()
            update_metric.assert_called_once()
            update_metric.assert_called_once_with('brew', 'rhel8-dupsign', mock.ANY)
