"""Tests for check_for_fixes."""
import os
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook.utils import check_for_fixes


class TestFixesChecker(NoSocketTestCase):
    """Tests for the various helper functions."""

    MOCK_MR = {'iid': '66',
               'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/66',
               'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
               'draft': True}

    MOCK_MR2 = {'iid': '67',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/67',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'draft': True}

    MOCK_MR_LIST = {'foo/bar/blah': [MOCK_MR, MOCK_MR2]}

    GQL_MRS = {'project':
               {'id': 'gid://gitlab/Project/1234',
                'mergeRequests':
                {'pageInfo': {'hasNextPage': False, 'endCursor': 'eyJjc'},
                 'nodes': [MOCK_MR, MOCK_MR2]}}}

    def test_get_open_mrs(self):
        mock_gql = mock.Mock()
        mock_gql.check_query_results.return_value = self.GQL_MRS
        namespace = 'foo/bar'
        namespace_type = 'project'
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='DEBUG') as logs:
            check_for_fixes.get_open_mrs(mock_gql, namespace, namespace_type)
            self.assertIn(f"Project MRs: found {self.MOCK_MR_LIST}", logs.output[-1])

    @mock.patch.dict(os.environ, {'GL_PROJECTS': 'cki-project/kernel-ark',
                                  'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    def test_get_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-T"]):
            args = check_for_fixes._get_parser_args()
            self.assertTrue(args.testing)
            self.assertEqual(args.projects, ['cki-project/kernel-ark'])
            self.assertEqual(args.sentry_ca_certs, '/etc/pki/certs/whatever.ca')

    def test_main_no_src(self):
        mock_args = mock.Mock(linux_src=None)
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='DEBUG') as logs:
            check_for_fixes.main(mock.Mock(), mock_args)
            self.assertIn("No valid Linux source git found, aborting!", logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.check_for_fixes.get_open_mrs')
    @mock.patch('webhook.utils.check_for_fixes.GitlabGraph')
    def test_main_no_open_mrs(self, mock_glgraph, mock_mrs):
        mock_args = mock.Mock(linux_src='/src/linux',
                              groups=['redhat/rhel/src/kernel'],
                              projects=['cki-project/kernel-ark'])
        mock_mrs.return_value = {}
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='INFO') as logs:
            check_for_fixes.main(mock.Mock(), mock_args)
            self.assertIn("Fetching latest upstream git commits...", logs.output[-3])
            self.assertIn("Finding open MRs in ['redhat/rhel/src/kernel'] "
                          "['cki-project/kernel-ark']...", logs.output[-2])
            self.assertIn("No open MRs to process.", logs.output[-1])

    @mock.patch('webhook.utils.check_for_fixes.report_results', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.check_for_fixes.FixesMR', mock.Mock())
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.check_for_fixes.get_instance')
    @mock.patch('webhook.utils.check_for_fixes.get_open_mrs')
    @mock.patch('webhook.utils.check_for_fixes.GitlabGraph')
    def test_main_open_mrs(self, mock_glgraph, mock_mrs, mock_get_instance):
        mock_args = mock.Mock(linux_src='/src/linux',
                              groups=[],
                              projects=['foo/bar/blah'])
        mock_mrs.return_value = self.MOCK_MR_LIST
        mock_instance = mock.Mock()
        mock_instance.projects.git.return_value = mock.Mock()
        mock_get_instance.return_value = mock_instance
        with self.assertLogs('cki.webhook.utils.check_for_fixes', level='INFO') as logs:
            check_for_fixes.main(mock.Mock(), mock_args)
            self.assertIn("Fetching latest upstream git commits...", logs.output[-2])
            self.assertIn("Finding open MRs in [] ['foo/bar/blah']...",
                          logs.output[-1])
