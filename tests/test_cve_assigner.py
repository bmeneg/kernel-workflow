"""Tests for zstream_backporter."""
from unittest import mock

from cki_lib import owners
from cki_lib import yaml

from tests.no_socket_test_case import NoSocketTestCase
from webhook.utils import cve_assigner


@mock.patch.dict('os.environ', {'PUBLIC_INBOX': '/src/linux-cve-announce',
                                'OWNERS_YAML': '/path/to/owners.yaml',
                                'REQUESTS_CA_BUNDLE': '/path/to/certs'
                                })
class TestCVEAssigner(NoSocketTestCase):
    """Tests for the various cve_assigner functions."""

    def test_get_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-c", "CVE-1984-10001",
                                     "-p", "/src/linux-cve-announce2"]):
            args = cve_assigner._get_parser_args()
        self.assertEqual(args.public_inbox, '/src/linux-cve-announce2')
        self.assertEqual(args.cve, 'CVE-1984-10001')
        self.assertEqual(args.owners_yaml, '/path/to/owners.yaml')
        self.assertEqual(args.sentry_ca_certs, '/path/to/certs')
        with mock.patch("sys.argv", ["_get_parser_args", "-c", "CVE-1984-10002",
                                     "-o", "/a/b/c/owners.yaml"]):
            args = cve_assigner._get_parser_args()
        self.assertEqual(args.public_inbox, '/src/linux-cve-announce')
        self.assertEqual(args.cve, 'CVE-1984-10002')
        self.assertEqual(args.owners_yaml, '/a/b/c/owners.yaml')
        self.assertEqual(args.sentry_ca_certs, '/path/to/certs')

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_get_patch_data(self):
        mock_args = mock.Mock(spec_set=['cve', 'public_inbox'])
        mock_args.cve = 'CVE-1984-1234'
        mock_args.public_inbox = '/src/linux-cve-announce'
        mock_commit = mock.Mock(spec_set=['hexsha'])
        mock_commit.hexsha = 'abcdef0123456789'
        mbox_data = ("Issue introduced in 4.8 with commit abcd1234 and fixed in 6.8 with "
                     "commit 13241324abcd\n"
                     "will be updated if fixes are backported, please check that for the most\n"
                     "up to date information about this issue.\n"
                     "\n"
                     "Affected files\n"
                     "==============\n"
                     "The file(s) affected by this issue are:\n"
                     "	drivers/misc/foobar.c\n"
                     "\n"
                     "Mitigation\n"
                     "==========\n")
        with mock.patch('builtins.open', mock.mock_open(read_data=mbox_data)):
            files, sha = cve_assigner.get_patch_data(mock_args, mock_commit)
            self.assertEqual(files, ['drivers/misc/foobar.c'])
            self.assertEqual(sha, '13241324abcd')

    @mock.patch('git.Repo')
    def test_search_for_cve_info(self, mock_repo):
        mock_repo.git.log.return_value = ('abc1234 CVE-1984-1999: foo: fix the flaw\n'
                                          'def5678 REJECTED: CVE-1984-1999: foo: fix the flaw\n')
        mock_commit = mock.Mock()
        mock_repo.commit.return_value = mock_commit
        commit, rej = cve_assigner.search_for_cve_info(mock_repo, 'CVE-1984-1999')
        self.assertEqual(commit, mock_commit)
        self.assertTrue(rej)

    @mock.patch('webhook.utils.cve_assigner.fetch_issues')
    def test_parse_jira_data(self, mock_fi):
        mock_args = mock.Mock(spec_set=['cve'])
        mock_args.cve = 'CVE-1984-1999'
        mock_sst = mock.Mock(spec_set=['jira_component'])
        mock_sst.jira_component = 'kernel / foo / bar'
        mock_issue = mock.Mock(spec_set=['key', 'fields'])
        mock_issue.key = 'RHEL-101'
        mock_fields = mock.Mock(spec_set=['components'])
        mock_component = mock.Mock(spec_set=['name'])
        mock_component.name = 'kernel-rt'
        mock_fields.components = [mock_component]
        mock_issue.fields = mock_fields
        mock_fi.return_value = []
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='INFO') as logs:
            ret = cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("No jira issues found for CVE-1984-1999", logs.output[-1])
            self.assertEqual(ret, [])
        mock_fi.return_value = [mock_issue]
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            ret = cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("Ignoring CVE-1984-1999 kernel-rt clone RHEL-101", logs.output[-2])
            self.assertIn("Issues to update: []", logs.output[-1])
            self.assertEqual(ret, [])
        mock_component.name = 'kernel'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='INFO') as logs:
            ret = cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("Issue RHEL-101 component mismatch", logs.output[-2])
            self.assertIn(f"Issues to update: [{mock_issue}]", logs.output[-1])
            self.assertEqual(ret, [mock_issue])
        mock_component.name = 'kernel / foo / bar'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            ret = cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("Issue RHEL-101 components match", logs.output[-2])
            self.assertIn("Issues to update: []", logs.output[-1])
            self.assertEqual(ret, [])

    @mock.patch('cki_lib.misc.is_production_or_staging', mock.Mock(return_value=True))
    def test_update_jira_data(self):
        mock_issue = mock.Mock(spec_set=['key', 'update'])
        mock_issue.key = 'RHEL-101'
        mock_subsystem = mock.Mock(spec_set=['jira_component'])
        mock_subsystem.jira_component = 'kernel / foo / bar'
        issues = [mock_issue]
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='INFO') as logs:
            cve_assigner.update_jira_data(issues, mock_subsystem, 'abcd1234abcd')
            self.assertIn("RHEL-101 component to kernel / foo / bar, commit hashes to abcd1234abcd",
                          logs.output[-1])
            mock_issue.update.assert_called_once()

    @mock.patch('webhook.utils.cve_assigner.update_jira_data', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.cve_assigner.parse_jira_data', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.cve_assigner.get_patch_data')
    @mock.patch('webhook.utils.cve_assigner.search_for_cve_info')
    @mock.patch('webhook.utils.cve_assigner.get_owners_parser')
    @mock.patch('git.Repo', mock.Mock(return_value=True))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_main(self, mock_gop, mock_sfci, mock_gpd):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       gluser: user4\n"
                       "       restricted: false\n"
                       "   jiraComponent: kernel / foobar\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        mock_args = mock.Mock(spec_set=['cve', 'public_inbox', 'owners_yaml'])
        mock_args.cve = ''
        mock_args.public_inbox = ''
        mock_commit = mock.Mock(spec_set=['hexsha', 'summary'])
        mock_commit.hexsha = 'abcdef012345'
        mock_commit.summary = 'CVE-1984-10001: fix the exploitable flaw'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='WARNING') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("No valid public inbox git found, aborting!", logs.output[-1])

        mock_args.public_inbox = '/src/linux-cve-announce'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='WARNING') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("No valid CVE provided, aborting!", logs.output[-1])

        mock_args.cve = 'CVE-1984-10001'
        mock_gop.return_value = owners.Parser(yaml.load(contents=owners_yaml))
        mock_sfci.return_value = None, True
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("Fetching latest upstream mbox data...", logs.output[-3])
            self.assertIn("CVE-1984-10001 was REJECTED upstream", logs.output[-2])
            self.assertIn("No commit found, exiting", logs.output[-1])

        mock_sfci.return_value = mock_commit, False
        mock_gpd.return_value = ['redhat/file.c'], 'abcd1234'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("Got subsystem of Some Subsystem for path(s) ['redhat/file.c']",
                          logs.output[-3])
            self.assertIn("Got UCID of abcd1234", logs.output[-2])
            self.assertIn("Jira Component mapping: 'kernel / foobar'", logs.output[-1])

        mock_gpd.return_value = ['unknown/file.c'], 'abcd1234'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='WARNING') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("Unable to find a matching subsystem for paths ['unknown/file.c']",
                          logs.output[-1])
