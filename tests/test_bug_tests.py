"""Tests for the bug_tests module."""
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook import bug_tests
from webhook.rh_metadata import Projects


class DepOnlyTests(NoSocketTestCase):
    """Validate the tests that are specific to dependency Bugs."""

    @staticmethod
    def _fake_bug(input_dict):
        """Return a mock object with spec set."""
        attr_dict = input_dict.copy()
        if 'alias' not in attr_dict:
            attr_dict['alias'] = 'FakeBug'
        if 'failed_tests' not in attr_dict:
            attr_dict['failed_tests'] = []
        if 'test_failed' not in attr_dict:
            attr_dict['test_failed'] = mock.Mock(return_value=False, spec=[])
        return mock.Mock(spec=list(attr_dict.keys()), **attr_dict)

    def _run_test(self, bug, test, expected_result=True, expected_scope=None,
                  expected_keep_going=True):
        """Run the given test on the given Bug."""
        if expected_scope is None:
            expected_scope = bug_tests.MrScope.READY_FOR_MERGE if expected_result else \
                bug_tests.MrScope.NEEDS_REVIEW
        actual_result, actual_scope, actual_keep_going = test(bug)
        self.assertIs(actual_result, expected_result)
        self.assertIs(actual_scope, expected_scope)
        self.assertIs(actual_keep_going, expected_keep_going)
        if expected_result is True:
            self.assertTrue(test.__name__ not in bug.failed_tests)
        else:
            self.assertTrue(test.__name__ in bug.failed_tests)

    def test_ParentCommitsMatch(self):
        """The Dependency MR commits should match the Dependant (parent) MR commits."""
        this_test = bug_tests.ParentCommitsMatch
        # The MR is not merged and commits == parent_mr_commits
        bug_values = {'commits': [1, 2, 3],
                      'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.OPENED),
                      'parent_mr_commits': [1, 2, 3]}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # The MR is not merged and commits != parent_mr_commits
        bug_values = {'commits': [1, 2, 3],
                      'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.OPENED),
                      'parent_mr_commits': [1, 2, 3, 4]}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # The MR is merged, but there are still visible commits on the parent that reference it.
        bug_values = {'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.MERGED),
                      'parent_mr_commits': [1, 2, 3]}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # The MR is merged and the parent MR does not have any commits that reference it 👍.
        bug_values = {'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.MERGED),
                      'parent_mr_commits': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

    def test_MRIsNotMerged(self):
        """Passes if the MR is not Merged, otherwise 'fails'."""
        this_test = bug_tests.MRIsNotMerged
        # It is not merged, passes.
        bug_values = {'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.OPENED)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # It IS merged, fails.
        bug_values = {'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.MERGED)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.READY_FOR_MERGE,
                       expected_keep_going=False)

        # The test is skipped (passes) if the ParentCommitsMatch test failed.
        bug_values = {'mr': mock.Mock(spec=['state'], state=bug_tests.MrState.MERGED),
                      'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)
        bug_values['test_failed'].assert_called_once_with('ParentCommitsMatch')

    def test_InMrDescription(self):
        """Passes if the Bug is listed in the MR description."""
        this_test = bug_tests.InMrDescription
        # A Bug without commits skips ("passes") the test.
        bug_values = {'commits': [],
                      'in_mr_description': False}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with commits which is in the MR description passes the test.
        bug_values = {'commits': [1, 2, 3],
                      'in_mr_description': True}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with commits which is NOT in the MR description fails the test.
        bug_values = {'commits': [1, 2, 3],
                      'in_mr_description': False}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.MISSING)

    def test_HasCommits(self):
        """Passes if the Bug has commits."""
        this_test = bug_tests.HasCommits
        # A Bug with commits passes the test.
        bug_values = {'commits': [1]}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug without commits fails the test.
        bug_values = {'commits': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.MISSING)

    def test_BZisNotUnknown(self):
        """Passes if the BZStatus is not UNKNOWN."""
        this_test = bug_tests.BZisNotUnknown
        # A Bug with a not UNKNOWN status passes the test.
        bug_values = {'bz_status': bug_tests.BZStatus.ASSIGNED}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with an UNKNOWN status fails the test.
        bug_values = {'bz_status': bug_tests.BZStatus.UNKNOWN}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.INVALID, expected_keep_going=False)

    def test_BZisNotClosed(self):
        """Passes if the BZStatus is not CLOSED."""
        this_test = bug_tests.BZisNotClosed
        # A Bug with a not Closed status passes the test.
        bug_values = {'bz_status': bug_tests.BZStatus.MODIFIED}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with a CLOSED status fails the test.
        bug_values = {'bz_status': bug_tests.BZStatus.CLOSED}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.CLOSED, expected_keep_going=False)

    def test_NotUntagged(self):
        """Passes if the Bug is not the special UNTAGGED variant."""
        this_test = bug_tests.NotUntagged
        # A "normal" Bug passes to the test.
        bug_values = {'untagged': False}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # The UNTAGGED Bug fails the test.
        bug_values = {'untagged': True}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_keep_going=False)

    def test_CveInMrDescription(self):
        """Passes if the CVE IDs in the BZ summary are in the MR description."""
        this_test = bug_tests.CveInMrDescription

        # No bz_cves, test passes.
        fake_bug = self._fake_bug({'bz_cves': []})
        self._run_test(bug=fake_bug, test=this_test)

        mr_cves_spec = ['cve_ids', 'in_mr_description']
        mr_cves = [mock.Mock(spec=mr_cves_spec, cve_ids=['CVE-5621-16611'], in_mr_description=True),
                   mock.Mock(spec=mr_cves_spec, cve_ids=['CVE-1234-26727'], in_mr_description=True)]

        # All the IDs in the summary are in the MR cves list, test passes.
        bug_values = {'bz_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                      'mr': mock.Mock(spec='cves', cves=mr_cves)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # Not all the IDs in the summary are in the MR cves list, test fails.
        bug_values = {'bz_cves': ['CVE-1234-26727', 'CVE-2367-8538', 'CVE-5621-16611'],
                      'mr': mock.Mock(spec='cves', cves=mr_cves)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # All the IDs in the summary are in the MR cves list, but CVEs are not in the description,
        # test fails.
        mr_cves[1].in_mr_description = False
        bug_values = {'bz_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                      'mr': mock.Mock(spec='cves', cves=mr_cves)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

    def test_IsValidInternal(self):
        """Passes if the Bug and MR are properly marked internal."""
        this_test = bug_tests.IsValidInternal
        # A "normal" Bug passes to the test.
        bug_values = {'internal': False,
                      'mr': mock.Mock(spec=['only_internal_files'], only_internal_files=False)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # An INTERNAL Bug passes the test when the MR has only_internal_files.
        bug_values = {'internal': True,
                      'mr': mock.Mock(spec=['only_internal_files'], only_internal_files=True)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # An INTERNAL Bug fails the test when the MR does NOT have only_internal_files.
        bug_values = {'internal': True,
                      'mr': mock.Mock(spec=['only_internal_files'], only_internal_files=False)}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

    def test_IsApproved(self):
        """Passes if the policy check passed."""
        this_test = bug_tests.IsApproved
        # An Approved Bug passes the test.
        bug_values = {'bz_policy_check_ok': (True, '')}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A not-approved Bug fails the test.
        bug_values = {'bz_policy_check_ok': (False, 'No BZ')}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

    def test_IsVerified(self):
        """Passes if the BZ is Verified:Tested."""
        this_test = bug_tests.IsVerified
        # A Bug with Verified:Tested passes the test.
        bug_values = {'bz_is_verified': True}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug without Verified:Tested fails the test.
        bug_values = {'bz_is_verified': False}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False,
                       expected_scope=bug_tests.MrScope.READY_FOR_QA)

        # The test is skipped (passes) if the IsApproved test failed.
        bug_values = {'bz_is_verified': False,
                      'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)
        bug_values['test_failed'].assert_called_once_with('IsApproved')

    def test_TargetReleaseSet(self):
        """Passes if either ITR or ZTR are set."""
        this_test = bug_tests.TargetReleaseSet
        # A Bug with no BZ skips (passes) the test.
        bug_values = {'bz': None}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with a rhel-6 BZ skips (passes) the test.
        bug_values = {'bz': mock.Mock(spec=['product'], product='Red Hat Enterprise Linux 6')}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with ZTR passes the test.
        bug_values = {'bz': mock.Mock(spec=['product'], product='Red Hat Enterprise Linux 9'),
                      'bz_itr': '',
                      'bz_ztr': '9.0.0'}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug without ITR or ZTR fails the test.
        bug_values = {'bz': mock.Mock(spec=['product'], product='Red Hat Enterprise Linux 9'),
                      'bz_itr': '',
                      'bz_ztr': ''}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

    def test_CentOSZStream(self):
        """Passes if a c9s Bug does not have zstream_target_release set."""
        this_test = bug_tests.CentOSZStream

        mock_project = mock.Mock(spec=['name'])
        mock_project.name = 'centos-stream-9'
        # A Bug that is a dependency skips (passes) the test.
        bug_values = {'is_dependency': True,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': mock.Mock(spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug in a non-c9s MR (passes) the test.
        mock_project.name = 'rhel-9'
        bug_values = {'is_dependency': False,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': mock.Mock(spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug without a bz (passes) the test.
        mock_project.name = 'centos-stream-9'
        bug_values = {'is_dependency': False,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': None}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug without a bz_branch fails the test.
        mock_bz = mock.Mock(spec=[])
        bug_values = {'is_dependency': False,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': mock_bz,
                      'bz_branch': None}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # A Bug with a zstream bz_branch fails the test.
        mock_branch = mock.Mock(spec=['zstream_target_release'], zstream_target_release='9.0')
        bug_values = {'is_dependency': False,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': mock_bz,
                      'bz_branch': mock_branch}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # A Bug with a ystream bz_branch passes the test.
        mock_branch = mock.Mock(spec=['internal_target_release', 'zstream_target_release'],
                                internal_target_release='9.1', zstream_target_release='')
        bug_values = {'is_dependency': False,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': mock_bz,
                      'bz_branch': mock_branch}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug that failed 'TargetReleaseSet' skips (passes) the test.
        mock_branch = mock.Mock(spec=['internal_target_release', 'zstream_target_release'],
                                internal_target_release='9.1', zstream_target_release='')
        bug_values = {'is_dependency': False,
                      'mr': mock.Mock(spec=['project'], project=mock_project),
                      'bz': mock_bz,
                      'bz_branch': mock_branch,
                      'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)
        bug_values['test_failed'].assert_called_once_with('TargetReleaseSet')

    def test_ComponentMatches(self):
        """Passes if the MR target branch component matches the BZ's component."""
        this_test = bug_tests.ComponentMatches
        mock_bz = mock.Mock(spec=['component'], component='kernel')
        mock_branch1 = mock.Mock(spec=['components', 'version'],
                                 components=['kernel'], version='9.0')
        mock_mr = mock.Mock(spec=['branch'])

        # A Bug with an MR branch version >= 9.3 must have a component of either 'kernel' or
        # 'kernel-rt'.
        mock_branch2 = mock.Mock(spec=['components', 'version'],
                                 components=['kernel'], version='9.4')
        mock_mr.branch = mock_branch2
        bug_values = {'bz': mock_bz, 'mr': mock_mr, 'bz_cves': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with an MR branch version < 9.3 can have a component of kernel, rt, or auto.
        mock_mr.branch = mock.Mock(spec=['components', 'version'],
                                   components=['kernel'], version='9.0')
        mock_bz2 = mock.Mock(spec=['component'], component='kernel-rt')
        bug_values = {'bz': mock_bz2, 'mr': mock_mr, 'bz_cves': ['CVE-2020-1234']}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with a component matching the MR branch component passes the test.
        mock_mr.branch = mock_branch1
        bug_values = {'bz': mock_bz, 'mr': mock_mr, 'bz_cves': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with a component not matching the MR branch component fails the test.
        mock_bz.component = 'kernel-rt'
        bug_values = {'bz': mock_bz, 'mr': mock_mr, 'bz_cves': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # A Bug without a bz skips (passes) the test.
        bug_values = {'bz': None, 'mr': mock_mr, 'bz_cves': []}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug that failed the CentOSZStream test skips (passes) the test.
        bug_values = {'bz': None, 'mr': mock_mr, 'bz_cves': [],
                      'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)
        bug_values['test_failed'].assert_called_once_with('CentOSZStream')

    def test_BranchMatches(self):
        """Passes if the MR target Branch matches the BZ's Branch."""
        this_test = bug_tests.BranchMatches
        mock_branch1 = mock.Mock(spec=['version'], version='9.0')
        mock_branch2 = mock.Mock(spec=['version'], version='9.0')
        mock_branch3 = mock.Mock(spec=['version'], version='9.4')

        # A Bug with a bz_branch.version that matches the MR branch version passes the test.
        mock_mr = mock.Mock(spec=['branch'], branch=mock_branch1)
        bug_values = {'bz_branch': mock_branch2, 'mr': mock_mr}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)

        # A Bug with a bz_branch.version that does not match the MR branch version fails the test.
        bug_values = {'bz_branch': mock_branch3, 'mr': mock_mr}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test, expected_result=False)

        # A Bug that failed CentOSZStream or ComponentMatches skips (passes) the test.
        bug_values = {'bz_branch': mock_branch2, 'mr': mock_mr,
                      'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_bug = self._fake_bug(bug_values)
        self._run_test(bug=fake_bug, test=this_test)
        bug_values['test_failed'].assert_called_once_with('TargetReleaseSet')

    def test_CvePriority(self):
        """Passes if the CVE's lead clone is on errata."""
        this_test = bug_tests.CvePriority

        # Skips (passes) the test if the CVE Priority is less than HIGH.
        cve_values = {'alias': 'FakeCve', 'bz_priority': bug_tests.BZPriority.MEDIUM}
        fake_cve = self._fake_bug(cve_values)
        self._run_test(bug=fake_cve, test=this_test)

        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        project1 = projects.projects[12345]
        branch1 = project1.branches[4]   # '8.5', ZTR 8.5.0
        branch2 = project1.branches[2]   # '8.6', ZTR 8.6.0
        branch3 = project1.branches[0]   # 'main', ITR 8.7.0

        bz1 = mock.Mock(spec=['component'], component='kernel')
        bug1 = self._fake_bug({'bz': bz1, 'bz_branch': branch1, 'bz_project': project1,
                               'bz_resolution': None})
        bz2 = mock.Mock(spec=['component'], component='kernel')
        bug2 = self._fake_bug({'bz': bz2, 'bz_branch': branch2, 'bz_project': project1,
                               'bz_resolution': None})
        bz3 = mock.Mock(spec=['component'], component='kernel')
        bug3 = self._fake_bug({'bz': bz3, 'bz_branch': branch3, 'bz_project': project1,
                               'bz_resolution': None})

        # Not rhel6 or 7 and the lead_clone Branch is not higher than the parent_clone, passes.
        cve_values = {'alias': 'FakeCve', 'bz_priority': bug_tests.BZPriority.HIGH,
                      'bz_depends_on': [bug1, bug2, bug3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch2)}
        fake_cve = self._fake_bug(cve_values)
        self._run_test(bug=fake_cve, test=this_test)

        # Not rhel6 or 7 and the lead_clone is higher than the parent and not on ERRATA, fails.
        cve_values = {'alias': 'FakeCve', 'bz_priority': bug_tests.BZPriority.HIGH,
                      'bz_depends_on': [bug1, bug2, bug3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_bug(cve_values)
        self._run_test(bug=fake_cve, test=this_test, expected_result=False)

        # Not rhel6 or 7 and the lead_clone is higher than the parent is on ERRATA, passes.
        bug2 = self._fake_bug({'bz': bz2, 'bz_branch': branch2, 'bz_project': project1,
                               'bz_resolution': bug_tests.BZResolution.ERRATA})
        cve_values = {'alias': 'FakeCve', 'bz_priority': bug_tests.BZPriority.HIGH,
                      'bz_depends_on': [bug1, bug2, bug3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_bug(cve_values)
        self._run_test(bug=fake_cve, test=this_test)

        # rhel7 so the lead_clone branch targets 'main'.
        project1.__dict__['name'] = 'rhel-7'
        bug2 = self._fake_bug({'bz': bz2, 'bz_branch': branch2, 'bz_project': project1,
                               'bz_resolution': None})
        bug3 = self._fake_bug({'bz': bz3, 'bz_branch': branch3, 'bz_project': project1,
                               'bz_resolution': bug_tests.BZResolution.ERRATA})
        cve_values = {'alias': 'FakeCve', 'bz_priority': bug_tests.BZPriority.HIGH,
                      'bz_depends_on': [bug1, bug2, bug3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_bug(cve_values)
        self._run_test(bug=fake_cve, test=this_test)
        project1.__dict__['name'] = 'rhel-8'
