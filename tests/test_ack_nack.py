"""Webhook interaction tests."""
from unittest import mock

from cki_lib import owners
from cki_lib import yaml
from gitlab.exceptions import GitlabGetError

from tests import fakes
from tests.no_socket_test_case import NoSocketTestCase
from webhook import ack_nack
from webhook import common
from webhook import defs
from webhook.session import SessionRunner


def ack_session_runner(
    gl_instance=None,
    graphql=None,
    args_str='--owners-yaml owners.yaml --rhkernel-src /src'
) -> SessionRunner:
    """Return a SessionRunner with some fake bits."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args_str.split())
    session = SessionRunner('ack_nack', args, ack_nack.HANDLERS)
    session.gl_instance = gl_instance or mock.Mock()
    session.graphql = graphql or mock.Mock()
    return session


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestAckNack(NoSocketTestCase):
    NATTRS = {'body': 'This should be unique per webhook\n\nEverything is fine, I swear...',
              'author': {'username': 'shadowman'},
              'id': '1234'}

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'user': {'id': 1, 'username': 'user1'},
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p',
                                'path_with_namespace': 'redhat/rhel/src/kernel/rhel-8'
                                },
                    'object_attributes': {'iid': 1,
                                          'noteable_type': 'MergeRequest',
                                          'note': 'comment',
                                          'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_comment_save(self, add_label, add_comment):
        mock_session = SessionRunner('ack_nack', [], ack_nack.HANDLERS)
        inst = mock.Mock()
        inst.user.username = "shadowman"
        proj = mock.Mock()
        proj.manager.gitlab = inst
        mreq = mock.MagicMock()
        mreq.state = 'open'
        status = "OK"
        labels = ["Acks::net::OK"]
        message = "Everything is fine, I swear..."
        ack_nack._save(mock_session, proj, mreq, status, labels, message)
        add_comment.assert_called_once()
        note = f'**ACK/NACK Summary:** ~"Acks::OK"\n\n{message}'
        add_comment.assert_called_with(mreq, note,
                                       bot_name="shadowman",
                                       identifier="**ACK/NACK Summary:")

    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    @mock.patch('webhook.ack_nack._edit_approval_rule')
    def test_get_reviewers(self, ear, mock_emails_to_usernames):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "     - name: User 3\n"
                       "       email: user3@redhat.com\n"
                       "       gluser: user3\n"
                       "       restricted: true\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       gluser: user4\n"
                       "       restricted: false\n"
                       "     - name: User 5\n"
                       "       email: user5@redhat.com\n"
                       "       gluser: user5\n"
                       "     - name: User 6\n"
                       "       email: user6@redhat.com\n"
                       "       gluser: user6\n"
                       "       restricted: true\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n"
                       " - subsystem: Another Subsystem\n"
                       "   labels:\n"
                       "     name: AnotherSubsystem\n"
                       "   requiredApproval: false\n"
                       "   maintainers:\n"
                       "     - name: User 7\n"
                       "       gluser: user7\n"
                       "       email: user7@redhat.com\n"
                       "   reviewers:\n"
                       "     - name: User 8\n"
                       "       gluser: user8\n"
                       "       email: user8@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - Documentation/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        mock_emails_to_usernames.return_value = [set(), []]
        gl_instance = mock.Mock()
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        gl_project = mock.Mock()
        ainfo = mock.Mock()
        ainfo.access_level = 30
        gl_project.members_all.get.return_value = ainfo
        gl_mergerequest = mock.Mock()
        gl_mergerequest.description = 'Cc: <ignored@example.com>'
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 2
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        gl_mergerequest.target_branch = "main"
        gl_mergerequest.author = {'id': 1, 'username': 'user1'}
        required, _ = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                              ['redhat/Makefile'], owners_parser)
        # user1@redhat.com is the merge request submitter and shouldn't be in the reviewer list.
        self.assertEqual(required, [(frozenset(['user2', 'user4', 'user5']), 'SomeSubsystem')])

        required, _ = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                              [], owners_parser)
        self.assertEqual(required, [])

        required, _ = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                              ['Documentation/foo'], owners_parser)
        self.assertEqual(required, [])

        # Returns a notifications list with Cc tagged users and missing.
        gl_mergerequest.description = 'Cc: <developer@redhat.com>\nCc: <unknown@redhat.com>'
        mock_emails_to_usernames.return_value = ({'developer'}, ['unknown@redhat.com'])
        required, notifications = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                                          ['Documentation/foo'], owners_parser)
        self.assertEqual(required, [])
        self.assertCountEqual(notifications, [('AnotherSubsystem', 0, {'user8', 'user7'}),
                                              ('Cc tagged users', 0, {'developer'}),
                                              ('MISSING', None, ['unknown@redhat.com'])])

        # simulate the case where we can't find the author details
        exceptions = [[GitlabGetError('404 Not Found', 404), True],
                      [GitlabGetError('random error', 0), False]]
        for e in exceptions:
            gl_project.members_all.get.side_effect = e[0]
            exception = False
            gitlab_exception = False
            try:
                required, _ = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser)
            except GitlabGetError:
                gitlab_exception = True
            except Exception:
                exception = True
            if e[1]:
                self.assertFalse(gitlab_exception)
            else:
                self.assertFalse(exception)

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_get_reviewers_no_self_review(self, to_user):
        # The person that opened the merge request is the only person listed as a subsystem
        # maintainer. That person should not be listed as a required reviewer.
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'username': 'user1'}
        gl_mergerequest.description = ''
        ainfo = mock.Mock()
        ainfo.access_level = 30
        gl_project.members_all.get.return_value = ainfo
        to_user.return_value = ['user1']
        reviewers, notify = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                                    ['redhat/Makefile'], owners_parser)
        self.assertEqual(reviewers, [])
        self.assertEqual(notify, [])

    @mock.patch('webhook.ack_nack._edit_approval_rule')
    def test_get_reviewers_zstream_maintainer_mr(self, ear):
        # This is a z-stream maintainer MR w/base approvals set to 0, in which case, we should
        # not set any required approvals for subsystems, just mark them optional
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "     - name: User 3\n"
                       "       email: user3@redhat.com\n"
                       "       gluser: user3\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        gl_instance = mock.Mock()
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        gl_project = mock.Mock()
        ainfo = mock.Mock()
        ainfo.access_level = 40
        gl_project.members_all.get.return_value = ainfo
        gl_mergerequest = mock.Mock()
        gl_mergerequest.description = ''
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        gl_mergerequest.author = {'id': 1, 'username': 'user1'}
        required, notify = ack_nack._get_reviewers(gl_project, gl_mergerequest,
                                                   ['redhat/README'], owners_parser)
        # user1@redhat.com is the merge request submitter and shouldn't be in the reviewer list.
        self.assertEqual(required, [(frozenset(['user2', 'user3']), 'SomeSubsystem')])
        ear.assert_called_with(gl_project, gl_mergerequest, 'SomeSubsystem', {'user2', 'user3'}, 0)

    def test_ensure_base_approval_rule(self):
        gl_project = mock.Mock()
        gl_project.name = "Testing"
        gl_mergerequest = mock.Mock()
        gl_mergerequest.state = "opened"
        gl_mergerequest.draft = False
        gl_mergerequest.iid = 666
        all_members_rule = mock.Mock()
        all_members_rule.approvals_required = 2
        all_members_rule.id = 1234
        gl_mergerequest.author = mock.MagicMock(id=3579)
        author_info = mock.Mock()
        author_info.access_level = 30
        gl_mergerequest.target_branch = 'main'
        gl_project.members_all.get.return_value = author_info
        gl_project.approvalrules.list.return_value = [all_members_rule]
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 2
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        # Everything is fine
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('DEBUG:cki.webhook.ack_nack:Testing MR 666 has '
                              '\'All Members\' rule set appropriately'))
        # Project has no All Members rule
        gl_project.approvalrules.list.return_value = []
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Project Testing has no '
                              '\'All Members\' approval rule'))
        # Project has required approvals set to 0
        all_members_rule.approvals_required = 0
        gl_project.approvalrules.list.return_value = [all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Project Testing isn\'t '
                              'requiring any reviewers for MRs'))
        # MR had approvals required set to 0 in main
        all_members_rule.approvals_required = 2
        gl_project.approvalrules.list.return_value = [all_members_rule]
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 (branch: main) had '
                              'approvals required set to 0'))
        # MR had approvals required set to 0 by non-maintainer
        gl_mergerequest.target_branch = 'z-stream'
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 (branch: z-stream) had '
                              'approvals required set to 0 by non-maintainer'))
        author_info.access_level = 40
        gl_project.members_all.get.return_value = author_info
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('DEBUG:cki.webhook.ack_nack:Testing MR 666 (branch: z-stream) has '
                              '\'All Members\' rule adjusted by maintainer'))
        # MR had no base approvals rule present
        gl_mergerequest.approval_rules.list.return_value = []
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 had '
                              'no base approvals required'))

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_recheck_base_approval_rule(self, is_prod):
        gl_mergerequest = mock.Mock()
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        gl_mergerequest.target_branch = "main"
        gl_mergerequest.draft = False

        # All Members has 0 required approvals
        error = {'body': '*ERROR*: MR has 0 required approvals'}
        ack_nack._recheck_base_approval_rule(gl_mergerequest)
        gl_mergerequest.discussions.create.assert_called_with(error)

        # All Members is missing
        mr_all_members_rule.name = "foo"
        error = {'body': '*ERROR*: MR has no "All Members" approval rule'}
        ack_nack._recheck_base_approval_rule(gl_mergerequest)
        gl_mergerequest.discussions.create.assert_called_with(error)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.process_config_items', mock.Mock(return_value=([], '')))
    def test_purge_stale_approval_rules(self):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "       restricted: false\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n"
                       " - subsystem: Another Subsystem\n"
                       "   labels:\n"
                       "     name: AnotherSubsystem\n"
                       "   requiredApproval: false\n"
                       "   maintainers:\n"
                       "     - name: User 3\n"
                       "       gluser: user3\n"
                       "       email: user3@redhat.com\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       gluser: user4\n"
                       "       email: user4@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - Documentation/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        path_list = ['Documentation/PLEASE_README.txt']
        stale_subsystem = "SomeSubsystem"
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 0
        mr_all_members_rule.id = 1
        gl_mergerequest = mock.Mock()
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        ack_nack.purge_stale_approval_rules(gl_mergerequest, owners_parser, path_list)
        gl_mergerequest.approval_rules.delete.assert_not_called()

        stale_rule = mock.Mock()
        stale_rule.name = stale_subsystem
        stale_rule.approvals_required = 1
        stale_rule.id = 1000
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule, stale_rule]
        ack_nack.purge_stale_approval_rules(gl_mergerequest, owners_parser, path_list)
        gl_mergerequest.approval_rules.delete.assert_called_with(stale_rule.id)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    @mock.patch('webhook.ack_nack._reviewers_to_gl_user_ids')
    def test_edit_approval_rule(self, rtgluid, is_prod):
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        rtgluid.return_value = [105, 106]
        rule1 = mock.Mock()
        rule1.name = "yeehaw"
        gl_mergerequest.approval_rules.list.return_value = [rule1]

        subsystem = "george"
        reviewers = ["user1", "user2"]
        num_req = 0
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            status = ack_nack._edit_approval_rule(gl_project, gl_mergerequest,
                                                  subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Create rule for ss george, '
                              'with 0 required approval(s) from user(s) '
                              '[\'user1\', \'user2\']'))
            self.assertTrue(status)

        subsystem = "bob"
        reviewers = ["user1", "user2"]
        num_req = 2
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            status = ack_nack._edit_approval_rule(gl_project, gl_mergerequest,
                                                  subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Create rule for ss bob, '
                              'with 2 required approval(s) from user(s) '
                              '[\'user1\', \'user2\']'))
            self.assertTrue(status)

        subsystem = "yeehaw"
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            status = ack_nack._edit_approval_rule(gl_project, gl_mergerequest,
                                                  subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ("INFO:cki.webhook.ack_nack:Approval rule for subsystem yeehaw "
                              "already exists"))
            self.assertFalse(status)

    def test_has_unsatisfied_blocking_approval_rule(self):
        gl_mergerequest = mock.Mock()
        user_id = 1234
        username = "shadowman"

        eligible_approver = [{'id': user_id, 'name': 'Shadow Man', 'username': 'shadowman'}]
        rule = mock.Mock()
        rule.id = 8675309
        rule.name = f'{defs.BLOCKED_BY_PREFIX} {username}'
        rule.eligible_approvers = eligible_approver
        rule.approvals_required = 1
        gl_mergerequest.approval_rules.list.return_value = [rule]

        # MR has been approved by the blocking user
        approved_by = [{'user': {'id': 1234, 'name': 'Shadow Man', 'username': 'shadowman'}}]
        approvals = mock.Mock()
        approvals.approvals_required = 1
        approvals.approved_by = approved_by
        gl_mergerequest.approvals.get.return_value = approvals
        result = ack_nack._has_unsatisfied_blocking_approval_rule(gl_mergerequest)
        self.assertFalse(result)

        # No approval provided
        approved_by = []
        approvals.approved_by = approved_by
        gl_mergerequest.approvals.get.return_value = approvals
        result = ack_nack._has_unsatisfied_blocking_approval_rule(gl_mergerequest)
        self.assertTrue(result)

    @mock.patch('webhook.common.get_pog_member')
    def test_reviewers_to_gl_user_ids(self, mock_get_pog_member):
        """Returns a list of user ids derived from the input reviewers list."""
        mock_project = mock.Mock()
        mock_project.name = 'cool_project'
        mock_user1 = mock.Mock()
        mock_user1.id = 1
        mock_user2 = mock.Mock()
        mock_user2.id = 2
        mock_user3 = mock.Mock()
        mock_user3.id = 3
        reviewers = ['user1', 'user2', 'user3', 'user4']
        mock_get_pog_member.side_effect = [mock_user1, mock_user2, mock_user3, None]
        with self.assertLogs('cki.webhook.ack_nack', level='WARNING') as logs:
            result = ack_nack._reviewers_to_gl_user_ids(mock_project, reviewers)
            self.assertEqual(result, [1, 2, 3])
            self.assertIn('user4 was not found', logs.output[-1])

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    @mock.patch('webhook.ack_nack._reviewers_to_gl_user_ids')
    def test_handle_block_action(self, rtgluid, is_prod):
        mock_session = SessionRunner('ack_nack', [], ack_nack.HANDLERS)
        namespace = 'group/project'
        mr_id = 123
        user_id = 1234
        username = 'shadowman'
        discussion = mock.Mock(attributes={'notes': [self.NATTRS]})

        gl_instance = fakes.FakeGitLab()
        gl_instance.add_project(456, namespace)
        gl_instance.user = mock.Mock(username=username)
        gl_mergerequest = fakes.FakeMergeRequest(mr_id, {})
        gl_mergerequest.discussions.list.return_value = [discussion]
        gl_mergerequest.notes.create = mock.Mock()
        gl_mergerequest.approval_rules.list.return_value = []
        gl_mergerequest.author = {'id': 4, 'username': 'reviewer'}
        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        mock_session.gl_instance = gl_instance

        action = "block"
        new_rule = f'{defs.BLOCKED_BY_PREFIX} {username}'

        # blocking rule needs to be created for user
        rule1 = mock.Mock()
        rule1.id = 8675309
        rule1.name = "yeehaw"
        gl_mergerequest.approval_rules.list.return_value = [rule1]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs, \
                self.assertLogs('cki.webhook.session', level='DEBUG') as logs_session:

            ack_nack.handle_block_action(mock_session, gl_mergerequest,
                                         action, username, user_id)
            self.assertEqual(logs.output[-2],
                             ('INFO:cki.webhook.ack_nack:Create blocking approval for '
                              'user shadowman'))
            gl_mergerequest.approvals.set_approvers.assert_called_with(approvals_required=1,
                                                                       approver_ids=[1234],
                                                                       approver_group_ids=[],
                                                                       approval_rule_name=new_rule)
            self.assertIn("Assigning reviewers ['@shadowman'] to MR", logs.output[-1])

            self.assertIn("Creating new webhook comment", ' '.join(logs_session.output))
            gl_mergerequest.notes.create.assert_called()

        # blocking rule already exists for user
        gl_mergerequest.approvals.set_approvers.call_count = 0
        rule1.name = new_rule
        gl_mergerequest.approval_rules.list.return_value = [rule1]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.handle_block_action(mock_session, gl_mergerequest,
                                         action, username, user_id)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Blocking approval for user shadowman '
                              'already exists'))
            gl_mergerequest.approvals.set_approvers.assert_not_called()

        # remove existing blocking rule
        action = "unblock"
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.handle_block_action(mock_session, gl_mergerequest,
                                         action, username, user_id)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Deleting blocking approval rule '
                              'for user shadowman'))
            gl_mergerequest.approval_rules.delete.assert_called_with(8675309)

        # unblock called, but rule already gone
        rule1.name = "yeehaw"
        gl_mergerequest.approval_rules.list.return_value = [rule1]
        gl_mergerequest.approval_rules.delete.call_count = 0
        ack_nack.handle_block_action(mock_session, gl_mergerequest,
                                     action, username, user_id)
        gl_mergerequest.approval_rules.delete.assert_not_called()

        # attempt to block your own MR
        action = "block"
        gl_mergerequest.author = {'id': user_id, 'username': username}
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.handle_block_action(mock_session, gl_mergerequest,
                                         action, username, user_id)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Attempt by shadowman to use a block '
                              'action on their own MR'))
            gl_mergerequest.approval_rules.delete.assert_called_with(8675309)

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_get_cc_reviewers(self, mock_emails_to_gl_user_names):
        """Returns the set of usernames derived from any CC tags in the mr description."""
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(defs.ARK_PROJECT_ID, 'cki-project/kernel-ark')
        mock_mr = mock_project.add_mr(123)
        mock_mr.manager = mock.Mock(gitlab=mock_instance)

        # File list is not exclusive to redhat/configs/ dir so don't return anything.
        mock_mr.description = 'Hey there'
        result = ack_nack.get_cc_reviewers(mock_mr)
        self.assertEqual(result, (set(), []))
        mock_emails_to_gl_user_names.assert_not_called()

        # Good file list, Cc tags, resolves emails to usernames
        mock_mr.description = ('Cc: Ark User1 <user1@redhat.com>\n'
                               'Cc: Ark User2 <user2@redhat.com>')
        mock_emails_to_gl_user_names.return_value = ({'user1', 'user2'}, [])
        result = ack_nack.get_cc_reviewers(mock_mr)
        self.assertEqual(result, mock_emails_to_gl_user_names.return_value)

        # Good file list, Cc tags, but emails didn't resolve.
        mock_emails_to_gl_user_names.return_value = (set(),
                                                     ['user1@redhat.com', 'user2@redhat.com'])
        result = ack_nack.get_cc_reviewers(mock_mr)
        self.assertEqual(result, mock_emails_to_gl_user_names.return_value)

    def test_get_reviewers_from_approval_rules(self):
        R1EA = [{'id': 12345, 'name': 'Ram Eater', 'username': 'ram'},
                {'id': 56789, 'name': 'Mike Memory', 'username': 'mmem'},
                {'id': 8675309, 'name': 'Jenny Cache', 'username': 'jcache'}]
        R2EA = [{'id': 1234, 'name': 'Joe Developer', 'username': 'jdev'},
                {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}]
        APPROVED_BY = [{'user': {'id': 12345, 'name': 'Ram Eater', 'username': 'ram'}},
                       {'user': {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}}]
        MM_RESULT = ['mm', 1, 1, ['ram', 'mmem', 'jcache'], [12345, 56789, 8675309]]
        X86_RESULT = ['x86', 0, 1, ['jdev', 'bob'], [1234, 5678]]

        rule1 = mock.Mock()
        rule1.name = 'mm'
        rule1.eligible_approvers = R1EA
        rule1.approvals_required = 1

        rule2 = mock.Mock()
        rule2.name = 'x86'
        rule2.eligible_approvers = R2EA
        rule2.approvals_required = 0

        approvals = mock.Mock()
        approvals.approved = False
        approvals.approvals_required = 3
        approvals.approvals_left = 1
        approvals.approved_by = APPROVED_BY

        gl_mergerequest = mock.Mock()
        gl_mergerequest.approvals.get.return_value = approvals
        gl_mergerequest.approval_rules.list.return_value = [rule1, rule2]

        result = ack_nack.get_reviewers_from_approval_rules(gl_mergerequest)
        self.assertEqual(result, [MM_RESULT, X86_RESULT])

    @mock.patch('webhook.ack_nack._get_old_subsystems_from_labels')
    def test_get_approval_summary(self, old_subsystems):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.manager.gitlab = gl_instance

        user1 = {'id': 1, 'name': 'Joe Developer', 'username': 'joedev'}
        user2 = {'id': 2, 'name': 'Bob Reviewer', 'username': 'bobrev'}
        user3 = {'id': 3, 'name': 'Ram Eater', 'username': 'rameat'}
        user4 = {'id': 4, 'name': 'Mike Michaelson', 'username': 'mikesq'}
        user5 = {'id': 5, 'name': 'User 5', 'username': 'user5'}
        user6 = {'id': 6, 'name': 'User 6', 'username': 'user6'}
        user7 = {'id': 7, 'name': 'User 7', 'username': 'user7'}
        user8 = {'id': 8, 'name': 'User 8', 'username': 'user8'}
        approvers = [{'user': user1}, {'user': user2}]
        gl_instance.users.get = \
            lambda x: mock.Mock(username=f'user{x}')
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        gl_project.only_allow_merge_if_all_discussions_are_resolved = True
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': True}
        gl_mergerequest.resourcelabelevents.list.return_value = []
        subsys1_rule = mock.Mock()
        subsys1_rule.name = 'subsys1'
        subsys1_rule.eligible_approvers = [user1, user2]
        subsys1_rule.approvals_required = 1
        subsys2_rule = mock.Mock()
        subsys2_rule.name = 'subsys2'
        subsys2_rule.eligible_approvers = [user4, user5]
        subsys2_rule.approvals_required = 1
        subsys3_rule = mock.Mock()
        subsys3_rule.name = 'subsys3'
        subsys3_rule.eligible_approvers = [user6, user7]
        subsys3_rule.approvals_required = 1
        subsys4_rule = mock.Mock()
        subsys4_rule.name = 'subsys4'
        subsys4_rule.eligible_approvers = [user1, user8]
        subsys4_rule.approvals_required = 1
        faked_reset = False
        gl_mergerequest.approval_rules.list.return_value = [subsys1_rule]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=2,
                                                               approved_by=[])
        old_subsystems.return_value = ([], [])
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [], faked_reset),
                         ('NeedsReview', [], '\n\nRequires 2 more Approval(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=[{'user': user1}])

        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [], faked_reset),
                         ('NeedsReview', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          '\nRequires 1 more Approval(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [], faked_reset),
                         ('OK', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nMerge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': False}
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [], faked_reset),
                         ('Blocked', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nAll discussions must be resolved.'))
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': True}

        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com',
                                                           'user2@redhat.com'], 'subsys1')], [],
                                                        faked_reset),
                         ('OK', ['Acks::subsys1::OK'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nMerge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com', 'user3@redhat.com'],
                                                          'subsys1')], [], faked_reset),
                         ('OK', ['Acks::subsys1::OK'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nMerge Request has all necessary Approvals.'))

        gl_mergerequest.approval_rules.list.return_value = [subsys1_rule, subsys2_rule]
        approvers = [{'user': user1}, {'user': user2}, {'user': user3}]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2')], [], faked_reset),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          ' - Ram Eater (rameat)\n'
                          '\nRequires 1 more Approval(s).'))

        gl_mergerequest.approval_rules.list.return_value = [subsys1_rule, subsys2_rule,
                                                            subsys3_rule, subsys4_rule]
        approvers = [{'user': user1}, {'user': user2}, {'user': user3}]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2'),
                                                         (['user6@redhat.com', 'user7@redhat.com'],
                                                          'subsys3'),
                                                         (['user1@redhat.com', 'user8@redhat.com'],
                                                          'subsys4')], [], faked_reset),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview',
                                          'Acks::subsys3::NeedsReview', 'Acks::subsys4::OK'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          ' - Ram Eater (rameat)\n'
                          '\nRequires 1 more Approval(s).'))

        # Approval Rules Reviewers based Required Approvals
        arr1 = ['RuleX', 1, 0, ['user1'], [1]]
        arr2 = ['RuleY', 0, 0, ['user2'], [2]]
        arr3 = ['RuleZ', 1, 1, ['user3'], [3]]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=[{'user': user2}])
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [arr1], faked_reset),
                         ('NeedsReview', [],
                          'Approved by:\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nRequired Approvals:  \n'
                          ' - Approval Rule "RuleX" requires at least 1 ACK(s) '
                          '(0 given) from set (user1).  \n'
                          '\nRequires 1 more Approval Rule Approval(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=[{'user': user1}])
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [arr2], faked_reset),
                         ('NeedsReview', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          '\nOptional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) from set (user2).  \n'
                          '\nRequires 1 more Approval(s).'))

        approvers = [{'user': user1}, {'user': user3}]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [arr2, arr3], faked_reset),
                         ('OK', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Ram Eater (rameat)\n'
                          '\nOptional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) from set (user2).  \n'
                          '\nSatisfied Approvals:  \n'
                          ' - Approval Rule "RuleZ" already has 1 ACK(s) (1 required).  \n'
                          '\nMerge Request has all necessary Approvals.'))

        # If the faked_reset flag is set then it should appear there are no approvals.
        faked_reset = True
        self.assertEqual(ack_nack._get_approval_summary(gl_project, gl_mergerequest,
                                                        [], [arr2, arr3], faked_reset),
                         ('NeedsReview', [],
                          '\nRequired Approvals:  \n'
                          ' - Approval Rule "RuleZ" requires at least 1 ACK(s) (0 given)'
                          ' from set (user3).  \n'
                          '\nOptional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) from set (user2).  \n'
                          '\nRequires 1 more Approval Rule Approval(s).'))

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.ack_nack.get_instance')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    def test_reset_merge_request_approvals(self, mock_update, mock_get_inst):
        mock_session = SessionRunner('ack_nack', 'args', ack_nack.HANDLERS)
        proj_id = "5000"
        mr_id = "66"
        mock_mr = mock.Mock(iid=mr_id)
        approver = {'id': 1234, 'name': 'Shadow Man', 'username': 'shadowman'}
        fake_approvers = [{'user': approver}]
        mock_mr.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                       approvals_left=0,
                                                       approved_by=fake_approvers)
        mock_proj = mock.Mock(id=proj_id)
        mock_proj.mergerequests.get.return_value = mock_mr
        mock_gl = mock.Mock()
        mock_gl.private_token = None
        mock_gl.auth.return_value = True
        mock_gl.projects.get.return_value = mock_proj
        mock_get_inst.return_value = mock_gl
        mock_mr.diffs.list.return_value = ['1', '2', '3']
        notification = (f"Approval(s) from @{approver['username']} removed due to code changes, "
                        "fresh approvals required on v3.\n\n")
        interdiff = "Code changes in revision v3 of this MR:  \ndiffstat"

        # The project is configured to reset approvals on push so we don't do it.
        mock_approvals = mock.Mock(reset_approvals_on_push=True)
        mock_proj.approvals.get.return_value = mock_approvals
        namespace = "somewhere/over/the/rainbow"
        mock_proj.path_with_namespace = namespace
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.reset_merge_request_approvals(mock_session, mock_proj, mr_id, "")
            mock_get_inst.assert_not_called()
            self.assertIn("reset_approvals_on_push: True", logs.output[0])
            self.assertIn("Expecting GL to reset approval rules", logs.output[-1])
            mock_mr.reset_approvals.assert_not_called()

        # The project does not reset approvals so we do it via API.
        mock_mr.reset_approvals.reset_mock()
        mock_approvals.reset_approvals_on_push = False
        namespace = f'{defs.RHEL_KERNEL_NAMESPACE}/kernel-test'
        mock_proj.path_with_namespace = namespace
        mock_gl.private_token = 'fake_private_token'
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.reset_merge_request_approvals(mock_session, mock_proj, mr_id, "diffstat")
            print('\n'.join(logs.output))
            mock_get_inst.assert_called_with(url=f'{defs.GITFORGE}/{namespace}',
                                             env_name='GITLAB_KWF_BOT_API_TOKENS')
            self.assertIn("reset_approvals_on_push: False", logs.output[0])
            self.assertIn(f"Clearing Approvals on {namespace} MR {mr_id} (Rev v3)",
                          logs.output[1])
            self.assertIn(f"Assigning reviewers ['@{approver['username']}'] to MR {mr_id}",
                          logs.output[2])
            self.assertIn("Resetting approval rules via API.", logs.output[-1])
            mock_update.assert_called_with(mock.ANY, notification + interdiff)
            mock_mr.reset_approvals.assert_called_once()

        # The project does not reset approvals so we do it via API.
        mock_mr.reset_approvals.reset_mock()
        namespace = f'{defs.CENTOS_STREAM_KERNEL_NAMESPACE}/i-know-kung-fu'
        mock_proj.path_with_namespace = namespace
        mock_gl.private_token = 'fake_private_token'
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.reset_merge_request_approvals(mock_session, mock_proj, mr_id, "")
            mock_get_inst.assert_called_with(url=f'{defs.GITFORGE}/{namespace}',
                                             env_name='GITLAB_KWF_BOT_API_TOKENS')
            self.assertIn("reset_approvals_on_push: False", logs.output[0])
            self.assertIn(f"Clearing Approvals on {namespace} MR {mr_id} (Rev v3)",
                          logs.output[1])
            self.assertIn(f"Assigning reviewers ['@{approver['username']}'] to MR {mr_id}",
                          logs.output[2])
            mock_update.assert_called_with(mock.ANY, notification)
            mock_mr.reset_approvals.assert_called_once()

        mock_gl.private_token = None
        exception_hit = False
        try:
            ack_nack.reset_merge_request_approvals(mock_session, mock_proj, mr_id, "")
        except ValueError:
            exception_hit = True
        mock_get_inst.assert_called_with(url=f'{defs.GITFORGE}/{namespace}',
                                         env_name='GITLAB_KWF_BOT_API_TOKENS')
        self.assertTrue(exception_hit)

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.common.get_owners_parser')
    @mock.patch('webhook.common.get_pog_member')
    @mock.patch('webhook.ack_nack.reset_merge_request_approvals')
    @mock.patch('webhook.ack_nack.get_reviewers_from_approval_rules')
    @mock.patch('webhook.cdlib.set_dependencies_label')
    @mock.patch('webhook.cdlib.get_filtered_changed_files')
    @mock.patch('webhook.ack_nack.purge_stale_approval_rules', mock.Mock(return_value=True))
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    @mock.patch('webhook.common.add_label_to_merge_request', mock.Mock(return_value=None))
    def test_process_merge_request1(self, get_files, set_label, rfar, reset, pog_member,
                                    get_owners_parser, gccount):
        get_files.return_value = ['include/linux/netdevice.h']
        set_label.return_value = "Dependencies::OK"
        rfar.return_value = []
        pog_member.return_value = mock.Mock(id=1234)
        gccount.return_value = (10, 30)

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        get_owners_parser.return_value = owners_parser

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: Impersonated User <notuser3@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 3))

        ainfo = mock.Mock()
        ainfo.access_level = 30
        APPROVED_BY = [{'user': {'id': 1234, 'name': 'Joe Developer', 'username': 'joedev'}},
                       {'user': {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bobrev'}}]
        mock_amr = mock.Mock()
        mock_amr.gl_mr = mock.Mock()
        mock_amr.gl_mr.author = {'id': 4, 'username': 'someuser'}
        mock_amr.gl_mr.description = ''
        mock_amr.gl_mr.notes.list.return_value = msgs
        mock_amr.gl_mr.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}],
                                               'blocking_discussions_resolved': True}
        mock_amr.gl_mr.labels = ['Dependencies::OK']
        mock_amr.gl_mr.commits.return_value = \
            [mock.Mock(committed_date='2021-01-08T20:00:00.000Z')]
        mock_amr.gl_mr.reviewers = []
        mock_amr.gl_mr.resourcelabelevents.list.return_value = []
        mock_amr.gl_mr.diffs.list.return_value = []
        mock_amr.gl_mr.approval_rules.list.return_value = []
        mock_amr.gl_mr.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                              approvals_left=0,
                                                              approved_by=APPROVED_BY)
        mock_amr.gl_mr.approvals_required = 2
        mock_amr.history = ['one']
        mock_amr.code_changes.return_value = []

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = mock_amr.gl_mr
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path=defs.RHEL_KERNEL_NAMESPACE)
        gl_project.commits.get.return_value = mock.Mock(parent_ids=["1234"])
        gl_project.members_all.get.return_value = ainfo
        gl_project.archived = False

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(id=author_id)
        gl_instance.users.list = \
            lambda search, iterator: [mock.Mock(username=search.split('@')[0])]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        gl_project.manager.gitlab = gl_instance

        mock_session = ack_session_runner(gl_instance=gl_instance)
        mock_session.get_gl_project = mock.Mock(return_value=gl_project)

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(mock_session, mock_amr)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary:** ~"Acks::OK"\n'
                              '\nApproved by:\n'
                              ' - Joe Developer (joedev)\n'
                              ' - Bob Reviewer (bobrev)\n'
                              '\nMerge Request has all necessary Approvals.'))

        set_label.return_value = 'Dependencies::deadbeefabcd'
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(mock_session, mock_amr)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary:** ~"Acks::OK"\n'
                              '\nApproved by:\n'
                              ' - Joe Developer (joedev)\n'
                              ' - Bob Reviewer (bobrev)\n'
                              '\nMerge Request has all necessary Approvals.'))
            get_files.assert_called_once()

        mock_amr.history = ['one', 'two']
        set_label.return_value = 'Dependencies::deadbeefabcd'
        gl_project.id = defs.ARK_PROJECT_ID
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(mock_session, mock_amr)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary:** ~"Acks::OK"\n'
                              '\nApproved by:\n'
                              ' - Joe Developer (joedev)\n'
                              ' - Bob Reviewer (bobrev)\n'
                              '\nMerge Request has all necessary Approvals.'))

    @mock.patch('webhook.common.get_owners_parser')
    @mock.patch('webhook.common.get_pog_member')
    @mock.patch('webhook.ack_nack.reset_merge_request_approvals')
    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.common.extract_all_from_message')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    @mock.patch('webhook.common.add_label_to_merge_request', mock.Mock(return_value=None))
    def test_process_merge_request2(self, ext_deps, ext_bzs, gccount, reset, pog_member,
                                    get_owners_parser):
        ext_deps.return_value = []
        ext_bzs.return_value = ([], [], [])
        gccount.return_value = 20, 30
        ainfo = mock.Mock()
        ainfo.access_level = 30
        pog_member.return_value = mock.Mock(id=1234)

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = owners.Parser(yaml.load(contents=owners_yaml))
        get_owners_parser.return_value = owners_parser

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        # Code is pushed up based on committed_date below; previous ack is no longer valid
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-10T07:00:00.000Z', 2))

        APPROVED_BY = [{'user': {'id': 1234, 'name': 'User 1', 'username': 'user1'}}]
        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.description = ''
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}],
                                                'blocking_discussions_resolved': True}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []
        diff = mock.MagicMock(id=1234, created_at='2021-01-10T03:00:00.000Z')
        gl_mergerequest.diffs.list.return_value = [diff]
        gl_mergerequest.approval_rules.list.return_value = []
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=APPROVED_BY)
        gl_mergerequest.approvals_required = 2
        mock_amr = mock.Mock()
        mock_amr.gl_mr = gl_mergerequest
        mock_amr.history = ['one']
        mock_amr.code_changes.return_value = [diff]

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path=defs.RHEL_KERNEL_NAMESPACE)
        gl_project.members_all.get.return_value = ainfo
        gl_project.archived = False

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(id=author_id)
        gl_instance.users.list = \
            lambda search, iterator: [mock.Mock(username=search.split('@')[0])]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        mock_session = ack_session_runner(gl_instance=gl_instance)
        mock_session.get_gl_project = mock.Mock(return_value=gl_project)

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(mock_session, mock_amr)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary:**'
                              ' ~"Acks::NeedsReview"\n'
                              '\nApproved by:\n'
                              ' - User 1 (user1)\n'
                              '\nRequires 1 more Approval(s).'))

        gccount.return_value = 2001, 30
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(mock_session, mock_amr)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary: *ERROR* **\n'
                              '\nThis Merge Request has too many commits for the approval webhook '
                              'to process (2001) -- please make sure you have targeted the correct '
                              'branch if you want reviewers automatically assigned.'))

    def __create_note_body(self, body, timestamp, author_id):
        ret = mock.Mock()
        ret.body = body
        ret.updated_at = timestamp
        ret.author = {'id': author_id, 'username': f'user{author_id}'}
        return ret

    def _create_merge_payload(self, action):
        return {'object_kind': 'merge_request',
                'user': {'id': 1, 'username': 'u1', 'name': 'User 1', 'email': 'user1@redhat.com'},
                'project': {'id': 1,
                            'web_url': 'https://web.url/g/p',
                            'path_with_namespace': 'g/p'
                            },
                'object_attributes': {'iid': 2, 'action': action,
                                      'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                'changes': {'labels': {'previous': [], 'current': []}}}

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_assign_reviewers(self):
        mock_mr = mock.MagicMock()
        reviewer = 'nobody'
        assign = {'body': f'/assign_reviewer @{reviewer}'}
        ack_nack._assign_reviewers(mock_mr, [f'@{reviewer}'])
        mock_mr.notes.create.assert_called_with(assign)

    def test_emails_to_gl_user_names(self):
        mock_instance = mock.Mock()
        user1 = mock.Mock(id="1", name="Some One", username="someone")
        user3 = mock.Mock(id="3", name="Red Hatter", username="redhatter")
        reviewers = ['someone@redhat.com', 'nobody@redhat.com', 'redhatter@redhat.com']
        mock_instance.users.list.side_effect = [[user1], [], [user3]]
        output = ack_nack._emails_to_gl_user_names(mock_instance, reviewers)
        self.assertEqual(output, ({'someone', 'redhatter'}, ['nobody@redhat.com']))

    def test_get_old_subsystems_from_labels(self):
        mock_mr = mock.Mock()
        mock_mr.labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Something::whatever']
        (subsys, labels) = ack_nack._get_old_subsystems_from_labels(mock_mr)
        self.assertEqual(subsys, ['net', 'mm'])
        self.assertEqual(labels, ['Acks::net::NeedsReview', 'Acks::mm::OK'])

    def test_get_stale_labels(self):
        old_subsystems = ['net', 'mm', 'foobar']
        old_labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Acks::foobar::NACKed']
        subsys_scoped_labels = {'net': 'NeedsReview', 'mm': 'OK'}
        output = ack_nack._get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
        self.assertEqual(output, ['Acks::foobar::NACKed'])
