"""Tests for the description library."""
import typing
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook import description


class TestMRDescription(NoSocketTestCase):
    """Tests for the MRDescription dataclass."""

    text = ('Hello\n'
            'THINGS THAT SHOULD MATCH:\n'
            'Bugzilla: https://bugzilla.redhat.com/123456  \n'                         # BZ 123456
            'Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=637382\n'            # BZ 637382
            'CVE: CVE-2021-61677\n'                                               # CVE-2021-61677
            'CVE: CVE-1992-15616\n'                                               # CVE-1992-15616
            'Depends: https://bugzilla.redhat.com/262727  \n'                         # Dep 262727
            'Signed-off-by: User Name <user@example.com> \n'  # DCO 'User Name', 'user@example.com'
            'Depends: !737\n'                                                          # Dep !737
            'Cc: <noname@example.com>   \n'
            f'Depends: {defs.GITFORGE}/group/project/-/merge_requests/267\n'           # Dep !267
            'Signed-off-by: Artist <artist@example.com>\n'    # DCO 'Artist', 'artist@example.com'
            'hey\nDepends: http://gitlab.com/group1/project/-/merge_requests/321  \n'  # Dep !321
            'THINGS THAT SHOULD NOT MATCH:\n'
            'Cc: Example User <example_user@redhat.com>  \n'
            'Bugzilla: 34567\nBugzilla: BZ-456789\n'
            'Cc: Example <example@fedoraproject.org>\n'
            'Cc: Someone Else <someone_else@example.com>\n'
            'Signed-off-by: example <example@example.com> 😎\n'
            '    Bugzilla: https://bugzilla.redhat.com/23456\n'
            '    Signed-off-by: Person <person@example.com>\n'
            )

    def test_MRDescription_empty(self):
        """Returns as False"""
        test_desc = description.MRDescription(text='')
        self.assertIs(bool(test_desc), False)
        self.assertEqual(test_desc.bugzilla, set())
        self.assertEqual(test_desc.cc, set())
        self.assertEqual(test_desc.cve, set())
        self.assertEqual(test_desc.depends, set())
        self.assertEqual(test_desc.depends_mrs, set())
        self.assertIs(test_desc.marked_internal, False)

    def test_MRDescription_equal(self):
        """Evaluates as equal when the text, namespace, and _depends match."""
        namespace = 'space/project'
        desc1 = description.MRDescription(text=self.text, namespace=namespace)
        desc1.depends_bzs.update([101, 202])
        desc2 = description.MRDescription(text=self.text, namespace=namespace)
        self.assertNotEqual(desc1, desc2)
        desc2.depends_bzs.update([101, 202])
        self.assertEqual(desc1, desc2)

    def test_MRDescription_bugzilla(self):
        """Parses the Bugzilla: tags."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.bugzilla, {123456, 637382})

    def test_MRDescription_cc(self):
        """Parses the Cc: tags."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.cc, {('Example', 'example@fedoraproject.org'),
                                        ('Someone Else', 'someone_else@example.com'),
                                        ('Example User', 'example_user@redhat.com'),
                                        ('', 'noname@example.com')})

    def test_MRDescription_cve(self):
        """Parses the CVE: tags."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.cve, {'CVE-2021-61677', 'CVE-1992-15616'})

    def test_MRDescription_depends_mrs_no_namespace(self):
        """Parses the Depends: tags for MR IDs."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.depends_mrs, {737})

    def test_MRDescription_depends_mrs_with_namespace_no_fetch_bugs(self):
        """Parses the Depends: tags for MR IDs."""
        namespace = 'group1/project'
        test_desc = description.MRDescription(text=self.text, namespace=namespace)
        self.assertEqual(test_desc.depends_mrs, {321, 737})

    def test_MRDescription_marked_internal(self):
        """Returns True if Bugzilla: INTERNAL is found, otherwise False."""
        test_desc = description.MRDescription(text=self.text)
        self.assertIs(test_desc.marked_internal, False)

        text = self.text + '\nBugzilla: INTERNAL  \n'
        test_desc = description.MRDescription(text=text)
        self.assertIs(test_desc.marked_internal, True)

    def test_MRDescription_signoff(self):
        """Parses the Signed-off-by: tags for name/email tuples."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.signoff, {('User Name', 'user@example.com'),
                                             ('Artist', 'artist@example.com')})

    def test_MRDescription_depends_mrs_with_graphql(self):
        """Parses the Depends tag for MR IDs and gets BZ IDs from dependant MRs."""
        mock_graph = mock.Mock()

        mr267_text = ('Bugzilla: https://bugzilla.redhat.com/5432543\n'
                      'Depends: https://bugzilla.redhat.com/2675423')
        mr737_text = ('Bugzilla: https://bugzilla.redhat.com/3254325\n'
                      'Depends: https://bugzilla.redhat.com/2675423')
        mock_graph.get_mr_descriptions.return_value = {267: mr267_text, 737: mr737_text}
        namespace = 'group/project'
        test_desc = description.MRDescription(self.text, namespace, mock_graph)
        self.assertEqual(test_desc.depends_mrs, {267, 737})


class TestCommit(NoSocketTestCase):
    """Tests for the Commit dataclass."""

    def test_commit_empty(self):
        """Returns an empty Commit with all default values."""
        commit = description.Commit()
        self.assertIs(commit.author, None)
        self.assertEqual(commit.author_email, '')
        self.assertEqual(commit.author_name, '')
        self.assertEqual(commit.committer_email, '')
        self.assertEqual(commit.committer_name, '')
        self.assertIs(commit.date, None)
        self.assertEqual(commit.description, description.Description())
        self.assertEqual(commit.sha, '')
        self.assertEqual(commit.short_sha, '')
        self.assertEqual(commit.title, '')
        self.assertIs(commit.expected_signoff, None)
        self.assertIn("Commit  ('')", str(commit))

    def test_commit_from_dict(self):
        """Returns a Commit populated from the input dict."""
        input_dict = {'author': {'username': 'test_user',
                                 'name': 'Test User',
                                 'gid': 'gid://Gitlab/User/1234567'},
                      'authorEmail': 'test_user@example.com',
                      'authorName': 'Test User',
                      'committerEmail': 'test_user@example.com',
                      'committerName': 'Test User',
                      'authoredDate': '2022-12-06T11:59:50Z',
                      'description': 'a commit',
                      'sha': 'd5de301285a720250ef97568f5dbdcbbe8480d5b',
                      'title': 'an example commit'
                      }
        commit = description.Commit(input_dict=input_dict)
        self.assertEqual(commit.author.username, 'test_user')
        self.assertEqual(commit.author_email, 'test_user@example.com')
        self.assertEqual(commit.author_name, 'Test User')
        self.assertEqual(commit.committer_email, 'test_user@example.com')
        self.assertEqual(commit.committer_name, 'Test User')
        self.assertTrue(isinstance(commit.date, description.datetime))
        self.assertEqual(commit.description.text, 'a commit')
        self.assertEqual(commit.sha, 'd5de301285a720250ef97568f5dbdcbbe8480d5b')
        self.assertEqual(commit.short_sha, 'd5de301285a7')
        self.assertEqual(commit.title, 'an example commit')
        self.assertEqual(commit.expected_signoff, ('Test User', 'test_user@example.com'))


class TestCommitDCOState(NoSocketTestCase):
    """Test the Commit.dco_state functionality."""

    def test_dco_state(self):
        """Returns the expected DCOState for the given Commit."""
        class DcoTest(typing.NamedTuple):
            expected_state: defs.DCOState
            commit_author: tuple
            commit_committer: tuple
            commit_description: str
            zstream: bool = False

        tests = [
            # Returns OK_NOT_REDHAT state because the author is not redhat.com.
            DcoTest(
                expected_state=defs.DCOState.OK_NOT_REDHAT,
                commit_author=('User Example', 'user@example.com'),
                commit_committer=('Ignored', 'ignored@example.com'),
                commit_description='Signed-off-by: User Example <user@example.com>'
            ),
            # Returns OK because the author has signed off and is from redhat.
            DcoTest(
                expected_state=defs.DCOState.OK,
                commit_author=('Red Hatter', 'user@redhat.com'),
                commit_committer=('Ignored', 'ignored@example.com'),
                commit_description='Signed-off-by: Red Hatter <user@redhat.com>'
            ),
            # Returns UNRECOGNIZED because the signoffs in the description do not match the author.
            DcoTest(
                expected_state=defs.DCOState.UNRECOGNIZED,
                commit_author=('Example User', 'example@redhat.com'),
                commit_committer=('Ignored', 'ignored@example.com'),
                commit_description='Signed-off-by: A User <a_user@redhat.com>'
            ),
            # Returns NAME_MATCHES state because a signoff was found with a matching name.
            DcoTest(
                expected_state=defs.DCOState.NAME_MATCHES,
                commit_author=('Example User', 'example@redhat.com'),
                commit_committer=('Ignored', 'ignored@example.com'),
                commit_description='Signed-off-by: Example User <a_user@redhat.com>'
            ),
            # Returns EMAIL_MATCHES state because a signoff was found with a matching email.
            DcoTest(
                expected_state=defs.DCOState.EMAIL_MATCHES,
                commit_author=('Example User', 'example@redhat.com'),
                commit_committer=('Ignored', 'ignored@example.com'),
                commit_description='Signed-off-by: Example User 2 <example@redhat.com>'
            ),
            # Uses unidecode so differences due to accents etc are ignored.
            DcoTest(
                expected_state=defs.DCOState.OK,
                commit_author=('Examplé User', 'example@redhat.com'),
                commit_committer=('Ignored', 'ignored@example.com'),
                commit_description='Signed-off-by: Example User <example@redhat.com>'
            ),
            # Returns OK because the committer has signed off and this is zstream.
            DcoTest(
                expected_state=defs.DCOState.OK,
                commit_author=('Ignored', 'ignored@example.com'),
                commit_committer=('Red Hatter', 'user@redhat.com'),
                commit_description='Signed-off-by: Red Hatter <user@redhat.com>',
                zstream=True
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_description = description.Description(test.commit_description)
                test_commit = description.Commit(
                    author_name=test.commit_author[0],
                    author_email=test.commit_author[1],
                    committer_name=test.commit_committer[0],
                    committer_email=test.commit_committer[1],
                    description=test_description,
                    zstream=test.zstream)
                self.assertIs(test.expected_state, test_commit.dco_state)
