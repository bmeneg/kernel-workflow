"""Tests for the users library."""
from copy import deepcopy
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook import users

USERS = [{'username': 'test_user',
          'gid': 'gid://gitlab/User/3246236',
          'name': 'Test User',
          'email': 'test_user@example.com'
          },
         {'username': 'example1',
          'gid': 'gid://gitlab/User/543254',
          'name': 'Example 1',
          'email': 'example1@example.com'
          },
         {'username': 'gl_user4',
          'gid': 'gid://gitlab/User/4',
          'name': 'Gitlab User 4'
          }]

NAMESPACE = 'group/project'


class TestUser(NoSocketTestCase):
    """Tests for the User dataclass."""

    def test_user_init(self):
        """Returns a User with the given parameters set."""
        auser = users.User(username='test_user',
                           name='Test User',
                           emails=['test_user@example.com'])
        self.assertEqual(auser.username, 'test_user')
        self.assertEqual(auser.gid, '')
        self.assertEqual(auser.id, 0)
        self.assertEqual(auser.name, 'Test User')
        self.assertEqual(auser.emails, ['test_user@example.com'])

        # This is lame but it makes cubertura happy.
        self.assertIn(auser.username, str(auser))
        self.assertIn(str(auser.id), str(auser))

        # Must set username, otherwise raises ValueError.
        with self.assertRaises(ValueError):
            auser = users.User()

    def test_from_user_dict(self):
        """Returns a populated User object."""
        user_dict = deepcopy(USERS[0])
        auser = users.User(user_dict=user_dict)
        self.assertEqual(auser.username, user_dict['username'])
        self.assertEqual(auser.gid, user_dict['gid'])
        self.assertEqual(auser.id, 3246236)
        self.assertEqual(auser.name, user_dict['name'])
        self.assertEqual(auser.emails, [user_dict['email']])

    def test_from_owners(self):
        """Creates a User from an owners.yaml entry."""
        fake_owners = {'name': 'Test User',
                       'email': 'test_user@example.com',
                       'gluser': 'test_user',
                       'restricted': True}
        auser = users.User(user_dict=fake_owners)
        self.assertEqual(auser.username, fake_owners['gluser'])
        self.assertEqual(auser.gid, '')
        self.assertEqual(auser.id, 0)
        self.assertEqual(auser.name, fake_owners['name'])
        self.assertEqual(auser.emails, [fake_owners['email']])

    def test_user_update(self):
        """Updates the User from the input dict."""
        auser = users.User(username='test_user',
                           name='Test User',
                           emails=['test_user@example.com'])
        user_dict = deepcopy(USERS[0])
        user_dict['email'] = 'test_user_alias@example.com'
        auser.update(user_dict)
        self.assertEqual(auser.username, 'test_user')
        self.assertEqual(auser.gid, user_dict['gid'])
        self.assertEqual(auser.id, 3246236)
        self.assertEqual(auser.name, 'Test User')
        self.assertEqual(auser.emails, ['test_user@example.com', 'test_user_alias@example.com'])

        # Blow up if the username in the input dict does not match the User.username.
        with self.assertRaises(ValueError):
            auser.update({'gluser': 'example_user', 'email': 'example_user@example.com'})


class TestUserCache(NoSocketTestCase):
    """Tests for the UserCache dict."""

    @staticmethod
    def get_new_cache(cache_items=None, namespace=None):
        """Return a new usercache."""
        if cache_items is None:
            cache_items = deepcopy(USERS)
        if namespace is None:
            namespace = NAMESPACE
        cache = users.UserCache(mock.Mock(), namespace)
        for raw_user in cache_items:
            cache.get(raw_user)
        return cache

    def test_usercache_init(self):
        """Sets up an empty cache."""
        user_cache = self.get_new_cache([])
        self.assertEqual(len(user_cache.data), 0)

        # Exercise the __repr__.
        self.assertIn('UserCache with 0 users', str(user_cache))

    def test_usercache_items(self):
        """Try out the cache."""
        user_cache = self.get_new_cache(USERS[:2])
        user_cache.graphql.find_member.side_effect = [{'username': 'email_user'}, None, None]
        user_cache.graphql.get_user_by_id.side_effect = [None, None]
        self.assertEqual(len(user_cache.data), 2)
        self.assertEqual(user_cache.get_by_email('test_user@example.com').username,
                         USERS[0]['username'])
        self.assertEqual(user_cache.get_by_email('unknown@email.com'),
                         user_cache.data['email_user'])
        self.assertEqual(user_cache.get_by_id(543254).username, USERS[1]['username'])
        self.assertEqual(user_cache.get_by_id(12345), None)
        self.assertEqual(user_cache.get_by_username('test_user').name, USERS[0]['name'])
        self.assertEqual(user_cache.get_by_username('unknwon_user'), None)

        # Does the right thing when using the get() method.
        self.assertEqual(user_cache.get('test_user@example.com').username, USERS[0]['username'])
        self.assertEqual(user_cache.get('unknown@email.com'), user_cache.data['email_user'])
        self.assertEqual(user_cache.get(543254).username, USERS[1]['username'])
        self.assertEqual(user_cache.get(12345), None)
        self.assertEqual(user_cache.get('test_user').name, USERS[0]['name'])
        self.assertEqual(user_cache.get('unknwon_user'), None)

        # Creates a new user when they don't exist in the cache...
        new_user = user_cache.get(USERS[2])
        self.assertEqual(new_user.username, USERS[2]['username'])
        # ... updates the user when they do exist in the cache.
        raw_user = deepcopy(USERS[2])
        raw_user['email'] = 'gluser4@gitlab.com'
        new_user_again = user_cache.get(raw_user)
        self.assertIs(new_user, new_user_again)
        self.assertEqual(new_user.emails, ['gluser4@gitlab.com'])
