"""Webhook interaction tests."""
from copy import deepcopy
from unittest import mock

from tests import fakes
from tests import test_base_mr
from tests.no_socket_test_case import NoSocketTestCase
from webhook import signoff
from webhook.defs import DCOState
from webhook.defs import GitlabObjectKind
from webhook.defs import GitlabURL
from webhook.description import Commit
from webhook.session import SessionRunner


class TestMR(NoSocketTestCase):
    """Tests for the signoff MR dataclass."""

    def test_mr_init(self):
        """Returns an MR object."""
        graphql = mock.Mock()
        graphql.client.query.side_effect = [test_base_mr.MR1_QUERY_RESULT,
                                            test_base_mr.MR1_COMMITS_RESULT]
        graphql.get_mr_descriptions.return_value = test_base_mr.MR1_DESCRIPTION_QUERY_RESULT
        test_mr = test_base_mr.create_mr(mr_func=signoff.MR, graphql=graphql)
        self.assertEqual(len(test_mr.commits), 2)

    def test_check_signoffs(self):
        """Looks up signoff emails for NAME_MATCHES commits and tries to link them to the author."""
        author = deepcopy(test_base_mr.AUTHOR1)
        author['email'] = 'author_email@redhat.com'
        new_commit = {'authorEmail': author['email'],
                      'authorName': author['name'],
                      'committerEmail': author['email'],
                      'committerName': author['name'],
                      'author': author,
                      'authoredDate': '2022-12-19T08:58:26+01:00',
                      'description': 'Bugzilla: https://bugzilla.redhat.com/12345\n'
                                     f"Signed-off-by: {author['name']}"
                                     f" <{author['email']}>",
                      'sha': 'aeefde19ed4139516342fc07624429da45e7e12b',
                      'title': 'mr1 head commit'}
        mr_commits = deepcopy(test_base_mr.MR1_COMMITS_RESULT)
        mr_commits['project']['mr']['commits']['nodes'].insert(0, new_commit)
        mock_graphql = mock.Mock()
        mock_graphql.client.query.side_effect = [test_base_mr.MR1_QUERY_RESULT, mr_commits]
        mock_graphql.get_mr_descriptions.return_value = {}
        test_mr = test_base_mr.create_mr(mr_func=signoff.MR, graphql=mock_graphql)
        commit = test_mr.commits[new_commit['sha']]
        self.assertIs(commit.dco_state, DCOState.OK)


class TestHelpers(NoSocketTestCase):
    """Tests for the various helper functions."""

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    def test_update_mr(self, mock_update_comment, mock_add_label):
        """Updates the comment and possibly sets a new label."""
        mock_session = SessionRunner('signoff', [], signoff.HANDLERS)
        namespace = 'goup/project'
        mr_id = 123
        username = 'mock_user'
        mock_instance = fakes.FakeGitLab()
        mock_instance.user = mock.Mock(username=username)
        mock_project = mock_instance.add_project(321, namespace)
        mock_mr = mock_project.add_mr(mr_id)

        # Scope changed, calls add_label.
        mock_mr.labels = ['Signoff::OK']
        signoff.update_mr(mock_session, mock_project, mr_id, 'text')
        mock_add_label.assert_called_once_with(mock_project, mr_id, ['Signoff::NeedsReview'])
        expected_text = signoff.REPORT_HEADER + ' ~"Signoff::NeedsReview"\n\ntext'
        mock_update_comment.assert_called_once_with(mock_mr, expected_text,
                                                    bot_name=mock_instance.user.username,
                                                    identifier=signoff.REPORT_HEADER)

        # Scope hasn't changed, no add_label.
        mock_add_label.reset_mock()
        mock_update_comment.reset_mock()
        mock_mr.labels = ['Signoff::OK']
        text = signoff.REPORT_STATUS % 'PASSED'
        signoff.update_mr(mock_session, mock_project, mr_id, text)
        mock_add_label.assert_not_called()
        expected_text = signoff.REPORT_HEADER + ' ~"Signoff::OK"\n\n' + text
        mock_update_comment.assert_called_once_with(mock_mr, expected_text,
                                                    bot_name=mock_instance.user.username,
                                                    identifier=signoff.REPORT_HEADER)

        # No existing label.
        mock_add_label.reset_mock()
        mock_update_comment.reset_mock()
        mock_mr.labels = []
        text = signoff.REPORT_STATUS % 'PASSED'
        signoff.update_mr(mock_session, mock_project, mr_id, text)
        mock_add_label.assert_called_once_with(mock_project, mr_id, ['Signoff::OK'])
        expected_text = signoff.REPORT_HEADER + ' ~"Signoff::OK"\n\n' + text
        mock_update_comment.assert_called_once_with(mock_mr, expected_text,
                                                    bot_name=mock_instance.user.username,
                                                    identifier=signoff.REPORT_HEADER)

    def test_generate_report(self):
        """Returns a markdown string of results."""
        # "Bad" results.
        commits = [Commit(input_dict=raw_commit) for raw_commit in test_base_mr.COMMITS]
        result = signoff.generate_report(commits, False)
        self.assertIn(signoff.REPORT_STATUS % 'FAILED', result)
        self.assertIn(signoff.REPORT_HELP, result)
        self.assertIn(signoff.REPORT_FOOTER, result)

        # "Good" results.
        commits = [mock.Mock(dco_state=DCOState.OK)]
        result = signoff.generate_report(commits, False)
        self.assertIn(signoff.REPORT_STATUS % 'PASSED', result)

        # "Bad" results due to no commits.
        commits = []
        result = signoff.generate_report(commits, False)
        self.assertIn(signoff.REPORT_STATUS % 'FAILED', result)
        self.assertIn(signoff.REPORT_NO_COMMITS, result)

    @mock.patch('webhook.signoff.MR')
    @mock.patch('webhook.signoff.generate_report')
    @mock.patch('webhook.signoff.update_mr')
    def test_process_mr(self, mock_update, mock_report, mock_mr):
        """Creates the MR object and updates the MR."""
        mock_session = SessionRunner('signoff', '', signoff.HANDLERS)
        mock_session.gl_instance = mock.Mock()
        mock_session.graphql = mock.Mock()
        mr_url = GitlabURL('https://gitlab.com/group/project/-/merge_requests/123')
        mock_event = mock.Mock(mr_url=mr_url, kind=GitlabObjectKind.MERGE_REQUEST)
        signoff.process_gl_event({}, mock_session, mock_event)
        mock_mr.assert_called_once()
        mock_report.assert_called_once_with(mock_mr.return_value.all_commits,
                                            mock_mr.return_value.project.public_signoff_ok)
        mock_update.assert_called_once()
