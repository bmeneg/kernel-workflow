"""Tests for external CI module."""
from unittest import mock

from tests import fakes
from tests.no_socket_test_case import NoSocketTestCase
import webhook.common as common
import webhook.limited_ci as limited_ci
from webhook.session import SessionRunner
from webhook.session_events import create_event

GITLAB_HEADER = {'message-type': 'gitlab'}


def init_fake_gitlab():
    """Initialize and return a fake gitlab.Gitlab instance."""
    glab = fakes.FakeGitLab()
    glab.add_group('group')
    glab.add_project(123, 'path/to/project')
    glab.add_project(456, 'outside-group')
    add_members_to = [glab.groups.get('group'),
                      glab.projects.get('path/to/project')]
    for g_or_p in add_members_to:
        g_or_p.members_all.add_member('testmember')
        g_or_p.members_all.add_member('anothermember')

    glab.user = fakes.FakeGitLabMember('good-bot')

    return glab


class TestProcessMessage(NoSocketTestCase):
    """Tests for common.process_message()."""

    HEADERS = {'message-type': 'gitlab'}

    def test_pipeline_call(self):
        """Verify correct handler is called for pipeline hook."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {
                       'id': 1,
                       'variables': [{'key': 'origin_path', 'value': 'none'}]
                   },
                   'commit': {'url': 'none'},
                   'user': {'username': 'testmember'},
                   'state': 'opened',
                   'project': {'web_url': 'project url'}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('No matching MR project for none' in o for o in logs.output))

    def test_mr_hook_no_project(self):
        """Verify MR with unknown project is handled correctly."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'path/to/project'},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='ERROR') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('Missing config for' in o for o in logs.output))

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG', {
        'path/to/project': {
            '.members_of': 'group',
        },
    })
    def test_mr_hook_group_member(self):
        """Check pipelines aren't triggered if the author is a group member."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'path/to/project'},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('Found internal contributor' in o for o in logs.output))

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG', {
        'path/to/project': {
            '.members_of': 'path/to/project',
        },
    })
    def test_mr_hook_project_member(self):
        """Test fallback with testing for a project member instead of group."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'path/to/project'},
                   'user': {'username': 'anothermember'},
                   'state': 'opened'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('Found internal contributor' in o for o in logs.output))

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG', {
        'path/to/project': {
            '.members_of': 'outside-group',
        },
    })
    def test_mr_hook_no_code_changes(self):
        """Check we abort if the MR code was not modified."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'path/to/project'},
                   'user': {'username': 'testmember'},
                   'state': 'opened',
                   'object_attributes': {'action': 'not-a-code-change'}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('Not a code change, ignoring' in o for o in logs.output))


class TestMain(NoSocketTestCase):
    """Sanity tests for limited_ci.main()."""

    @mock.patch.dict('os.environ', {'CONFIG_PATH': 'filepath'})
    @mock.patch.dict('webhook.limited_ci.CI_CONFIG', {
        'path/to/project': {
            '.members_of': 'path/to/project',
        },
    })
    @mock.patch('webhook.limited_ci.load_config', mock.Mock())
    @mock.patch('webhook.session.get_instance')
    def test_manual(self, mock_gitlab):
        """Verify manual request is correctly handled.

        This includes checking get_manual_hook_data() function.
        """
        args = ['--merge-request',
                'https://gitlab.example/path/to/project/-/merge_requests/11']
        fake_gitlab = init_fake_gitlab()
        fake_project = fake_gitlab.projects.get('path/to/project')
        fake_project.attributes = {
            'name': 'path/to/project',
            'http_url_to_repo': 'https://gitlab.example/path/to/project'
        }
        fake_project.add_mr(
            11,
            {'sha': 'last_commit',
             'author': {'username': 'testmember'},
             'target_branch': 'devel'},
            {'web_url': 'https://url'}
        )
        mock_gitlab.return_value = fake_gitlab

        with self.assertLogs('cki.webhook.limited_ci', level='DEBUG') as logs:
            limited_ci.main(args)
            self.assertTrue(any('Found internal contributor' in o for o in logs.output))


class TestStageSuffix(NoSocketTestCase):
    """Tests for limited_ci.get_stage_suffix()."""

    def test_suffix(self):
        """Verify the function returns expected strings."""
        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success')]
        self.assertEqual('', limited_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success'),
                fakes.FakeGitLabJob(125, 'stage2', 'skipped')]
        self.assertEqual('', limited_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success'),
                fakes.FakeGitLabJob(125, 'stage2', 'failed')]
        self.assertEqual('::stage2', limited_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'failed'),
                fakes.FakeGitLabJob(125, 'stage2', 'success')]
        self.assertEqual('::stage', limited_ci.get_stage_suffix(jobs))


class TestUpdatePipelineData(NoSocketTestCase):
    """Tests for limited_ci.update_pipeline_data()."""

    HEADERS = {'message-type': 'gitlab'}

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    def test_no_project(self):
        """Verify correct behavior when no matching MR project is found."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {'id': 10, 'variables': []},
                   'user': {'username': 'testmember'},
                   'commit': {'url': 'link'},
                   'project': {'web_url': 'project url'}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('No matching MR project' in o for o in logs.output))

    @mock.patch.dict('webhook.limited_ci.CI_CONFIG', {
        'path/to/project': {
            '.members_of': 'path/to/project',
        },
    })
    def test_retriggered(self):
        """Verify correct behavior when the pipeline is a retrigger."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {'id': 10, 'status': 'failed', 'variables': [
                       {'key': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'retrigger'},
                       {'key': 'origin_path', 'value': 'path/to/project'},
                   ]},
                   'user': {'username': 'testmember'},
                   'commit': {'url': 'link'},
                   'project': {'web_url': 'project url'}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        test_session = SessionRunner('limited_ci', args, limited_ci.HANDLERS)
        test_session.gl_instance = init_fake_gitlab()
        test_session.rh_projects = mock.Mock()

        test_event = create_event(test_session, GITLAB_HEADER, message)

        kind = limited_ci.GitlabObjectKind.get(message['object_kind'])
        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            limited_ci.HANDLERS[kind]('', test_session, test_event)
            self.assertTrue(any('Handling a retriggered pipeline' in o for o in logs.output))


class TestHandleMR(NoSocketTestCase):
    """Tests for limited_ci.handle_mr()."""

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.limited_ci.already_commented')
    def test_no_config(self, mock_commented, mock_labeler):
        """Verify the function bails out if there is no pipeline project/branch config."""
        mr_data = {
            'object_attributes': {
                'iid': 11,
                'target_branch': 'target',
                'url': 'https://dummy-link-to.mr',
                'last_commit': {
                    'id': '0123abcd'
                },
                'target': {
                    'git_http_url': 'giturl'
                }
            },
            'project': {
                'path_with_namespace': 'path/to/project',
                'id': 123
            }
        }
        project_config = {
            '.mr_project': 'path/to/project',
            '.members_of': 'path/to/project'
        }
        glab = init_fake_gitlab()
        mock_mr = glab.projects.get('path/to/project').add_mr(11)
        mock_mr.diff_refs = {'base_sha': 'blablabla'}
        mock_commented.return_value = True

        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            limited_ci.handle_mr(mock.Mock(), glab, project_config, mr_data)
            self.assertTrue(
                any('No pipeline project or branch configured' in o for o in logs.output))

    @mock.patch('webhook.limited_ci.cki_pipeline.trigger_multiple')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.limited_ci.already_commented')
    def test_handle_mr(self, mock_commented, mock_label, mock_trigger):
        """Verify the function doesn't do anything weird."""
        mr_data = {
            'object_attributes': {
                'iid': 11,
                'target_branch': 'target',
                'url': 'https://dummy-link-to.mr',
                'last_commit': {
                    'id': '0123abcd'
                },
                'target': {
                    'git_http_url': 'giturl'
                }
            },
            'project': {
                'path_with_namespace': 'path/to/project',
                'id': 123
            }
        }
        project_config = {
            '.mr_project': 'path/to/project',
            '.members_of': 'path/to/project',
            '.cki_external_pipeline_branch': 'branch',
            '.cki_external_pipeline_project': 'project'
        }
        glab = init_fake_gitlab()
        mock_mr = glab.projects.get('path/to/project').add_mr(11)
        mock_mr.diff_refs = {'base_sha': 'blablabla'}
        mock_commented.return_value = True

        mock_trigger.return_value = [fakes.FakeGitLabPipeline(123, {'web_url': 'https://url'})]
        limited_ci.handle_mr(mock.Mock(), glab, project_config, mr_data)

    @mock.patch('webhook.limited_ci.sleep', mock.Mock())
    def test_wait_for_attribute(self):
        """Loops until the instance attribute exists and is not null."""
        mr1 = mock.Mock(spec=[])
        mr2 = mock.Mock(diff_refs=None)
        mr3 = mock.Mock(diff_refs={'hi': 'there'})
        mock_project = mock.Mock()
        mock_project.mergerequests.get.side_effect = [mr1, mr2, mr3]
        kwargs = {'id': 123}
        with self.assertLogs('cki.webhook.limited_ci', level='INFO') as logs:
            result = limited_ci.wait_for_attribute(mr1, 'diff_refs',
                                                   mock_project.mergerequests.get, kwargs)
            self.assertTrue(any('diff_refs after 3 loops' in o for o in logs.output))
            self.assertIs(result, mr3)
            self.assertEqual(mock_project.mergerequests.get.call_count, 3)
            mock_project.mergerequests.get.assert_called_with(**kwargs)

        # Raises ValueError if we loop too much.
        mock_project.mergerequests.get.reset_mock(side_effect=True)
        mock_project.mergerequests.get.return_value = mr1
        with self.assertRaises(ValueError):
            limited_ci.wait_for_attribute(mr1, 'diff_refs',
                                          mock_project.mergerequests.get, {'id': 123})
            self.assertEqual(mock_project.mergerequests.get.call_count, 5)
