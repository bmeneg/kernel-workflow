"""Tests of the libbz library."""
from copy import deepcopy
from datetime import datetime
from datetime import timezone
from http.client import RemoteDisconnected
from unittest import mock
from xmlrpc.client import DateTime

from tests import fakes_bz
from tests.no_socket_test_case import NoSocketTestCase
from webhook import libbz
from webhook.defs import BZStatus


class TestFetchBugs(NoSocketTestCase):
    """Tests for fetch_bugs and its helpers."""

    def test_getbugs(self):
        """Returns bugzilla library getbugs()."""
        mock_bzcon = mock.Mock()
        bug_list = [123, 456, 789]
        result = libbz._getbugs(mock_bzcon, bug_list)
        mock_bzcon.getbugs.assert_called_with(bug_list, include_fields=libbz.BUG_FIELDS)
        self.assertIs(result, mock_bzcon.getbugs.return_value)

    def test_getbugs_retries(self):
        """Retries up to MAX_CONNECTION_RETRIES times when there is a ConnectionError."""
        mock_bzcon = mock.Mock()
        connection_exception = ConnectionError(
            ('Connection aborted.',
             RemoteDisconnected('Remote end closed connection without response'))
        )
        # Retries the maximum times, raises ConnectionError.
        mock_bzcon.getbugs.side_effect = connection_exception
        with self.assertRaises(ConnectionError):
            libbz._getbugs(mock_bzcon, [1234567])
        self.assertEqual(mock_bzcon.getbugs.call_count, libbz.MAX_CONNECTION_RETRIES)

        # Retries one time, returns bugs.
        mock_bzcon.reset_mock(side_effect=True)
        mock_bug = mock.Mock()
        mock_bzcon.getbugs.side_effect = [connection_exception, [mock_bug]]
        result = libbz._getbugs(mock_bzcon, [1234567])
        self.assertEqual(result, [mock_bug])
        self.assertEqual(mock_bzcon.getbugs.call_count, 2)

    def test_get_missing_bugs(self):
        """Returns the entries from the input bug_names which are not in the bug_cache."""
        bug_cache_list = {7777777: fakes_bz.BZ7777777,
                          2345678: fakes_bz.BZ2345678,
                          3456789: fakes_bz.BZ3456789}.values()
        bug_names = [7777777, 2233456, 'CVE-1235-13516', 2345678, 'CVE-1921-16127']
        self.assertEqual(libbz.get_missing_bugs(bug_names, bug_cache_list),
                         {2233456, 'CVE-1921-16127'})

    def test_fetch_bugs_no_input(self):
        """Returns an empty list because the input bug_list is empty."""
        self.assertEqual(libbz.fetch_bugs([]), [])

    @mock.patch('webhook.libbz._getbugs')
    @mock.patch('webhook.libbz.get_bzcon')
    def test_fetch_bugs_all_new(self, mock_bzcon, mock_getbugs):
        """Clears the cache before proceeding, no input bug_list."""
        mock_getbugs.return_value = [fakes_bz.BZ7777777, fakes_bz.BZ2345678, fakes_bz.BZ3456789]
        bz_ids = [7777777, 2345678, 'CVE-1235-13516']

        result = libbz.fetch_bugs(bz_ids)
        mock_getbugs.assert_called_with(mock_bzcon.return_value, bz_ids)
        self.assertCountEqual(result, mock_getbugs.return_value)


class TestHelpers(NoSocketTestCase):
    """Tests for helper functions."""
    BUG01 = fakes_bz.FakeBZ(id=1, status='UNKNOWN')
    BUG02 = fakes_bz.FakeBZ(id=2, status='NEW')
    BUG03 = fakes_bz.FakeBZ(id=3, status='ASSIGNED')
    BUG04 = fakes_bz.FakeBZ(id=4, status='POST')
    BUG05 = fakes_bz.FakeBZ(id=5, status='MODIFIED')
    BUG06 = fakes_bz.FakeBZ(id=6, status='ON_DEV')
    BUG07 = fakes_bz.FakeBZ(id=7, status='ON_QA')
    BUG08 = fakes_bz.FakeBZ(id=8, status='VERIFIED')
    BUG09 = fakes_bz.FakeBZ(id=9, status='RELEASE_PENDING')
    BUG10 = fakes_bz.FakeBZ(id=10, status='CLOSED')

    def test_bugs_with_lower_status(self):
        """Returns the bugs whose status is lower than status and at least min_status."""

        bug_list = [self.BUG01, self.BUG02, self.BUG03, self.BUG04, self.BUG05,
                    self.BUG06, self.BUG07, self.BUG08, self.BUG09, self.BUG10]
        self.assertCountEqual(libbz.bugs_with_lower_status(bug_list, BZStatus.POST),
                              [self.BUG02, self.BUG03])
        self.assertCountEqual(libbz.bugs_with_lower_status(bug_list, BZStatus.VERIFIED),
                              [self.BUG02, self.BUG03, self.BUG04,
                               self.BUG05, self.BUG06, self.BUG07])
        self.assertCountEqual(libbz.bugs_with_lower_status(bug_list, BZStatus.RELEASE_PENDING,
                              BZStatus.MODIFIED), [self.BUG05, self.BUG06, self.BUG07, self.BUG08])

    def test_latest_Faiedqa_timestamp(self):
        """Returns the most recent timestamp from the history when FailedQA was added."""
        # No timestamp found.
        bug_history = [{'changes': [{'added': 'qa_ack?',
                                     'field_name': 'flagtypes.name',
                                     'removed': ''}],
                        'when': DateTime('20210916T06:21:38'),
                        'who': 'bot@example.com'
                        },
                       {'changes': [{'added': '',
                                     'field_name': 'flagtypes.name',
                                     'removed': 'needinfo?(user@example.com)'},
                                    {'added': 'ASSIGNED',
                                     'field_name': 'status',
                                     'removed': 'NEW'}],
                        'when': DateTime('20210918T08:00:12'),
                        'who': 'user@example.com'
                        },
                       {'changes': [{'added': 'POST',
                                     'field_name': 'status',
                                     'removed': 'ASSIGNED'}],
                        'when': DateTime('20210919T16:26:42'),
                        'who': 'user@example.com'
                        }]
        self.assertIs(libbz.latest_failedqa_timestamp(bug_history), None)
        bug_history.insert(2, {'changes': [{'added': 'FailedQA',
                                            'field_name': 'cf_verified',
                                            'removed': 'Tested '},
                                           {'added': 'ASSIGNED',
                                            'field_name': 'status',
                                            'removed': 'POST'}],
                               'when': DateTime('20210921T12:00:00'),
                               'who': 'qa@example.com'})
        result = libbz.latest_failedqa_timestamp(bug_history)
        self.assertEqual(result.replace(tzinfo=timezone.utc).timestamp(), 1632225600.0)
        bug_history.append({'changes': [{'added': 'FailedQA',
                                         'field_name': 'cf_verified',
                                         'removed': 'Tested '},
                                        {'added': 'ASSIGNED',
                                         'field_name': 'status',
                                         'removed': 'POST'}],
                            'when': DateTime('20210925T16:00:00'),
                            'who': 'qa@example.com'})
        result = libbz.latest_failedqa_timestamp(bug_history)
        self.assertEqual(result.replace(tzinfo=timezone.utc).timestamp(), 1632585600.0)

    @mock.patch('webhook.libbz.latest_failedqa_timestamp', wraps=libbz.latest_failedqa_timestamp)
    def test_bugs_to_move_to_post_no_failedqa(self, mock_failedqa):
        """Returns the list of bugs that are ready to move to POST."""
        bug1 = deepcopy(self.BUG01)
        bug2 = deepcopy(self.BUG02)
        bug3 = deepcopy(self.BUG03)
        bug4 = deepcopy(self.BUG04)
        bug5 = deepcopy(self.BUG05)
        bug_list = [bug1, bug2, bug3, bug4, bug5]

        # No FailedQA bugs. bug2 and bug3 can move to POST.
        pipeline_stamp = datetime.now()
        result = libbz.bugs_to_move_to_post(bug_list, pipeline_stamp)
        self.assertCountEqual(result, [bug2, bug3])
        bug_list[0].bugzilla.bugs_history_raw.assert_not_called()

        # bug3 has 'FailedQA' more recently than the pipeline so only bug2 is returned.
        bug3.cf_verified = ['FailedQA']
        pipeline_stamp = datetime.fromtimestamp(1631937600.0)
        history = [{'changes': [{'added': 'FailedQA',
                                          'field_name': 'cf_verified',
                                          'removed': 'Tested '},
                                {'added': 'ASSIGNED',
                                 'field_name': 'status',
                                 'removed': 'POST'}],
                    'when': DateTime('20210921T12:00:00'),
                    'who': 'qa@example.com'}]
        bug_list[0].bugzilla.bugs_history_raw.return_value = {'bugs': [{'id': 3,
                                                                        'alias': [],
                                                                        'history': history}
                                                                       ]}
        result = libbz.bugs_to_move_to_post(bug_list, pipeline_stamp)
        mock_failedqa.assert_called_once_with(history)
        self.assertEqual(result, [bug2])

    def test_update_bug_status_no_prod(self):
        """Moves qualifying bugs to the given status and returns them in a list."""
        # Given an empty bug_list there is nothing to do.
        self.assertEqual(libbz.update_bug_status([], libbz.BZStatus.POST), [])

        # If none of the bugs have a status less than the input, nothing to do.
        self.assertEqual(libbz.update_bug_status([self.BUG06], libbz.BZStatus.POST), [])

        # Not production, just set the new status and return the list.
        bug1 = deepcopy(self.BUG01)
        bug2 = deepcopy(self.BUG02)
        bug3 = deepcopy(self.BUG03)
        bug4 = deepcopy(self.BUG04)
        bug5 = deepcopy(self.BUG05)
        bug_list = [bug1, bug2, bug3, bug4, bug5]
        result = libbz.update_bug_status(bug_list, libbz.BZStatus.MODIFIED)
        self.assertEqual(result, [bug2, bug3, bug4])
        self.assertEqual(bug2.status, 'MODIFIED')
        self.assertEqual(bug3.status, 'MODIFIED')
        self.assertEqual(bug4.status, 'MODIFIED')

    @mock.patch('webhook.libbz.is_production_or_staging', mock.Mock(return_value=True))
    def test_update_bug_status_prod(self):
        """Moves qualifying bugs to the given status and returns them in a list."""
        # Production, set the new status and return the list.
        bug1 = deepcopy(self.BUG01)
        bug2 = deepcopy(self.BUG02)
        bug3 = deepcopy(self.BUG03)
        bug4 = deepcopy(self.BUG04)
        bug5 = deepcopy(self.BUG05)
        bug_list = [bug1, bug2, bug3, bug4, bug5]

        bug2_result = {'id': bug2.id, 'changes': {'status': {'added': 'MODIFIED'}}}
        bug3_result = {'id': bug3.id, 'changes': {'status': {'added': 'MODIFIED'}}}
        bug4_result = {'id': bug4.id, 'changes': {'status': {'added': 'MODIFIED'}}}
        update_return = {'bugs': [bug2_result, bug3_result, bug4_result]}
        bug1.bugzilla.update_bugs.return_value = update_return

        result = libbz.update_bug_status(bug_list, libbz.BZStatus.MODIFIED)
        bug1.bugzilla.build_update.assert_called_with(status=libbz.BZStatus.MODIFIED.name)
        bug1.bugzilla.update_bugs.assert_called_with([2, 3, 4],
                                                     bug1.bugzilla.build_update.return_value)
        self.assertEqual(result, [bug2, bug3, bug4])
        self.assertEqual(bug2.status, 'MODIFIED')
        self.assertEqual(bug3.status, 'MODIFIED')
        self.assertEqual(bug4.status, 'MODIFIED')
