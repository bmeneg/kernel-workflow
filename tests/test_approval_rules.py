"""Tests for the approval rules classes."""
from copy import deepcopy
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from tests.test_users import USERS
from webhook import approval_rules
from webhook.users import User
from webhook.users import UserCache

X86_RULE = {'approvalsRequired': 1,
            'approved': True,
            'approvedBy': {'nodes': [USERS[1]]},
            'eligibleApprovers': USERS,
            'id': 'gid://gitlab/ApprovalMergeRequestRule/86',
            'name': 'x86',
            'overridden': False,
            'sourceRule': None,
            'type': 'REGULAR'}

ORIGINAL_ALL_MEMBERS = {'approvalsRequired': 2,
                        'eligibleApprovers': [],
                        'id': 'gid://gitlab/ApprovalProjectRule/65432',
                        'name': 'All Members',
                        'overridden': False,
                        'type': 'ANY_APPROVER'}

ALL_MEMBERS_RULE = {'approvalsRequired': 1,
                    'approved': False,
                    'approvedBy': {'nodes': [USERS[1]]},
                    'eligibleApprovers': [],
                    'id': 'gid://gitlab/ApprovalMergeRequestRule/12345',
                    'name': 'All Members',
                    'overridden': True,
                    'sourceRule': ORIGINAL_ALL_MEMBERS,
                    'type': 'ANY_APPROVER'}


class TestApprovalRuleType(NoSocketTestCase):
    """Tests for the ApprovalRuleType enum."""

    def test_approvalruletype(self):
        """Returns the expected ApprovalRuleType."""
        rule_types = ('UNKNOWN', 'ANY_APPROVER', 'CODE_OWNER', 'REGULAR', 'REPORT_APPROVER',
                      'BLOCKING', 'OWNERS', 'SOURCE')

        self.assertEqual(len(approval_rules.ApprovalRuleType), len(rule_types))
        for rule_type in rule_types:
            approval_rule_type = getattr(approval_rules.ApprovalRuleType, rule_type)
            self.assertIs(approval_rules.ApprovalRuleType.get(rule_type), approval_rule_type)


class TestApprovalUser(NoSocketTestCase):
    """Tests for the ApprovalUser dataclass."""

    def test_approval_user(self):
        """Wraps a User."""
        auser = User(username='example', emails=['example@example.com'])
        approval_user = approval_rules.ApprovalUser(auser, True)
        self.assertIs(approval_user.user, auser)
        self.assertIs(approval_user.approved, True)
        self.assertEqual(approval_user.emails, auser.emails)

    def test_approval_user_new(self):
        """Returns a new ApprovalUser from the given UserCache."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        user_cache = UserCache(mock_graphql, namespace)
        approval_user = approval_rules.ApprovalUser.new(user_cache, USERS[0], True)
        self.assertIs(approval_user.user, user_cache.data[USERS[0]['username']])
        self.assertIs(approval_user.approved, True)


class TestApprovalRule(NoSocketTestCase):
    """Tests for the ApprovalRule class."""

    def test_init_regular(self):
        """Returns an ApprovalRule with useful properties."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        user_cache = UserCache(mock_graphql, namespace)
        input_dict = deepcopy(X86_RULE)
        x86_rule = approval_rules.ApprovalRule(user_cache, input_dict)

        self.assertIs(x86_rule.user_cache, user_cache)
        self.assertIs(x86_rule.data, input_dict)
        self.assertIs(x86_rule.source_rule, False)
        self.assertIs(x86_rule.approved, True)
        self.assertEqual(len(x86_rule.approved_by), 1)
        self.assertIs(x86_rule.approved_by.pop().user, user_cache.data[USERS[1]['username']])
        self.assertEqual({auser.user for auser in x86_rule.eligible},
                         {user for user in user_cache.data.values()})
        self.assertEqual(x86_rule.id, 86)
        self.assertEqual(x86_rule.missing, set())
        self.assertEqual(x86_rule.name, X86_RULE['name'])
        self.assertIs(x86_rule.overridden, False)
        self.assertEqual(x86_rule.required, 1)
        self.assertIs(x86_rule.source, None)
        self.assertIs(x86_rule.type, approval_rules.ApprovalRuleType.REGULAR)

        self.assertIn('approved:', str(x86_rule))

    def test_init_any_approver(self):
        """Returns an ApprovalRule with useful properties."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        user_cache = UserCache(mock_graphql, namespace)
        input_dict = deepcopy(ALL_MEMBERS_RULE)
        any_member_rule = approval_rules.ApprovalRule(user_cache, input_dict)

        self.assertIs(any_member_rule.user_cache, user_cache)
        self.assertIs(any_member_rule.data, input_dict)
        self.assertIs(any_member_rule.source_rule, False)
        self.assertIs(any_member_rule.approved, True)
        self.assertEqual(len(any_member_rule.approved_by), 1)
        self.assertIs(any_member_rule.approved_by.pop().user, user_cache.data[USERS[1]['username']])
        self.assertEqual(any_member_rule.eligible, set())
        self.assertEqual(any_member_rule.id, 12345)
        self.assertEqual(any_member_rule.missing, set())
        self.assertEqual(any_member_rule.name, ALL_MEMBERS_RULE['name'])
        self.assertIs(any_member_rule.overridden, True)
        self.assertEqual(any_member_rule.required, 1)
        self.assertIs(any_member_rule.type, approval_rules.ApprovalRuleType.ANY_APPROVER)

        self.assertIn('eligible: ∞', str(any_member_rule))

        source_rule = any_member_rule.source

        self.assertIs(source_rule.user_cache, user_cache)
        self.assertIs(source_rule.data, input_dict['sourceRule'])
        self.assertIs(source_rule.source_rule, True)
        self.assertIs(source_rule.approved, False)
        self.assertEqual(len(source_rule.approved_by), 1)
        self.assertIs(source_rule.approved_by.pop().user, user_cache.data[USERS[1]['username']])
        self.assertEqual(source_rule.eligible, set())
        self.assertEqual(source_rule.id, 65432)
        self.assertEqual(source_rule.missing, set())
        self.assertEqual(source_rule.name, ORIGINAL_ALL_MEMBERS['name'])
        self.assertIs(source_rule.overridden, False)
        self.assertEqual(source_rule.required, 2)
        self.assertIs(source_rule.type, approval_rules.ApprovalRuleType.SOURCE)

        self.assertIn('eligible: ∞', str(source_rule))

    def test_from_entry(self):
        """Creates a valid ApprovalRule from an owners Entry object."""
        mock_graphql = mock.Mock()
        mock_graphql.find_member.side_effect = USERS + [None]
        namespace = 'group/project'
        user_cache = UserCache(mock_graphql, namespace)

        users = deepcopy(USERS)
        for user in users:
            user['gluser'] = user.pop('username')
        reviewers = [users[0]]
        maintainers = users[1:] + [{'gluser': 'missing_user', 'name': 'Missing User'}]
        entry_spec = {'required_approvals': True,
                      'reviewers': reviewers,
                      'maintainers': maintainers,
                      'subsystem_label': 'cool_subsystem'}
        mock_entry = mock.Mock(spec=entry_spec.keys(), **entry_spec)

        entry_rule = approval_rules.ApprovalRule.from_entry(user_cache, mock_entry)

        self.assertIs(entry_rule.user_cache, user_cache)
        self.assertIs(entry_rule.source_rule, False)
        self.assertIs(entry_rule.approved, False)
        self.assertEqual(entry_rule.approved_by, set())
        self.assertEqual({auser.user for auser in entry_rule.eligible},
                         {user for user in user_cache.data.values()})
        self.assertEqual(entry_rule.id, 0)
        self.assertEqual(len(entry_rule.missing), 1)
        missing_user = entry_rule.missing.pop()
        self.assertEqual(missing_user.username, 'missing_user')
        self.assertEqual(entry_rule.name, entry_spec['subsystem_label'])
        self.assertIs(entry_rule.overridden, False)
        self.assertEqual(entry_rule.required, 1)
        self.assertIs(entry_rule.source, None)
        self.assertIs(entry_rule.type, approval_rules.ApprovalRuleType.OWNERS)
