"""Tests for merge_conflicts_check."""
import os
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook.utils import merge_conflicts_check


class TestMergeConflictsChecker(NoSocketTestCase):
    """Tests for the various helper functions."""

    MOCK_MR = {'iid': '66',
               'title': 'This is the title of my MR',
               'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/66',
               'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
               'targetBranch': 'main',
               'author': {'username': 'shadowman',
                          'name': 'Shadow Man',
                          'email': 'shadowman@redhat.com'},
               'labels': {'nodes': [{'title': 'Merge::OK',
                                     'description': 'This MR can be merged'}]},
               'draft': False}

    MOCK_MR2 = {'iid': '67',
                'title': 'This is the title of my 2nd MR',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/67',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'targetBranch': 'main',
                'author': {'username': 'shadowman',
                           'name': 'Shadow Man',
                           'email': 'shadowman@redhat.com'},
                'labels': {'nodes': [{'title': 'Merge::Warning',
                                      'description': 'This MR conflicts with other MRs'}]},
                'draft': False}

    MOCK_MR3 = {'iid': '68',
                'title': 'This is the title of my 3rd MR',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/68',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'targetBranch': 'main',
                'author': {'username': 'shadowman',
                           'name': 'Shadow Man',
                           'email': 'shadowman@redhat.com'},
                'labels': {'nodes': [{'title': 'Merge::OK',
                                      'description': 'This MR can be merged'}]},
                'draft': False}

    MOCK_MR_LIST = {f"foo/bar/{MOCK_MR['project']['name']}_{MOCK_MR['targetBranch']}":
                    [MOCK_MR, MOCK_MR2]}

    GQL_MRS = {'project':
               {'id': 'gid://gitlab/Project/1234',
                'mergeRequests':
                {'pageInfo': {'hasNextPage': False, 'endCursor': 'eyJjc'},
                 'nodes': [MOCK_MR, MOCK_MR2]}}}

    def test_get_open_mrs(self):
        mock_gql = mock.Mock()
        mock_gql.check_query_results.return_value = self.GQL_MRS
        namespace = 'foo/bar'
        namespace_type = 'project'
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            merge_conflicts_check.get_open_mrs(mock_gql, namespace, namespace_type)
            self.assertIn(f"Project MRs: found {self.MOCK_MR_LIST}", logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_prep_branch_for_merges(self):
        rhkernel_src = '/src/linux'
        mock_proj = mock.Mock()
        mock_proj.name = 'blah'
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        ret = merge_conflicts_check.prep_branch_for_merges(rhkernel_src, mr_params)
        self.assertEqual(ret, '/src/blah-main-megamerge/')

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_try_merging_all(self):
        mrs = [self.MOCK_MR2, self.MOCK_MR3]
        mock_proj = mock.Mock(name='blah')
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        ret = merge_conflicts_check.try_merging_all(mrs, mr_params)
        self.assertEqual(ret, [])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_mergeable')
    def test_check_pending_conflicts(self, mock_mergeable):
        this_mr = self.MOCK_MR
        pname = 'blah'
        conflict_mrs = [self.MOCK_MR2]
        merge_dir = '/src/foo-bar-blah_main'
        reset_branch = 'blah_main-reset'
        mock_mergeable.return_value = True
        ret = merge_conflicts_check.check_pending_conflicts(this_mr, pname, conflict_mrs,
                                                            merge_dir, reset_branch)
        self.assertEqual(ret, [])
        mock_mergeable.return_value = False
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            ret = merge_conflicts_check.check_pending_conflicts(this_mr, pname, conflict_mrs,
                                                                merge_dir, reset_branch)
            self.assertIn("MR 67 can't be merged by itself, skipping conflict check",
                          logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.Projects')
    @mock.patch('webhook.utils.merge_conflicts_check.check_pending_conflicts')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    @mock.patch('webhook.utils.merge_conflicts_check.format_conflict_info')
    @mock.patch('webhook.utils.merge_conflicts_check.check_for_merge_conflicts')
    def test_find_direct_conflicts(self, mock_check, mock_format, mock_update, mock_checkp,
                                   mock_projects):
        args = mock.Mock(testing=False)
        conflict_mrs = [self.MOCK_MR2, self.MOCK_MR3]
        mock_mr = mock.Mock()
        mock_proj = mock.Mock(name='blah')
        mock_proj.mergerequests.get.return_value = mock_mr
        mock_projects = mock.Mock()
        mock_projects.return_value.get_project_by_id.return_value = mock.Mock(confidential=False)
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        user = mock.Mock()
        # MR has conflicts with target branch
        mock_check.return_value = 'This MR has conflicts with its target branch'
        merge_conflicts_check.find_direct_conflicts(
            mock.Mock(), args, conflict_mrs, mr_params, user)
        mock_mr.notes.create.assert_called_with({'body': f'/label {defs.MERGE_CONFLICT_LABEL}'})
        mock_checkp.assert_not_called()
        # MR has conflicts with other MRs
        mock_check.return_value = ''
        mock_checkp.return_value = 'This MR has conflicts with other MRs'
        merge_conflicts_check.find_direct_conflicts(
            mock.Mock(), args, conflict_mrs, mr_params, user)
        mock_mr.notes.create.assert_called_with({'body': f'/label {defs.MERGE_WARNING_LABEL}'})

    @mock.patch('webhook.kgit.clean_up_temp_merge_branch')
    def test_clean_up_temp_merge_branches(self, mock_cleanup):
        args = mock.Mock(rhkernel_src='/src/kernel')
        merge_dirs = {'foo/bar/blah_main': '/src/foo-bar-blah_main'}
        merge_conflicts_check.clean_up_temp_merge_branches(args, merge_dirs)
        mock_cleanup.assert_called_with('/src/kernel', 'foo-bar-blah_main',
                                        '/src/foo-bar-blah_main')

    def test_set_merge_ok_labels(self):
        args = mock.Mock(testing=False)
        mock_mrs = [self.MOCK_MR2]
        mock_mr = mock.Mock()
        mock_proj = mock.Mock(name='blah')
        mock_proj.mergerequests.get.return_value = mock_mr
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main'}
        merge_conflicts_check.set_merge_ok_labels(args, mock_mrs, [], mr_params)
        mock_mr.notes.create.assert_called_with({'body': f'/label Merge::{defs.READY_SUFFIX}'})
        mock_mr.notes.create.call_count = 0
        merge_conflicts_check.set_merge_ok_labels(args, mock_mrs, [self.MOCK_MR2], mr_params)
        mock_mr.notes.create.assert_not_called()

    @mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    def test_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-r", "/src/linux"]):
            args = merge_conflicts_check._get_parser_args()
            self.assertFalse(args.testing)
            self.assertEqual(args.rhkernel_src, '/src/linux')
            self.assertEqual(args.sentry_ca_certs, '/etc/pki/certs/whatever.ca')

    def test_main_no_src(self):
        mock_args = mock.Mock(rhkernel_src=None)
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            merge_conflicts_check.main(mock.Mock(), mock_args)
            self.assertIn("No path to RH Kernel source git found, aborting!", logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.get_open_mrs')
    @mock.patch('webhook.utils.merge_conflicts_check.GitlabGraph')
    def test_main_no_open_mrs(self, mock_glgraph, mock_mrs):
        mock_args = mock.Mock(rhkernel_src='/src/linux',
                              groups=['redhat/rhel/src/kernel'],
                              projects=['cki-project/kernel-ark'])
        mock_mrs.return_value = {}
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='INFO') as logs:
            merge_conflicts_check.main(mock.Mock(), mock_args)
            self.assertIn("Fetching from git remotes to ensure we have the latest data needed",
                          logs.output[-3])
            self.assertIn("Finding open MRs for groups ['redhat/rhel/src/kernel'] "
                          "and projects ['cki-project/kernel-ark']", logs.output[-2])
            self.assertIn("No open MRs to process.", logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.prep_branch_for_merges',
                mock.Mock(return_value='foo/bar/blah'))
    @mock.patch('webhook.utils.merge_conflicts_check.try_merging_all',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.find_direct_conflicts',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.set_merge_ok_labels',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.clean_up_temp_merge_branches',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.get_instance')
    @mock.patch('webhook.utils.merge_conflicts_check.get_open_mrs')
    @mock.patch('webhook.utils.merge_conflicts_check.GitlabGraph')
    def test_main_open_mrs(self, mock_glgraph, mock_mrs, mock_get_instance):
        mock_args = mock.Mock(rhkernel_src='/src/linux',
                              groups=[], projects=['foo/bar/blah'])
        mock_mrs.return_value = self.MOCK_MR_LIST
        mock_instance = mock.Mock()
        mock_instance.projects.git.return_value = mock.Mock()
        mock_get_instance.return_value = mock_instance
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='INFO') as logs:
            merge_conflicts_check.main(mock.Mock(), mock_args)
            self.assertIn("Finding open MRs for groups [] and projects ['foo/bar/blah']",
                          logs.output[-1])
