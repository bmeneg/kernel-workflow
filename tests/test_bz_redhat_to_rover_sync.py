"""Tests for check_for_fixes."""
import os
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook.utils import bz_redhat_to_rover_sync


class TestRoverSync(NoSocketTestCase):
    """Tests for the various helper functions."""

    MOCK_LDAP = [('uid=shadowman,ou=users,dc=redhat,dc=com',
                  {'memberOf':
                   [b'cn=Employee,ou=userClass,dc=redhat,dc=com',
                    b'cn=rhel-sst-kmaint,ou=adhoc,ou=managedGroups,dc=redhat,dc=com'],
                   'rhatPrimaryMail': [b'shadowman@redhat.com'],
                   'sendmailMTAAliasValue': [b'coolcat@redhat.com']})]

    MOCK_LDAP2 = [('uid=departed,ou=users,dc=redhat,dc=com',
                   {'memberOf':
                    [b'cn=Employee,ou=userClass,dc=redhat,dc=com',
                     b'cn=rhel-sst-kmaint,ou=adhoc,ou=managedGroups,dc=redhat,dc=com'],
                    'rhatPrimaryMail': [b'departed@redhat.com'],
                    'sendmailMTAAliasValue': [b'/usr/bin/formail someoneelse@redhat.com']})]

    MOCK_LDAP3 = [('uid=departed,ou=users,dc=redhat,dc=com',
                   {'memberOf':
                    [b'cn=Employee,ou=userClass,dc=redhat,dc=com',
                     b'cn=rhel-sst-kmaint,ou=adhoc,ou=managedGroups,dc=redhat,dc=com'],
                    'rhatPrimaryMail': [b'mylist@redhat.com'],
                    'sendmailMTAAliasValue': [b'mailing-list@redhat.com']})]

    MOCK_LDAP4 = [('uid=joe.developer,ou=users,dc=redhat,dc=com',
                  {'memberOf':
                   [b'cn=Employee,ou=userClass,dc=redhat,dc=com',
                    b'cn=rhel-sst-kmaint,ou=adhoc,ou=managedGroups,dc=redhat,dc=com'],
                   'rhatPrimaryMail': [b'joe.developer@redhat.com'],
                   'sendmailMTAAliasValue': [b'user1']})]

    ENV_VARS = {'ROVER_URL': 'http://rover.example.com/',
                'ROVER_GROUP': 'some_group',
                'CERTIFICATE_CRT': '/path/to/ca.crt',
                'CERTIFICATE_KEY': '/path/to/ca.key',
                'LDAP_DN_URL': 'ldap://ldap.example.com',
                'LDAP_BASE_DN': 'ou=users,dc=example,dc=com',
                'LDAP_MX_DN': 'ou=users,dc=mx,dc=example,dc=com',
                'LDAP_ATTRS': 'memberOf rhatPrimaryMail rhatPreferredAlias',
                'LDAP_MX_ATTRS': 'sendmailMTAAliasValue'}

    ARGS = mock.Mock(rover_url=ENV_VARS['ROVER_URL'],
                     rover_group=ENV_VARS['ROVER_GROUP'],
                     cert_crt=ENV_VARS['CERTIFICATE_CRT'],
                     cert_key=ENV_VARS['CERTIFICATE_KEY'],
                     ldap_url=ENV_VARS['LDAP_DN_URL'],
                     ldap_base_dn=ENV_VARS['LDAP_BASE_DN'],
                     ldap_mx_nd=ENV_VARS['LDAP_MX_DN'],
                     ldap_attrs=ENV_VARS['LDAP_ATTRS'],
                     ldap_mx_attrs=ENV_VARS['LDAP_MX_ATTRS'])

    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.address_exists_in_ldap')
    def test_get_email_from_alias(self, mock_exists):
        rh_ld = mock.Mock()
        mock_exists.return_value = True
        rover_group = {'user1@redhat.com', 'user2@redhat.com'}
        tests = [
            # (expected result, log text (if any), input email, search value, existence)
            # Bad user
            ('', "BAD LDAP email (shadowman@redhonk.com).", 'shadowman@redhonk.com', None, True),
            # Redirect alias
            ('', "IGNORING: EX-EMPLOYEE: departed@redhat.com",
             'departed@redhat.com', self.MOCK_LDAP2, True),
            # A mailing list
            ('', "IGNORING: List email: mailing-list@redhat.com",
             'mylist@redhat.com', self.MOCK_LDAP3, True),
            # Alias does not exist in LDAP
            ('', "INVALID LDAP email (coolcat@redhat.com) from LDAP alias (departed@redhat.com)",
             'departed@redhat.com', self.MOCK_LDAP, False)
        ]

        logger = 'cki.webhook.utils.bz_redhat_to_rover_sync'
        for expected_result, log_line, email, ldap_search, existence in tests:
            with self.subTest(expected_result=expected_result, log_line=log_line,
                              email=email, ldap_search=ldap_search):
                rh_ld.search_s.return_value = ldap_search
                mock_exists.return_value = existence
                with self.assertLogs(logger, level='INFO') as logs:
                    ret = bz_redhat_to_rover_sync.get_email_from_alias(self.ARGS, rh_ld,
                                                                       rover_group, email)
                    self.assertEqual(expected_result, ret)
                    self.assertIn(log_line, logs.output[-1])

        # User with an alias
        mock_exists.return_value = True
        rh_ld.search_s.return_value = self.MOCK_LDAP
        ret = bz_redhat_to_rover_sync.get_email_from_alias(self.ARGS, rh_ld, rover_group, email)
        exp = 'coolcat@redhat.com'
        self.assertEqual(exp, ret)

        # User with an alias that already exists in Rover
        mock_exists.return_value = True
        rh_ld.search_s.return_value = self.MOCK_LDAP4
        ret = bz_redhat_to_rover_sync.get_email_from_alias(self.ARGS, rh_ld, rover_group, email)
        exp = 'user1@redhat.com'
        self.assertEqual(exp, ret)

    @mock.patch('ldap.sasl.sasl', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.get_email_from_alias')
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.address_exists_in_ldap')
    @mock.patch('ldap.initialize')
    def test_ldap_sanity_check_emails(self, mock_ldap_init, mock_exists, mock_email):
        mock_ldap = mock.Mock()
        mock_ldap.sasl_interactive_bind_s.return_value = True
        mock_ldap.search_s.return_value = self.MOCK_LDAP
        mock_ldap_init.return_value = mock_ldap
        rover_group = {'joedev@redhat.com', 'my_fancy_alias@redhat.com'}
        email_list = ['mailing-list@redhat.com',  # mailing lists should be ignored
                      'cki-kwf-bot@redhat.com',  # bot accounts should be ignored
                      'some-team@redhat.com']  # addresses with a "-" are likely lists, ignore
        tests = [
            # (expected result, input list, existence, base email)
            # Normal email address, exists in ldap
            ({'joedev@redhat.com'}, ['joedev@redhat.com'], True, ''),
            # Normal email address with + extension, which should be discarded
            ({'shadowman@redhat.com'}, ['shadowman+rover@redhat.com'], True, ''),
            # An alias that exists and maps to a base email address in ldap
            ({'joedev@redhat.com'}, ['my_fancy_alias@redhat.com'], False, 'joedev@redhat.com'),
            # Invalid email addresses (mailing list, ex-employee, team list)
            (set(), email_list, False, ''),
        ]

        for expected_result, input_list, existence, base_email in tests:
            with self.subTest(expected_result=expected_result, input_list=input_list,
                              existence=existence, base_email=base_email):
                mock_exists.return_value = existence
                mock_email.return_value = base_email
                ret = bz_redhat_to_rover_sync.ldap_sanity_check_emails(self.ARGS, rover_group,
                                                                       input_list)
                self.assertEqual(expected_result, ret)

    @mock.patch('requests.post')
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.ldap_sanity_check_emails')
    def test_sync_bz_to_rover_group(self, mock_verified, mock_post):
        bz_group = {'shadowman@redhat.com'}
        rover_group = {'shadowman@redhat.com'}
        mock_verified.return_value = {'shadowman@redhat.com'}

        # Nothing to sync
        with self.assertLogs('cki.webhook.utils.bz_redhat_to_rover_sync', level='INFO') as logs:
            bz_redhat_to_rover_sync.sync_bz_to_rover_group(self.ARGS, bz_group, rover_group)
            self.assertIn("No new additions", logs.output[-1])
            mock_post.assert_not_called()

        # We have at least one user to sync
        bz_group = {'user1@redhat.com', 'user2@redhat.com'}
        rover_group = {'user1@redhat.com'}
        mock_verified.return_value = {'user1@redhat.com', 'user2@redhat.com'}
        bz_redhat_to_rover_sync.sync_bz_to_rover_group(self.ARGS, bz_group, rover_group)
        endpoint = self.ARGS.rover_url + self.ARGS.rover_group + '/membersMod'
        cert = (self.ARGS.cert_crt, self.ARGS.cert_key)
        mock_post.assert_called_with(endpoint,
                                     json={'additions': [{'type': 'user', 'id': 'user2'}]},
                                     headers={'Content-Type': 'application/json',
                                              'Accept': 'application/json'},
                                     cert=cert, timeout=120)

    @mock.patch('bugzilla.Bugzilla')
    def test_get_bz_redhat_group(self, mock_bzcon):
        mock_bug = mock.Mock()
        mock_bzcon.return_value = mock_bug
        mock_bz_group = mock.Mock()
        mock_bz_group.membership = [{'email': 'shadowman@redhat.com'}]
        mock_bug.getgroup.return_value = mock_bz_group
        exp = {'shadowman@redhat.com'}
        ret = bz_redhat_to_rover_sync.get_bz_redhat_group(self.ARGS)
        self.assertEqual(exp, ret)

    @mock.patch('requests.get')
    def test_get_rover_redhat_group(self, mock_req_get):
        mock_resp = mock.Mock()
        mock_resp.json.return_value = {'members': [{'id': 'shadowman'}]}
        mock_req_get.return_value = mock_resp
        exp = {'shadowman@redhat.com'}
        ret = bz_redhat_to_rover_sync.get_rover_redhat_group(self.ARGS)
        self.assertEqual(exp, ret)

    def test_get_parser_args(self):
        with mock.patch("sys.argv", ["get_parser_args", "-a", "thisismyapikey"]):
            with mock.patch.dict(os.environ, self.ENV_VARS):
                args = bz_redhat_to_rover_sync.get_parser_args()
                self.assertEqual(args.apikey, 'thisismyapikey')
                self.assertEqual(args.ldap_base_dn, 'ou=users,dc=example,dc=com')
                self.assertEqual(args.ldap_mx_attrs, 'sendmailMTAAliasValue')

    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.sync_bz_to_rover_group',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.get_bz_redhat_group',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.get_rover_redhat_group',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.sentry_init',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.bz_redhat_to_rover_sync.get_parser_args')
    def test_main(self, mock_args):
        with self.assertLogs('cki.webhook.utils.bz_redhat_to_rover_sync', level='INFO') as logs:
            mock_args.return_value = mock.Mock(sentry_ca_certs='/path')
            bz_redhat_to_rover_sync.main()
            self.assertIn("Gathering names from Rover redhat group", logs.output[-4])
            self.assertIn("Gathering names from bugzilla redhat group", logs.output[-3])
            self.assertIn("Sync bugzilla redhat group to Rover redhat group", logs.output[-2])
            self.assertIn("Done.", logs.output[-1])
