"""Tests for the base library."""
from dataclasses import dataclass
from unittest import mock

from tests import fakes
from tests.no_socket_test_case import NoSocketTestCase
from webhook import base_mr
from webhook.base_mr_mixins import CommitsMixin
from webhook.base_mr_mixins import DependsMixin
from webhook.rh_metadata import Projects

MR_ID = 123
NAMESPACE = 'group/project'
MR_URL = f'https://gitlab.com/{NAMESPACE}/-/merge_requests/{MR_ID}'
PROJECT_ID = 7654

CURRENT_USER = {'email': 'bot@example.com',
                'gid': 'gid://gitlab/User/7654321',
                'name': 'Bot User',
                'username': 'bot_user'}
AUTHOR1 = {'email': 'test_user1@example.com',
           'gid': 'gid://gitlab/User/1111',
           'name': 'Test User1',
           'username': 'test_user1'}
MR1_QUERY = {'author': AUTHOR1,
             'approved': False,
             'commitCount': 2,
             'description': 'Bugzilla: https://bugzilla.redhat.com/12345\nDepends: !456',
             'draft': False,
             'files': [{'path': 'include/linux.h'}],
             'headPipeline': {'id': 'gid://gitlab/Ci::Pipeline/12345678'},
             'global_id': 'gid://gitlab/MergeRequest/12121212',
             'labels': {'nodes': [{'title': 'Acks::NeedsReview'},
                                  {'title': 'Bugzilla::NeedsReview'},
                                  {'title': 'CKI::Running'},
                                  {'title': 'CKI_RT::Running'},
                                  {'title': 'CommitRefs::OK'},
                                  {'title': 'Dependencies::OK'},
                                  {'title': 'Signoff::OK'}]},
             'state': 'opened',
             'sourceBranch': 'feature',
             'targetBranch': 'main',
             'title': 'Another test MR'}
AUTHOR2 = {'email': 'test_user2@example.com',
           'gid': 'gid://gitlab/User/2222',
           'name': 'Test User2',
           'username': 'test_user2'}
MR2_QUERY = {'author': AUTHOR2,
             'approved': False,
             'commitCount': 1,
             'description': 'Bugzilla: https://bugzilla.redhat.com/645323',
             'draft': True,
             'files': [{'path': 'include/linux.h'}],
             'headPipeline': {'id': 'gid://gitlab/Ci::Pipeline/7654321'},
             'global_id': 'gid://gitlab/MergeRequest/23232323',
             'labels': {'nodes': [{'title': 'Acks::NeedsReview'},
                                  {'title': 'Bugzilla::OK'},
                                  {'title': 'CKI::OK'},
                                  {'title': 'CKI_RT::Failed::merge'},
                                  {'title': 'CommitRefs::OK'},
                                  {'title': 'Dependencies::OK'},
                                  {'title': 'Signoff::OK'}]},
             'state': 'opened',
             'sourceBranch': 'feature',
             'targetBranch': 'main',
             'title': 'The first test MR'}

MR1_QUERY_RESULT = {'currentUser': CURRENT_USER,
                    'project': {'id': 'gid://gitlab/Project/24152864',
                                'userPermissions': {'pushCode': False},
                                'mr': MR1_QUERY}}
MR2_QUERY_RESULT = {'currentUser': CURRENT_USER,
                    'project': {'id': 'gid://gitlab/Project/24152864',
                                'userPermissions': {'pushCode': False},
                                'mr': MR2_QUERY}}

MR1_DESCRIPTION_QUERY_RESULT = {456: MR2_QUERY['description']}

COMMITS = [{'authorEmail': AUTHOR1['email'],
            'authorName': AUTHOR1['name'],
            'committerEmail': AUTHOR1['email'],
            'committerName': AUTHOR1['name'],
            'author': AUTHOR1,
            'authoredDate': '2022-12-19T08:58:26+01:00',
            'description': 'Bugzilla: https://bugzilla.redhat.com/12345',
            'sha': 'ba94fe49a2345372f6e59e5c7fc39335efed344d',
            'title': 'mr1 commit'},
           {'authorEmail': AUTHOR2['email'],
            'authorName': AUTHOR2['name'],
            'committerEmail': AUTHOR2['email'],
            'committerName': AUTHOR2['name'],
            'author': AUTHOR2,
            'authoredDate': '2022-12-19T08:58:26+01:00',
            'description': 'Bugzilla: https://bugzilla.redhat.com/645323',
            'sha': '57498dee55913567669dd00c46bb07ecc2b711c3',
            'title': 'mr2 commit'}]

MR1_COMMITS_RESULT = {'project': {'mr': {'commits': {'nodes': COMMITS}}}}
MR2_COMMITS_RESULT = {'project': {'mr': {'commits': {'nodes': COMMITS[1:]}}}}


def create_mr(**kwargs):
    """Return a fresh MR object."""
    graphql = kwargs.get('graphql') or mock.Mock()
    if kwargs.get('query_side_effect'):
        graphql.client.query.side_effect = kwargs['query_side_effect']
    else:
        graphql.client.query.return_value = kwargs.get('query_return_value') or {}
    url = kwargs.get('mr_url') or MR_URL
    if not (gl_instance := kwargs.get('gl_instance')):
        gl_instance = fakes.FakeGitLab()
        gl_project = gl_instance.add_project(PROJECT_ID, NAMESPACE)
        gl_project.add_mr(int(url.rsplit('/', 1)[-1]))
    mr_func = kwargs.get('mr_func') or base_mr.BaseMR
    extra_args = kwargs.get('extra_args', [])
    extra_kwargs = kwargs.get('extra_kwargs', {})
    projects = kwargs.get('projects') or Projects()
    return mr_func(graphql, gl_instance, projects, url, *extra_args, **extra_kwargs)


class TestBaseMR(NoSocketTestCase):
    """Tests for the BaseMR dataclass."""

    def test_basemr_empty(self):
        """Returns a useles but sane object."""
        test_mr = create_mr()
        # Basic attributes.
        self.assertEqual(test_mr.url, MR_URL)
        self.assertIs(test_mr.author, None)
        self.assertIs(test_mr.author_can_push, False)
        self.assertEqual(test_mr.commit_count, 0)
        self.assertIs(test_mr.current_user, None)
        self.assertEqual(test_mr.description.text, '')
        self.assertIs(test_mr.draft, False)
        self.assertEqual(test_mr.files, [])
        self.assertEqual(test_mr.labels, [])
        self.assertEqual(test_mr.head_pipeline_id, 0)
        self.assertEqual(test_mr.global_id, '')
        self.assertEqual(test_mr.project_id, 0)
        self.assertIs(test_mr.state, base_mr.defs.MrState.UNKNOWN)
        self.assertEqual(test_mr.target_branch, '')
        self.assertEqual(test_mr.title, '')
        # Derived properties.
        self.assertEqual(test_mr.namespace, NAMESPACE)
        self.assertEqual(test_mr.iid, MR_ID)
        self.assertIs(test_mr.branch, None)
        self.assertIs(test_mr.gl_project, test_mr.gl_instance.projects.get(NAMESPACE))
        self.assertIs(test_mr.gl_mr, test_mr.gl_project.mergerequests.get(MR_ID))
        self.assertIs(test_mr.project, None)

        # Test the __repr__.
        repr_str = str(test_mr)
        self.assertIn(f'{NAMESPACE}!{MR_ID}', repr_str)

    def test_basemr_with_content(self):
        """Returns a working BaseMR object."""
        graphql = mock.Mock()
        graphql.client.query.side_effect = [MR1_QUERY_RESULT]
        graphql.get_mr_descriptions.return_value = MR1_DESCRIPTION_QUERY_RESULT
        mr_labels = [label['title'] for label in MR1_QUERY['labels']['nodes']]
        graphql.get_all_mr_labels.return_value = mr_labels

        mr1 = create_mr(graphql=graphql)

        # mr1 content
        self.assertEqual(mr1.url, MR_URL)
        self.assertEqual(mr1.author.name, AUTHOR1['name'])
        self.assertIs(mr1.author_can_push, False)
        self.assertEqual(mr1.commit_count, 2)
        self.assertIs(mr1.current_user.gid, CURRENT_USER['gid'])
        self.assertEqual(mr1.description.text, MR1_QUERY['description'])
        self.assertIs(mr1.draft, False)
        self.assertEqual(mr1.files, ['include/linux.h'])
        self.assertEqual(mr1.labels, mr_labels)
        self.assertEqual(mr1.global_id, MR1_QUERY['global_id'])
        self.assertEqual(mr1.project_id, 24152864)
        self.assertEqual(mr1.head_pipeline_id, 12345678)
        self.assertIs(mr1.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(mr1.target_branch, 'main')
        self.assertEqual(mr1.title, MR1_QUERY['title'])
        # mr1 derived properties.
        self.assertEqual(mr1.namespace, NAMESPACE)
        self.assertEqual(mr1.iid, MR_ID)
        self.assertIs(mr1.branch, mr1.projects.get_target_branch(24152864, 'main'))
        self.assertIs(mr1.gl_project, mr1.gl_instance.projects.get(NAMESPACE))

    def test_basemr_with_commits_and_depends(self):
        """Returns a working BaseMR object."""
        @dataclass(repr=False)
        class BaseWithCommits(CommitsMixin, DependsMixin, base_mr.BaseMR):
            pass

        graphql = mock.Mock()
        graphql.client.query.side_effect = [MR1_QUERY_RESULT, MR1_COMMITS_RESULT,
                                            MR2_QUERY_RESULT, MR2_COMMITS_RESULT]
        graphql.get_mr_descriptions.return_value = MR1_DESCRIPTION_QUERY_RESULT
        mr1_labels = [label['title'] for label in MR1_QUERY['labels']['nodes']]
        mr2_labels = [label['title'] for label in MR2_QUERY['labels']['nodes']]
        graphql.get_all_mr_labels.side_effect = [mr1_labels, mr2_labels]

        mr1 = create_mr(graphql=graphql, mr_func=BaseWithCommits)
        mr1_commits = mr1.commits
        mr1_dependencies = mr1.depends_mrs
        mr2 = mr1_dependencies[0]
        mr2_commits = mr2.commits

        mr1_diffs = [{'new_path': file['path']} for file in MR1_QUERY['files']]
        mr1.gl_project.repository_compare = mock.Mock(return_value={'diffs': mr1_diffs})

        # mr1 content
        self.assertEqual(mr1.namespace, NAMESPACE)
        self.assertEqual(mr1.iid, MR_ID)
        self.assertEqual(mr1.author.name, AUTHOR1['name'])
        self.assertIs(mr1.current_user.gid, CURRENT_USER['gid'])
        self.assertEqual(mr1.description.text, MR1_QUERY['description'])
        self.assertIs(mr1.draft, False)
        self.assertEqual(mr1.labels, mr1_labels)
        self.assertEqual(mr1.global_id, MR1_QUERY['global_id'])
        self.assertIs(mr1.is_dependency, False)
        self.assertEqual(mr1.project_id, 24152864)
        self.assertIs(mr1.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(mr1.target_branch, 'main')
        self.assertEqual(mr1.title, MR1_QUERY['title'])
        # mr1 derived properties.
        self.assertEqual(len(mr1_commits), 2)
        self.assertEqual(len(mr1.commits_without_depends), 1)
        self.assertEqual(len(mr1.all_commits), 3)
        self.assertEqual(len(mr1.all_descriptions), 3)
        self.assertEqual(mr1.files, ['include/linux.h'])
        self.assertIs(mr1.branch, mr1.projects.get_target_branch(24152864, 'main'))
        self.assertEqual(mr1.first_dep_sha, COMMITS[1]['sha'])
        self.assertIs(mr1.gl_project, mr1.gl_instance.projects.get(NAMESPACE))
        self.assertIs(mr1.has_internal, False)
        self.assertIs(mr1.has_untagged, False)

        # mr2 content
        self.assertEqual(mr2.namespace, NAMESPACE)
        self.assertEqual(mr2.iid, 456)
        self.assertEqual(mr2.author.name, AUTHOR2['name'])
        self.assertIs(mr2.current_user.gid, CURRENT_USER['gid'])
        self.assertEqual(mr2.description.text, MR2_QUERY['description'])
        self.assertIs(mr2.draft, True)
        self.assertEqual(mr2.labels, mr2_labels)
        self.assertEqual(mr2.global_id, MR2_QUERY['global_id'])
        self.assertEqual(mr2.head_pipeline_id, 7654321)
        self.assertIs(mr2.is_dependency, True)
        self.assertEqual(mr2.project_id, 24152864)
        self.assertIs(mr2.state, base_mr.defs.MrState.OPENED)
        self.assertEqual(mr2.target_branch, 'main')
        self.assertEqual(mr2.title, MR2_QUERY['title'])
        # mr2 derived properties.
        self.assertEqual(len(mr2_commits), 1)
        self.assertEqual(len(mr2.commits_without_depends), 1)
        self.assertEqual(len(mr2.all_commits), 2)
        self.assertEqual(len(mr2.all_descriptions), 2)
        self.assertIs(mr2.files, mr2._files)
        self.assertIs(mr2.branch, mr2.projects.get_target_branch(24152864, 'main'))
        self.assertEqual(mr2.first_dep_sha, '')
        self.assertIs(mr2.gl_project, mr1.gl_instance.projects.get(NAMESPACE))
        self.assertIs(mr2.has_internal, False)
        self.assertIs(mr2.has_untagged, False)

    def test_basemr_labels(self):
        """Adds/removes labels and validates the results."""
        test_mr = create_mr()
        to_add = ['label1']
        to_remove = ['label2']
        with mock.patch('webhook.common.add_label_to_merge_request') as mock_add:
            mock_add.return_value = to_add
            test_mr.add_labels(to_add)
            self.assertEqual(test_mr.labels, to_add)

        with mock.patch('webhook.common.add_label_to_merge_request') as mock_add:
            mock_add.return_value = []
            with self.assertRaises(RuntimeError):
                test_mr.add_labels(to_add)

        existing = test_mr.labels.copy()
        test_mr.labels.append(to_remove)
        with mock.patch('webhook.common.remove_labels_from_merge_request') as mock_remove:
            mock_remove.return_value = existing
            test_mr.remove_labels(to_remove)
            self.assertEqual(test_mr.labels, existing)

        test_mr.labels.append(to_remove)
        with mock.patch('webhook.common.remove_labels_from_merge_request') as mock_remove:
            mock_remove.return_value = to_remove
            with self.assertRaises(RuntimeError):
                test_mr.remove_labels(to_remove)
