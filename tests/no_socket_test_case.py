"""TestCase with socket.socket disabled."""
from unittest import TestCase
from unittest import mock


class NoSocketTestCase(TestCase):
    """TestCase with socket.socket disabled."""

    def setUp(self):
        """Set up each test with socket.socket patched."""
        super().setUp()
        patched_socket = mock.patch('socket.socket', mock.Mock())
        patched_socket.start()
        self.addCleanup(patched_socket.stop)
