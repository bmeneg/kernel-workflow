"""Tests for the defs."""
import typing
from unittest import TestCase

from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook.libjira import connect_jira


class TestBZStatus(NoSocketTestCase):
    """Tests for the BZStatus enum."""

    def test_from_str(self):
        """Returns the BZStatus member whose name matches the input string, or UNKNOWN."""
        self.assertEqual(len(defs.BZStatus), 10)
        self.assertIs(defs.BZStatus.from_str('New'), defs.BZStatus.NEW)
        self.assertIs(defs.BZStatus.from_str('POST'), defs.BZStatus.POST)
        self.assertIs(defs.BZStatus.from_str('modified'), defs.BZStatus.MODIFIED)
        self.assertIs(defs.BZStatus.from_str('crazy'), defs.BZStatus.UNKNOWN)


class TestMrScope(NoSocketTestCase):
    """Tests for the MrScope enum."""

    def test_mrscope_label(self):
        """Returns a label string."""
        prefix = 'Bugzilla'
        self.assertEqual(defs.MrScope.INVALID.label(prefix), f'{prefix}::Invalid')
        self.assertEqual(defs.MrScope.NEEDS_REVIEW.label(prefix),
                         f'{prefix}::{defs.NEEDS_REVIEW_SUFFIX}')
        self.assertEqual(defs.MrScope.READY_FOR_QA.label(prefix),
                         f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}')
        self.assertEqual(defs.MrScope.READY_FOR_MERGE.label(prefix),
                         f'{prefix}::{defs.READY_SUFFIX}')
        self.assertEqual(defs.MrScope.CLOSED.label(prefix), f'{prefix}::Closed')
        self.assertEqual(defs.MrScope.FAILED.label(prefix), f'{prefix}::Failed')
        self.assertEqual(defs.MrScope.TESTING_FAILED.label(prefix),
                         f'{prefix}::{defs.TESTING_FAILED_SUFFIX}')
        self.assertEqual(defs.MrScope.WAIVED.label(prefix),
                         f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}')

    def test_mrscope_get(self):
        """Returns the MrScope with the matching name."""
        names = ['invalid', 'closed', 'needs_review', 'ready_for_qa', 'ready_for_merge', 'merged',
                 'ok']
        for name in names:
            self.assertIs(defs.MrScope.get(name), getattr(defs.MrScope, name.upper()))


class TestMrState(NoSocketTestCase):
    """Tests for the MrState enum."""

    def test_mrstate_from_str(self):
        """Returns the MrState member whose name matches the given string."""
        self.assertIs(defs.MrState.from_str('closed'), defs.MrState.CLOSED)
        self.assertIs(defs.MrState.from_str('Locked'), defs.MrState.LOCKED)
        self.assertIs(defs.MrState.from_str('MERGED'), defs.MrState.MERGED)
        self.assertIs(defs.MrState.from_str('opened'), defs.MrState.OPENED)
        self.assertIs(defs.MrState.from_str('chicken'), defs.MrState.UNKNOWN)


class TestDCOState(NoSocketTestCase):
    """Tests for the DCOState enum."""

    def test_dcostate(self):
        """Returns the right footnote and formatted title."""
        for state in defs.DCOState:
            self.assertEqual(state.footnote, defs.DCO_FOOTNOTES[state])
            title = 'OK' if state is defs.DCOState.OK else state.name.replace('_', ' ').capitalize()
            self.assertEqual(state.title, title)


class TestLabel(NoSocketTestCase):
    """Tests for the Label object."""

    def test_label(self):
        """Returns valid values."""
        label_str = defs.Label('Acks::NeedsReview')
        self.assertEqual(label_str.gl_prefix, 'Acks')
        self.assertEqual(label_str.prefix, 'Acks')
        self.assertEqual(label_str.scope, defs.MrScope.NEEDS_REVIEW)
        self.assertEqual(label_str.scoped, 1)
        self.assertEqual(label_str.primary, 'NeedsReview')
        self.assertEqual(label_str.secondary, None)

        label_str = defs.Label('ExternalCI::lnst::OK')
        self.assertEqual(label_str.gl_prefix, 'ExternalCI::lnst')
        self.assertEqual(label_str.prefix, 'ExternalCI')
        self.assertEqual(label_str.scope, defs.MrScope.OK)
        self.assertEqual(label_str.scoped, 2)
        self.assertEqual(label_str.primary, 'lnst')
        self.assertEqual(label_str.secondary, 'OK')

        label_str = defs.Label('readyForMerge')
        self.assertEqual(label_str.gl_prefix, 'readyForMerge')
        self.assertEqual(label_str.prefix, 'readyForMerge')
        self.assertEqual(label_str.scope, None)
        self.assertEqual(label_str.scoped, 0)
        self.assertEqual(label_str.primary, None)
        self.assertEqual(label_str.secondary, None)

        label_str = defs.Label('Subsystem:networking')
        self.assertEqual(label_str.gl_prefix, 'Subsystem:networking')
        self.assertEqual(label_str.prefix, 'Subsystem:networking')
        self.assertEqual(label_str.scope, None)
        self.assertEqual(label_str.scoped, 0)
        self.assertEqual(label_str.primary, None)
        self.assertEqual(label_str.secondary, None)

        # Don't even try more than two :: pairs.
        with self.assertRaises(ValueError):
            defs.Label('readyForMerge::really::now::go')


class TestFixVersions(NoSocketTestCase):
    """Tests for the FixVersions."""

    def test_valid_fix_versions(self):
        """Returns an object with the expected attributes."""
        class FVTest(typing.NamedTuple):
            input: str
            product: str
            major: int
            minor: int
            cycle: str
            zstream: bool

        tests = [
            FVTest('rhel-8.9.0', 'rhel', 8, 9, 'release', False),
            FVTest('rhel-8.9.0.z', 'rhel', 8, 9, 'release', True),
            FVTest('rhel-11.2', 'rhel', 11, 2, 'release', False),
            FVTest('rhel-11.0.public.beta', 'rhel', 11, 0, 'beta', False),
            FVTest('rhel-12.0.public.beta.z', 'rhel', 12, 0, 'beta', False),
            # rhel-6-els is a special case, minor is set to 10, considered z-stream
            FVTest('rhel-6-els', 'rhel', 6, 10, 'release', True),
            FVTest('rhel-7.9', 'rhel', 7, 9, 'release', False),
            FVTest('rhel-7.9.z', 'rhel', 7, 9, 'release', True),
            FVTest('CentOS Stream 9', 'centos', 9, 0, 'release', False),
            FVTest('CentOS Stream 10', 'centos', 10, 0, 'release', False),
            # These two are 'archived' on the RHEL project but we still have old metadata that
            # includes them.
            FVTest('rhel-8.0-alpha', 'rhel', 8, 0, 'alpha', False),
            FVTest('rhel-8.0-beta', 'rhel', 8, 0, 'beta', False),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fix_version = defs.FixVersion(test.input)
                for attr in ('product', 'major', 'minor', 'cycle', 'zstream'):
                    self.assertEqual(getattr(test, attr), getattr(fix_version, attr))

    def test_invalid_fix_versions(self):
        """Raises a ValueError on unexpected input."""
        tests = [
            'rhel-10',
            'rhel-8',
            'linux-1.2.3.z',
            'CentOS Stream 9.1',
            'rhel-6.els'  # this is no longer a valid fix version 🤷
        ]

        for bad_fix_version_str in tests:
            with self.subTest(bad_fix_version_str=bad_fix_version_str):
                with self.assertRaises(ValueError):
                    defs.FixVersion(bad_fix_version_str)


# This tests needs IRL network connectivity.
class TestFixVersionsLive(TestCase):
    """Tests for the FixVersions."""

    def test_rhel_project_versions(self):
        """Checks that all current RHEL project 'versions' can be represented as a FixVersion."""
        jira = connect_jira()
        rhel_project = jira.project('RHEL')
        versions = [version for version in rhel_project.versions if
                    version.released and not version.archived]
        print(f'Found {len(versions)} released versions on the RHEL project.')
        self.assertGreater(len(versions), 0)
        for version in versions:
            with self.subTest(version=version):
                if version.name.startswith('test'):
                    continue
                try:
                    print(f"Testing FixVersion value '{version} ({version.id})'")
                    defs.FixVersion(version.name)
                except ValueError:
                    print(f'Version name from {repr(version)} is not parsable by defs.FixVersion')
                    raise


class TestGitlabURL(NoSocketTestCase):
    """Tests for the GitlabURL class."""

    def test_gitlab_url_good(self):
        """Returns a GitlabURL object with the expected attributes."""
        class URLTest(typing.NamedTuple):
            """Input string and expected GitlabURL attributes."""
            input_str: str
            namespace: str = ''
            type: str = ''
            id: int = 0
            view: str = ''
            reference: typing.Union[str, None] = None

        tests = [
            URLTest(
                input_str='https://gitlab.com/group/project/-/merge_requests/123',
                namespace='group/project',
                type='merge_requests',
                id=123,
                view='',
                reference='group/project!123'
            ),
            URLTest(
                input_str='https://gitlab.com/bip/boop/-/issues/555',
                namespace='bip/boop',
                type='issues',
                id=555,
                view='',
                reference='bip/boop#555'
            ),
            URLTest(
                input_str='https://gitlab.com/bip/boop/-/pipelines/6254364',
                namespace='bip/boop',
                type='pipelines',
                id=6254364,
                view='',
                reference=None
            ),
        ]

        attributes = ('namespace', 'type', 'id', 'view', 'reference')

        for test in tests:
            with self.subTest(**test._asdict()):
                test_url = defs.GitlabURL(test.input_str)
                self.assertEqual(test_url, test.input_str)
                for attrib in attributes:
                    self.assertEqual(getattr(test_url, attrib), getattr(test, attrib))

    def test_gitlab_url_bad(self):
        """Raises a ValueError when a URL is not recognized."""
        test_inputs = (
            'https://google.com',
            'group/project!999'
        )

        for test_str in test_inputs:
            with self.assertRaises(ValueError):
                defs.GitlabURL(test_str)
