"""Tests for the owners_validator."""
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook.session import BaseSession
from webhook.utils import owners_validator


class TestGLNamespace(NoSocketTestCase):
    """Tests for the GLNamespace dataclass."""

    def test_new(self):
        """Creates a new GLNamespace with the given parameters."""
        ns_properties = {'name': 'group/subgroup',
                         'type': owners_validator.NS_TYPE.group,
                         'members': set(),
                         'missing': set()}
        new_ns = owners_validator.GLNamespace(**ns_properties)
        for key, value in ns_properties.items():
            self.assertEqual(getattr(new_ns, key), value)
        self.assertEqual(new_ns.markdown, f'group @{ns_properties["name"]}')
        self.assertIn("GLNamespace group: 'group/subgroup'", str(new_ns))


class TestHelpers(NoSocketTestCase):
    """Tests for the various helper functions."""

    REDHAT = owners_validator.GLNamespace('redhat',
                                          owners_validator.NS_TYPE.group,
                                          {'user1', 'user2', 'user3', 'user4', 'user5'},
                                          {'user6', 'user7', 'uesr9'})
    ARK = owners_validator.GLNamespace('cki-project/kernel-ark',
                                       owners_validator.NS_TYPE.project,
                                       {'user1', 'user2', 'user3', 'user7'},
                                       {'user4', 'user6', 'uesr9'})
    NS_MEMBERS = {'redhat': REDHAT, 'cki-project/kernel-ark': ARK}
    OWNERS = {'user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7', 'user8', 'user9'}

    def test_build_namespace_dict(self):
        """Queries for namespace mambers, calculates missing, and creates a GLNamespace for each."""
        mock_graphql = mock.Mock(get_all_members=mock.Mock())

        owners_users = {'userA', 'userB', 'userC', 'userD', 'userE'}
        groups = {'group1': ['userA', 'userB', 'userC', 'userD', 'userE'],
                  'group2': ['userC', 'userF']}
        projects = {'project1': ['userB', 'userC']}

        side_effects = []
        for users in groups.values():
            side_effects.append(dict.fromkeys(users))
        for users in projects.values():
            side_effects.append(dict.fromkeys(users))
        mock_graphql.get_all_members.side_effect = side_effects
        result = owners_validator.build_namespace_dict(mock_graphql, list(groups.keys()),
                                                       list(projects.keys()), owners_users)
        self.assertEqual(len(result), 3)
        for ns in result.values():
            self.assertCountEqual(ns.members, (groups | projects)[ns.name])
            self.assertEqual(ns.missing, owners_users.difference(ns.members))

    def test_build_namespace_dict_exception(self):
        """Raises an exception if the get_all_members call is empty."""
        mock_graphql = mock.Mock()
        mock_graphql.get_all_members.return_value = {}
        with self.assertRaises(ValueError):
            owners_validator.build_namespace_dict(mock_graphql, ['group1'], [], set())

    @mock.patch('webhook.utils.owners_validator.get_owners_parser')
    def test_usernames_in_owners(self, mock_get_owners):
        """Returns the set of all usernames in the owners.yaml."""
        users = [{'gluser': f'user{num}'} for num in range(0, 11)]
        subsystem1 = mock.Mock(maintainers=users[:3], reviewers=users[7:])
        subsystem2 = mock.Mock(maintainers=users[2:4], reviewers=users[8:])
        expected = {'user0', 'user1', 'user2', 'user3', 'user7', 'user8', 'user9', 'user10'}
        mock_owners = mock.Mock(subsystems=[subsystem1, subsystem2])
        mock_get_owners.return_value = mock_owners
        results = owners_validator.usernames_in_owners('owners_yaml_path')
        self.assertEqual(results, expected)

        # Blow up if there are no usernames because that would be weird.
        mock_owners.subsystems = []
        with self.assertRaises(ValueError):
            results = owners_validator.usernames_in_owners('owners_yaml_path')

    @mock.patch('webhook.utils.owners_validator.METRIC_KWF_ARK_MISSING_FROM_REDHAT')
    def test_check_ark_members(self, mock_metric):
        """Logs about ark members who are not RH members if the data is available."""
        # Needed data not available, nothing to do.
        with self.assertLogs('cki.webhook.owners_validator', level='DEBUG') as logs:
            owners_validator.check_ark_members({}, True)
            self.assertIn('data needed is not present', logs.output[-1])
            mock_metric.set.assert_not_called()

        redhat = owners_validator.GLNamespace('redhat',
                                              owners_validator.NS_TYPE.group,
                                              {'user1', 'user2', 'user3', 'user4', 'user5'}, set())
        ark = owners_validator.GLNamespace('cki-project/kernel-ark',
                                           owners_validator.NS_TYPE.project,
                                           {'user1', 'user2', 'user3', 'user7'}, set())

        # Finds some non-RH members and triggers the metric.
        mock_metric.reset_mock()
        ns_members = {'redhat': redhat, 'cki-project/kernel-ark': ark}
        with self.assertLogs('cki.webhook.owners_validator', level='DEBUG') as logs:
            owners_validator.check_ark_members(ns_members, True)
            self.assertIn("not 'redhat' group members: {'user7'}", logs.output[-1])
            mock_metric.set.assert_called_once_with(1)

        # All ark members in 'redhat', no metric.
        mock_metric.reset_mock()
        ark.members.remove('user7')
        with self.assertLogs('cki.webhook.owners_validator', level='DEBUG') as logs:
            owners_validator.check_ark_members(ns_members, False)
            self.assertIn("All kernel-ark project members are 'redhat' group members.",
                          logs.output[-1])
            mock_metric.set.assert_not_called()

    def test_create_issues_no_labels(self):
        """Fetches existing issues and creates any needed new issues."""
        mock_graphql = mock.Mock()
        docs_ns = 'group/project'
        existing_issues = [{'iid': 4, 'title': owners_validator.ISSUE_TITLE + 'user4',
                            'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/4'},
                           {'iid': 7, 'title': owners_validator.ISSUE_TITLE + 'user7',
                            'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/7'}]
        mock_graphql.get_all_issues.return_value = existing_issues
        mock_graphql.get_user.side_effect = [True, False]
        mock_graphql.create_project_issue.return_value = \
            {'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/0'}

        # Creates an issue without any labels.
        owners_validator.create_issues(mock_graphql, docs_ns, self.NS_MEMBERS)
        self.assertEqual(2, mock_graphql.create_project_issue.call_count)
        for call in mock_graphql.create_project_issue.mock_calls:
            self.assertIs(call.kwargs['extra_input'], None)

    def test_create_issues_with_footer(self):
        """Fetches existing issues and creates any needed new issues."""
        args = owners_validator.get_parser().parse_args('--owners-yaml owners.yaml'.split())
        test_session = BaseSession('owners_validator', args)
        test_session.graphql = mock.Mock()

        footer = test_session.comment.gitlab_footer()

        docs_ns = 'group/project'
        existing_issues = [{'iid': 4, 'title': owners_validator.ISSUE_TITLE + 'user4',
                            'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/4'},
                           {'iid': 7, 'title': owners_validator.ISSUE_TITLE + 'user7',
                            'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/7'}]
        test_session.graphql.get_all_issues.return_value = existing_issues
        test_session.graphql.get_user.side_effect = [True, False]
        test_session.graphql.create_project_issue.return_value = \
            {'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/0'}

        # Creates an issue without any labels.
        owners_validator.create_issues(test_session.graphql, docs_ns, self.NS_MEMBERS,
                                       footer=footer)
        self.assertEqual(2, test_session.graphql.create_project_issue.call_count)
        for call in test_session.graphql.create_project_issue.mock_calls:
            self.assertIs(call.kwargs['extra_input'], None)
            self.assertIn(footer, call.args[2])

    def test_create_issues_with_labels(self):
        """Fetches existing issues and creates any needed new issues."""
        mock_graphql = mock.Mock()
        docs_ns = 'group/project'
        existing_issues = [{'iid': 4, 'title': owners_validator.ISSUE_TITLE + 'user4',
                            'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/4'},
                           {'iid': 7, 'title': owners_validator.ISSUE_TITLE + 'user7',
                            'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/7'}]
        mock_graphql.get_all_issues.return_value = existing_issues
        mock_graphql.get_user.side_effect = [True, False]
        mock_graphql.create_project_issue.return_value = \
            {'webUrl': f'https://gitlab.com/{docs_ns}/-/issues/0'}

        # Creates an issue with some labels.
        owners_validator.create_issues(mock_graphql, docs_ns, self.NS_MEMBERS, labels=['aLabel'])
        self.assertEqual(2, mock_graphql.create_project_issue.call_count)
        for call in mock_graphql.create_project_issue.mock_calls:
            self.assertEqual(call.kwargs['extra_input'], {'labels': ['aLabel']})

    @mock.patch('webhook.utils.owners_validator.usernames_in_owners')
    def test_main_no_groups(self, mock_usernames_in_owners):
        """Exits with the number of valid usernames."""
        args = owners_validator.get_parser().parse_args('--owners-yaml owners.yaml'.split())
        test_session = BaseSession('owners_validator', args)
        test_session.graphql = mock.Mock()

        # All users known, exit code is 0.
        test_session.graphql.return_value.get_user.side_effect = \
            [{'user1': {}}, {'user2': {}}, {'user3': {}}]
        mock_usernames_in_owners.return_value = {'user1', 'user2', 'user3'}
        with self.assertRaises(SystemExit) as sys_exit:
            owners_validator.main(test_session)
            self.assertEqual(0, sys_exit.exception.code)

        # Missing two users, exit code is 2.
        test_session.graphql.return_value.get_user.side_effect = [{'user1': {}}, None, None]
        with self.assertRaises(SystemExit) as sys_exit:
            owners_validator.main(test_session)
            self.assertEqual(2, sys_exit.exception.code)

    @mock.patch('webhook.utils.owners_validator.create_issues')
    @mock.patch('webhook.utils.owners_validator.METRIC_KWF_MISSING_USERS')
    @mock.patch('webhook.utils.owners_validator.build_namespace_dict')
    @mock.patch('webhook.utils.owners_validator.usernames_in_owners')
    def test_main_with_groups(self, mock_usernames_in_owners,
                              mock_build_namespace_dict, mock_metric, mock_create_issues):
        """Does the whole thing."""
        args_str = ('--groups redhat --projects cki-project/kernel-ark --owners-yaml owners.yaml'
                    ' --create-issues --metrics')
        args = owners_validator.get_parser().parse_args(args_str.split())
        test_session = BaseSession('owners_validator', args)
        test_session.graphql = mock.Mock()

        mock_args = mock.Mock(groups=['redhat'], projects=['cki-project/kernel-ark'],
                              owners_yaml='owners.yaml', create_issues=True, metrics=True)
        mock_usernames_in_owners.return_value = self.OWNERS
        mock_build_namespace_dict.return_value = self.NS_MEMBERS
        test_session.graphql.return_value.create_project_issue.return_value = \
            {'webUrl': f'https://gitlab.com/{mock_args.docs_project}/-/issues/0'}

        owners_validator.main(mock_args)
        mock_metric.labels('redhat').set.assert_called_with(len(self.REDHAT.missing))
        mock_metric.labels('cki-project/kernel-ark').set.assert_called_with(len(self.ARK.missing))
        mock_create_issues.assert_called()
