"""Tests for the cache module."""
import typing

from tests.no_socket_test_case import NoSocketTestCase
from webhook import cache


class TestBaseCache(NoSocketTestCase):
    """Tests for the BaseCache class."""

    def test_base_cache_init(self) -> None:
        """Assigns session and data attributes."""
        class CacheInitTest(typing.NamedTuple):
            data: typing.Dict
            size: int
            populate: bool = False
            raises: bool = False

        tests = [
            # No input data, self.data is an empty dict.
            CacheInitTest(
                data=None,
                size=0
            ),
            # self.data is the input dict.
            CacheInitTest(
                data={'a': 1, 'b': 2, 'c': 3},
                size=3
            ),
            # Call populate, no data.
            CacheInitTest(
                data=None,
                size=0,
                populate=True
            ),
            # Raises a ValueError on bad input data type.
            CacheInitTest(
                data=[1, 2, 3],
                size=0,
                raises=True
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                if test.raises:
                    with self.assertRaises(ValueError):
                        cache.BaseCache(data=test.data, populate=test.populate)
                    continue
                test_cache = cache.BaseCache(data=test.data, populate=test.populate)
                if test.data is None:
                    self.assertEqual(test_cache.data, {})
                else:
                    self.assertEqual(test_cache.data, test.data)
                self.assertIn(f'items: {test.size}>', repr(test_cache))
