"""Tests for the Labels module."""
from importlib import resources
import json
import re
import typing
from unittest import mock

import responses

from tests.no_socket_test_case import NoSocketTestCase
from webhook import labels
from webhook.session import BaseSession

if typing.TYPE_CHECKING:
    import pathlib

    from webhook.rh_metadata import Project as RH_Project

LABELS_YAML_ASSET = resources.files('tests.assets').joinpath('labels.yaml')
API_URL = 'https://gitlab.com/api/v4'


class TestYamlLabel(NoSocketTestCase):
    """Tests for the YamlLabel dataclass."""

    def test_yamllabel_new(self) -> None:
        """Replaces yaml 'regex' True value with re.Pattern object from yaml 'name'."""
        class TestYamlLabelNew(typing.NamedTuple):
            name: str
            color: str
            description: typing.Optional[str] = None
            devaction: typing.Optional[str] = None
            regex: typing.Optional[bool] = None
            regex_field_id: typing.Optional[int] = None

        tests = [
            # Basic label, no regex.
            TestYamlLabelNew(
                name='hello',
                color='#123456',
                description='a happy label',
            ),
            # Regex label, regex attr is now a re.Pattern.
            TestYamlLabelNew(
                name='[Hh]ello',
                color='#123456',
                description='a happy label',
                regex=True,

            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                yaml_dict = {k: v for k, v in test._asdict().items() if v is not None}
                test_label = labels.YamlLabel.new(yaml_dict)

                for attr_name, attr_value in yaml_dict.items():
                    if attr_name == 'regex' and attr_value is True:
                        self.assertIsInstance(test_label.regex, re.Pattern)
                        self.assertEqual(test_label.regex.pattern, test.name)
                        continue
                    self.assertEqual(getattr(test_label, attr_name), attr_value)

    def test_yamllabel_match(self) -> None:
        """Returns self on a non-regex match, or a copy of self on a regex match."""
        class TestYamlLabelMatch(typing.NamedTuple):
            expected_to_match: bool
            match_str: str
            yl_name: str
            yl_description: str
            yl_regex: bool = False
            match_description: typing.Optional[str] = None

        tests = [
            # Basic label, does not match.
            TestYamlLabelMatch(
                expected_to_match=False,
                match_str='high_prio',
                yl_name='low_prio',
                yl_description='a low prio label',
            ),
            # Basic label, matches.
            TestYamlLabelMatch(
                expected_to_match=True,
                match_str='low_prio',
                yl_name='low_prio',
                yl_description='a low prio label',
            ),
            # Regex label, no match.
            TestYamlLabelMatch(
                expected_to_match=False,
                match_str='Acks::OK',
                yl_name='^Subsystem:(\\w+)$',
                yl_description='Some subsystem label',
                yl_regex=True,
            ),
            # Regex label, matches.
            TestYamlLabelMatch(
                expected_to_match=True,
                match_str='Subsystem:zipdisk',
                yl_name='^Subsystem:(\\w[\\w\\-]*)$',
                yl_description='An MR that affects code related to %s.',
                yl_regex=True,
                match_description='An MR that affects code related to zipdisk.',
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_label = labels.YamlLabel(
                    name=test.yl_name,
                    color='#FFFFFF',
                    description=test.yl_description,
                    regex=re.compile(test.yl_name) if test.yl_regex else None
                )
                result = test_label.match(test.match_str)
                if test.expected_to_match:
                    self.assertIsInstance(result, labels.YamlLabel)
                    if test.yl_regex:
                        self.assertIsNot(test_label, result)
                        self.assertEqual(test.match_str, result.name)
                        self.assertEqual(test.match_description, result.description)
                    else:
                        self.assertIs(test_label, result)
                else:
                    self.assertIsNone(result)


class TestHelpers(NoSocketTestCase):
    """Tests for non-class functions."""

    def test_get_labels_yaml_data(self) -> None:
        """Returns a set of YamlLabel objects loaded from the given yaml path."""
        labels_data = labels.get_labels_yaml_data(LABELS_YAML_ASSET)
        self.assertIsInstance(labels_data, set)
        self.assertEqual(len(labels_data), 97)
        self.assertTrue(all(isinstance(label, labels.YamlLabel) for label in labels_data))

    def test_get_labels_yaml_data_asserts(self) -> None:
        """Raises RuntimeError when no label data is found."""
        with mock.patch('webhook.labels.load_yaml_data', mock.Mock(return_value={})):
            with self.assertRaises(RuntimeError):
                labels.get_labels_yaml_data('empty.yaml')


def make_gl_label(
    name: str,
    color: str,
    description: str,
    spec_addons: typing.List[str] = None
) -> mock.Mock:
    """Return a mock python gitlab label."""
    spec_set = ['name', 'color', 'description', 'save', 'delete', 'id']
    if spec_addons:
        spec_set.extend(spec_addons)
    mock_label = mock.Mock(spec_set=spec_set, name=name, color=color, description=description)
    return mock_label


GL_RESET_ASSETS = 'tests.assets.gitlab_rest_api'


def get_asset_json(asset_dir: str, filename: str) -> 'pathlib.PosixPath':
    resource_file = resources.files(asset_dir).joinpath(filename)
    return json.loads(resource_file.read_text())


USER_RESPONSE = {
    'method': 'GET',
    'url': f'{API_URL}/user',
    'json': {
        'id': 5991344,
        'username': 'user',
        'name': 'User Name',
        'state': 'active',
    }
}


@mock.patch('webhook.defs.LABELS_YAML_PATH', LABELS_YAML_ASSET)
class TestLabelsCache(NoSocketTestCase):
    """Tests for the LabelsCache object."""

    def test_labels_cache_init(self) -> None:
        """Returns a new LabelsCache object with the labels_yaml class attribute set."""
        self.assertIs(labels.LabelsCache._data_type, list)
        mock_session = mock.Mock()
        test_label_cache = labels.LabelsCache(mock_session)
        self.assertIs(test_label_cache.session, mock_session)
        self.assertEqual(test_label_cache._size(), 0)
        self.assertEqual(len(test_label_cache.labels_yaml), 97)

    def test_populate(self) -> None:
        """Returns a dict of sets of GL Label objects."""
        class TestPopulate(typing.NamedTuple):
            namespaces: typing.List[str]
            expected_count: int
            group_ids: typing.List[int] = []
            project_ids: typing.List[int] = []
            check_labels: bool = False
            bad_colors: typing.List = []
            bad_descriptions: typing.List = []

        tests = [
            # Just get all the labels (100) for the given group (10873929).
            TestPopulate(
                namespaces=['redhat/centos-stream/src/kernel/centos-stream-9'],
                group_ids=[10873929],
                expected_count=100
            ),
            # Get all the expected labels (93) for the given group (10873929).
            TestPopulate(
                namespaces=['redhat/centos-stream/src/kernel/centos-stream-9'],
                group_ids=[10873929],
                expected_count=93,
                check_labels=True,
                bad_colors=['Acks::x86fpu::OK'],
                bad_descriptions=['Acks::net::NeedsReview'],
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                responses_list = [USER_RESPONSE]
                for group_id in test.group_ids:
                    # groups.get() response.
                    group_get_response = {
                        'method': 'GET',
                        'url': f'{API_URL}/groups/{group_id}',
                        'json': get_asset_json(GL_RESET_ASSETS, f'groups_get_{group_id}.json')
                    }
                    # labels.list() response.
                    label_list_response = {
                        'method': 'GET',
                        'url': f'{API_URL}/groups/{group_id}/labels?per_page=100',
                        'json': get_asset_json(GL_RESET_ASSETS, f'groups_{group_id}_labels.json')
                    }
                    responses_list.extend([group_get_response, label_list_response])

                with responses.RequestsMock() as mock_responses:
                    for response_dict in responses_list:
                        mock_responses.add(**response_dict)
                    test_session = BaseSession('ack_nack', '')

                    with self.assertLogs('cki.webhook.labels') as logs:
                        test_label_cache = labels.LabelsCache(
                            test_session,
                            populate=True,
                            namespaces=test.namespaces,
                            check_labels=test.check_labels,
                        )

                    log_str = '\n'.join(logs.output)
                    for bad_color_label in test.bad_colors:
                        self.assertIn(f"'{bad_color_label}' color does not match", log_str)
                    for bad_desc_label in test.bad_descriptions:
                        self.assertIn(f"'{bad_desc_label}' description does not match", log_str)
                    if not test.check_labels:
                        self.assertFalse(
                            [log.message for log in logs.records if log.levelname == 'WARNING']
                        )
                    self.assertEqual(len(test_label_cache.data), test.expected_count)

    def test_get_label_from_yaml(self) -> None:
        """Returns the YamlLabel object if it exists, otherwise None."""
        test_session = BaseSession('ckihook', '')
        test_label_cache = labels.LabelsCache(test_session)
        # These labels exist in the yaml.
        good_labels = ('Acks::NACKed', 'CKI_RT::Failed::kernel-results', 'readyForMerge')
        for label in good_labels:
            self.assertIsInstance(test_label_cache.get_label_from_yaml(label), labels.YamlLabel)

        # These labels are not in the yaml.
        bad_labels = ('Funy label', 'CKI_Automotive::boop::beep', 'readyForAction')
        for label in bad_labels:
            self.assertIsNone(test_label_cache.get_label_from_yaml(label))

    def test_check_gl_label(self) -> None:
        """Returns the GL Label if it exists in the yaml, or returns None."""
        class CheckGLLabelTest(typing.NamedTuple):
            gl_name: str
            yaml_label: bool
            prune: bool = False
            is_prod_or_stage: bool = False

        tests = [
            # Label matches.
            CheckGLLabelTest(
                gl_name='alabel',
                yaml_label=True
            ),
            # No matching label, no prune; just returns None.
            CheckGLLabelTest(
                gl_name='alabel',
                yaml_label=False
            ),
            # No matching label, with prune in dev environment, just returns None.
            CheckGLLabelTest(
                gl_name='alabel',
                yaml_label=False,
                prune=True,
            ),
            # No matching label, with prune in live environment, deletes label.
            CheckGLLabelTest(
                gl_name='alabel',
                yaml_label=False,
                prune=True,
                is_prod_or_stage=True
            )
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = BaseSession('ckihook', '')
                test_label_cache = labels.LabelsCache(test_session)
                test_label_cache.get_label_from_yaml = mock.Mock(return_value=test.yaml_label)
                test_label_cache.update_label = mock.Mock()
                mock_gl_label = make_gl_label(test.gl_name, '#FFFFFF', 'a label')
                mock_is_prod = mock.Mock(return_value=test.is_prod_or_stage)
                with mock.patch('webhook.labels.is_production_or_staging', mock_is_prod):
                    result = test_label_cache.check_gl_label(mock_gl_label, prune=test.prune)
                    if test.yaml_label:
                        test_label_cache.update_label.assert_called_once()
                        self.assertEqual(result, test_label_cache.update_label.return_value)
                        mock_gl_label.delete.assert_not_called()
                    else:
                        if test.prune and test.is_prod_or_stage:
                            mock_gl_label.delete.assert_called_once()
                        else:
                            mock_gl_label.delete.assert_not_called()
                        self.assertIsNone(result)

    def test_rh_project_labels(self) -> None:
        """Return the cache labels matching the rh project."""
        rh_project_spec = ['id', 'group_id', 'group_labels']

        # A project not using group labels.
        mock_rh_project1 = mock.Mock(
            spec_set=rh_project_spec, group_labels=False, group_id='444', id=222
        )
        rh_project1_labels = []
        for label_name in ['Acks::OK', 'readyForMerge', 'CKI_RT::Waived', 'Signoff::OK']:
            mock_label = make_gl_label(
                name=label_name, color='#123456', description='hey', spec_addons=['project_id']
            )
            # The project_id set by python-gitlab is a string.
            mock_label.project_id = str(mock_rh_project1.id)
            rh_project1_labels.append(mock_label)

        # A project using group labels.
        mock_rh_project2 = mock.Mock(
            spec_set=rh_project_spec, group_labels=True, group_id='333', id=777
        )
        rh_project2_labels = []
        for label_name in ['Acks::NeedsReview', 'readyForQA', 'CKI_RT::OK', 'Subsystem:video']:
            mock_label = make_gl_label(
                name=label_name, color='#123456', description='hey', spec_addons=['group_id']
            )
            # The group_id set by python-gitlab is a string.
            mock_label.group_id = str(mock_rh_project2.group_id)
            rh_project2_labels.append(mock_label)

        # A cache with labels from both lists.
        test_session = BaseSession('ckihook', '')
        test_label_cache = \
            labels.LabelsCache(test_session, data=rh_project1_labels + rh_project2_labels)

        # rh_project_labels() returns the expected list of labels from the cache.
        rh_project1_result = test_label_cache.rh_project_labels(mock_rh_project1)
        rh_project2_result = test_label_cache.rh_project_labels(mock_rh_project2)
        self.assertCountEqual(rh_project1_result, rh_project1_labels)
        self.assertCountEqual(rh_project2_result, rh_project2_labels)


class TestGetLabel(NoSocketTestCase):
    """Tests for the LabelsCache.get_label method."""

    @responses.activate
    @mock.patch('webhook.defs.LABELS_YAML_PATH', LABELS_YAML_ASSET)
    def setup_cache(
        self,
        namespaces: typing.Optional[typing.List] = None,
    ) -> typing.Tuple[labels.LabelsCache, typing.List['RH_Project']]:
        """Set up a populated LabelsCache."""
        namespaces = namespaces or ['redhat/centos-stream/src/kernel/centos-stream-9']
        responses_list = [USER_RESPONSE]
        # groups.get() response.
        group_get_response = {
            'method': 'GET',
            'url': f'{API_URL}/groups/10873929',
            'json': get_asset_json(GL_RESET_ASSETS, 'groups_get_10873929.json')
        }
        # labels.list() response.
        label_list_response = {
            'method': 'GET',
            'url': f'{API_URL}/groups/10873929/labels?per_page=100',
            'json': get_asset_json(GL_RESET_ASSETS, 'groups_10873929_labels.json')
        }
        responses_list.extend([group_get_response, label_list_response])
        for response_dict in responses_list:
            responses.add(**response_dict)

        session = BaseSession('ckihook', '')
        label_cache = labels.LabelsCache(session, populate=True, namespaces=namespaces)
        projects = [session.rh_projects.get_project_by_namespace(ns) for ns in namespaces]
        return (label_cache, projects)

    def test_get_label(self) -> None:
        """Returns the matching label from the cache."""
        class TestParams(typing.NamedTuple):
            label_name: str
            gets_label: bool
            create_missing: bool = False
            calls_create_label: bool = False

        tests = [
            # Returns the matching label.
            TestParams(
                label_name='Acks::OK',
                gets_label=True
            ),
            TestParams(
                label_name='CKI::Failed',
                gets_label=True
            ),
            TestParams(
                label_name='Acks::kexec_kdump::OK',
                gets_label=True
            ),
            # No match, returns None.
            TestParams(
                label_name='readyForFun',
                gets_label=False
            ),
            TestParams(
                label_name='Subsystem:arugula',
                gets_label=False
            ),
            # No match, but asked to create the label.
            TestParams(
                label_name='readyForFun',
                gets_label=True,
                create_missing=True,
                calls_create_label=True
            ),
            TestParams(
                label_name='Subsystem:arugula',
                gets_label=True,
                create_missing=True,
                calls_create_label=True
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_label_cache, rh_projects = self.setup_cache()
                test_label_cache.create_label = mock.Mock()
                test_label_cache.create_label.return_value.name = test.label_name
                label = test_label_cache.get_label(
                    rh_projects[0],
                    test.label_name,
                    create_missing=test.create_missing
                )
                if test.gets_label:
                    self.assertEqual(label.name, test.label_name)
                else:
                    self.assertIsNone(label)
                if test.calls_create_label:
                    test_label_cache.create_label.assert_called_once()
                else:
                    test_label_cache.create_label.assert_not_called()

    def test_create_label(self) -> None:
        """Creates the requested label."""
        class TestParams(typing.NamedTuple):
            label_name: str
            in_yaml: bool = True
            in_cache: bool = False
            is_prod_or_stage: bool = True
            gl_err_code: typing.Optional[int] = None

        tests = [
            # Not a valid label name per yaml, raises ValueError.
            TestParams(
                label_name='not a valid label name',
                in_yaml=False
            ),
            # Valid yaml label name but already in the cache, nothing to create.
            TestParams(
                label_name='Bugzilla::NeedsTesting',
                in_cache=True
            ),
            # Valid yaml, not in cache, but not prod_or_stage so returns a fake label.
            TestParams(
                label_name='Subsystem:scooters',
                is_prod_or_stage=False
            ),
            # Valid yaml, not in cache, is prod so calls create.
            TestParams(
                label_name='readyForMerge',
            ),
            # Tries to creat an existing label, gets 409, fetches it instead.
            TestParams(
                label_name='readyForMerge',
                gl_err_code=409
            ),
            # Tries to creat an existing label, gets non-409 code, raises.
            TestParams(
                label_name='readyForMerge',
                gl_err_code=400
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_label_cache, rh_projects = self.setup_cache()

                # Not a valid label name per yaml, raises.
                if not test.in_yaml:
                    with self.assertRaises(ValueError):
                        test_label_cache.create_label(rh_projects[0], test.label_name)
                    continue

                # Create the label.
                mock_is_prod = mock.Mock(return_value=test.is_prod_or_stage)
                with mock.patch('webhook.labels.is_production_or_staging', mock_is_prod):
                    with responses.RequestsMock() as rsps:
                        if test.is_prod_or_stage and not test.in_cache:
                            label_rsps_json = {
                                'id': 999999,
                                'name': test.label_name,
                                'color': '#123456',
                                'description': 'new test label'
                            }
                            if test.gl_err_code:
                                err = labels.GitlabCreateError(response_code=test.gl_err_code)
                                rsps.post(f'{API_URL}/groups/10873929/labels', body=err)
                                if test.gl_err_code == 409:
                                    rsps.get(f'{API_URL}/groups/10873929/labels/{test.label_name}',
                                             json=label_rsps_json)
                            else:
                                rsps.post(f'{API_URL}/groups/10873929/labels', json=label_rsps_json)
                        if test.gl_err_code and test.gl_err_code != 409:
                            with self.assertRaises(labels.GitlabCreateError):
                                test_label_cache.create_label(rh_projects[0], test.label_name)
                            continue
                        result = test_label_cache.create_label(rh_projects[0], test.label_name)

                # It was in the cache so nothing to create.
                if test.in_cache:
                    self.assertIs(
                        result, test_label_cache.get_label(rh_projects[0], test.label_name)
                    )
                    continue

                # We made a fake label.
                fake_attr_value = bool(not test.is_prod_or_stage)
                self.assertIs(fake_attr_value, getattr(result, 'fake', False))
