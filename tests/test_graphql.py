"""Tests for graphql module."""
import typing
from unittest import mock

from gql.transport.exceptions import TransportQueryError
import responses
from responses.matchers import json_params_matcher

from tests.no_socket_test_case import NoSocketTestCase
from webhook import graphql
from webhook.defs import GITFORGE

API_URL = f'{GITFORGE}/api/graphql'


class TestHelpers(NoSocketTestCase):
    """Test helper functions."""

    @mock.patch('webhook.graphql.GitlabGraph._check_user', wraps=graphql.GitlabGraph._check_user)
    @mock.patch('webhook.graphql.GitlabGraph._check_keys', wraps=graphql.GitlabGraph._check_keys)
    def test_check_query_results(self, mock_check_keys, mock_check_user):
        """Test check_query_results."""
        # nothing to do
        mock_results = {}
        self.assertTrue(graphql.GitlabGraph.check_query_results(
            mock_results, None, None) is mock_results)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_not_called()

        # check user and it doesn´t match
        mock_results = {'currentUser': {'username': 'cool_guy'}}
        self.assertTrue(graphql.GitlabGraph.check_query_results(
            mock_results, None, 'steve') is mock_results)
        mock_check_user.assert_called_with(mock_results, 'steve')
        mock_check_keys.assert_not_called()

        # check user matches
        mock_check_user.reset_mock()
        self.assertEqual(graphql.GitlabGraph.check_query_results(
            mock_results, None, 'cool_guy'), None)
        mock_check_user.assert_called_with(mock_results, 'cool_guy')
        mock_check_keys.assert_not_called()

        # check keys and they are all there
        mock_check_user.reset_mock()
        mock_results = {'users': {}, 'groups': {}}
        self.assertTrue(graphql.GitlabGraph.check_query_results(
            mock_results, {'users'}, None) is mock_results)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_called_with(mock_results, {'users'})

        # check keys and they are not all there
        mock_check_keys.reset_mock()
        mock_results = {'users': {}, 'groups': {}}
        with self.assertRaises(RuntimeError):
            graphql.GitlabGraph.check_query_results(mock_results, {'fans', 'users'}, None)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_called_with(mock_results, {'fans', 'users'})


class TestGitlabGraph(NoSocketTestCase):
    """Test GitlabGraph methods."""

    @mock.patch('cki_lib.gitlab._GitLabClient')
    def test_init(self, mock_client):
        """Test a new object sets up the client."""
        mygraph = graphql.GitlabGraph()
        self.assertEqual(mygraph.client, mock_client())

    @mock.patch('cki_lib.gitlab._GitLabClient')
    def test_user(self, mock_client):
        """Test the user* properties."""
        user_result = {'currentUser': {'gid': 'gid//gitlab/User/1234',
                                       'name': 'Example User',
                                       'username': 'user1'}
                       }
        mock_client.return_value.query.return_value = user_result
        mygraph = graphql.GitlabGraph(get_user=True)
        mygraph.client.query.assert_called_with(graphql.GET_USER_DETAILS_QUERY)
        self.assertEqual(mygraph.user, user_result['currentUser'])
        self.assertEqual(mygraph.username, user_result['currentUser']['username'])
        self.assertEqual(mygraph.user_id, 1234)

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_client_query(self):
        """Test the client.query method."""
        query = '{currentUser {username}}'
        mygraph = graphql.GitlabGraph()
        result = mygraph.client.query(query)
        mygraph.client.query.assert_called_with(query)
        self.assertEqual(result, mygraph.client.query.return_value)

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_client_query_error(self):
        """Test client.query raises the expected error."""
        query = '{currentUser1 {username}}'
        mygraph = graphql.GitlabGraph()
        mygraph.client.query.side_effect = \
            TransportQueryError("Encountered 1 error(s) executing query: {currentUser1 {username}}",
                                errors=["Field 'currentUser1' doesn't exist on type 'Query'"])

        with self.assertRaises(TransportQueryError):
            mygraph.client.query(query)

    def test_paged_query(self):
        """Test paged query returns the expected results."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.execute = mock.Mock()

        query = '{currentUser {username}}'
        paged_key = 'project/mergeRequest/commits'
        variable_values = {'namespace': 'group/project', 'mr_id': 123}

        # No results returns None
        mygraph.client.execute.return_value = None
        result = mygraph.client.query(query, variable_values=variable_values, paged_key=paged_key)
        self.assertEqual(result, None)
        mygraph.client.execute.assert_called_once_with(mock.ANY, variable_values=variable_values)

        # Some results
        mygraph.client.execute.reset_mock(return_value=True)

        # Expected results of client.query
        commits1 = {'pageInfo': {'hasNextPage': True, 'endCursor': 'Abc'}, 'nodes': [1, 2, 3]}
        commits2 = {'pageInfo': {'hasNextPage': True, 'endCursor': 'Def'}, 'nodes': [4, 5, 6]}
        commits3 = {'pageInfo': {'hasNextPage': False, 'endCursor': 'Ghi'}, 'nodes': [7, 8]}
        result1 = {'project': {'mergeRequest': {'commits': commits1, 'desciption': 'hey'}}}
        result2 = {'project': {'mergeRequest': {'commits': commits2}}}
        result3 = {'project': {'mergeRequest': {'commits': commits3}}}
        mygraph.client.execute.side_effect = [result1, result2, result3]

        result = mygraph.client.query(query, paged_key=paged_key, variable_values=variable_values)
        # Expected return of execute_paged_query is the first result updated with all node values
        commits1['pageInfo']['nodes'] = [1, 2, 3, 4, 5, 6, 7, 8]
        expected = {'project': {'mergeRequest': {'commits': commits1, 'desciption': 'hey'}}}
        self.assertEqual(result, expected)

        # client.query should be called thrice with updated cursor
        self.assertEqual(mygraph.client.execute.call_count, 3)
        call1 = mock.call(mock.ANY, variable_values=variable_values)
        variable_values['after'] = 'Abc'
        variable_values['first'] = False
        call2 = mock.call(mock.ANY, variable_values=variable_values)
        variable_values['after'] = 'Def'
        call3 = mock.call(mock.ANY, variable_values=variable_values)
        mygraph.client.execute.assert_has_calls([call1, call2, call3])

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_get_user_by_id(self):
        """Returns the user dict for the matching user."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.query = mock.Mock()

        # User found.
        user = {'username': 'example', 'id': 12345}
        mygraph.client.query.return_value = {'user': user}
        result = mygraph.get_user_by_id(user['id'])
        self.assertEqual(result, user)
        mygraph.client.query.assert_called_with(mock.ANY,
                                                {'userid': f'gid://gitlab/User/{user["id"]}'})

        # No user found.
        mygraph.client.query.return_value = {'user': None}
        result = mygraph.get_user_by_id(f'gid://gitlab/User/{user["id"]}')
        self.assertIs(result, None)
        mygraph.client.query.assert_called_with(mock.ANY,
                                                {'userid': f'gid://gitlab/User/{user["id"]}'})

        # Gotta pass a valid string or int or ValueError is raised.
        with self.assertRaises(ValueError):
            mygraph.get_user_by_id('123456')
        with self.assertRaises(ValueError):
            mygraph.get_user_by_id('guid::/gitlab/users/12345')

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_find_member(self):
        """Returns the user dict for the matching user."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.query = mock.Mock()

        namespace = 'group/project'
        attribute = 'username'
        search_key = 'user'
        usernames = ['user1', 'user', 'user-test']
        nodes = [{'user': {'username': user}} for user in usernames]
        mygraph.client.query.return_value = {'project': {'projectMembers': {'nodes': nodes}}}
        result = mygraph.find_member(namespace, attribute, search_key)
        self.assertEqual(result, nodes[1]['user'])

        # Raises ValueError if no attribute or search_key.
        with self.assertRaises(ValueError):
            result = mygraph.find_member(namespace, '', search_key)
        with self.assertRaises(ValueError):
            result = mygraph.find_member(namespace, attribute, '')

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_find_member_by_email(self):
        """Returns the user dict for the matching user."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.query = mock.Mock()

        # Email resolves to the expected user.
        namespace = 'group/project'
        email = 'user@example.com'
        username = 'user'
        nodes = [{'user': {'username': username, 'email': email}}]
        mygraph.client.query.return_value = {'group': {'groupMembers': {'nodes': nodes}}}
        result = mygraph.find_member_by_email(namespace, email, username)
        self.assertEqual(result, nodes[0]['user'])

        # Email does not resolve to the expected user.
        nodes = [{'user': {'username': 'someone_else', 'email': email}}]
        mygraph.client.query.return_value = {'project': {'projectMembers': {'nodes': nodes}}}
        result = mygraph.find_member_by_email(namespace, email, username)
        self.assertEqual(result, None)

        # Raises ValueError if no email or username.
        with self.assertRaises(ValueError):
            result = mygraph.find_member(namespace, '', username)
        with self.assertRaises(ValueError):
            result = mygraph.find_member(namespace, email, '')


class TestGQLMethods(NoSocketTestCase):
    """Unit tests for the new GitlabGraph methods."""

    GROUPS = {'group1': ['user1', 'user2', 'user3', 'user5'],
              'group2': ['user3', 'user4', 'user7', 'user9'],
              'group3': ['user4', 'user5', 'user8', 'user9'],
              'group4': ['user6', 'user8', 'user11'],
              'group5': ['user6', 'user9', 'user15'],
              }

    PROJECTS = {'project1': ['user1', 'user2', 'user3', 'user5'],
                'project2': ['user3', 'user4', 'user7', 'user9'],
                'project3': ['user4', 'user5', 'user8', 'user9'],
                'project4': ['user6', 'user8', 'user11'],
                'project5': ['user6', 'user9', 'user15'],
                }

    USER_RANGE = 20
    USERS = {f'user{gid}': {'gid': f'gid://gitlab/User/{gid}',
                            'email': f'user{gid}@example.com',
                            'name': f'User {gid}',
                            'username': f'user{gid}'} for gid in range(1, USER_RANGE + 1)}

    @responses.activate
    def test_get_all_members(self):
        """Returns a list of all the """
        mygraph = graphql.GitlabGraph()

        # Set up get_all_members reponses.
        for group_name, group_data in self.GROUPS.items():
            match_query = graphql.ALL_MEMBERS_QUERY.strip('\n') % ('group', 'group')
            json_data = {'pageInfo': {'hasNextPage': False, 'endCursor': None},
                         'nodes': [{'user': self.USERS[user]} for user in group_data]}
            responses.post(API_URL, json={'data': {'group': {'groupMembers': json_data}}},
                           match=[json_params_matcher({'query': match_query,
                                                       'variables': {'namespace': group_name}},
                                                      strict_match=False)])
        for project_name, project_data in self.PROJECTS.items():
            match_query = graphql.ALL_MEMBERS_QUERY.strip('\n') % ('project', 'project')
            json_data = {'pageInfo': {'hasNextPage': False, 'endCursor': None},
                         'nodes': [{'user': self.USERS[user]} for user in project_data]}
            responses.post(API_URL, json={'data': {'project': {'projectMembers': json_data}}},
                           match=[json_params_matcher({'query': match_query,
                                                       'variables': {'namespace': project_name}},
                                                      strict_match=False)])
        # Namespace exists but returns no members.
        json_data = {'pageInfo': {'hasNextPage': False, 'endCursor': None}, 'nodes': []}
        responses.post(API_URL, json={'data': {'project': {'projectMembers': json_data}}},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'namespace': 'missing'}},
                                                  strict_match=False)])
        # Namespace does not exist.
        responses.post(API_URL, json={'data': {'group': None}})

        # Groups that exist.
        for namespace, ns_data in self.GROUPS.items():
            result = mygraph.get_all_members(namespace, 'group')
            self.assertEqual(ns_data, list(result.keys()))

        # Projects that exist.
        for namespace, ns_data in self.PROJECTS.items():
            result = mygraph.get_all_members(namespace, 'project')
            self.assertEqual(ns_data, list(result.keys()))

        # Namespace does not exist.
        result = mygraph.get_all_members('boop', 'group')
        self.assertIs(result, None)

        # Raises ValueError if namespace_type is not 'group' or 'project'.
        with self.assertRaises(ValueError):
            result = mygraph.get_all_members(namespace, 'PROJECT')

    @responses.activate
    def test_get_all_issues(self):
        """Returns a dict representing every open group or project issue."""
        mygraph = graphql.GitlabGraph()
        namespace = 'group/project'
        match_query = graphql.ALL_PROJECT_ISSUES_QUERY.strip('\n')

        # Set up response for a project that exists.
        nodes = [{'iid': 123}, {'iid': 456}]
        json_data = {'pageInfo': {'hasNextPage': False, 'endCursor': None}, 'nodes': nodes}
        responses.post(API_URL, json={'data': {'project': {'issues': json_data}}},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'namespace': namespace}},
                                                  strict_match=False)])
        # A project that cannot be found.
        responses.post(API_URL, json={'data': {'project': None}})
        result = mygraph.get_all_issues('fakespace')
        self.assertIs(result, None)

    @responses.activate
    def test_create_project_issue(self):
        """Creates an issue on the project and returns the iid and webUrl."""
        mygraph = graphql.GitlabGraph()
        match_query = graphql.CREATE_ISSUE_MUTATION.strip('\n')

        namespace = 'group/project'
        title = 'a new issue'
        body = 'this is a new issue'

        # Set up the responses.
        # Basic usage.
        expected_vars1 = {'projectPath': namespace, 'title': title, 'description': body}
        issue_dict1 = {'iid': 5, 'webUrl': 'https://gitlab.com/group/project/-/issues/5'}
        responses.post(API_URL, json={'data': {'createIssue': {'issue': issue_dict1}}},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'input': expected_vars1}},
                                                  strict_match=False)])

        # With extra_input set.
        expected_vars2 = {'projectPath': namespace, 'title': title, 'description': body,
                          'labels': ['label1', 'label2']}
        issue_dict2 = {'iid': 13, 'webUrl': 'https://gitlab.com/group/project/-/issues/13'}
        responses.post(API_URL, json={'data': {'createIssue': {'issue': issue_dict2}}},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'input': expected_vars2}},
                                                  strict_match=False)])

        # Gets the expected response in production.
        with mock.patch('webhook.graphql.is_production_or_staging', mock.Mock()):
            result = mygraph.create_project_issue(namespace, title, body)
            self.assertEqual(result, issue_dict1)

        # If this isn't production then get an issue IID of 0.
        result = mygraph.create_project_issue(namespace, title, body)
        faux_dict = {'iid': 0, 'webUrl': 'https://gitlab.com/group/project/-/issues/0'}
        self.assertEqual(result, faux_dict)

        # extra_input is applied on top of params['input'].
        extra_input2 = {'labels': ['label1', 'label2']}
        with mock.patch('webhook.graphql.is_production_or_staging', mock.Mock()):
            result = mygraph.create_project_issue(namespace, title, body, extra_input=extra_input2)
            self.assertEqual(result, issue_dict2)

    @responses.activate
    def test_get_user(self):
        """Returns a dict about the single username given."""
        mygraph = graphql.GitlabGraph()
        match_query = graphql.GET_USER_QUERY.strip('\n')

        # Set up get_user responses.
        for user_data in self.USERS.values():
            responses.post(API_URL, json={'data': {'user': user_data}},
                           match=[json_params_matcher({'query': match_query,
                                                       'variables':
                                                       {'username': user_data['username']}},
                                                      strict_match=False)])
        # Set up the response for when the user is not found.
        responses.post(API_URL, json={'data': {'user': None}})

        # Users that exist.
        for username, user_dict in self.USERS.items():
            result = mygraph.get_user(username)
            self.assertEqual(result, user_dict)

        # A user that doesn't exist.
        result = mygraph.get_user('fake_user_123')
        self.assertIs(result, None)

        # Gets mad if you don't give a proper username string.
        with self.assertRaises(ValueError):
            result = mygraph.get_user('')

    @responses.activate
    @mock.patch('webhook.graphql.is_production_or_staging', mock.Mock(return_value=True))
    def test_set_mr_reviewers_production(self):
        """Sets the reviewers on the MR and returns the updated list of reviewers."""
        mygraph = graphql.GitlabGraph()
        match_query = graphql.SET_MR_REVIEWERS_MUTATION.strip('\n')
        namespace = 'group/project'
        mr_id = 123
        usernames = ['user1', 'user2']

        # Raises ValueError due to invalid mode.
        mode = 'ENTANGLE'
        with self.assertRaises(ValueError):
            mygraph.set_mr_reviewers(namespace, mr_id, usernames, mode)

        # Raises ValueError due to no usernames.
        mode = 'APPEND'
        with self.assertRaises(ValueError):
            mygraph.set_mr_reviewers(namespace, mr_id, [], mode)

        # Raises RuntimeError on unexpected response.
        mode = 'REPLACE'
        input_params = {'projectPath': namespace,
                        'iid': str(mr_id),
                        'reviewerUsernames': usernames,
                        'operationMode': mode}
        responses.post(API_URL, json={'data': {}},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'input': input_params}}
                                                  )])
        with self.assertRaises(RuntimeError):
            mygraph.set_mr_reviewers(namespace, mr_id, usernames, mode)

        # Returns reviewers nodes on success.
        reviewers = [{'username': username} for username in usernames]
        response_data = {'mergeRequestSetReviewers': {'mr': {'reviewers': {'nodes': reviewers}}}}
        responses.post(API_URL, json={'data': response_data},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'input': input_params}}
                                                  )])
        result = mygraph.set_mr_reviewers(namespace, mr_id, usernames, mode)
        self.assertEqual(result, reviewers)

    @mock.patch('webhook.graphql.is_production_or_staging', mock.Mock(return_value=False))
    def test_set_mr_reviewers_non_production(self):
        """Just returns the users dict."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.query = mock.Mock()
        namespace = 'group/project'
        mr_id = 123
        usernames = ['user1', 'user2']

        mode = 'REPLACE'

        reviewers = [{'username': username} for username in usernames]
        result = mygraph.set_mr_reviewers(namespace, mr_id, usernames, mode)
        self.assertEqual(result, reviewers)
        mygraph.client.query.assert_not_called()

    @responses.activate
    def test_check_mr_state(self):
        """Returns True if the MR is open and has less than MAX_COMMITS_PER_MR."""
        mygraph = graphql.GitlabGraph()
        match_query = graphql.CHECK_MR_STATE.strip('\n')

        class TestData(typing.NamedTuple):
            """Tiny test holder."""
            namespace: str
            mr_id: int
            graphql_response_data: dict
            mr_state: bool

        test_data = [
            # Working example returns True.
            TestData(
                'group/project',
                123,
                {'project': {'mr': {'state': 'opened', 'commitCount': 5}}},
                True
            ),
            # Working example but with too many commits, returns False.
            TestData(
                'group/project',
                123,
                {'project': {'mr': {'state': 'opened', 'commitCount': 999999}}},
                False
            ),
            # Working example but not opened, returns False.
            TestData(
                'group/project',
                123,
                {'project': {'mr': {'state': 'closed', 'commitCount': 5}}},
                False
            ),
            # Response does not contain any MR data, returns False.
            TestData(
                'group/project',
                55555555,
                {'project': {'mr': None}},
                False
            ),
            # Response does not contain any project data, returns False.
            TestData(
                'group/bad_project',
                123,
                {'project': None},
                False
            ),
        ]

        # Set up all the responses up front.
        for test in test_data:
            responses.post(
                API_URL,
                json={'data': test.graphql_response_data},
                match=[
                    json_params_matcher(
                        {'query': match_query,
                         'variables': {'namespace': test.namespace, 'mr_id': str(test.mr_id)}}
                    )
                ]
            )

        # Run each test.
        for test_number, test in enumerate(test_data, start=1):
            with self.subTest(test_number=test_number, test=test):
                self.assertEqual(mygraph.check_mr_state(test.namespace, test.mr_id), test.mr_state)

    def test_get_all_mr_labels(self):
        """Returns a list of Label objects for the given input."""
        class TestData(typing.NamedTuple):
            """Test parameters and expected results."""
            # Values for 'existing_results'
            hasNextPage: bool | None
            labels_nodes: list[dict] | None
            # Expected query result
            query_result: dict | None
            # Expected return value
            expected_return: list | ValueError

        tests = [
            # Existing results but no hasNextPage so no query is done; just return the labels from
            # the existing results.
            TestData(
                hasNextPage=False,
                labels_nodes=[{'title': 'label1'}, {'title': 'label2'}],
                query_result=None,
                expected_return=['label1', 'label2']
            ),
            # existing_results indicate more pages so does a query and returns extended list.
            TestData(
                hasNextPage=True,
                labels_nodes=[{'title': 'label1'}, {'title': 'label2'}],
                query_result={'project': {
                    'mr': {'labels': {'nodes': [{'title': 'label3'}, {'title': 'l4'}]}}
                }},
                expected_return=['label1', 'label2', 'label3', 'l4']
            ),
            # existing_results indicate more but query results didn't provide more nodes so Raise.
            TestData(
                hasNextPage=True,
                labels_nodes=[],
                query_result={'project': {'mr': None}},
                expected_return=ValueError()
            )
        ]

        mygraph = graphql.GitlabGraph()
        mygraph.client.query = mock.Mock()

        for test_number, test in enumerate(tests, start=1):
            # Reset the mock query function.
            mygraph.client.query.reset_mock(side_effect=True, return_value=True)
            # Set up the query function response.
            if test.query_result:
                mygraph.client.query.return_value = test.query_result

            # Create the 'existing_results' input from the test values.
            if any(getattr(test, attr) is not None for attr in ('hasNextPage', 'labels_nodes')):
                existing_results = {
                    'pageInfo': {'hasNextPage': test.hasNextPage, 'endCursor': 'mock_cursor_str'},
                    'nodes': test.labels_nodes
                }
            else:
                existing_results = None

            # Run the test.
            with self.subTest(
                test_number=test_number,
                existing_results=existing_results,
                query_result=test.query_result,
                expected_return=test.expected_return
            ):
                if isinstance(test.expected_return, Exception):
                    with self.assertRaises(test.expected_return.__class__):
                        mygraph.get_all_mr_labels('namespace', 123, existing_results)
                else:
                    result = mygraph.get_all_mr_labels('namespace', 123, existing_results)
                    self.assertEqual(test.expected_return, result)
                if not test.hasNextPage:
                    mygraph.client.query.assert_not_called()
                else:
                    mygraph.client.query.assert_called_once()
