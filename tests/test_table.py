"""Tests for the table module."""

from tests.no_socket_test_case import NoSocketTestCase
from webhook import table


class Row1(table.TableRow):
    """Simple test TableRow."""

    def __init__(self, c1='', c2='', c3=''):
        """Init."""
        self.Column_1 = c1
        self.Column_2 = c2
        self.Column_3 = c3


class Row2(Row1):
    """Another simple test TableRow."""

    def __init__(self, c1='', c2='', c3='', c4=''):
        """Init."""
        super().__init__(c1, c2, c3)
        self.Column_4 = c4

    def _format_Column_4(self, value):
        """Sets value for Column_4."""
        return f'formatted {value}'


class TestTableClass(NoSocketTestCase):
    """Tests for the Table dataclass."""

    def test_Table_creation(self):
        """Creates a Table with an empty footnotes and rows lists."""
        test_table = table.Table()
        self.assertEqual(test_table.footnote_list, [])
        self.assertEqual(test_table.rows, [])
        self.assertEqual(len(test_table), 0)
        self.assertEqual(str(test_table), '')
        self.assertEqual(test_table.columns, [])
        self.assertEqual(test_table.footnotes, '')
        self.assertEqual(test_table.header, '')

    def test_add_row(self):
        """Adds a TableRow to the row list attribute."""
        test_table = table.Table()
        test_row1 = Row1('row1_c1', 'row1_c2', 'row1_c3')
        test_table.add_row(test_row1)
        self.assertIs(test_table.rows[0], test_row1)
        self.assertEqual(len(test_table), 1)
        self.assertEqual(test_table.columns, ['Column 1', 'Column 2', 'Column 3'])
        header = '|Column 1|Column 2|Column 3|\n|:------|:------|:------|'
        self.assertEqual(test_table.header, header)
        self.assertEqual(str(test_table), header + '\n|row1_c1|row1_c2|row1_c3|\n\n\n')

    def test_add_row_bad_type(self):
        """Refuses to add something that isn't a TableRow."""
        test_table = table.Table()
        exception_hit = False
        try:
            test_table.add_row(['c1', 'c2', 'c3'])
        except TypeError:
            exception_hit = True
        self.assertIs(exception_hit, True)
        self.assertEqual(len(test_table.rows), 0)

    def test_add_row_bad_len(self):
        """Refuses to add another TableRow if the first one is a different length."""
        test_table = table.Table()
        test_row1 = Row1('row1_c1', 'row1_c2', 'row1_c3')
        test_table.add_row(test_row1)
        test_row2 = Row2('row2_c1', 'row2_c2', 'row2_c3', 'row2_c4')

        exception_hit = False
        try:
            test_table.add_row(test_row2)
        except ValueError:
            exception_hit = True
        self.assertIs(exception_hit, True)
        self.assertEqual(len(test_table.rows), 1)


class TestTableRowClass(NoSocketTestCase):
    """Tests for the TableRow class."""

    def test_creation(self):
        """Creates an empty TableRow."""
        test_row = table.TableRow()
        self.assertEqual(len(test_row), 0)
        self.assertEqual(str(test_row), '')

    def test_set_value(self):
        """Transforms the input and stores it as a string in the given attribute."""
        test_row = Row2()

        # Barfs if the column doesn't exist.
        exception_hit = False
        try:
            test_row.set_value('Missing_Column', 'hello!')
        except AttributeError:
            exception_hit = True
        self.assertIs(exception_hit, True)

        # Calls parse_func with the expected value.
        test_row.set_value('Column_4', 'hello')
        self.assertEqual(test_row.Column_4, 'formatted hello')

        # Splits a list into a string separated by <br>.
        test_row.set_value('Column_1', ['hello', 'there', 'you'])
        self.assertEqual(test_row.Column_1, 'hello<br>there<br>you')

        # Turns a dict into a numbered list.
        test_row.set_value('Column_2', {'hey': 'item 1', 'there': 'item 2', 'you': 'item 3'})
        self.assertEqual(test_row.Column_2, '1. item 1\n2. item 2\n3. item 3\n')

        # For everything else, just set the column to the value.
        test_row.set_value('Column_3', 'oh wow')
        self.assertEqual(test_row.Column_3, 'oh wow')
