"""Tests for the table module."""
from tests import fakes_bz
from tests import fakes_mrs
from tests.no_socket_test_case import NoSocketTestCase
from webhook import bug
from webhook import bug_tests
from webhook.rh_metadata import Projects


class TestBug(NoSocketTestCase):
    """Tests for the Bug class."""

    # expected Bug values when no BZ or MRs are set.
    empty_mr_equal = {'_mrs': [],
                      'alias': 'Bug #0',
                      'failed_tests': [],
                      '_id': 0,
                      'id': 'BOGUS',       # This should be set by the tester,
                      'bz_cves': [],
                      'bz_depends_on': [],
                      'bz_itr': '',
                      'bz_policy_check_ok': (None, 'Check not done: No BZ'),
                      'commits': [],
                      'parent_mr_commits': [],
                      'bz_ztr': ''}

    empty_mr_is = {'_bz': None,
                   'scope': bug.MrScope.INVALID,
                   'internal': False,
                   'untagged': False,
                   'bz': None,
                   'mr': None,
                   'parent_mr': None,
                   'bz_branch': None,
                   'bz_is_verified': False,
                   'bz_priority': bug.BZPriority.UNKNOWN,
                   'bz_project': None,
                   'bz_resolution': None,
                   'bz_status': bug.BZStatus.UNKNOWN,
                   'cve_ids': None,
                   'is_cve_tracker': False,
                   'is_dependency': False,
                   'is_merged': False,
                   'in_mr_description': False,
                   'test_list': ['BOGUS']}  # This should be set by the tester.

    def validate_bug(self, bug, assert_equal, assert_is):
        """Helper to validate a Bug object."""
        print(f'Testing Bug {bug}...')
        for attribute, value in assert_equal.items():
            print(f'{attribute} should equal: {value}')
            self.assertEqual(getattr(bug, attribute), value)
        for attribute, value in assert_is.items():
            print(f'{attribute} should be: {value}')
            self.assertIs(getattr(bug, attribute), value)

    def test_bug_init_empty(self):
        """Creates a Bug with no BZ object set or MRs."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for an empty Bug.
        assert_equal['id'] = 0
        assert_equal['_id'] = 0
        assert_is.pop('_id', None)
        assert_is['test_list'] = bug_tests.BUG_TESTS

        test_bug = bug.Bug()
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_internal_no_mrs(self):
        """Creates a Bug representing descriptions marked INTERNAL."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Internal Bug
        assert_equal['alias'] = 'Bug #INTERNAL'
        assert_equal['id'] = bug.INTERNAL_BUG
        assert_is['internal'] = True
        assert_is['test_list'] = bug_tests.INTERNAL_TESTS

        test_bug = bug.Bug.new_internal(mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_missing_no_mrs(self):
        """Creates a Bug representing a MISSING bz."""
        bug_id = 1234567
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing Bug
        assert_equal['alias'] = 'Bug #1234567'
        assert_equal['id'] = bug_id
        assert_equal['_id'] = bug_id
        assert_is.pop('_id', None)
        assert_is['test_list'] = bug_tests.BUG_TESTS

        test_bug = bug.Bug.new_missing(bug_id, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_missing_cve_no_mrs(self):
        """Creates a Bug representing a MISSING CVE tracker bz."""
        bug_id = 'CVE-1998-12345'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing CVE Bug
        assert_equal['alias'] = 'CVE-1998-12345 (BZ missing)'
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: CVE tracker bug')
        assert_equal['cve_ids'] = [bug_id]
        assert_equal['id'] = bug_id
        assert_equal['_id'] = bug_id
        assert_is.pop('_id', None)
        assert_is.pop('cve_ids', None)
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = bug_tests.CVE_TESTS

        test_bug = bug.Bug.new_missing(bug_id, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_untagged_no_mrs(self):
        """Creates a Bug representing descriptions with no tags (UNTAGGED)."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Untagged Bug
        assert_equal['alias'] = 'Bug #UNTAGGED'
        assert_equal['id'] = bug.UNTAGGED_BUG
        assert_is['untagged'] = True
        assert_is['test_list'] = bug_tests.UNTAGGED_TESTS

        test_bug = bug.Bug.new_untagged(mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_from_bz_no_mrs_bz1234567(self):
        """Creates a Bug linked to the given BZ."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'Bug #1234567'
        assert_equal['id'] = 1234567
        assert_equal['bz_cves'] = ['CVE-1235-13516']
        assert_equal['bz_itr'] = '9.1.0'
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['bz'] = fakes_bz.BZ1234567
        assert_is['_bz'] = fakes_bz.BZ1234567
        assert_is['bz_priority'] = bug.BZPriority.UNSPECIFIED
        assert_is['bz_status'] = bug.BZStatus.POST
        assert_is['test_list'] = bug_tests.BUG_TESTS

        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ1234567, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_from_bz_no_mrs_bz2323232(self):
        """Creates a Bug linked to the given BZ."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'Bug #2323232'
        assert_equal['id'] = 2323232
        assert_equal['bz_itr'] = '9.1.0'
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['bz'] = fakes_bz.BZ2323232
        assert_is['_bz'] = fakes_bz.BZ2323232
        assert_is['bz_priority'] = bug.BZPriority.UNSPECIFIED
        assert_is['bz_status'] = bug.BZStatus.MODIFIED
        assert_is['test_list'] = bug_tests.BUG_TESTS
        assert_is['bz_is_verified'] = True

        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ2323232, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_from_bz_no_mrs_bz7777777(self):
        """Creates a Bug linked to the given BZ."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'Bug #7777777'
        assert_equal['id'] = 7777777
        assert_equal['bz_itr'] = '9.1'
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['bz'] = fakes_bz.BZ7777777
        assert_is['_bz'] = fakes_bz.BZ7777777
        assert_is['bz_priority'] = bug.BZPriority.UNSPECIFIED
        assert_is['bz_status'] = bug.BZStatus.POST
        assert_is['test_list'] = bug_tests.BUG_TESTS

        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ7777777, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_from_bz_no_mrs_bz2345678(self):
        """Creates a Bug linked to the given BZ."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'Bug #2345678'
        assert_equal['id'] = 2345678
        assert_equal['bz_cves'] = ['CVE-2022-43210']
        assert_equal['bz_ztr'] = '9.0'
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['bz'] = fakes_bz.BZ2345678
        assert_is['_bz'] = fakes_bz.BZ2345678
        assert_is['bz_priority'] = bug.BZPriority.LOW
        assert_is['bz_status'] = bug.BZStatus.MODIFIED
        assert_is['test_list'] = bug_tests.BUG_TESTS
        assert_is['bz_is_verified'] = True

        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ2345678, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_from_bz_no_mrs_bz3456789(self):
        """Creates a Bug linked to the given BZ."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'CVE-1235-13516 (Bug #3456789)'
        assert_equal['id'] = 3456789
        assert_equal['bz_cves'] = ['CVE-1235-13516']
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: CVE tracker bug')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_is.pop('cve_ids', None)
        assert_is['bz'] = fakes_bz.BZ3456789
        assert_is['_bz'] = fakes_bz.BZ3456789
        assert_is['bz_priority'] = bug.BZPriority.HIGH
        assert_is['bz_status'] = bug.BZStatus.NEW
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = bug_tests.CVE_TESTS

        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_bug_new_from_bz_no_mrs_bz4567890(self):
        """Creates a Bug linked to the given BZ."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'CVE-2022-7549, CVE-2022-7550 (Bug #4567890)'
        assert_equal['id'] = 4567890
        assert_equal['bz_cves'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: CVE tracker bug')
        assert_equal['cve_ids'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_is.pop('cve_ids', None)
        assert_is['bz'] = fakes_bz.BZ4567890
        assert_is['_bz'] = fakes_bz.BZ4567890
        assert_is['bz_priority'] = bug.BZPriority.URGENT
        assert_is['bz_resolution'] = bug.BZResolution.ERRATA
        assert_is['bz_status'] = bug.BZStatus.CLOSED
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = bug_tests.CVE_TESTS

        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ4567890, mrs=[])
        self.validate_bug(test_bug, assert_equal, assert_is)

    def test_equality(self):
        """Returns True if the IDs match, otherwise False."""
        bug1 = bug.Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=[])
        bug2 = bug.Bug.new_from_bz(bz=fakes_bz.BZ4567890, mrs=[])
        bug3 = bug.Bug.new_missing(bz_id=3456789, mrs=[])

        self.assertTrue(bug1 != bug2)
        self.assertTrue(bug1 == bug3)

        # Returns False if the other is not a Bug instance.
        self.assertFalse(bug1 == 'hello')

    def test_id_setter(self):
        """Sets the id property if self.bz is not set and not internal/untagged."""
        # Setting id property raises ValueError if the bz property is set.
        test_bug = bug.Bug.new_from_bz(bz=fakes_bz.BZ2323232, mrs=[])
        with self.assertRaises(ValueError):
            test_bug.id = 1234567

        # Setting the id property of a Bug marked internal or untagged raises a ValueError
        test_bug = bug.Bug.new_internal(mrs=[])
        with self.assertRaises(ValueError):
            test_bug.id = 7654321
        test_bug = bug.Bug.new_untagged(mrs=[])
        with self.assertRaises(ValueError):
            test_bug.id = 7654321

        # Setting the id property works for a Bug with no bz.
        test_bug = bug.Bug.new_missing(1234567, mrs=[])
        self.assertEqual(test_bug.id, 1234567)
        test_bug.id = 7654321
        self.assertEqual(test_bug.id, 7654321)

    def test_bugs_with_mrs(self):
        """Returns the expected values when MRs are present."""
        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        mr309 = fakes_mrs.make_mr('redhat/centos-stream/src/kernel/centos-stream-9', 309,
                                  projects=projects,
                                  query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])
        mr303 = fakes_mrs.make_mr('redhat/centos-stream/src/kernel/centos-stream-9', 303,
                                  projects=projects, query_results_list=[fakes_mrs.MR303_DICT],
                                  is_dependency=True)
        mr309.depends_mrs.append(mr303)
        mr309.description.depends_bzs.update({2323232})
        mrs = [mr309, mr303]
        bz1234567 = bug.Bug.new_from_bz(bz=fakes_bz.BZ1234567, mrs=mrs)
        bz2323232 = bug.Bug.new_from_bz(bz=fakes_bz.BZ2323232, mrs=mrs)
        bz3456789 = bug.Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=mrs)

        # bz1234567 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'Bug #1234567'
        assert_equal['id'] = 1234567
        assert_equal['bz_cves'] = ['CVE-1235-13516']
        assert_equal['bz_itr'] = '9.1.0'
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: Branch has no policy')
        assert_equal['commits'] = ['0aa467549b4e997d023c29f4d481aee01b9e9471',
                                   'e53eab9f887f784044ad32ef5c082695831d90d9',
                                   '88cdd4035228dac16878eb907381afea6ceffeaa']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is['bz'] = fakes_bz.BZ1234567
        assert_is['_bz'] = fakes_bz.BZ1234567
        assert_is['bz_branch'] = mr309.projects.projects[24152864].branches[0]
        assert_is['bz_priority'] = bug.BZPriority.UNSPECIFIED
        assert_is['bz_project'] = mr309.projects.projects[24152864]
        assert_is['bz_status'] = bug.BZStatus.POST
        assert_is['in_mr_description'] = True
        assert_is['mr'] = mr309
        assert_is['parent_mr'] = mr309
        assert_is['test_list'] = bug_tests.BUG_TESTS

        self.validate_bug(bz1234567, assert_equal, assert_is)

        # bz2323232 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'Bug #2323232'
        assert_equal['id'] = 2323232
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: Branch has no policy')
        assert_equal['bz_itr'] = '9.1.0'
        assert_equal['commits'] = ['ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                   'f77278fcd9cef99358adc7f5e077be795a54ffca']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is['bz'] = fakes_bz.BZ2323232
        assert_is['_bz'] = fakes_bz.BZ2323232
        assert_is['bz_branch'] = mr309.projects.projects[24152864].branches[0]
        assert_is['bz_priority'] = bug.BZPriority.UNSPECIFIED
        assert_is['bz_project'] = mr309.projects.projects[24152864]
        assert_is['bz_status'] = bug.BZStatus.MODIFIED
        assert_is['in_mr_description'] = True
        assert_is['is_dependency'] = True
        assert_is['mr'] = mr303
        assert_is['parent_mr'] = mr309
        assert_is['test_list'] = bug_tests.DEP_TESTS
        assert_is['bz_is_verified'] = True

        self.validate_bug(bz2323232, assert_equal, assert_is)

        # bz3456789 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = 'CVE-1235-13516 (Bug #3456789)'
        assert_equal['id'] = 3456789
        assert_equal['bz_cves'] = ['CVE-1235-13516']
        assert_equal['bz_policy_check_ok'] = (None, 'Check not done: CVE tracker bug')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_equal['commits'] = ['0aa467549b4e997d023c29f4d481aee01b9e9471',
                                   'e53eab9f887f784044ad32ef5c082695831d90d9',
                                   '88cdd4035228dac16878eb907381afea6ceffeaa']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is.pop('cve_ids', None)
        assert_is['bz'] = fakes_bz.BZ3456789
        assert_is['_bz'] = fakes_bz.BZ3456789
        assert_is['bz_priority'] = bug.BZPriority.HIGH
        assert_is['bz_status'] = bug.BZStatus.NEW
        assert_is['in_mr_description'] = True
        assert_is['is_cve_tracker'] = True
        assert_is['mr'] = mr309
        assert_is['parent_mr'] = mr309
        assert_is['test_list'] = bug_tests.CVE_TESTS

        self.validate_bug(bz3456789, assert_equal, assert_is)

        mr309.depends_mrs.clear()

    def test_bug_project_branch(self):
        """Returns the expected Project and Branch objects."""
        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        main_branch = projects.projects[12345].branches[0]
        self.assertEqual(main_branch.components, {'kernel'})
        self.assertEqual(main_branch.internal_target_release, '8.7.0')
        self.assertEqual(main_branch.sub_component, '')
        main_auto_branch = projects.projects[12345].branches[-4]
        self.assertEqual(main_auto_branch.components, {'kernel'})
        self.assertEqual(main_branch.internal_target_release, '8.7.0')
        self.assertEqual(main_auto_branch.sub_component, 'automotive')

        # A Bug matching the 'main' branch.
        bz2222222 = bug.Bug.new_from_bz(bz=fakes_bz.BZ2222222, mrs=[], projects=projects)
        self.assertEqual(bz2222222.bz_project, projects.projects[12345])
        self.assertEqual(bz2222222.bz_branch, main_branch)

        # A Bug matching the 'main-automotive' branch.
        bz3333333 = bug.Bug.new_from_bz(bz=fakes_bz.BZ3333333, mrs=[], projects=projects)
        self.assertEqual(bz3333333.bz_project, projects.projects[12345])
        self.assertEqual(bz3333333.bz_branch, main_auto_branch)
