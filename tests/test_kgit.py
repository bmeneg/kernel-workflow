"""Webhook interaction tests."""
from datetime import datetime
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import mock

from cki_lib import messagequeue

from tests.no_socket_test_case import NoSocketTestCase
from webhook import kgit


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestKGit(NoSocketTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_branch_copy(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.branch_copy(location, branch)
            self.assertIn(f'git branch --copy {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_checkout(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.checkout(location, branch)
            self.assertIn(f'git checkout {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_reset(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.hard_reset(location, branch)
            self.assertIn(f'git reset --hard {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_merge(self):
        location = '/src/kernel-ark/'
        reference = 'origin/merge-requests/66'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.merge(location, reference)
            self.assertIn(f'git merge --quiet --no-edit {reference}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_branch_delete(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.branch_delete(location, branch)
            self.assertIn(f'git branch -D {branch}', logs.output[-1])

    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    def test_branch_mergeable_false(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        target_branch = 'kernel-ark/os-build'
        merge_branch = 'temp-merge-branch'
        m_args = ['git', 'merge', '--quiet', '--no-edit', merge_branch]
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = kgit.branch_mergeable(worktree_dir, target_branch, merge_branch)
            self.assertFalse(ret)

    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    def test_branch_mergeable(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        target_branch = 'kernel-ark/os-build'
        merge_branch = 'temp-merge-branch'
        m_args = ['git', 'merge', '--quiet', '--no-edit', merge_branch]
        self._add_run_result(m_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = kgit.branch_mergeable(worktree_dir, target_branch, merge_branch)
            self.assertTrue(ret)

    @mock.patch('webhook.kgit.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('os.path.exists', mock.Mock(return_value=False))
    @mock.patch('webhook.kgit.clean_up_temp_merge_branch')
    def test_handle_stale_worktree(self, clean_up):
        rhkernel_src = '/src/kernel'
        merge_branch = 'kernel-ark/os-build'
        worktree_dir = '/tmp/worktree'
        clean_up.return_value = True

        m_args = ['git', 'branch', '-D', f'{merge_branch}-save']
        self._add_run_result(m_args, 4, 'Uhhh yeah it exploded')
        with mock.patch('builtins.open', mock.mock_open(read_data='20200504112233')) as m:
            with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
                kgit.handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_called_once()

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
        clean_up.call_count = 0
        with mock.patch('builtins.open', mock.mock_open(read_data=timestamp)) as m2:
            with self.assertRaises(messagequeue.QuietNackException):
                kgit.handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)
            m2.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_not_called()

    def test_create_worktree_timestamp(self):
        worktree_dir = '/tmp/worktree'
        with mock.patch('builtins.open', mock.mock_open()) as m:
            kgit.create_worktree_timestamp(worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'w', encoding='ascii')
            handle = m()
            handle.write.assert_called_once()

    @mock.patch('os.path.isdir', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.handle_stale_worktree')
    @mock.patch('webhook.kgit.worktree_add')
    def test_prep_temp_merge_branch(self, mock_add, mock_stale):
        remote = 'foobar'
        mock_mr = mock.Mock(iid='1234', target_branch='main')
        kernel_src = '/src/linux'
        ret_branch, ret_dir = kgit.prep_temp_merge_branch(remote, mock_mr, kernel_src)
        mock_stale.assert_called_once()
        mock_add.assert_called_once()
        self.assertEqual(ret_branch, 'foobar-main-1234')
        self.assertEqual(ret_dir, '/src/foobar-main-1234-merge/')

    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.worktree_remove', mock.Mock(return_value=True))
    def test_clean_up_temp_merge_branch(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        merge_branch = 'temp-merge-branch'
        with self.assertLogs('cki.webhook.kgit', level='DEBUG') as logs:
            kgit.clean_up_temp_merge_branch('mocked', merge_branch, worktree_dir)
            self.assertIn(f'Removed worktree {worktree_dir} and deleted branch {merge_branch}',
                          logs.output[-1])
