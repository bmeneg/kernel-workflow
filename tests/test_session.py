"""Tests for the Session and Handler objects."""
from copy import deepcopy
import json
import typing
from unittest import mock

from cki_lib.misc import get_nested_key
from jira.exceptions import JIRAError

from tests import fake_payloads
from tests import fakes
from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook import session
from webhook import session_events
from webhook.common import get_arg_parser
from webhook.rh_metadata import Projects


class TestSessionFunctions(NoSocketTestCase):
    """Tests for the new_session and get_session functions."""

    def setUp(self):
        """Reset the SESSION global to None."""
        session.SESSION = None

    def tearDown(self):
        """Reset the SESSION global to None."""
        session.SESSION = None

    def test_new_base_session(self):
        """Returns a new BaseSession object and sets the SESSION global."""
        test_session = session.new_session('bughook')
        self.assertIsInstance(test_session, session.BaseSession)
        self.assertIs(test_session, session.SESSION)
        self.assertIs(test_session, session.get_session())

    def test_new_session_runner(self):
        """Returns a new SessionRunner object and sets the SESSION global."""
        handlers = {session.MessageType.AMQP: mock.Mock(name='amqp_handler_func')}
        test_session = session.new_session('buglinker', handlers=handlers)
        self.assertIsInstance(test_session, session.SessionRunner)
        self.assertIs(test_session, session.SESSION)
        self.assertIs(test_session, session.get_session())

    def test_get_session(self):
        """Raises a RuntimeError when the SESSION global is not set."""
        with self.assertRaises(RuntimeError):
            session.get_session()


class TestHelpers(NoSocketTestCase):
    """Tests for the extra functions."""

    def test_make_logging_extras_amqp(self):
        """Returns a dict of some extra logging values."""
        headers = {}
        tests = [
            # (expected_result, body)
            # amqp-bridge event happy with nothing
            ({'bugzilla_id': None, 'bugzilla_user': None}, {}),
            # amqp-bridge event with something to say
            ({'bugzilla_id': 123456, 'bugzilla_user': 'user@example.com'},
             {'event': {'bug_id': 123456, 'user': {'login': 'user@example.com'}}}),
        ]

        for expected_result, body in tests:
            with self.subTest(expected_result=expected_result, body=body):
                self.assertEqual(
                    expected_result,
                    session.make_amqp_logging_extras(headers, body)
                )

    def test_make_logging_extras_umb(self):
        """Returns a dict of some extra logging values."""
        tests = [
            # (expected_result, headers, body)
            # umb-bridge event happy with nothing
            ({}, {}, {}),
            # umb-bridge event with something to say
            ({'mr_id': 3456, 'path_with_namespace': 'group/project', 'event_source': 'bugzilla'},
             {'source': 'bugzilla'},
             {'mrpath': 'group/project!3456'})
        ]

        for expected_result, headers, body in tests:
            with self.subTest(expected_result=expected_result, headers=headers, body=body):
                self.assertEqual(
                    expected_result,
                    session.make_umb_bridge_logging_extras(headers, body)
                )

    def test_make_dw_logging_extras(self):
        """Test session.make_dw_logging_extras works as expected."""
        tests = [
            # (description, expected, body)
            (
                "Empty body",
                {},
                {"status": None, "object_type": None, "object_id": None},
            ),
            (
                "Full body",
                fake_payloads.DATAWAREHOUSE_PAYLOAD,
                {
                    "status": "checkout_issueoccurrences_changed",
                    "object_type": "checkout",
                    "object_id": "redhat:1",
                },
            ),
        ]
        empty_headers = {}  # make_dw_logging_extras doesn't use headers

        for description, body, expected in tests:
            with self.subTest(expected=expected, body=body):
                result = session.make_dw_logging_extras(empty_headers, body)
                self.assertEqual(expected, result)


class TestBaseSessionInit(NoSocketTestCase):
    """Tests for the initilization of the BaseSession class."""

    ENV_NAMES = ['development', 'staging', 'production']

    NATTRS = {'body': 'This should be unique per webhook\n\nEverything is fine, I swear...',
              'author': {'username': 'shadowman'},
              'id': '1234'}

    @mock.patch.object(Projects, 'do_load_policies')
    def test_init_webhook_exists(self, mock_do_load_policies):
        """Sets up a new BaseSession object and reports the correct environment."""
        # List of webhook names to create sessions for.
        defined_webhooks = list(Projects().webhooks.keys())
        self.assertGreater(len(defined_webhooks), 3)

        # Loop over the good webhook names and create/test a BaseSession
        for webhook_name in defined_webhooks:
            for mock_env in ('development', 'production', 'staging'):
                with self.subTest(mock_env=mock_env, webhook_name=webhook_name):
                    # Reset Projects.do_load_policies() mock.
                    mock_do_load_policies.reset_mock()
                    with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': mock_env}):
                        test_session = session.BaseSession(webhook_name, 'args',
                                                           load_policies=True)
                        test_session.rh_projects.do_load_policies.assert_called()
                        self.assertEqual(test_session.environment, mock_env)
                        match mock_env:
                            case 'development':
                                self.assertFalse(test_session.is_production)
                                self.assertFalse(test_session.is_staging)
                                self.assertFalse(test_session.is_production_or_staging)
                                self.assertIn('env: development', str(test_session))
                            case 'production':
                                self.assertTrue(test_session.is_production)
                                self.assertFalse(test_session.is_staging)
                                self.assertTrue(test_session.is_production_or_staging)
                                self.assertIn('env: PRODUCTION', str(test_session))
                            case 'staging':
                                self.assertFalse(test_session.is_production)
                                self.assertTrue(test_session.is_staging)
                                self.assertTrue(test_session.is_production_or_staging)
                                self.assertIn('env: staging', str(test_session))

                        gl_mergerequest = mock.Mock()
                        discussion = mock.Mock(attributes={'notes': [self.NATTRS]})
                        # discussion.notes.get.return_value = '1234'
                        gl_mergerequest.discussions.list.return_value = [discussion]
                        bot_name = "shadowman"
                        identifier = "This should be unique per webhook"
                        new_comment = "This is my new comment"
                        with self.assertLogs('cki.webhook.session', level='DEBUG') as logs:
                            test_session.update_webhook_comment(
                                gl_mergerequest, new_comment, bot_name=bot_name,
                                identifier=identifier)
                            if test_session.is_production_or_staging:
                                self.assertIn("Overwriting existing webhook comment",
                                              ' '.join(logs.output))
                            else:
                                self.assertIn("Would overwrite existing webhook comment",
                                              ' '.join(logs.output))
                            gl_mergerequest.notes.create.assert_not_called()
                        bot_name = "a_different_user"
                        with self.assertLogs('cki.webhook.session', level='DEBUG') as logs:
                            test_session.update_webhook_comment(
                                gl_mergerequest, new_comment, bot_name=bot_name,
                                identifier=identifier)
                            if test_session.is_production_or_staging:
                                self.assertIn("Creating new webhook comment", ' '.join(logs.output))
                            else:
                                self.assertIn("Would create new webhook comment",
                                              ' '.join(logs.output))
                            if test_session.is_production_or_staging:
                                gl_mergerequest.notes.create.assert_called()

    def test_init_webhook_unknown(self):
        """Raises a RuntimeError when the webhook name is not found in Projects."""
        with self.assertRaises(RuntimeError):
            session.BaseSession('fake hook name', 'args')


class TestBaseSessionAPIProperties(NoSocketTestCase):
    """Tests for the BaseSession class properties for APIs."""

    def test_gl_instance(self):
        """Returns a python-Gitlab REST API instance."""
        with mock.patch('webhook.session.get_instance') as mock_get_instance:
            mock_instance = mock.Mock(spec=['auth', 'user'])
            mock_get_instance.return_value = mock_instance
            test_session = session.BaseSession('ack_nack', 'gl_instance')

            # This is a cached property so webhook.session.get_instance should only be called once.
            self.assertIs(test_session.gl_instance, mock_instance)
            self.assertIs(test_session.gl_instance, mock_instance)
            self.assertIs(test_session.gl_instance, mock_instance)
            mock_get_instance.assert_called_once()
            # Must have called auth().
            mock_instance.auth.assert_called_once()
            # The gl_user property returns the instance user.
            self.assertEqual(test_session.gl_user, mock_instance.user)

        # Raises a RuntimeError if we're not authenticated.
        with mock.patch('webhook.session.get_instance') as mock_get_instance:
            mock_instance = mock.Mock(spec=['auth'])
            mock_get_instance.return_value = mock_instance
            with self.assertRaises(RuntimeError):
                test_session = session.BaseSession('ack_nack', 'gl_instance')
                test_session.gl_instance

    def test_graphql(self):
        """Returns a GitlabGraph instance."""
        with mock.patch('webhook.session.GitlabGraph') as mock_gitlabgraph:
            mock_instance = mock.Mock(spec=['user'])
            mock_gitlabgraph.return_value = mock_instance
            test_session = session.BaseSession('ack_nack', 'graphql')

            # This is a cached property so webhook.session.graphql should only be called once.
            self.assertIs(test_session.graphql, mock_instance)
            self.assertIs(test_session.graphql, mock_instance)
            self.assertIs(test_session.graphql, mock_instance)
            mock_gitlabgraph.assert_called_once()

        # Raises a RuntimeError if we're not authenticated.
        with mock.patch('webhook.session.GitlabGraph') as mock_gitlabgraph:
            mock_instance = mock.Mock(spec=['user'])
            mock_instance.user = None
            mock_gitlabgraph.return_value = mock_instance
            with self.assertRaises(RuntimeError):
                test_session = session.BaseSession('ack_nack', 'graphql')
                test_session.graphql

    def test_jira(self):
        """Returns a Jira instance."""
        with mock.patch('webhook.session.connect_jira') as mock_connect_jira:
            mock_instance = mock.Mock(spec=['myself'])
            mock_connect_jira.return_value = mock_instance
            test_session = session.BaseSession('ack_nack', 'jira')

            # This is a cached property so webhook.session.jira should only be called once.
            self.assertIs(test_session.jira, mock_instance)
            self.assertIs(test_session.jira, mock_instance)
            self.assertIs(test_session.jira, mock_instance)
            mock_connect_jira.assert_called_once()

        # Raises a RuntimeError if we're not authenticated.
        with mock.patch('webhook.session.connect_jira') as mock_connect_jira:
            mock_instance = mock.Mock(spec=['myself'])
            mock_instance.myself.side_effect = JIRAError('oh no!')
            mock_connect_jira.return_value = mock_instance
            with self.assertRaises(RuntimeError):
                test_session = session.BaseSession('ack_nack', 'graphql')
                test_session.jira

    def test_get_gl_group(self) -> None:
        """Gets a group from the cache (or adds it), and then returns it."""
        gl_group_spec = ['id', 'full_path']

        def make_mock_group(gid: int, full_path: str) -> mock.Mock:
            mock_gl_group = mock.Mock(spec_set=gl_group_spec, full_path=full_path)
            mock_gl_group.id = gid
            return mock_gl_group

        class GroupTest(typing.NamedTuple):
            matching_item: typing.Tuple[int, str]
            group_id: typing.Union[int, str]
            groups_get_called: bool

        tests = [
            # Empty cache, calls groups.get() by id and adds result to cache.
            GroupTest(
                matching_item=(1234, 'group/subgroup'),
                group_id=1234,
                groups_get_called=True
            ),
            # Empty cache, calls groups.get() by namespace and adds result to cache.
            GroupTest(
                matching_item=(1234, 'group/subgroup'),
                group_id='group/subgroup',
                groups_get_called=True
            ),
            # Finds the item in the cache.
            GroupTest(
                matching_item=(1234, 'group/subgroup'),
                group_id=1234,
                groups_get_called=False
            ),
            # Finds the item in the cache.
            GroupTest(
                matching_item=(1234, 'group/subgroup'),
                group_id='group/subgroup',
                groups_get_called=False
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = session.BaseSession('ack_nack', '')
                test_session.gl_instance = mock.Mock()
                matching_item = make_mock_group(*test.matching_item)
                if test.groups_get_called:
                    test_session.gl_instance.groups.get.side_effect = [matching_item]
                else:
                    test_session._gl_groups.append(matching_item)

                self.assertEqual(matching_item, test_session.get_gl_group(test.group_id))
                if test.groups_get_called:
                    test_session.gl_instance.groups.get.assert_called_once_with(test.group_id)
                else:
                    test_session.gl_instance.groups.get.assert_not_called()
                self.assertTrue(matching_item in test_session._gl_groups)

    def test_gl_labels(self) -> None:
        """Returns a LabelsCache object."""
        test_session = session.BaseSession('ack_nack', '')
        with mock.patch('webhook.session.LabelsCache') as mock_gl_labels:
            self.assertEqual(test_session.gl_labels, mock_gl_labels.return_value)
            mock_gl_labels.assert_called_once_with(session=test_session)


@mock.patch.dict('os.environ', {'TEST_ROUTING_KEYS': 'a.b b.c'})
class TestSessionRunner(NoSocketTestCase):
    """Tests for SessionRunning init and process_* methods."""

    def test_init(self):
        """Sets up the handlers."""
        # All the possible MessageTypes and GitlabObjectKinds.
        message_types = [mtype for mtype in session.MessageType if mtype is not
                         session.MessageType.GITLAB]
        object_kinds = [okind for okind in session.GitlabObjectKind]

        # Create the input handler dict, one of each.
        mock_handlers = {}
        for member in message_types + object_kinds:
            mock_handlers[member] = mock.Mock()

        # Create a SessionRunner.
        test_session = session.SessionRunner('ack_nack', 'args', mock_handlers)

        # We should have the same number of handlers.
        self.assertEqual(len(test_session.handlers), len(mock_handlers))

        for member in message_types + object_kinds:
            # It is mentioned in the repr.
            self.assertIn(member.name, str(test_session))

    def test_build_cmdline_message_raises(self):
        """Raises ValueError if the --merge-request arg is not set."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        with self.assertRaises(ValueError):
            session.SessionRunner.build_cmdline_message(args)

    def test_handle_merge_request_url(self):
        """Handles a --merge-request."""
        mr_url = fake_payloads.MR_URL
        kinds = (session.GitlabObjectKind.MERGE_REQUEST,
                 session.GitlabObjectKind.NOTE,
                 session.GitlabObjectKind.PIPELINE,
                 session.GitlabObjectKind.PUSH)
        handlers = {kind: mock.Mock() for kind in kinds}
        parser = get_arg_parser('TEST')

        tests = [(f'--merge-request {mr_url}', session.GitlabObjectKind.MERGE_REQUEST),
                 (f'--merge-request {mr_url} --note hello', session.GitlabObjectKind.NOTE),
                 (f'--merge-request {mr_url} --action pipeline', session.GitlabObjectKind.PIPELINE),
                 (f'--merge-request {mr_url} --action push', session.GitlabObjectKind.PUSH)
                 ]

        for cmdline, expected_gl_kind in tests:
            with self.subTest(cmdline=cmdline, expected_gl_kind=expected_gl_kind):
                args = parser.parse_args(cmdline.split())

                with mock.patch.object(session.SessionRunner, 'process_one_message') as \
                        mock_process_one_message:
                    test_session = session.SessionRunner('buglinker', args, handlers)
                    result = test_session.run()

                self.assertEqual(result, mock_process_one_message.return_value)

                gitlab_msg = session.SessionRunner.build_cmdline_message(args)
                self.assertIs(gitlab_msg.object_kind, expected_gl_kind)

                mock_process_one_message.assert_called_once_with(
                    routing_key='cmdline',
                    headers={'message-type': 'gitlab'},
                    body=gitlab_msg.as_payload()
                )

    @mock.patch.object(session.SessionRunner, 'process_one_message')
    def test_handle_process_json(self, mock_process_one_message):
        """Handles a --json-message-file."""
        filename = 'jason_file.json'
        handlers = {kind: mock.Mock() for kind in session.GitlabObjectKind}
        parser = get_arg_parser('TEST')
        args = parser.parse_args(f'--json-message-file={filename}'.split())

        payloads = [fake_payloads.MR_PAYLOAD,
                    fake_payloads.NOTE_PAYLOAD,
                    fake_payloads.BUILD_PAYLOAD,
                    fake_payloads.PIPELINE_PAYLOAD,
                    fake_payloads.PUSH_PAYLOAD]

        for count, payload in enumerate(payloads):
            mock_process_one_message.reset_mock()
            with self.subTest(payloads_index=count):
                # Create the Session before we patch pathlib.Path because __post_init__ uses
                # that library to read the rh_metadata.yaml.
                test_session = session.SessionRunner('bughook', args, handlers)

                with mock.patch.object(session.Path, 'read_text') as mock_read_text:
                    mock_read_text.return_value = json.dumps(payload)
                    result = test_session.run()

                self.assertEqual(result, mock_process_one_message.return_value)
                mock_process_one_message.assert_called_once_with(
                    routing_key=filename,
                    headers={'message-type': 'gitlab'},
                    body=json.loads(mock_read_text.return_value)
                )

    @mock.patch.object(session_events.BaseEvent, 'matches_environment')
    def test_process_one_message_no_handler(self, mock_matches_environment):
        """Returns False when there is no registered Handler for the given message type."""
        handlers = {session.GitlabObjectKind.NOTE: mock.Mock()}
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])

        routing_key = 'kwf.gitlab.cki-project.kernel-ark.merge-request'
        headers = {'message-type': 'gitlab'}
        body = {'object_kind': 'merge_request'}

        test_session = session.SessionRunner('bughook', args, handlers)
        self.assertFalse(test_session.process_one_message(routing_key, headers, body))
        handlers[session.GitlabObjectKind.NOTE].assert_not_called()
        mock_matches_environment.assert_not_called()

    @mock.patch('webhook.session.add_labels')
    @mock.patch('webhook.session.create_event')
    def test_process_one_message(self, mock_create_event, mock_add_label):
        """Returns True if should_be_processed was called; optionally calls add_label_to_mr."""
        class ProcessOneTest(typing.NamedTuple):
            """Expected result and input values for a process_one_message test."""
            expected_result: bool
            session_handlers: list[typing.Union[session.MessageType, session.GitlabObjectKind]]
            event_kind: typing.Union[session.MessageType, session.GitlabObjectKind]
            event_matches_environment: bool = False
            event_passes_filters: bool = False
            event_matches_trigger: bool = False
            event_should_be_processed: bool = False

        tests = [
            # No matching handler for event, returns False.
            ProcessOneTest(
                expected_result=False,
                session_handlers=[session.MessageType.JIRA],
                event_kind=session.GitlabObjectKind.MERGE_REQUEST,
            ),
            # Event has a matching handler but does not match environment, returns False.
            ProcessOneTest(
                expected_result=False,
                session_handlers=[session.GitlabObjectKind.NOTE],
                event_kind=session.GitlabObjectKind.NOTE,
            ),
            # Event matches handler but does not pass filters, returns False.
            ProcessOneTest(
                expected_result=False,
                session_handlers=[session.GitlabObjectKind.MERGE_REQUEST],
                event_kind=session.GitlabObjectKind.MERGE_REQUEST,
                event_matches_environment=True
            ),
            # Event matches handler but does not match any trigger, returns False.
            ProcessOneTest(
                expected_result=False,
                session_handlers=[session.GitlabObjectKind.MERGE_REQUEST],
                event_kind=session.GitlabObjectKind.MERGE_REQUEST,
                event_matches_environment=True,
                event_passes_filters=True
            ),
            # Event passes all filters and matches a trigger, returns True.
            ProcessOneTest(
                expected_result=True,
                session_handlers=[session.GitlabObjectKind.MERGE_REQUEST],
                event_kind=session.GitlabObjectKind.MERGE_REQUEST,
                event_matches_environment=True,
                event_passes_filters=True,
                event_matches_trigger=True,
                event_should_be_processed=True
            ),
        ]

        args = get_arg_parser('TEST').parse_args([])
        routing_key = 'kwf.gitlab.cki-project.kernel-ark.merge-request'
        event_spec = [
            'type', 'kind', 'changed_labels', 'namespace', 'mr_url', 'matches_environment',
            'passes_filters', 'matches_trigger'
        ]
        headers = {}
        body = {}

        for test_num, test in enumerate(tests):
            with self.subTest(test_num=test_num, **test._asdict()):
                mock_add_label.reset_mock()

                test_handlers = \
                    {ht: mock.Mock(name=f'{ht.name}_func') for ht in test.session_handlers}
                test_session = session.SessionRunner('subsystems', args, test_handlers)
                test_session.gl_instance = mock.Mock()
                test_session.update_special_labels = mock.Mock()
                test_event = mock.Mock(spec_set=event_spec)
                test_event.type = defs.MessageType.GITLAB
                test_event.kind = test.event_kind
                test_event.matches_environment = test.event_matches_environment
                test_event.passes_filters = test.event_passes_filters
                test_event.matches_trigger = test.event_matches_trigger
                mock_create_event.return_value = test_event

                result = test_session.process_one_message(routing_key, headers, body)
                self.assertIs(test.expected_result, result)

                for handler_type, handler_func in test_handlers.items():
                    if handler_type is test.event_kind:
                        if test.event_matches_environment and test.event_passes_filters:
                            test_session.update_special_labels.assert_called_once()
                        if test.expected_result is True:
                            handler_func.assert_called_once()
                    else:
                        handler_func.assert_not_called()

    @mock.patch('webhook.session.remove_labels')
    @mock.patch('webhook.session.add_labels')
    def _update_special_labels_test(self, test_session, test_event, label_call, label_call_labels,
                                    mock_add_labels, mock_remove_labels):
        """Calls test_session.update_special_labels(test_event) and validates the calls."""
        test_session.update_special_labels(test_event)
        if label_call == 'add_labels':
            expected_label_call = mock_add_labels
            unused_label_call = mock_remove_labels
        elif label_call == 'remove_labels':
            expected_label_call = mock_remove_labels
            unused_label_call = mock_add_labels
        else:
            expected_label_call = None
        if expected_label_call:
            expected_label_call.assert_called_once_with(
                test_session.get_gl_project.return_value, test_event.mr_url.id, label_call_labels
            )
            unused_label_call.assert_not_called()
        else:
            mock_add_labels.assert_not_called()
            mock_remove_labels.assert_not_called()

    def test_update_special_labels(self):
        """Calls add/remove_labels when the Blocked or readyForX labels need updating."""
        webhook_spec = ['manage_special_labels']
        event_spec = ['type',
                      'blocked_label_mismatch',
                      'draft_status',
                      'changed_labels',
                      'namespace',
                      'labels',
                      'mr_url']

        class UpdateSpecialTest(typing.NamedTuple):
            webhook_manage_special_labels: bool = True
            event_message_type: defs.MessageType = defs.MessageType.GITLAB
            event_blocked_label_mismatch: bool = False
            event_changed_labels: typing.List[str] = []
            event_draft_status: bool = False
            event_labels: typing.List[str] = []
            label_call: typing.Literal[None, 'add_labels', 'remove_labels'] = None
            label_call_labels: typing.List[str] = []

        tests = [
            # Webhook does not manage special labels, nothing to do.
            UpdateSpecialTest(
                webhook_manage_special_labels=False
            ),
            # Webhook manages the special labels but event has no relevant changes, nothing to do.
            UpdateSpecialTest(),
            # A ready label changed, calls add_labels.
            UpdateSpecialTest(
                event_changed_labels=['readyForMerge'],
                label_call='add_labels',
            ),
            # The Blocked label needs to be added, calls add_labels with ['Blocked'].
            UpdateSpecialTest(
                event_blocked_label_mismatch=True,
                label_call='add_labels',
                label_call_labels=[defs.BLOCKED_LABEL]
            ),
            # The Blocked label needs to be removed, calls remove_labels with ['Blocked'].
            UpdateSpecialTest(
                event_blocked_label_mismatch=True,
                event_labels=[defs.BLOCKED_LABEL],
                label_call='remove_labels',
                label_call_labels=[defs.BLOCKED_LABEL]
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = session.SessionRunner('ack_nack', [], {})
                test_session.webhook = mock.Mock(
                    spec_set=webhook_spec,
                    manage_special_labels=test.webhook_manage_special_labels
                )
                test_session.get_gl_project = mock.Mock()
                test_event = mock.Mock(
                    spec_set=event_spec,
                    type=test.event_message_type,
                    blocked_label_mismatch=test.event_blocked_label_mismatch,
                    changed_labels=test.event_changed_labels,
                    draft_status=mock.Mock(spec_set=['is_draft'], is_draft=test.event_draft_status),
                    namespace='group/project',
                    labels=test.event_labels,
                    mr_url=defs.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123')
                )
                self._update_special_labels_test(test_session, test_event, test.label_call,
                                                 test.label_call_labels)

    def test_queue(self):
        """Returns a MessageQueue instance."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        handlers = {session.MessageType.AMQP: mock.Mock(name='amqp_func')}

        with mock.patch('webhook.session.MessageQueue') as mock_messagequeue:
            mock_instance = mock.Mock()
            mock_messagequeue.return_value = mock_instance
            test_session = session.SessionRunner('ack_nack', args, handlers)

            # This is a cached property so webhook.session.MessageQueue should only be called once.
            self.assertIs(test_session.queue, mock_instance)
            self.assertIs(test_session.queue, mock_instance)
            self.assertIs(test_session.queue, mock_instance)
            mock_messagequeue.assert_called_once()

            test_session.consume_messages()
            mock_instance.consume_messages.assert_called_once()


class TestSessionRunnerRun(NoSocketTestCase):
    """Tests for the run() method."""

    def test_run_with_merge_request(self):
        """Calls the process_cmdline_message function."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--merge-request https://test.url.string.biz'.split())
        with mock.patch.object(session.SessionRunner, 'process_cmdline_message') as mock_process:
            test_session = session.SessionRunner('bughook', args, {})

            result = test_session.run()
            self.assertIs(result, mock_process.return_value)
            mock_process.assert_called_once()

    def test_run_with_json_message_file(self):
        """Calls the process_json_message function."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--json-message-file /home/user/event.json`'.split())
        with mock.patch.object(session.SessionRunner, 'process_json_message') as mock_process:
            test_session = session.SessionRunner('bughook', args, {})

            result = test_session.run()
            self.assertIs(result, mock_process.return_value)
            mock_process.assert_called_once()

    def test_run_consume_messages(self):
        """Calls the consume_messages function."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--rabbitmq-routing-key a.b.c'.split())
        with mock.patch.object(session.SessionRunner, 'consume_messages') as mock_consume:
            test_session = session.SessionRunner('ckihook', args, {})

            result = test_session.run()
            self.assertIs(result, mock_consume.return_value)
            mock_consume.assert_called_once()

    def test_run_consume_messages_raises(self):
        """Raises a RuntimeError when --rabbitmq-routing-key is not set."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])

        test_session = session.SessionRunner('ckihook', args, {})
        with self.assertRaises(RuntimeError):
            test_session.run()


class HandlerTest(typing.NamedTuple):
    """Data for an individual handler test."""

    expected_result: bool
    object_kind: defs.GitlabObjectKind
    body: dict
    test_session: typing.Optional[session.SessionRunner] = None
    hook_name: str = 'bughook'
    webhook_attrs: dict = {}
    skip_filters: bool = False
    skip_triggers: bool = False
    target_test: typing.Optional[str] = None


class TestGitlabEvent(NoSocketTestCase):
    """Tests for the GitlabEvent class."""

    @mock.patch.object(session.BaseSession, 'gl_instance', mock.Mock())
    @mock.patch.object(session.BaseSession, 'graphql', mock.Mock())
    @mock.patch.object(session.BaseSession, 'jira', mock.Mock())
    def create_session(self, hook_name, cmdline=None, gl_instance=None,
                       graphql=None, jira=None, handlers=None):
        """Return a new Session instance with the given params."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args(cmdline or [])
        if handlers:
            test_session = session.SessionRunner(hook_name, args, handlers)
        else:
            test_session = session.BaseSession(hook_name, args)

        test_session.gl_instance = gl_instance or fakes.FakeGitLab()
        test_session.graphql = graphql or mock.Mock()
        test_session.jira = jira or mock.Mock()

        test_session.gl_instance.auth()
        return test_session

    def run_handler_tests(self, test_list: typing.List[HandlerTest]) -> None:
        """Validate the created GitlabEvent gets the expected should_be_processed value."""
        headers = {'message-type': 'gitlab'}
        for test in test_list:
            with self.subTest(**test._asdict()):
                test_session = test.test_session if test.test_session else \
                    self.create_session(test.hook_name)
                for attr, value in test.webhook_attrs.items():
                    setattr(test_session.webhook, attr, value)
                test_event = session.create_event(test_session, headers, test.body)
                self.assertIs(test_event.type, session.MessageType.GITLAB)
                self.assertIs(test_event.kind, test.object_kind)
                if test.target_test:
                    self.assertEqual(test.expected_result, getattr(test_event, test.target_test))
                else:
                    filters_result = test_event.matches_environment and test_event.passes_filters \
                        if not test.skip_filters else True
                    triggers_result = test_event.matches_trigger if not test.skip_triggers else True
                    self.assertEqual(test.expected_result, filters_result and triggers_result)

    @staticmethod
    def set_payload_project_id_namespace(body, project_id=None, namespace=None):
        """Do what it says."""
        if project_id:
            if variables := get_nested_key(body, 'object_attributes/variables'):
                for var in variables:
                    if var['key'] == 'mr_project_id':
                        var['value'] = project_id
            elif body['object_kind'] == 'build':
                body['project_id'] = project_id
            else:
                body['project']['id'] = project_id
        if namespace and 'project' in body:
            body['project']['path_with_namespace'] = namespace

    def test_should_process_message_no_project(self):
        """Returns False because these payloads have no matching project in the yaml."""
        tests = [
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.MERGE_REQUEST,
                body=deepcopy(fake_payloads.MR_PAYLOAD),
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.NOTE,
                body=deepcopy(fake_payloads.NOTE_PAYLOAD),
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_PAYLOAD),
            ),
        ]
        self.run_handler_tests([tests[1]])

    def test_should_process_message_good_result(self):
        """Returns True because the user, project, environment, and hook are all okay."""
        tests = [
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
                hook_name='ckihook'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
                hook_name='bughook'
            ),
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.MERGE_REQUEST,
                body=deepcopy(fake_payloads.MR_PAYLOAD),
            ),
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.NOTE,
                body=deepcopy(fake_payloads.NOTE_PAYLOAD),
            ),
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_PAYLOAD),
                webhook_attrs={'run_on_pipeline_upstream': True}
            ),
        ]

        # Fix up the payload data.
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        mr_url = f'https://gitlab.com/{namespace}/-/merge_requests/123'
        for test in tests:
            body = test.body
            if body_vars := get_nested_key(body, 'object_attributes/variables'):
                for var in body_vars:
                    if var['key'] == 'mr_url':
                        var['value'] = mr_url
            if 'merge_request' in body:
                body['merge_request']['url'] = mr_url
            # Force the payload project ID to be one that exists in rh_metadata.yaml.
            self.set_payload_project_id_namespace(body, project_id, namespace)

        self.run_handler_tests(tests)

    def test_should_process_message_bot_user(self):
        """Returns False due to event from same user we are logged in as (bot)."""
        # Or, for build & pipelines we don't check the event user so these should pass.
        tests = [
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
                target_test='filter_bots'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.MERGE_REQUEST,
                body=deepcopy(fake_payloads.MR_PAYLOAD),
                target_test='filter_bots'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.NOTE,
                body=deepcopy(fake_payloads.NOTE_PAYLOAD),
                target_test='filter_bots'
            ),
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_PAYLOAD),
                target_test='filter_bots'
            ),
            HandlerTest(
                expected_result=True,
                object_kind=defs.GitlabObjectKind.BUILD,
                body=deepcopy(fake_payloads.BUILD_PAYLOAD),
                target_test='filter_bots'
            ),
        ]
        user_dict = deepcopy(fakes.AUTH_USER)
        user_dict['email'] = user_dict.pop('public_email')
        for test in tests:
            body = test[2]
            body['user'] = user_dict

        # Fix up the payload data.
        user_dict = deepcopy(fakes.AUTH_USER)
        user_dict['email'] = user_dict.pop('public_email')
        for test in tests:
            # Set the payload user to be the same as we're logged in as.
            body['user'] = user_dict

        self.run_handler_tests(tests)

    def test_should_process_message_staging_non_sandbox(self):
        """Returns False as the Project is not sandbox but we are in a Staging env."""
        tests = [
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.MERGE_REQUEST,
                body=deepcopy(fake_payloads.MR_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.NOTE,
                body=deepcopy(fake_payloads.NOTE_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.BUILD,
                body=deepcopy(fake_payloads.BUILD_PAYLOAD),
                target_test='matches_environment'
            ),
        ]

        # Fix up the payload data.
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        for test in tests:
            body = test[2]
            # Force the payload project ID to be one that exists in rh_metadata.yaml.
            self.set_payload_project_id_namespace(body, project_id, namespace)

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.run_handler_tests(tests)

    def test_should_process_message_production_sandbox(self):
        """Returns False as the Project is sandbox and we are in a Production env."""
        tests = [
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.MERGE_REQUEST,
                body=deepcopy(fake_payloads.MR_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.NOTE,
                body=deepcopy(fake_payloads.NOTE_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_PAYLOAD),
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.BUILD,
                body=deepcopy(fake_payloads.BUILD_PAYLOAD),
                target_test='matches_environment'
            ),
        ]

        # Fix up the payload data.
        project_id = 56789
        namespace = 'redhat/rhel/src/kernel/rhel-8-sandbox'
        for test in tests:
            body = test[2]
            # Force the payload project ID to be one that exists in rh_metadata.yaml,
            # this time a sandbox one!
            self.set_payload_project_id_namespace(body, project_id, namespace)

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}):
            self.run_handler_tests(tests)

    def test_should_process_message_no_project_matching(self):
        """Ignores mismatched projects if match_to_projects is False."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--disable-user-check'.split())
        headers = {'message-type': 'gitlab'}
        body = {'object_kind': 'merge_request',
                'project': {'path_with_namespace': 'some/unknown/project'}}

        tests = [
            # (expected_result, webhook_name)
            (False, 'ack_nack'),
            (False, 'bughook'),
            (False, 'buglinker'),
            (False, 'ckihook'),
            (False, 'mergehook'),
            (True, 'sprinter'),
            (True, 'terminator'),
            (False, 'umb_bridge'),
        ]

        for expected_result, webhook_name in tests:
            test_session = session.BaseSession(webhook_name, args)
            test_event = session.create_event(test_session, headers, body)
            with self.subTest(expected_result=expected_result, webhook_name=webhook_name):
                with self.assertLogs('cki.webhook.session_event', level='INFO') as logs:
                    self.assertEqual(expected_result, test_event.matches_environment)
                    # Try to confirm it did it for the right reason.
                    logging_assertion = self.assertIn if expected_result else self.assertNotIn
                    logging_assertion('Skipping Project check for this Event',
                                      '\n'.join(logs.output))

    def test_should_process_message_unexpected_webhook(self):
        """Returns False as the current webhook is not expected on this Project."""
        # Create our own session so we can rip out one of the Project webhooks 😬.
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('ckihook', args)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        del test_session.rh_projects.projects[24152864].webhooks['ckihook']

        tests = [
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD),
                test_session=test_session,
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.MERGE_REQUEST,
                body=deepcopy(fake_payloads.MR_PAYLOAD),
                test_session=test_session,
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.NOTE,
                body=deepcopy(fake_payloads.NOTE_PAYLOAD),
                test_session=test_session,
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.PIPELINE,
                body=deepcopy(fake_payloads.PIPELINE_PAYLOAD),
                test_session=test_session,
                target_test='matches_environment'
            ),
            HandlerTest(
                expected_result=False,
                object_kind=defs.GitlabObjectKind.BUILD,
                body=deepcopy(fake_payloads.BUILD_PAYLOAD),
                test_session=test_session,
                target_test='matches_environment'
            ),
        ]

        # Fix up the payload data.
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        for test in tests:
            body = test[2]
            self.set_payload_project_id_namespace(body, project_id, namespace)

        self.run_handler_tests(tests)

    def test_event_project_build(self):
        """Gets the upstream project_id of a downstream build (job) event."""
        # Mock up the payload.
        upstream_ns = 'redhat/centos-stream/src/kernel/centos-stream-9'
        body = deepcopy(fake_payloads.BUILD_PAYLOAD)
        body['source_pipeline'] = {'project': {'path_with_namespace': upstream_ns}}

        test_session = self.create_session(
            'ckihook',
            handlers={defs.GitlabObjectKind.BUILD: mock.Mock()},
            gl_instance=mock.Mock()
        )
        mock_pipe = mock.Mock()
        mock_pipe.variables.list.return_value = [
            mock.Mock(key='mr_url', value=f'https://gitlab.com/{upstream_ns}/-/merge_requests/1')
        ]
        mock_ds_project = mock.Mock()
        mock_ds_project.pipelines.get.return_value = mock_pipe
        test_session.get_gl_project = mock.Mock(return_value=mock_ds_project)

        test = HandlerTest(
            expected_result=True,
            object_kind=defs.GitlabObjectKind.BUILD,
            body=body,
            test_session=test_session,
        )
        self.run_handler_tests([test])

    def test_filter_messages_bad_note(self):
        """Returns False because a note event is of the wrong type."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('ckihook', args)

        headers = {'message-type': 'gitlab'}
        body = deepcopy(fake_payloads.NOTE_PAYLOAD)
        body['object_attributes']['noteable_type'] = 'Issue'

        test_gl_event = session.create_event(test_session, headers, body)
        self.assertFalse(test_gl_event.filter_note_type)

    def test_filter_messages_closed_mr(self):
        """Returns False because the MR is closed and the action is not 'close'."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('buglinker', args)

        headers = {'message-type': 'gitlab'}
        body = deepcopy(fake_payloads.MR_PAYLOAD)
        body['object_attributes']['state'] = 'closed'
        body['object_attributes']['action'] = 'update'

        test_gl_event = session.create_event(test_session, headers, body)
        self.assertFalse(test_gl_event.filter_status)


@mock.patch.dict('os.environ', {'BUGZILLA_EMAIL': 'bot@example.com'})
class TestAmqpEvent(NoSocketTestCase):
    """Tests for the AmqpHandler."""

    def test_processing_amqp_message(self):
        """Returns True when the event should be processed."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('umb_bridge', args)

        headers = {'message-type': session.MessageType.AMQP}

        tests = [
            # Non-bot user, has bug_id: True
            (True, {'event': {'user': {'login': 'user@example.com'}, 'bug_id': 2356427}}),
            # Bot user (matches BUGZILLA_EMAIL env): False
            (False, {'event': {'user': {'login': 'bot@example.com'}, 'bug_id': 2356427}}),
            # No bug_id: False
            (False, {'event': {'user': {'login': 'user@example.com'}}}),
        ]

        for expected_result, body in tests:
            with self.subTest(expected_result=expected_result, body=body):
                test_amqp_event = session.create_event(test_session, headers, body)
                self.assertIs(
                    expected_result,
                    test_amqp_event.passes_filters and test_amqp_event.matches_trigger
                )


class TestJiraHandler(NoSocketTestCase):
    """Tests for the Jira event handler."""

    def test_processing_jira_message(self):
        """Returns True when the event should be processed."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('bughook', args)
        test_session.jira = mock.Mock()
        headers = {'message-type': session.MessageType.JIRA}

        headers = {}  # We don't look at the jira event headers other than to match message type.

        valid_bot_user = session_events.JIRA_BOT_ACCOUNTS[0]
        linked_url = 'https://gitlab.com/redhat/rhel/src/kernel/rhel-8/-/merge_requests/4824'

        tests = [
            # Non-bot user, issue is in the RHEL space, linked to an MR in known project: True
            (True, {'issue': {'key': 'RHEL-12345'}, 'user': {'name': 'jira_user'}}),
            # Bot user: False
            (False, {'issue': {'key': 'RHEL-12345'}, 'user': {'name': valid_bot_user}}),
            # Not in the RHEL space: False
            (False, {'issue': {'key': 'RHELPLAN-5345'}, 'user': {'name': 'jira_user'}}),
        ]

        for expected_result, body in tests:
            with self.subTest(expected_result=expected_result, body=body):
                with mock.patch('webhook.session_events.get_linked_mrs') as mock_get_linked_mrs:
                    test_jira_event = session_events.JiraEvent(test_session, headers, body)
                    mock_get_linked_mrs.return_value = [linked_url]
                    self.assertIs(
                        expected_result,
                        test_jira_event.passes_filters and test_jira_event.matches_trigger
                    )


class TestUmbBridgeHandler(NoSocketTestCase):
    """Tests for the UmbBridgeHandler class."""

    def test_processing_umb_bridge_message(self):
        """Returns True if the message should be processed."""
        class BridgeTest(typing.NamedTuple):
            """Values to test with."""
            session: str
            headers: dict
            body: dict
            expected_result: bool

        # Generate a set of tests for each relevant webhook.
        webhooks_that_use_umb_bridge = ('bughook', 'jirahook')
        for webhook_name in webhooks_that_use_umb_bridge:
            args = get_arg_parser('TEST').parse_args([])
            test_session = session.BaseSession(webhook_name, args)
            # All the namespaces where this webhook is active.
            namespaces = [project.namespace for project in
                          test_session.rh_projects.projects.values() if
                          webhook_name in project.webhooks]
            # If there are no projects in the yaml where the hook is active then we can't test!
            self.assertTrue(namespaces)

            tests = []
            for namespace in namespaces:
                tests.append(
                    # Project is known and source header key matches: passes
                    BridgeTest(
                        session=test_session,
                        headers={'event_target_webhook': webhook_name},
                        body={'mrpath': f'{namespace}!123'},
                        expected_result=True
                    )
                )
                tests.append(
                    # Header event_target_webhook does not match session webhook: fails
                    BridgeTest(
                        session=test_session,
                        headers={'event_target_webhook': 'signoff'},
                        body={'mrpath': f'{namespace}!123'},
                        expected_result=False
                    )
                )

            tests.append(
                # Event body's mrpath namespace is not in the yaml: fails
                BridgeTest(
                    session=test_session,
                    headers={'event_target_webhook': webhook_name},
                    body={'mrpath': 'some/unknown/namespace!123'},
                    expected_result=False
                )
            )

        for test in tests:
            with self.subTest(**test._asdict()):
                test_bridge_event = \
                    session_events.UmbBridgeEvent(test.session, test.headers, test.body)
                self.assertIs(
                    test.expected_result,
                    test_bridge_event.matches_environment and test_bridge_event.passes_filters and
                    test_bridge_event.matches_trigger
                )
