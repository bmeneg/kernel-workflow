"""Tests for the jissue_tests module."""
import typing
from unittest import mock

from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook import jissue_tests
from webhook.rh_metadata import Projects


class DepOnlyTests(NoSocketTestCase):
    """Validate the tests that are specific to dependency JIRA Issues."""

    @staticmethod
    def _fake_jissue(input_dict):
        """Return a mock object with spec set."""
        attr_dict = input_dict.copy()
        if 'alias' not in attr_dict:
            attr_dict['alias'] = 'FakeJIssue'
        if 'failed_tests' not in attr_dict:
            attr_dict['failed_tests'] = []
        if 'test_failed' not in attr_dict:
            attr_dict['test_failed'] = mock.Mock(return_value=False, spec=[])
        return mock.Mock(spec=list(attr_dict.keys()), **attr_dict)

    def _run_test(self, jissue, test, expected_result=True, expected_scope=None,
                  expected_keep_going=True):
        """Run the given test on the given JIRA Issue."""
        if expected_scope is None:
            expected_scope = jissue_tests.MrScope.READY_FOR_MERGE if expected_result else \
                jissue_tests.MrScope.IN_PROGRESS
        actual_result, actual_scope, actual_keep_going = test(jissue)
        self.assertIs(actual_result, expected_result)
        self.assertIs(actual_scope, expected_scope)
        self.assertIs(actual_keep_going, expected_keep_going)
        if expected_result is True:
            self.assertTrue(test.__name__ not in jissue.failed_tests)
        else:
            self.assertTrue(test.__name__ in jissue.failed_tests)

    def test_ParentCommitsMatch(self):
        """The Dependency MR commits should match the Dependant (parent) MR commits."""
        this_test = jissue_tests.ParentCommitsMatch
        # The MR is not merged and commits == parent_mr_commits
        issue_values = {'commits': [1, 2, 3],
                        'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.OPENED),
                        'parent_mr_commits': [1, 2, 3]}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # The MR is not merged and commits != parent_mr_commits
        issue_values = {'commits': [1, 2, 3],
                        'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.OPENED),
                        'parent_mr_commits': [1, 2, 3, 4]}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False)

        # The MR is merged, but there are still visible commits on the parent that reference it.
        issue_values = {'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.MERGED),
                        'parent_mr_commits': [1, 2, 3]}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False)

        # The MR is merged and the parent MR does not have any commits that reference it 👍.
        issue_values = {'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.MERGED),
                        'parent_mr_commits': []}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

    def test_MRIsNotMerged(self):
        """Passes if the MR is not Merged, otherwise 'fails'."""
        this_test = jissue_tests.MRIsNotMerged
        # It is not merged, passes.
        issue_values = {'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.OPENED)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # It IS merged, fails.
        issue_values = {'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.MERGED)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.READY_FOR_MERGE,
                       expected_keep_going=False)

        # The test is skipped (passes) if the ParentCommitsMatch test failed.
        issue_values = {'mr': mock.Mock(spec=['state'], state=jissue_tests.MrState.MERGED),
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('ParentCommitsMatch')

    def test_InMrDescription(self):
        """Passes if the JIRA Issue is listed in the MR description."""
        this_test = jissue_tests.InMrDescription
        # A JIRA Issue without commits skips ("passes") the test.
        issue_values = {'commits': [], 'in_mr_description': False}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with commits which are in the MR description passes the test.
        issue_values = {'commits': [1, 2, 3], 'in_mr_description': True}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with commits any of which are NOT in the MR description fails the test.
        issue_values = {'commits': [1, 2, 3], 'in_mr_description': False}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.MISSING)

    def test_HasCommits(self):
        """Passes if the JIRA Issue has commits."""
        this_test = jissue_tests.HasCommits
        # A JIRA Issue with commits passes the test.
        issue_values = {'commits': [1]}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue without commits fails the test.
        issue_values = {'commits': []}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.MISSING)

    def test_JIisNotWrongType(self):
        """Passes if the issuetype is in JIRA_SUPPORT_ISSUE_TYPES."""
        this_test = jissue_tests.JIisNotWrongType

        class TypeTest(typing.NamedTuple):
            issue_values: dict
            expected_result: bool

        tests = [
            # No ji_type, fails the test.
            TypeTest(issue_values={'ji_type': ''}, expected_result=False),
            # A good type, passes the test
            TypeTest(issue_values={'ji_type': 'Bug'}, expected_result=True),
            TypeTest(issue_values={'ji_type': 'Story'}, expected_result=True),
            # A type the kwf doesn't support, fails the test.
            TypeTest(issue_values={'ji_type': 'Epic'}, expected_result=False),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_jissue = self._fake_jissue(test.issue_values)
                expected_scope = jissue_tests.MrScope.OK if test.expected_result else \
                    jissue_tests.MrScope.INVALID
                self._run_test(
                    jissue=fake_jissue,
                    test=this_test,
                    expected_result=test.expected_result,
                    expected_scope=expected_scope,
                    expected_keep_going=test.expected_result
                )

    def test_JIisNotUnknown(self):
        """Passes if the JIStatus is not UNKNOWN."""
        this_test = jissue_tests.JIisNotUnknown
        # A JIRA Issue with a not UNKNOWN status passes the test.
        issue_values = {'ji_status': jissue_tests.JIStatus.IN_PROGRESS}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with an UNKNOWN status fails the test.
        issue_values = {'ji_status': jissue_tests.JIStatus.UNKNOWN}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.INVALID, expected_keep_going=False)

    def test_JIisNotClosed(self):
        """Passes if the JIStatus is not CLOSED."""
        this_test = jissue_tests.JIisNotClosed
        # A JIRA Issue with a not Closed status passes the test.
        issue_values = {'ji_status': jissue_tests.JIStatus.READY_FOR_QA}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with a CLOSED status fails the test.
        issue_values = {'ji_status': jissue_tests.JIStatus.CLOSED}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.CLOSED, expected_keep_going=False)

    def test_NotUntagged(self):
        """Passes if the JIRA Issue is not the special UNTAGGED variant."""
        this_test = jissue_tests.NotUntagged
        # A "normal" JIRA Issue passes to the test.
        issue_values = {'untagged': False}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # The UNTAGGED JIRA Issue fails the test.
        issue_values = {'untagged': True}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.MISSING, expected_keep_going=False)

    def test_JIhasPriority(self):
        """Passes if the JIRA Issues, when referencing a CVE, has Priority set"""
        this_test = jissue_tests.JIhasPriority

        # some definitions
        TEST_CVE_ARRAY = ["CVE-2024-12345"]
        TEST_PRIORITY = jissue_tests.JIPriority.NORMAL

        # from webhook.jissue import JIssue
        JIssue = __import__("webhook.jissue", fromlist=["JIssue"]).JIssue

        # test "plain" issue - nothing set
        fake_jissue = JIssue()
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=True)

        # test issue with Priority no CVEs set
        with mock.patch('webhook.jissue.JIssue.ji_priority', new_callable=mock.PropertyMock) \
                as mock_ji_priority:
            mock_ji_priority.return_value = TEST_PRIORITY
            fake_jissue = JIssue()
            self._run_test(jissue=fake_jissue, test=this_test, expected_result=True)

        # test issue without Priority, but CVE in labels
        with mock.patch('webhook.jissue.JIssue.labels', new_callable=mock.PropertyMock) \
                as mock_labels:
            mock_labels.return_value = TEST_CVE_ARRAY
            fake_jissue = JIssue()
            self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                           expected_scope=jissue_tests.MrScope.NEEDS_REVIEW,
                           expected_keep_going=True)

        # test issue with Priority and CVE in labels
        with mock.patch('webhook.jissue.JIssue.ji_priority', new_callable=mock.PropertyMock) \
                as mock_ji_priority, \
                mock.patch('webhook.jissue.JIssue.labels', new_callable=mock.PropertyMock) \
                as mock_labels:
            mock_ji_priority.return_value = TEST_PRIORITY
            mock_labels.return_value = TEST_CVE_ARRAY
            fake_jissue = JIssue()
            self._run_test(jissue=fake_jissue, test=this_test, expected_result=True)

    def test_CveInMrDescription(self):
        """Passes if the CVE IDs in the JIRA Issue summary are in the MR description."""
        this_test = jissue_tests.CveInMrDescription

        # No ji_cves, test passes.
        fake_jissue = self._fake_jissue({'ji_cves': []})
        self._run_test(jissue=fake_jissue, test=this_test)

        mr_cves_spec = ['cve_ids', 'in_mr_description']
        mr_cves = [mock.Mock(spec=mr_cves_spec, cve_ids=['CVE-5621-16611'], in_mr_description=True),
                   mock.Mock(spec=mr_cves_spec, cve_ids=['CVE-1234-26727'], in_mr_description=True)]

        # All the IDs in the summary are in the MR cves list, test passes.
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec='cves', cves=mr_cves)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # Not all the IDs in the summary are in the MR cves list, test fails.
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-2367-8538', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec='cves', cves=mr_cves)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.NEEDS_REVIEW)

        # All the IDs in the summary are in the MR cves list, but CVEs are not in the description,
        # test fails.
        mr_cves[1].in_mr_description = False
        issue_values = {'ji_cves': ['CVE-1234-26727', 'CVE-5621-16611'],
                        'mr': mock.Mock(spec='cves', cves=mr_cves)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.NEEDS_REVIEW)

    def test_IsValidInternal(self):
        """Passes if the JIRA Issue and MR are properly marked internal."""
        this_test = jissue_tests.IsValidInternal
        # A "normal" JIRA Issue passes to the test.
        issue_values = {'internal': False,
                        'mr': mock.Mock(spec=['only_internal_files'], only_internal_files=False)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # An INTERNAL JIRA Issue passes the test when the MR has only_internal_files.
        issue_values = {'internal': True,
                        'mr': mock.Mock(spec=['only_internal_files'], only_internal_files=True)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # An INTERNAL JIRA Issue fails the test when the MR does NOT have only_internal_files.
        issue_values = {'internal': True,
                        'mr': mock.Mock(spec=['only_internal_files'], only_internal_files=False)}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.INVALID)

    def test_IsAssigned(self) -> None:
        """Passes if the issue is assigned to someone."""
        this_test = jissue_tests.IsAssigned
        passing_scope = jissue_tests.MrScope.READY_FOR_MERGE
        failure_scope = jissue_tests.MrScope.NEEDS_REVIEW

        class IsAssignedTest(typing.NamedTuple):
            issue_values: typing.Dict
            expected_result: bool

        tests = [
            # An issue that is assigned to someone passes the test.
            IsAssignedTest(
                issue_values={'assignee': mock.Mock()},
                expected_result=True
            ),
            # An issue that is not assigned fails the test.
            IsAssignedTest(
                issue_values={'assignee': None},
                expected_result=False
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_jissue = self._fake_jissue(test.issue_values)
                expected_scope = passing_scope if test.expected_result else failure_scope
                self._run_test(jissue=fake_jissue, test=this_test,
                               expected_result=test.expected_result, expected_scope=expected_scope)

    def test_HasQAContact(self) -> None:
        """Passes if the issue has a QA Contact."""
        this_test = jissue_tests.HasQAContact
        passing_scope = jissue_tests.MrScope.READY_FOR_MERGE
        failure_scope = jissue_tests.MrScope.NEEDS_REVIEW

        class HasQAContactTest(typing.NamedTuple):
            issue_values: typing.Dict
            expected_result: bool

        tests = [
            # An issue that is assigned to someone passes the test.
            HasQAContactTest(
                issue_values={'qa_contact': mock.Mock()},
                expected_result=True
            ),
            # An issue that is not assigned fails the test.
            HasQAContactTest(
                issue_values={'qa_contact': None},
                expected_result=False
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fake_jissue = self._fake_jissue(test.issue_values)
                expected_scope = passing_scope if test.expected_result else failure_scope
                self._run_test(jissue=fake_jissue, test=this_test,
                               expected_result=test.expected_result, expected_scope=expected_scope)

    def test_QEApproved(self):
        """Passes if the ITM field has a value set."""
        this_test = jissue_tests.QEApproved
        mock_ji = mock.Mock()

        # No ITM set, do not pass check
        mock_ji.fields.customfield_12321040 = None
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

        # No ITM set, but it's z-stream
        mock_ji.fields.customfield_12321040 = None
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.9.0.z')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # No ITM set, but it's RHEL-6-ELS
        mock_ji.fields.customfield_12321040 = None
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-6-els')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # ITM is set, test should pass
        mock_ji.fields.customfield_12321040 = mock.Mock(value='1')
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

    def test_DevApproved(self):
        """Passes if the DTM field has a value set."""
        this_test = jissue_tests.DevApproved
        mock_ji = mock.Mock()

        # No DTM set, do not pass check
        mock_ji.fields.customfield_12318141 = None
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

        # No DTM set, but it's z-stream
        mock_ji.fields.customfield_12318141 = None
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.9.0.z')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # DTM is set, test should pass
        mock_ji.fields.customfield_12318141 = mock.Mock(value='1')
        issue_values = {'ji': mock_ji, 'ji_fix_version': defs.FixVersion('rhel-8.10.0')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

    def test_JIisNotNewMRisDraft(self):
        """Passes if the Jira issue isn't in New and MR isn't in Draft."""
        this_test = jissue_tests.JIisNotNewMRisDraft

        # This should fail
        mock_mr = mock.Mock(is_draft=True)
        issue_values = {'ji_status': jissue_tests.JIStatus.NEW, 'mr': mock_mr}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test,
                       expected_scope=jissue_tests.MrScope.NEW, expected_result=False)

        # This should succeed
        mock_mr = mock.Mock(is_draft=False)
        issue_values = {'ji_status': jissue_tests.JIStatus.PLANNING, 'mr': mock_mr}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

    def test_CommitPolicyApproved(self):
        """Passes if the policy check passed."""
        this_test = jissue_tests.CommitPolicyApproved
        # An Approved JIRA Issue passes the test.
        issue_values = {'policy_check_ok': (True, '')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A not-approved JIRA Issue fails the test.
        issue_values = {'policy_check_ok': (False, 'No JIRA Issue')}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False)

    def test_PrelimTestingPass(self):
        """Passes if the JIRA Issue is Tested."""
        this_test = jissue_tests.PrelimTestingPass
        # A JIRA Issue with Preliminary Testing: Pass passes the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue without Preliminary Testing: Pass fails the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.FAIL}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.READY_FOR_QA)

        # The test is skipped (passes) if the CommitPolicyApproved test failed.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('CommitPolicyApproved')

    def test_NotPrelimTestingFail(self):
        """Passes if the JIRA Issue did not Fail Preliminary Testing."""
        this_test = jissue_tests.NotPrelimTestingFail
        # A JIRA Issue without Preliminary Testing: Fail passes the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with Preliminary Testing: Fail fails the test.
        issue_values = {'ji_pt_status': defs.JIPTStatus.FAIL}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

        # The test is skipped (passes) if the CommitPolicyApproved test failed.
        issue_values = {'ji_pt_status': defs.JIPTStatus.PASS,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('CommitPolicyApproved')

    def test_TargetReleaseSet(self):
        """Passes if either ITR or ZTR are set."""
        this_test = jissue_tests.TargetReleaseSet
        mock_ji = mock.Mock()
        mock_ji.fields = mock.Mock()
        # A JIRA Issue with no JIRA Issue skips (passes) the test.
        issue_values = {'ji': None}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue without a fix version fails the test.
        mock_ji.fields.fixVersions = []
        issue_values = {'ji': mock_ji, 'ji_fix_version': []}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

    def test_CentOSZStream(self):
        """Passes if a c9s JIRA Issue does not have zstream_target_release set."""
        this_test = jissue_tests.CentOSZStream

        mock_project = mock.Mock(spec=['name'])
        mock_project.name = 'centos-stream-9'
        # A JIRA Issue that is a dependency skips (passes) the test.
        issue_values = {'is_dependency': True,
                        'mr': mock.Mock(spec=['project'], project=mock_project),
                        'ji': mock.Mock(spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue in a non-c9s MR (passes) the test.
        mock_project.name = 'rhel-9'
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['project'], project=mock_project),
                        'ji': mock.Mock(spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue without a jissue (passes) the test.
        mock_project.name = 'centos-stream-9'
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['project'], project=mock_project), 'ji': None}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue without a ji_branch fails the test.
        mock_ji = mock.Mock(spec=[])
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['project'], project=mock_project),
                        'ji': mock_ji, 'ji_branch': None}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

        # A JIRA Issue with a zstream ji_branch fails the test.
        mock_branch = mock.Mock(spec=['zstream_target_release'], zstream_target_release='9.0')
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['project'], project=mock_project),
                        'ji': mock_ji, 'ji_branch': mock_branch}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

        # A JIRA Issue with a ystream ji_branch passes the test.
        mock_branch = mock.Mock(spec=['internal_target_release', 'zstream_target_release'],
                                internal_target_release='9.1', zstream_target_release='')
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['project'], project=mock_project),
                        'ji': mock_ji, 'ji_branch': mock_branch}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue that failed 'TargetReleaseSet' skips (passes) the test.
        mock_branch = mock.Mock(spec=['internal_target_release', 'zstream_target_release'],
                                internal_target_release='9.1', zstream_target_release='')
        issue_values = {'is_dependency': False,
                        'mr': mock.Mock(spec=['project'], project=mock_project),
                        'ji': mock_ji, 'ji_branch': mock_branch,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('TargetReleaseSet')

    def test_ComponentMatches(self):
        """Passes if the MR target branch component matches the JIRA Issue's component."""
        this_test = jissue_tests.ComponentMatches
        mock_mr = mock.Mock(spec=['branch'])
        mock_branch1 = mock.Mock(spec=['components', 'version'],
                                 components={'kernel'}, version='9.4')
        mock_branch2 = mock.Mock(spec=['components', 'version'],
                                 components={'kernel'}, version='8.9')

        # A jissue with a component that is in the MR Branch passes the test.
        mock_mr.branch = mock_branch1
        issue_values = {'ji': mock.Mock(), 'mr': mock_mr, 'ji_components': {'kernel'},
                        'ji_cves': []}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with a component not matching the MR branch component fails the test.
        issue_values = {'ji': mock.Mock(), 'mr': mock_mr, 'ji_components': {'systemd'},
                        'ji_cves': []}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False)

        # A JIRA Issue without a ji skips (passes) the test.
        issue_values = {'ji': None}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue that failed the CentOSZStream test skips (passes) the test.
        issue_values = {'ji': None, 'mr': mock_mr,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('CentOSZStream')

        # A JIRA Issue with a different component and CVEs passes the test.
        mock_mr.branch = mock_branch2
        issue_values = {'ji': mock.Mock(), 'mr': mock_mr, 'ji_components': {'kernel-rt'},
                        'ji_cves': ['CVE-1991-00001']}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

    def test_BranchMatches(self):
        """Passes if the MR target Branch matches the JIRA Issue's Branch."""
        this_test = jissue_tests.BranchMatches
        mock_branch1 = mock.Mock(spec=['version'], version='9.0')
        mock_branch2 = mock.Mock(spec=['version'], version='9.4')

        # A JIRA Issue with an MR branch version >= 9.3 always passes the test
        mock_mr = mock.Mock(spec=['branch'], branch=mock_branch2)
        issue_values = {'ji_branch': mock_branch2, 'mr': mock_mr}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with a ji_branch that matches the MR branch passes the test.
        mock_mr = mock.Mock(spec=['branch'], branch=mock_branch1)
        issue_values = {'ji_branch': mock_branch1, 'mr': mock_mr}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)

        # A JIRA Issue with a ji_branch that does not match the MR branch fails the test.
        issue_values = {'ji_branch': mock_branch2, 'mr': mock_mr}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test, expected_result=False,
                       expected_scope=jissue_tests.MrScope.PLANNING)

        # A JIRA Issue that failed CentOSZStream or ComponentMatches skips (passes) the test.
        issue_values = {'ji_branch': mock_branch1, 'mr': mock_mr,
                        'test_failed': mock.Mock(return_value=True, spec=[])}
        fake_jissue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_jissue, test=this_test)
        issue_values['test_failed'].assert_called_once_with('TargetReleaseSet')

    def test_ComponentIsKernel(self) -> None:
        """Passes if the MR target issue component is kernel."""
        this_test = jissue_tests.ComponentIsKernel
        issue_values = {'ji': mock.Mock(), 'ji_components': {'kernel-rt'}}
        fake_issue = self._fake_jissue(issue_values)
        self._run_test(jissue=fake_issue, test=this_test)

    def test_CvePriority(self):
        """Passes if the CVE's lead clone is on errata."""
        this_test = jissue_tests.CvePriority

        # Skips (passes) the test if the CVE Priority is less than MAJOR.
        cve_values = {'alias': 'FakeCve', 'ji_priority': jissue_tests.JIPriority.NORMAL}
        fake_cve = self._fake_jissue(cve_values)
        self._run_test(jissue=fake_cve, test=this_test)

        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        project1 = projects.projects[12345]
        branch1 = project1.branches[4]   # '8.5', ZTR 8.5.0
        branch2 = project1.branches[2]   # '8.6', ZTR 8.6.0
        branch3 = project1.branches[0]   # 'main', ITR 8.7.0

        ji1 = mock.Mock()
        ji1.fields = mock.Mock()
        ji1.fields.components = ['kernel']
        jissue1 = self._fake_jissue({'ji': ji1, 'ji_branch': branch1, 'ji_project': project1,
                                    'ji_resolution': None})
        ji2 = mock.Mock()
        ji2.fields = mock.Mock()
        ji2.fields.components = ['kernel']
        jissue2 = self._fake_jissue({'ji': ji2, 'ji_branch': branch2, 'ji_project': project1,
                                    'ji_resolution': None})
        ji3 = mock.Mock()
        ji3.fields = mock.Mock()
        ji3.fields.components = ['kernel']
        jissue3 = self._fake_jissue({'ji': ji3, 'ji_branch': branch3, 'ji_project': project1,
                                    'ji_resolution': None})

        # Not rhel6 or 7 and the lead_clone Branch is not higher than the parent_clone, passes.
        cve_values = {'alias': 'FakeCve', 'ji_priority': jissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [jissue1, jissue2, jissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch2)}
        fake_cve = self._fake_jissue(cve_values)
        self._run_test(jissue=fake_cve, test=this_test)

        # Not rhel6 or 7 and the lead_clone is higher than the parent and not on ERRATA, fails.
        cve_values = {'alias': 'FakeCve', 'ji_priority': jissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [jissue1, jissue2, jissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_jissue(cve_values)
        self._run_test(jissue=fake_cve, test=this_test, expected_result=False)

        # Not rhel6 or 7 and the lead_clone is higher than the parent is on ERRATA, passes.
        jissue2 = self._fake_jissue({'ji': ji2, 'ji_branch': branch2, 'ji_project': project1,
                                    'ji_resolution': jissue_tests.JIResolution.ERRATA})
        cve_values = {'alias': 'FakeCve', 'ji_priority': jissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [jissue1, jissue2, jissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_jissue(cve_values)
        self._run_test(jissue=fake_cve, test=this_test)

        # rhel7 so the lead_clone branch targets 'main'.
        project1.__dict__['name'] = 'rhel-7'
        jissue2 = self._fake_jissue({'ji': ji2, 'ji_branch': branch2, 'ji_project': project1,
                                    'ji_resolution': None})
        jissue3 = self._fake_jissue({'ji': ji3, 'ji_branch': branch3, 'ji_project': project1,
                                    'ji_resolution': jissue_tests.JIResolution.ERRATA})
        cve_values = {'alias': 'FakeCve', 'ji_priority': jissue_tests.JIPriority.MAJOR,
                      'ji_depends_on': [jissue1, jissue2, jissue3],
                      'parent_mr': mock.Mock(spec=['branch'], branch=branch1)}
        fake_cve = self._fake_jissue(cve_values)
        self._run_test(jissue=fake_cve, test=this_test)
        project1.__dict__['name'] = 'rhel-8'

    @mock.patch("webhook.jissue_tests.fetch_issues")
    def test_CveKernelRTVariant(self, fetch_issues: mock.Mock) -> None:
        this_test = jissue_tests.CveKernelRTVariant
        cve = 'CVE_-2024-12345'

        issue_values = {
            'ji': mock.Mock(),
            'ji_fix_version': defs.FixVersion('rhel-9.2.0.z'),
            'ji_cves': [cve],
            'ji_components': {'kernel'},
            'ji_branch': mock.Mock(components=['kernel', 'kernel-rt']),
        }

        jissue = self._fake_jissue(issue_values)
        issue_values['ji_components'] = {'kernel-rt'}
        fetch_issues.side_effect = [[self._fake_jissue(issue_values)], []]

        with self.subTest("Issue has a kernel-rt variant"):
            self._run_test(jissue=jissue, test=this_test)

        with self.subTest("Issue doesn't has a kernel-rt variant"):
            self._run_test(jissue=jissue, test=this_test, expected_result=False)

        with self.subTest("Branch doesn't have kernel-rt component"):
            issue_values['ji_branch'].components = ['kernel']
            jissue = self._fake_jissue(issue_values)
            self._run_test(jissue=jissue, test=this_test)
