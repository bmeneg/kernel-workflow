"""Tests for the bughook."""
from datetime import datetime
from unittest import mock

from tests import fakes
from tests import fakes_bz
from tests import fakes_mrs
from tests.no_socket_test_case import NoSocketTestCase
from webhook import bughook
from webhook import defs
from webhook.bug import Bug
from webhook.description import MRDescription
from webhook.rh_metadata import Projects
from webhook.session import SessionRunner

# expected assertEquals check results for fakes_mrs.MR309
MR309_EQUALS = {'mr_id': 309,
                'global_id': fakes_mrs.MR309_DICT['mr']['id'],
                'depends_mrs': [],
                'description': MRDescription(fakes_mrs.MR309_DICT['mr']['description'],
                                             namespace='group/centos-stream-9'),
                'all_bz_ids': {1234567, 2323232},
                'first_dep_sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3'}

MR309_IS = {'project': None,
            'branch': None,
            'is_dependency': False,
            'state': defs.MrState.OPENED,
            'only_internal_files': False,
            'pipeline_finished': None,
            'is_draft': False,
            'has_internal': False,
            'has_untagged': False}

# expected assertEquals check results for fakes_mrs.MR309
MR303_EQUALS = {'mr_id': 303,
                'global_id': fakes_mrs.MR303_DICT['mr']['id'],
                'depends_mrs': [],
                'description': MRDescription(fakes_mrs.MR303_DICT['mr']['description'],
                                             namespace='group/centos-stream-9'),
                'pipeline_finished':
                    datetime.fromisoformat(
                        fakes_mrs.MR303_DICT['mr']['headPipeline']['finishedAt'][:19]
                ),
                'all_bz_ids': {2323232},
                'first_dep_sha': ''}

MR303_IS = {'project': None,
            'is_dependency': True,
            'branch': None,
            'state': defs.MrState.OPENED,
            'only_internal_files': False,
            'is_draft': False,
            'has_internal': False,
            'has_untagged': False}

DEP_MR10 = {'iid': '10', 'state': 'opened', 'targetBranch': 'main',
            'description': 'Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=5556667\n'}

DEP_MR20 = {'iid': '20', 'state': 'merged', 'targetBranch': 'main',
            'description': 'Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=7778889\n'}

DEP_MR30 = {'iid': '30', 'state': 'opened', 'targetBranch': 'main',
            'description': 'Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=9990001\n'
                           f'Depends: {defs.GITFORGE}/group/project/-/merge_requests/20\n'}


@mock.patch('cki_lib.gitlab.get_graphql_client', mock.Mock)
@mock.patch('webhook.libbz.get_bzcon', mock.Mock)
class TestMR(NoSocketTestCase):
    """Tests for the MR dataclass."""

    def _test_mr(self, mr, equals_dict, is_dict):
        print(f'Testing MR {mr.mr_id}...')
        for key, value in equals_dict.items():
            print(f'{key} should be: {value}')
            self.assertEqual(getattr(mr, key), value)
        for key, value in is_dict.items():
            print(f'{key} should be: {value}')
            self.assertIs(getattr(mr, key), value)

    def test_mr_init(self):
        """Sets expected default attribute values."""
        namespace = 'group/project'
        mr_id = 123
        equals_dict = {'namespace': namespace,
                       'mr_id': mr_id,
                       'commits': {},
                       'global_id': '',
                       'depends_mrs': [],
                       'bugs': [],
                       # 'bugs_with_scopes': [],
                       'cves': [],
                       # 'cves_with_scopes': [],
                       'all_bz_ids': set(),
                       'all_descriptions': [],
                       'first_dep_sha': ''}
        is_dict = {'project': None,
                   'is_dependency': False,
                   'description': None,
                   'branch': None,
                   'state': defs.MrState.UNKNOWN,
                   'only_internal_files': False,
                   'pipeline_finished': None,
                   'is_draft': False,
                   'has_internal': False,
                   'has_untagged': False}
        # an empty regular MR
        mr = fakes_mrs.make_mr(namespace=namespace, mr_id=mr_id)
        self._test_mr(mr, equals_dict, is_dict)
        # a missing MR?
        mr = fakes_mrs.make_mr(namespace=namespace, mr_id=mr_id,
                               query_results_list=[{'mr': None}])
        self._test_mr(mr, equals_dict, is_dict)
        # an empty dependency MR
        mr = fakes_mrs.make_mr(namespace=namespace, mr_id=mr_id, is_dependency=True)
        is_dict['is_dependency'] = True
        self._test_mr(mr, equals_dict, is_dict)

    @mock.patch('webhook.bug.is_bug_ready')
    @mock.patch('webhook.libbz._getbugs')
    def test_mr_load_data_309(self, mock_getbugs, mock_is_bug_ready):
        """Updates attributes with data from the query."""
        mock_getbugs.side_effect = [[fakes_bz.BZ1234567, fakes_bz.BZ2323232], [fakes_bz.BZ3456789]]
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_bug_ready.return_value = False, cpc_err

        mr = fakes_mrs.make_mr('group/centos-stream-9', 309,
                               query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])
        projects = mr.projects
        mr.description.depends_bzs.update({2323232})
        MR309_EQUALS['description'].depends_bzs.update({2323232})
        MR309_IS['project'] = projects.projects[24152864]
        MR309_IS['branch'] = projects.projects[24152864].branches[0]
        self._test_mr(mr, MR309_EQUALS, MR309_IS)
        self.assertCountEqual(mr.commits.keys(), [commit['sha'] for commit in
                                                  fakes_mrs.MR309_DICT['mr']['commits']['nodes']])
        self.assertEqual(len(mr.bugs), 2)
        self.assertEqual(len(mr.cves), 1)
        self.assertEqual(len(mr.all_descriptions),
                         len(fakes_mrs.MR309_DICT['mr']['commits']['nodes'])+1)
        self.assertEqual(len(mr.bugs_with_scopes), 2)
        self.assertEqual(len(mr.cves_with_scopes), 1)

    @mock.patch('webhook.bug.is_bug_ready')
    @mock.patch('webhook.libbz._getbugs')
    def test_mr_load_data_303(self, mock_getbugs, mock_is_bug_ready):
        """Updates attributes with data from the query."""
        mock_getbugs.return_value = [fakes_bz.BZ2323232]
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_bug_ready.return_value = False, cpc_err

        mr = fakes_mrs.make_mr('group/centos-stream-9', 303,
                               query_results_list=[fakes_mrs.MR303_DICT], is_dependency=True)
        projects = mr.projects
        MR303_IS['project'] = projects.projects[24152864]
        MR303_IS['branch'] = projects.projects[24152864].branches[0]
        self._test_mr(mr, MR303_EQUALS, MR303_IS)
        self.assertCountEqual(mr.commits.keys(), [commit['sha'] for commit in
                                                  fakes_mrs.MR303_DICT['mr']['commits']['nodes']])
        self.assertEqual(len(mr.bugs), 1)
        self.assertEqual(len(mr.cves), 0)
        self.assertEqual(len(mr.all_descriptions),
                         len(fakes_mrs.MR303_DICT['mr']['commits']['nodes'])+1)
        self.assertEqual(len(mr.bugs_with_scopes), 1)
        self.assertEqual(len(mr.cves_with_scopes), 0)

    def test_mr_has_only_internal_files(self):
        """Returns True if all the paths in the list startwith defs.INTERNAL_FILES."""
        path_list = ['include/net.h', 'redhat/scripts/check.sh']
        self.assertIs(bughook.MR._mr_has_only_internal_files(path_list), False)
        path_list = ['.gitlab.yaml', 'redhat/scripts/check.sh']
        self.assertIs(bughook.MR._mr_has_only_internal_files(path_list), True)


class TestBugRow(NoSocketTestCase):
    """Tests for the BugRow class."""

    def test_bugrow_populate_empty(self):
        """Updates the attribute values of a BugRow."""
        test_bug = Bug.new_missing(bz_id=1234567, mrs=[])
        test_bugrow = bughook.BugRow()
        test_bugrow.populate(test_bug, '')
        self.assertEqual(test_bugrow.BZ, 'BZ-1234567 (UNKNOWN)')
        self.assertEqual(test_bugrow.CVEs, 'None')
        self.assertEqual(test_bugrow.Commits, 'None')
        self.assertEqual(test_bugrow.Readiness, 'INVALID')
        self.assertEqual(test_bugrow.Policy_Check, 'Check not done: No BZ')
        self.assertEqual(test_bugrow.Notes, '-')

    @mock.patch('webhook.bug.is_bug_ready')
    def test_bugrow_populate(self, mock_is_bug_ready):
        """Updates the attribute vales of a BugRow."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_bug_ready.return_value = False, cpc_err

        mr309 = fakes_mrs.make_mr('group/centos-stream-9', 309,
                                  query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])
        mr309.projects.projects[24152864].branches[0].__dict__['policy'] = [1]
        test_bug = Bug.new_from_bz(bz=fakes_bz.BZ1234567, mrs=[mr309])
        test_bug.scope = defs.MrScope.READY_FOR_QA

        test_bugrow = bughook.BugRow()
        test_bugrow.populate(test_bug, [1, 3, 5])

        self.assertEqual(test_bugrow.BZ, 'BZ-1234567 (POST)')
        self.assertEqual(test_bugrow.CVEs,
                         '[CVE-1235-13516](https://bugzilla.redhat.com/CVE-1235-13516)<br>')
        self.assertIn('0aa467549b4e997d023c29f4d481aee01b9e9471', test_bugrow.Commits)
        self.assertIn('e53eab9f887f784044ad32ef5c082695831d90d9', test_bugrow.Commits)
        self.assertIn('88cdd4035228dac16878eb907381afea6ceffeaa', test_bugrow.Commits)
        self.assertEqual(test_bugrow.Readiness, 'READY_FOR_QA')
        self.assertEqual(test_bugrow.Policy_Check, f'Failed:<br>{cpc_err}')
        self.assertEqual(test_bugrow.Notes, 'See 1<br>See 3<br>See 5')
        mr309.projects.projects[24152864].branches[0].__dict__['policy'] = []


class TestCveRow(NoSocketTestCase):
    """Tests for the CveRow class."""

    def test_cverow_populate_empty(self):
        """Updates the attribute values of CveRow."""
        test_cve = Bug.new_missing(bz_id='CVE-2101-21515', mrs=[])
        test_cverow = bughook.CveRow()
        test_cverow.populate(test_cve, '')
        self.assertEqual(test_cverow.CVEs,
                         '[CVE-2101-21515](https://bugzilla.redhat.com/CVE-2101-21515)<br>')
        self.assertEqual(test_cverow.Priority, 'Unknown')
        self.assertEqual(test_cverow.Commits, 'None')
        self.assertEqual(test_cverow.Clones, 'Unknown')
        self.assertEqual(test_cverow.Readiness, 'INVALID')
        self.assertEqual(test_cverow.Notes, '-')

    def test_cverow_populate(self):
        """Updates the attribute values of CveRow."""
        mr309 = fakes_mrs.make_mr('group/centos-stream-9', 309,
                                  query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])
        test_cve = Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=[mr309])

        test_cverow = bughook.CveRow()
        test_cverow.populate(test_cve, [2, 3])

        self.assertEqual(test_cverow.CVEs,
                         '[CVE-1235-13516](https://bugzilla.redhat.com/CVE-1235-13516)<br>')
        self.assertEqual(test_cverow.Priority, 'High')
        self.assertIn('0aa467549b4e997d023c29f4d481aee01b9e9471', test_cverow.Commits)
        self.assertIn('e53eab9f887f784044ad32ef5c082695831d90d9', test_cverow.Commits)
        self.assertIn('88cdd4035228dac16878eb907381afea6ceffeaa', test_cverow.Commits)
        self.assertEqual(test_cverow.Clones, 'None')
        self.assertEqual(test_cverow.Readiness, 'INVALID')
        self.assertEqual(test_cverow.Notes, 'See 2<br>See 3')

    def test_format_clones(self):
        """Returns a string describing the CVE clones, if any."""
        # No parent_mr, returns 'Unknown'
        mock_cve = mock.Mock(parent_mr=None)
        self.assertEqual(bughook.CveRow._format_Clones(mock_cve), 'Unknown')

        # Not a High Prio, returns 'N/A'
        mock_cve = mock.Mock(parent_mr=True, bz_priority=bughook.defs.BZPriority.LOW)
        self.assertEqual(bughook.CveRow._format_Clones(mock_cve), 'N/A')

        # High Prio but RHEL-6, returns 'N/A'
        mock_mr = mock.Mock(project=mock.Mock())
        mock_mr.project.name = 'rhel-6'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=bughook.defs.BZPriority.HIGH)
        self.assertEqual(bughook.CveRow._format_Clones(mock_cve), 'N/A')

        # No bz_depends_on, returns 'None'
        mock_mr = mock.Mock(project=mock.Mock())
        mock_mr.project.name = 'rhel-9'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=bughook.defs.BZPriority.URGENT,
                             bz_depends_on=[])
        self.assertEqual(bughook.CveRow._format_Clones(mock_cve), 'None')

        # No parent_clone, nothing is **bold**.
        mock_mr.branch = 2
        mock_bug = mock.Mock(bz_branch=mock.Mock(internal_target_release='9.1.0'),
                             bz_status=bughook.defs.BZStatus.POST,
                             bz=mock.Mock(component='kernel'))
        mock_bug.id = 1234567
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=bughook.defs.BZPriority.URGENT,
                             bz_depends_on=[mock_bug])
        self.assertEqual(bughook.CveRow._format_Clones(mock_cve),
                         '9.1.0 (kernel): BZ-1234567 (POST)<br>')

        # The parent_clone is the same branch as the MR, clone is **bold**.
        mock_mr.branch = mock_bug.bz_branch
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=bughook.defs.BZPriority.URGENT,
                             bz_depends_on=[mock_bug])
        self.assertEqual(bughook.CveRow._format_Clones(mock_cve),
                         '**9.1.0 (kernel): BZ-1234567 (POST)**<br>')


class TestDepRow(NoSocketTestCase):
    """Tests for the DepRow class."""

    def test_deprow_populate_empty(self):
        """Updates the attribute values of a DepRow."""
        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        mr309 = fakes_mrs.make_mr('group/centos-stream-9', 309, projects=projects,
                                  query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])
        mr303 = fakes_mrs.make_mr('group/centos-stream-9', 303, projects=projects,
                                  query_results_list=[fakes_mrs.MR303_DICT], is_dependency=True)
        test_dep = Bug.new_missing(bz_id=2323232, mrs=[mr309, mr303])

        test_deprow = bughook.DepRow()
        test_deprow.populate(test_dep, '')

        self.assertEqual(test_deprow.MR, '!303 (main)')
        self.assertEqual(test_deprow.BZ, 'BZ-2323232 (UNKNOWN)')
        self.assertEqual(test_deprow.CVEs, 'None')
        self.assertIn('ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3', test_deprow.Commits)
        self.assertIn('f77278fcd9cef99358adc7f5e077be795a54ffca', test_deprow.Commits)
        self.assertEqual(test_deprow.Readiness, 'INVALID')
        self.assertEqual(test_deprow.Policy_Check, 'Check not done: No BZ')
        self.assertEqual(test_deprow.Notes, '-')

    @mock.patch('webhook.bug.is_bug_ready')
    def test_deprow_populate(self, mock_is_bug_ready):
        """Updates the attribute vales of a DepRow."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_bug_ready.return_value = False, cpc_err

        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        mr309 = fakes_mrs.make_mr('group/centos-stream-9', 309, projects=projects,
                                  query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])
        mr303 = fakes_mrs.make_mr('group/centos-stream-9', 303, projects=projects,
                                  query_results_list=[fakes_mrs.MR303_DICT], is_dependency=True)
        mr309.projects.projects[24152864].branches[0].__dict__['policy'] = [1]
        test_dep = Bug.new_from_bz(bz=fakes_bz.BZ2323232, mrs=[mr309, mr303])
        test_dep.scope = defs.MrScope.READY_FOR_QA

        test_deprow = bughook.DepRow()

        test_deprow.populate(test_dep, [1, 3, 5])
        self.assertEqual(test_deprow.MR, '!303 (main)')
        self.assertEqual(test_deprow.BZ, 'BZ-2323232 (MODIFIED)')
        self.assertEqual(test_deprow.CVEs, 'None')
        self.assertIn('ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3', test_deprow.Commits)
        self.assertIn('f77278fcd9cef99358adc7f5e077be795a54ffca', test_deprow.Commits)
        self.assertEqual(test_deprow.Readiness, 'READY_FOR_QA')
        self.assertEqual(test_deprow.Policy_Check, f'Failed:<br>{cpc_err}')
        mr309.projects.projects[24152864].branches[0].__dict__['policy'] = []


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestMisc(NoSocketTestCase):
    """Tests for misc functions."""

    def test_find_needed_footnotes(self):
        """Returns a dict of testnames and footnote messages from the list of tag items."""
        bug1 = Bug.new_missing(bz_id=1, mrs=[])
        bug2 = Bug.new_missing(bz_id=2, mrs=[])
        bug3 = Bug.new_missing(bz_id=3, mrs=[])
        bug4 = Bug.new_missing(bz_id=4, mrs=[])
        bug1.failed_tests = ['BranchMatches']
        bug2.failed_tests = ['BZisNotClosed', 'IsVerified']
        bug3.failed_tests = []
        bug4.failed_tests = ['IsVerified', 'IsApproved']

        result = bughook.find_needed_footnotes([bug1, bug2, bug3, bug4])
        self.assertEqual(len(result), 4)
        self.assertTrue('BranchMatches' in result)
        self.assertTrue('BZisNotClosed' in result)
        self.assertTrue('IsVerified' in result)
        self.assertTrue('IsApproved' in result)

    def test_create_table(self):
        """Creates a Table from the list of items with the given type of Row class."""
        bug1 = Bug.new_from_bz(bz=fakes_bz.BZ1234567)
        bug2 = Bug.new_from_bz(bz=fakes_bz.BZ2323232)
        bug3 = Bug.new_from_bz(bz=fakes_bz.BZ2345678)
        test_items = [bug1, bug2, bug3]
        results = bughook.create_table(bughook.BugRow, test_items)
        self.assertEqual(len(results), 3)

    @mock.patch('webhook.bug.is_bug_ready')
    def test_generate_comment(self, mock_is_bug_ready):
        """Returns a string of markdown that will render the MR status and Tag tables."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_bug_ready.return_value = False, cpc_err
        mr309 = fakes_mrs.make_mr('group/centos-stream-9', 309,
                                  query_results_list=[fakes_mrs.MR309_DICT, fakes_mrs.MR303_DICT])

        bug_items = [Bug.new_from_bz(bz=fakes_bz.BZ1234567, mrs=[mr309])]
        bug_table = bughook.create_table(bughook.BugRow, bug_items)

        dep_items = [Bug.new_from_bz(bz=fakes_bz.BZ2323232, mrs=[mr309]),
                     Bug.new_from_bz(bz=fakes_bz.BZ2345678, mrs=[mr309])]
        dep_table = bughook.create_table(bughook.DepRow, dep_items)

        cve_items = [Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=[mr309]),
                     Bug.new_from_bz(bz=fakes_bz.BZ4567890, mrs=[mr309])]
        cve_table = bughook.create_table(bughook.CveRow, cve_items)

        target_branch = '9.1'
        mr_scope = defs.MrScope.READY_FOR_QA
        tables = (bug_table, dep_table, cve_table)
        comment = bughook.generate_comment(target_branch, mr_scope, tables)
        self.assertIn(f'Branch: {target_branch}', comment)
        self.assertIn('**passes**', comment)
        self.assertIn('Bugzilla tags', comment)
        self.assertIn('Depends tags', comment)
        self.assertIn('CVE tags', comment)

    @mock.patch('webhook.common.add_merge_request_to_milestone')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    @mock.patch('webhook.bughook.get_instance')
    def test_update_gitlab_comment_with_labels(self, mock_get_instance, mock_update_comment,
                                               mock_add_label, mock_milestone):
        """A comment with labels."""
        mock_session = SessionRunner('bughook', [], bughook.HANDLERS)
        project_id = 456
        namespace = 'group/project'
        mr_id = 123
        username = 'user1'
        comment = 'a good comment'
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(project_id, namespace)
        mock_mr = mock_project.add_mr(mr_id)
        mock_get_instance.return_value = mock_gl
        mock_graphql = mock.Mock()
        mock_graphql.username = username
        mock_this_mr = mock.Mock(namespace=namespace, mr_id=mr_id, graphql=mock_graphql)
        mock_mr.namespace = namespace
        mock_mr.mr_id = mr_id
        mock_mr.graphql = mock_graphql

        # Post Comment is True and we have labels.
        labels = ['Bugzilla::OK', 'Dependencies::OK']
        mock_add_label.return_value = labels + ['readyForMerge']
        bughook.update_gitlab(mock_session, mock_this_mr, labels, comment)
        mock_update_comment.assert_called_once_with(mock_mr, comment,
                                                    bot_name=mock_this_mr.graphql.username,
                                                    identifier=bughook.COMMENT_TITLE)
        mock_add_label.assert_called_once_with(mock_project, mr_id, labels)

    @mock.patch('webhook.libbz.get_depends_on_rt_bzs')
    @mock.patch('webhook.libbz.bugs_to_move_to_post')
    @mock.patch('webhook.libbz.update_bug_status')
    def test_move_bugs_to_post(self, mock_update_status, mock_move_to_post, mock_get_depends_on):
        """Calls update_bug_status with the expected bugzilla BZ objects."""
        mock_branch = mock.Mock(internal_target_release='', zstream_target_release='9.1.0')
        mock_branch.name = '9.1'
        mock_mr = mock.Mock(branch=mock_branch, description=mock.Mock(cve='CVE=1973-12345'))
        mock_bug = mock.Mock(bz=mock.Mock(), mr=mock_mr)
        mock_bz1 = mock.Mock(cf_zstream_target_release='9.1.0',
                             spec=['cf_zstream_target_release'])
        mock_mr.bugs = [mock_bug]
        mock_get_depends_on.return_value = [mock_bz1]
        bughook.move_bugs_to_post(mock_mr)
        mock_move_to_post.assert_called_with([mock_bug.bz, mock_bz1], mock_mr.pipeline_finished)
        mock_update_status.assert_called_with(mock_move_to_post.return_value, defs.BZStatus.POST)


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestProcessMR(NoSocketTestCase):
    """Tests for process_mr function."""

    @mock.patch('webhook.bughook.MR')
    def test_process_mr_check_mr_state(self, mock_mr):
        """Does nothing since check_mr_state returns False."""
        mock_session = mock.Mock()
        mock_session.graphql.check_mr_state.return_value = False
        bughook.process_event({}, mock_session, mock.Mock())
        mock_mr.assert_not_called()

    @mock.patch('webhook.bughook.MR')
    def test_process_mr_skip_funny(self, mock_MR):
        """Skips MRs with nothing to do."""
        mock_session = SessionRunner('bughook', [], bughook.HANDLERS)
        mock_session.rh_projects.do_load_policies = mock.Mock()
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mock_session.graphql = mock.Mock(username=username)

        mock_event = mock.Mock()
        mock_event.mr_url = \
            defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_event.kind = defs.GitlabObjectKind.MERGE_REQUEST

        # Nothing to do with an MR with no Description.
        mock_mr = mock.Mock(state=bughook.defs.MrState.OPENED, description=None, commits=True)
        mock_MR.return_value = mock_mr
        bughook.process_event({}, mock_session, mock_event)
        mock_MR.assert_called_with(mock_session.graphql, mock_session.rh_projects, namespace, mr_id)
        mock_mr.get_depends_mrs.assert_not_called()

        # Nothing to do if there are no commits.
        mock_mr = mock.Mock(state=bughook.defs.MrState.OPENED, description=True, commits=[])
        mock_MR.return_value = mock_mr
        bughook.process_event({}, mock_session, mock_event)
        mock_MR.assert_called_with(mock_session.graphql, mock_session.rh_projects, namespace, mr_id)
        mock_mr.get_depends_mrs.assert_not_called()

    @mock.patch('webhook.bughook.update_gitlab')
    @mock.patch('webhook.bughook.MR')
    def test_process_mr_jira_only(self, mock_MR, mock_update):
        """Skips MRs with nothing to do."""
        mock_session = SessionRunner('bughook', [], bughook.HANDLERS)
        mock_session.rh_projects.do_load_policies = mock.Mock()
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mock_session.graphql = mock.Mock(username=username)

        # If the MR has no bugs in it, but has jira issues, assume it's jira-only and do nothing
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(jissue=True),
                            first_dep_sha='',
                            is_draft=False,
                            state=bughook.defs.MrState.OPENED)
        mock_mr.bugs = []
        mock_mr.bugs_with_scopes = mock_mr.bugs
        mock_mr.cves_with_scopes = []
        mock_MR.return_value = mock_mr

        mock_event = mock.Mock()
        mock_event.mr_url = \
            defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_event.kind = defs.GitlabObjectKind.MERGE_REQUEST

        bughook.process_event({}, mock_session, mock_event)
        mock_session.graphql.replace_note.assert_not_called()
        mock_update.assert_called_once()

    @mock.patch('webhook.bughook.move_bugs_to_post')
    @mock.patch('webhook.bughook.update_gitlab')
    @mock.patch('webhook.bughook.MR')
    def test_process_mr(self, mock_MR, mock_update, mock_to_post):
        """Does not update BZs or MR labels but leaves a comment."""
        mock_session = SessionRunner('bughook', [], bughook.HANDLERS)
        mock_session.rh_projects.do_load_policies = mock.Mock()
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123

        mock_session.graphql = mock.Mock(username=username)
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(jissue=False),
                            first_dep_sha='',
                            is_draft=False,
                            state=bughook.defs.MrState.OPENED)
        mock_bug = mock.Mock(bz_cves=[],
                             bz_policy_check_ok=(False, 'Nope'),
                             commits=[],
                             failed_tests=[],
                             mr=mock_mr,
                             scope=bughook.defs.MrScope.READY_FOR_MERGE,
                             test_list=[])
        mock_bug.id = 1234567
        mock_mr.bugs = [mock_bug]
        mock_mr.bugs_with_scopes = mock_mr.bugs
        mock_mr.cves_with_scopes = []
        mock_MR.return_value = mock_mr

        mock_event = mock.Mock()
        mock_event.mr_url = \
            defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_event.kind = defs.GitlabObjectKind.MERGE_REQUEST

        bughook.process_event({}, mock_session, mock_event)
        mock_to_post.assert_called_once()
        mock_mr.get_depends_mrs.assert_called_once_with()
        mock_update.assert_called_once_with(mock_session, mock_mr, ['Bugzilla::OK'], mock.ANY)

    @mock.patch('webhook.bughook.move_bugs_to_post')
    @mock.patch('webhook.bughook.update_gitlab')
    @mock.patch('webhook.bughook.MR')
    def test_process_mr_with_draft(self, mock_MR, mock_update, mock_to_post):
        """Does not update BZs or MR labels but leaves a comment."""
        mock_session = SessionRunner('bughook', [], bughook.HANDLERS)
        mock_session.rh_projects.do_load_policies = mock.Mock()
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123

        mock_session.graphql = mock.Mock(username=username)
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(jissue=False),
                            first_dep_sha='',
                            is_draft=True,
                            state=bughook.defs.MrState.OPENED)
        mock_bug = mock.Mock(bz_cves=[],
                             bz_policy_check_ok=(False, 'Nope'),
                             commits=[],
                             failed_tests=[],
                             mr=mock_mr,
                             scope=bughook.defs.MrScope.READY_FOR_MERGE,
                             test_list=[])
        mock_bug.id = 1234567
        mock_mr.bugs = [mock_bug]
        mock_mr.bugs_with_scopes = mock_mr.bugs
        mock_mr.cves_with_scopes = []
        mock_MR.return_value = mock_mr

        mock_event = mock.Mock()
        mock_event.mr_url = \
            defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_event.kind = defs.GitlabObjectKind.MERGE_REQUEST

        bughook.process_event({}, mock_session, mock_event)
        mock_to_post.assert_not_called()
        mock_mr.get_depends_mrs.assert_called_once_with()
        mock_update.assert_called_once_with(mock_session, mock_mr, ['Bugzilla::OK'], mock.ANY)
