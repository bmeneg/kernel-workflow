"""Test the terminator webhook."""
# pylint: disable=no-self-use
import json
import os
import subprocess
import tempfile
from unittest import mock

import responses

from tests.no_socket_test_case import NoSocketTestCase
from webhook import session
from webhook import terminator


@mock.patch('webhook.session.GITFORGE', 'https://h')
class TestTerminator(NoSocketTestCase):
    """Test the Terminator class."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if check and returncode:
            raise subprocess.CalledProcessError(returncode, args, output=stdout)
        return subprocess.CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    def _main_message(self, message):
        with tempfile.NamedTemporaryFile('w') as message_file:
            message_file.write(json.dumps(message))
            message_file.flush()
            terminator.main(['--json-message-file', message_file.name])

    def setUp(self):
        """Reset the session.SESSION."""
        session.SESSION = None

    @mock.patch('webhook.terminator.Terminator.load_running_beaker_jobs')
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines')
    @mock.patch('webhook.terminator.Terminator.process_pipeline_project')
    def test_process_pipeline_project_main(self, process_pipeline_project, load_aws, load_beaker):
        """Check that the CLI correctly proceeds to evaluate projects."""
        process_pipeline_project.return_value = 0
        terminator.main(['--pipeline-project-url', 'https://project1',
                         '--pipeline-project-url', 'https://project2'])
        load_aws.assert_called_once()
        load_beaker.assert_called_once()
        process_pipeline_project.assert_has_calls([mock.call('https://project1'),
                                                   mock.call('https://project2')])

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_beaker_jobs', mock.Mock())
    @mock.patch('webhook.terminator.Terminator.return_machine')
    @mock.patch('boto3.client')
    def test_process_pipeline_project_no_matching_tag(self, mock_aws, mock_return):
        """Check that AWS resources without a matching tag are correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={})
        mock_aws.return_value.describe_instances.return_value = {'Reservations': [{'Instances': [
            # Instance without tags:
            {'InstanceId': 'i-123'},
            # No tag match:
            {'InstanceId': 'i-124', 'Tags': [{'Key': 'irrelevant', 'Value': 'meh'},
                                             {'Key': 'nope', 'Value': '11'}]},
            # Broken value, we don't know where it came from:
            {'InstanceId': 'i-124', 'Tags': [{'Key': 'CkiGitLabJobId', 'Value': '?'}]}
        ]}]}
        terminator.main(['--pipeline-project-url', 'https://h/project1'])
        mock_return.assert_not_called()

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    def test_process_pipeline_project_no_whiteboard(self):
        """Check that a Beaker job without a whiteboard is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job/>')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            terminator.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    def test_process_pipeline_project_wrong_whiteboard(self):
        """Check that a Beaker job with a non-matching whiteboard is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(['bkr', 'job-results', 'J:1'], 0,
                             '<job><whiteboard>foobar</whiteboard></job>')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            terminator.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    def test_process_pipeline_project_missing_job(self):
        """Check that a Beaker job with a missing GitLab job is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2', status=404)
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            terminator.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    def test_process_pipeline_project_job_running(self):
        """Check that a Beaker job with a running GitLab job is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2',
                      json={'status': 'running'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            terminator.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    @mock.patch.dict(os.environ, {'BEAKER_OWNER': 'owner'})
    def test_process_pipeline_project_owner_filter(self):
        """Check that a Beaker job with a running job is correctly ignored."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished',
                              '--owner', 'owner'], 0, '[]')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            terminator.main(['--pipeline-project-url', 'https://h/project1'])

    @responses.activate
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_process_pipeline_project_job_success_cancel_fail(self):
        """Check that errors during cancelling do not affect other jobs."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1", "J:2"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        self._add_run_result(
            ['bkr', 'job-results', 'J:2'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 1)
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:2'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self.assertRaises(SystemExit, terminator.main,
                              ['--pipeline-project-url', 'https://h/project1'])
        self.assertIn(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], self._mocked_calls)
        self.assertIn(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:2'], self._mocked_calls)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.terminator.Terminator.load_running_aws_machines', mock.Mock())
    def test_process_pipeline_project_pipeline_success(self):
        """Check that Beaker jobs can be cancelled."""
        responses.add(responses.GET, 'https://h/api/v4/projects/project1', json={'id': 3})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', 'cki@gitlab', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            terminator.main(['--pipeline-project-url', 'https://h/project1'])
        self.assertIn(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], self._mocked_calls)

    @mock.patch('webhook.terminator.Terminator.run')
    def test_run_main(self, run):
        """Check that the message loop is run."""
        terminator.main([])
        run.assert_called_once()

    @responses.activate
    def test_run_main_running(self):
        """Check that resources for running jobs are ignored."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        self._main_message({'object_kind': 'build',
                            'build_id': 2,
                            'build_stage': 'test',
                            'build_status': 'running',
                            'pipeline_id': 123,
                            'project_id': 3,
                            'repository': {'homepage': 'https://h/projects/3'},
                            'user': {'username': 'somebody'},
                            })

    @responses.activate
    def test_run_main_not_a_test(self):
        """Check that resources for non-test jobs are ignored."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        self._main_message({'object_kind': 'build',
                            'build_id': 2,
                            'build_stage': 'setup',
                            'build_status': 'success',
                            'pipeline_id': 123,
                            'project_id': 3,
                            'repository': {'homepage': 'https://h/projects/3'},
                            'user': {'username': 'somebody'},
                            })

    @responses.activate
    @mock.patch('boto3.client')
    def test_run_main_success(self, mock_aws):
        """Check that resources for finished jobs are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        mock_aws.return_value.describe_instances.return_value = {'Reservations': [{'Instances': [
            {'InstanceId': 'i-124', 'Tags': [
                {'Key': 'something_else', 'Value': 'string'},
                {'Key': 'CkiGitLabJobId', 'Value': '2'}
            ]}
        ]}]}
        self._add_run_result(['bkr', 'job-list', '--whiteboard', '2', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            with self.assertLogs('cki.webhook.terminator', level='INFO') as logs:
                self._main_message({'object_kind': 'build',
                                    'build_id': 2,
                                    'build_stage': 'test',
                                    'build_status': 'success',
                                    'pipeline_id': 123,
                                    'project_id': 3,
                                    'repository': {'homepage': 'https://h/projects/3'},
                                    'user': {'username': 'somebody'},
                                    })
                self.assertEqual(len(logs.output), 2)
                for log in logs.output:
                    self.assertIn('of finished job web_url', log)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_run_main_success_cancel_exception(self):
        """Check that errors during Beaker job cancelling cause exceptions."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        self._add_run_result(['bkr', 'job-list', '--whiteboard', '2', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 1)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self.assertRaises(subprocess.CalledProcessError, self._main_message, {
                'object_kind': 'build',
                'build_id': 2,
                'build_stage': 'test',
                'build_status': 'success',
                'pipeline_id': 123,
                'project_id': 3,
                'repository': {'homepage': 'https://h/projects/3'},
                'user': {'username': 'somebody'},
            })

    @responses.activate
    @mock.patch.dict(os.environ, {'BEAKER_URL': 'beaker_url'})
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('boto3.client')
    @mock.patch('cki_lib.chatbot.send_message')
    def test_run_main_success_cancel(self, send_message, mock_aws):
        """Check that Beaker jobs can be cancelled."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/jobs/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        mock_aws.return_value.describe_instances.return_value = {'Reservations': []}
        self._add_run_result(['bkr', 'job-list', '--whiteboard', '2', '--unfinished'], 0,
                             '["J:1"]')
        self._add_run_result(
            ['bkr', 'job-results', 'J:1'], 0,
            '<job><whiteboard>{"name": "cki@gitlab:123", "job_id": "2"}</whiteboard></job>'
        )
        self._add_run_result(['bkr', 'job-cancel',  '--msg', mock.ANY, 'J:1'], 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({
                'object_kind': 'build',
                'build_id': 2,
                'build_stage': 'test',
                'build_status': 'success',
                'pipeline_id': 123,
                'project_id': 3,
                'repository': {'homepage': 'https://h/projects/3'},
                'user': {'username': 'somebody'},
            })
        send_message.assert_called()

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('cki_lib.chatbot.send_message')
    def test_run_main_parent_parent_canceled(self, send_message):
        """Check that jobs for canceled pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'canceled', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges',
                      json=[{'downstream_pipeline': {
                          'id': 5,
                          'project_id': 4,
                          'status': 'running'}}])
        responses.add(responses.GET, 'https://h/api/v4/projects/4/pipelines/5',
                      json={'id': 5, 'status': 'running', 'web_url': 'child_url'})
        responses.add(responses.POST, 'https://h/api/v4/projects/4/pipelines/5/cancel',
                      json={})
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1',
                                            'path_with_namespace': 'project1'},
                                'user': {'username': 'somebody'},
                                })
        self.assertEqual(responses.calls[-1].request.method, responses.POST)
        send_message.assert_called()

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_run_main_parent_success(self):
        """Check that jobs for finished pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges',
                      json=[{'downstream_pipeline': {
                          'id': 5,
                          'project_id': 4,
                          'status': 'success'}}])
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1'},
                                'user': {'username': 'somebody'},
                                })

    @responses.activate
    def test_run_main_parent_running_report(self):
        """Check that jobs for finished pipelines are processed."""
        responses.add(responses.GET, 'https://h/api/v4/user', json={'username': 'username',
                      'web_url': 'https://h/username'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2',
                      json={'id': 2, 'status': 'success', 'web_url': 'web_url'})
        responses.add(responses.GET, 'https://h/api/v4/projects/3/pipelines/2/bridges',
                      json=[{'downstream_pipeline': {
                          'id': 5,
                          'project_id': 4,
                          'status': 'running'}}])
        responses.add(responses.GET, 'https://h/api/v4/projects/4/pipelines/5',
                      json={'id': 5, 'status': 'running', 'web_url': 'child_url'})
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            self._main_message({'object_kind': 'pipeline',
                                'object_attributes': {'id': 2},
                                'project': {'id': 3, 'web_url': 'https://h/project1'},
                                'user': {'username': 'somebody'},
                                })
