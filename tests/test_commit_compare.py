"""Webhook interaction tests."""
import copy
from datetime import datetime
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import mock

from cki_lib import messagequeue

from tests.no_socket_test_case import NoSocketTestCase
import webhook.commit_compare
import webhook.common
import webhook.defs
from webhook.defs import GitlabObjectKind
from webhook.session import SessionRunner
from webhook.session_events import create_event

# a hunk of upstream linux kernel commit ID 1fc70edb7d7b5ce1ae32b0cf90183f4879ad421a
PATCH_A = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_A += "index blahblah..blahblah 100644\n"
PATCH_A += "--- a/include/linux/netdevice.h\n"
PATCH_A += "+++ b/include/linux/netdevice.h\n"
PATCH_A += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_A += "        unsigned short          type;\n"
PATCH_A += "        unsigned short          hard_header_len;\n"
PATCH_A += "        unsigned char           min_header_len;\n"
PATCH_A += "+       unsigned char           name_assign_type;\n"
PATCH_A += "\n"
PATCH_A += "        unsigned short          needed_headroom;\n"
PATCH_A += "        unsigned short          needed_tailroom;\n"

PATCH_B = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_B += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_B += "--- a/include/linux/netdevice.h\n"
PATCH_B += "+++ b/include/linux/netdevice.h\n"
PATCH_B += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_B += "        unsigned short          type;\n"
PATCH_B += "        unsigned short          hard_header_len;\n"
PATCH_B += "        unsigned char           min_header_len;\n"
PATCH_B += "+       unsigned char           name_assign_type;\n"
PATCH_B += "\n"
PATCH_B += "        unsigned short          needed_headroom;\n"
PATCH_B += "        unsigned short          needed_tailroom;\n"

PATCH_C = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_C += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_C += "--- a/include/linux/netdevice.h\n"
PATCH_C += "+++ b/include/linux/netdevice.h\n"
PATCH_C += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_C += "        unsigned short          type;\n"
PATCH_C += "        unsigned short          hard_header_len;\n"
PATCH_C += "        unsigned char           min_header_len;\n"
PATCH_C += "+       RH_KABI_EXTEND(unsigned char name_assignee_type)\n"
PATCH_C += "\n"
PATCH_C += "        unsigned short          needed_headroom;\n"
PATCH_C += "        unsigned short          needed_tailroom;\n"

PATCH_D = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netdevice.h\n"
PATCH_D += "+++ b/include/linux/netdevice.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"
PATCH_D += "diff --git a/include/linux/netlink.h b/include/linux/netlink.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netlink.h\n"
PATCH_D += "+++ b/include/linux/netlink.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"

PATCH_E = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_E += "new file mode 100644\n"
PATCH_E += "index blahblah..blahblah 100644\n"
PATCH_E += "--- /dev/null\n"
PATCH_E += "+++ b/include/linux/netdevice.h\n"
PATCH_E += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_E += "        unsigned short          type;\n"
PATCH_E += "        unsigned short          hard_header_len;\n"
PATCH_E += "        unsigned char           min_header_len;\n"
PATCH_E += "+       unsigned char           name_assign_type;\n"
PATCH_E += "\n"
PATCH_E += "        unsigned short          needed_headroom;\n"
PATCH_E += "        unsigned short          needed_tailroom;\n"

PATCH_F = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_F += "deleted file mode 100644\n"
PATCH_F += "index blahblah..blahblah 100644\n"
PATCH_F += "--- a/include/linux/netdevice.h\n"
PATCH_F += "+++ /dev/null\n"
PATCH_F += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_F += "        unsigned short          type;\n"
PATCH_F += "        unsigned short          hard_header_len;\n"
PATCH_F += "        unsigned char           min_header_len;\n"
PATCH_F += "+       unsigned char           name_assign_type;\n"
PATCH_F += "\n"
PATCH_F += "        unsigned short          needed_headroom;\n"
PATCH_F += "        unsigned short          needed_tailroom;\n"

PATCH_G = "diff --git a/include/linux/netdevice.h b/include/linux/foobar.h\n"
PATCH_G += "similarity index xx%\n"
PATCH_G += "rename from include/linux/netdevice.h\n"
PATCH_G += "rename to include/linux/foobar.h\n"
PATCH_G += "index blahblah..blahblah 100644\n"
PATCH_G += "--- a/include/linux/netdevice.h\n"
PATCH_G += "+++ b/include/linux/foobar.h\n"
PATCH_G += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_G += "        unsigned short          type;\n"
PATCH_G += "        unsigned short          hard_header_len;\n"
PATCH_G += "        unsigned char           min_header_len;\n"
PATCH_G += "+       unsigned char           name_assign_type;\n"
PATCH_G += "\n"
PATCH_G += "        unsigned short          needed_headroom;\n"
PATCH_G += "        unsigned short          needed_tailroom;\n"

PATCH_H = "@@ -0,0 +1,117 @@\n"
PATCH_H += "+.. SPDX-License-Identifier: GPL-2.0\n+\n+====================\n"
PATCH_H += "+Kernel Testing Guide\n+====================\n+\n+\n"
PATCH_H += "+There are a number of different tools for testing the Linux kernel, so knowing\n"
PATCH_H += "+when to use each of them can be a challenge. This document provides a rough\n"
PATCH_H += "+overview of their differences, and how they fit together.\n+\n+\n"
PATCH_H += "+Writing and Running Tests\n+=========================\n+\n"
PATCH_H += "+The bulk of kernel tests are written using either the kselftest or KUnit\n"
PATCH_H += "+frameworks. These both provide infrastructure to help make running tests and\n"
PATCH_H += "+groups of tests easier, as well as providing helpers to aid in writing new\n"
PATCH_H += "+tests.\n+\n"
PATCH_H += "+If you're looking to verify the behaviour of the Kernel — particularly specific\n"
PATCH_H += "+parts of the kernel — then you'll want to use KUnit or kselftest.\n+\n+\n"
PATCH_H += "+The Difference Between KUnit and kselftest\n"
PATCH_H += "+------------------------------------------\n+\n"
PATCH_H += "+KUnit (Documentation/dev-tools/kunit/index.rst) is an entirely in-kernel system\n"
PATCH_H += "+for \"white box\" testing: because test code is part of the kernel, it can access\n"
PATCH_H += "+internal structures and functions which aren't exposed to userspace.\n+\n"
PATCH_H += "+KUnit tests therefore are best written against small, self-contained parts\n"
PATCH_H += "+of the kernel, which can be tested in isolation. This aligns well with the\n"
PATCH_H += "+concept of 'unit' testing.\n+\n"
PATCH_H += "+For example, a KUnit test might test an individual kernel function (or even a\n"
PATCH_H += "+single codepath through a function, such as an error handling case), rather\n"
PATCH_H += "+than a feature as a whole.\n+\n"
PATCH_H += "+This also makes KUnit tests very fast to build and run, allowing them to be\n"
PATCH_H += "+run frequently as part of the development process.\n+\n"
PATCH_H += "+There is a KUnit test style guide which may give further pointers in\n"
PATCH_H += "+Documentation/dev-tools/kunit/style.rst\n+\n+\n"
PATCH_H += "+kselftest (Documentation/dev-tools/kselftest.rst), on the other hand, is\n"
PATCH_H += "+largely implemented in userspace, and tests are normal userspace scripts or\n"
PATCH_H += "+programs.\n+\n"
PATCH_H += "+This makes it easier to write more complicated tests, or tests which need to\n"
PATCH_H += "+manipulate the overall system state more (e.g., spawning processes, etc.).\n"
PATCH_H += "+However, it's not possible to call kernel functions directly from kselftest.\n"
PATCH_H += "+This means that only kernel functionality which is exposed to userspace somehow\n"
PATCH_H += "+(e.g. by a syscall, device, filesystem, etc.) can be tested with kselftest.  To\n"
PATCH_H += "+work around this, some tests include a companion kernel module which exposes\n"
PATCH_H += "+more information or functionality. If a test runs mostly or entirely within the\n"
PATCH_H += "+kernel, however,  KUnit may be the more appropriate tool.\n+\n"
PATCH_H += "+kselftest is therefore suited well to tests of whole features, as these will\n"
PATCH_H += "+expose an interface to userspace, which can be tested, but not implementation\n"
PATCH_H += "+details. This aligns well with 'system' or 'end-to-end' testing.\n+\n"
PATCH_H += "+For example, all new system calls should be accompanied by kselftest tests.\n+\n"
PATCH_H += "+Code Coverage Tools\n+===================\n+\n"
PATCH_H += "+The Linux Kernel supports two different code coverage measurement tools. These\n"
PATCH_H += "+can be used to verify that a test is executing particular functions or lines\n"
PATCH_H += "+of code. This is useful for determining how much of the kernel is being tested,\n"
PATCH_H += "+and for finding corner-cases which are not covered by the appropriate test.\n+\n"
PATCH_H += "+:doc:`gcov` is GCC's coverage testing tool, which can be used with the kernel\n"
PATCH_H += "+to get global or per-module coverage. Unlike KCOV, it does not record per-task\n"
PATCH_H += "+coverage. Coverage data can be read from debugfs, and interpreted using the\n"
PATCH_H += "+usual gcov tooling.\n+\n"
PATCH_H += "+:doc:`kcov` is a feature which can be built in to the kernel to allow\n"
PATCH_H += "+capturing coverage on a per-task level. It's therefore useful for fuzzing and\n"
PATCH_H += "+other situations where information about code executed during, for example, a\n"
PATCH_H += "+single syscall is useful.\n+\n+\n+Dynamic Analysis Tools\n"
PATCH_H += "+======================\n+\n"
PATCH_H += "+The kernel also supports a number of dynamic analysis tools, which attempt to\n"
PATCH_H += "+detect classes of issues when they occur in a running kernel. These typically\n"
PATCH_H += "+each look for a different class of bugs, such as invalid memory accesses,\n"
PATCH_H += "+concurrency issues such as data races, or other undefined behaviour like\n"
PATCH_H += "+integer overflows.\n+\n"
PATCH_H += "+Some of these tools are listed below:\n+\n"
PATCH_H += "+* kmemleak detects possible memory leaks. See\n"
PATCH_H += "+  Documentation/dev-tools/kmemleak.rst\n"
PATCH_H += "+* KASAN detects invalid memory accesses such as out-of-bounds and\n"
PATCH_H += "+  use-after-free errors. See Documentation/dev-tools/kasan.rst\n"
PATCH_H += "+* UBSAN detects behaviour that is undefined by the C standard, like integer\n"
PATCH_H += "+  overflows. See Documentation/dev-tools/ubsan.rst\n"
PATCH_H += "+* KCSAN detects data races. See Documentation/dev-tools/kcsan.rst\n"
PATCH_H += "+* KFENCE is a low-overhead detector of memory issues, which is much faster than\n"
PATCH_H += "+  KASAN and can be used in production. See Documentation/dev-tools/kfence.rst\n"
PATCH_H += "+* lockdep is a locking correctness validator. See\n"
PATCH_H += "+  Documentation/locking/lockdep-design.rst\n"
PATCH_H += "+* There are several other pieces of debug instrumentation in the kernel, many\n"
PATCH_H += "+  of which can be found in lib/Kconfig.debug\n+\n"
PATCH_H += "+These tools tend to test the kernel as a whole, and do not \"pass\" like\n"
PATCH_H += "+kselftest or KUnit tests. They can be combined with KUnit or kselftest by\n"
PATCH_H += "+running tests on a kernel with these tools enabled: you can then be sure\n"
PATCH_H += "+that none of these errors are occurring during the test.\n+\n"
PATCH_H += "+Some of these tools integrate with KUnit or kselftest and will\n"
PATCH_H += "+automatically fail tests if an issue is detected.\n+\n"

EXPECTED_REPORT = ("\n\nUpstream Commit ID Readiness Report\n"
                   "\n"
                   "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n"
                   "|:-------|:-------|:-------|:---------|:-------|\n"
                   "|1|348742bd|RHELonly|n/a       |3|\n"
                   "|2|1cd738b1|0c55f51a|Diffs     |2|\n"
                   "|4|1508d8fb|6cb6a4a2|Merge commit||\n"
                   "|5|6cb6a4a3|Posted|n/a       |1|\n"
                   "1. This commit has Upstream Status as Posted, but we're not able to "
                   "auto-compare it.  Reviewers should take additional care when reviewing "
                   "these commits.\n"
                   "2. This commit differs from the referenced upstream commit and should be "
                   "evaluated accordingly.\n"
                   "3. This commit has Upstream Status as RHEL-only and has no corresponding "
                   "upstream commit.  The author of this MR should verify if this commit has "
                   "to be applied to "
                   "[future versions of RHEL](https://gitlab.com/cki-project/kernel-ark). "
                   "Reviewers should take additional care when reviewing these commits.\n"
                   "\n"
                   "Total number of commits analyzed: **5**\n"
                   "Please verify differences from upstream.\n"
                   "* Patches that match upstream 100% not shown in table\n"
                   "\n"
                   "Merge Request passes upstream commit ID validation.\n")

EXPECTED_REPORT_EMPTY = ("\n\nUpstream Commit ID Readiness Report\n"
                         "\n"
                         "Patches all match upstream 100%\n"
                         "\n"
                         "Total number of commits analyzed: **5**\n"
                         "\n"
                         "Merge Request passes upstream commit ID validation.\n")


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommitCompare(NoSocketTestCase):
    """ Test Webhook class."""

    # For mocking subprocess.run
    _mocked_runs = []
    _mocked_calls = []

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'g/p'
                                 },
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'description': 'dummy description',
                     'state': 'opened',
                     'user': {'username': 'test_user'}
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'archived': False,
                                'web_url': 'https://web.url/g/p',
                                'path_with_namespace': 'g/p'
                                },
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'description': 'dummy description',
                    'state': 'opened',
                    'user': {'username': 'test_user'}
                    }

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_merge_request(self, sdiff, udiff, dep_data, ext_deps, gcc):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'os-build'
        payload['object_attributes']['action'] = 'open'
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        gcc.return_value = 100, 50
        self._test_payload(True, payload=payload)

    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_ucid_re_evaluation(self, sdiff, udiff, dep_data, ext_deps):
        """Check handling of commit ID re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-commit-id-evaluation"
        payload["object_attributes"]["noteable_type"] = "MergeRequest"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        self._test_payload(True, payload)
        payload["object_attributes"]["state"] = "closed"
        self._test_payload(False, payload)
        payload["project"]["archived"] = True
        self._test_payload(False, payload)

    def test_get_match_info(self):
        """Check that we get sane strings back for match types."""
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.FULL)
        self.assertEqual(output, "100% match")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.PARTIAL)
        self.assertEqual(output, "Partial   ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.DIFFS)
        self.assertEqual(output, "Diffs     ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.KABI)
        self.assertEqual(output, "kABI Diffs")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.RHELONLY)
        self.assertEqual(output, "n/a       ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.POSTED)
        self.assertEqual(output, "n/a       ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.CONFIG)
        self.assertEqual(output, "Config    ")
        # No match
        output = webhook.commit_compare.get_match_info(-1)
        self.assertEqual(output, "No UCID   ")

    def test_find_kabi_hints(self):
        """Check that we can find a kabi hint in a patch hunk."""
        output = webhook.commit_compare.find_kabi_hints('desc', PATCH_A)
        self.assertFalse(output)
        output = webhook.commit_compare.find_kabi_hints('desc', PATCH_C)
        self.assertTrue(output)
        description = "This patch is pretty basic, nothing to see"
        output = webhook.commit_compare.find_kabi_hints(description, 'nada')
        self.assertFalse(output)
        description = "This patch contains kABI work-arounds"
        output = webhook.commit_compare.find_kabi_hints(description, 'nada')
        self.assertTrue(output)
        description = "This patch references genksyms"
        output = webhook.commit_compare.find_kabi_hints(description, 'nada')
        self.assertTrue(output)
        patch = "we're using an RH_RESERVED field here"
        output = webhook.commit_compare.find_kabi_hints('nada', patch)
        self.assertTrue(output)

    def test_ucid_compare(self):
        """Check that diff engine actually compares things properly."""
        # compare two identical patches (should be equal)
        output = webhook.cdlib.compare_commits(PATCH_A, PATCH_A)
        self.assertEqual(output, [])
        # compare two patches with only context differences (should be equal)
        output = webhook.cdlib.compare_commits(PATCH_A, PATCH_B)
        self.assertEqual(output, [])
        # compare two patches with actual differences (should NOT be equal)
        output = webhook.cdlib.compare_commits(PATCH_A, PATCH_C)
        self.assertNotEqual(output, [])

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}
    )
    @mock.patch('webhook.cdlib.get_dependencies_data', return_value=(False, "abcd"))
    def test_run_zstream_comparison_checks(self, gdd):
        """Make sure z-stream comparisons look sane."""
        zcommit = mock.Mock(id="1234567890ab",
                            message="Y-Commit: abc012345678",
                            parent_ids=['abc'])
        zdifflist = []
        path = {'old_path': 'include/linux/netdevice.h',
                'new_path': 'include/linux/netdevice.h',
                'new_file': False,
                'renamed_file': False,
                'deleted_file': False,
                'a_mode': '100644',
                'b_mode': '100644',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        zdifflist.append(path)
        zcommit.diff.return_value = zdifflist

        ycommit = mock.Mock(id="abc012345678",
                            message="commit a1b2c3d4e5f6",
                            parent_ids=['xyz'])
        ydifflist = []
        ypath = {'old_path': 'include/linux/netdevice.h',
                 'new_path': 'include/linux/netdevice.h',
                 'new_file': False,
                 'renamed_file': False,
                 'deleted_file': False,
                 'a_mode': '100644',
                 'b_mode': '100644',
                 'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                         "        unsigned short          type;\n"
                         "        unsigned short          hard_header_len;\n"
                         "        unsigned char           min_header_len;\n"
                         "+       unsigned char           name_assign_type2;\n"
                         "\n"
                         "        unsigned short          needed_headspace;\n"
                         "        unsigned short          needed_tailroom;\n"}
        ydifflist.append(ypath)
        ycommit.diff.return_value = ydifflist

        mri = mock.Mock()
        merge_request = mock.Mock()
        project = mock.Mock()
        merge_request.commits.return_value = [zcommit]
        mri.merge_request = merge_request
        mri.authlevel = 50
        project.commits.get.return_value = ycommit
        project.id = 12345
        mri.project = project

        # Don't run on main
        merge_request.target_branch = "main"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertEqual("", output)

        # Don't run on branch without zstream_target_release
        merge_request.target_branch = "8.9"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertEqual("", output)

        merge_request.target_branch = "8.6"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab do not match", output)

        ydifflist = []
        ydifflist.append(path)
        ycommit.diff.return_value = ydifflist
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab match 100%", output)

        zcommit.message = "There is no spoon"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertIn("Z-Commit 1234567890ab has no Y-Commit reference", output)

        # skip merge commit
        zcommit.parent_ids = ['abcd', 'wxyz']
        ycommit.parent_ids = ['abcd', 'wxyz']
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertEqual("", output)

    def test_print_text_report(self):
        """See that we get a sane-looking report output table."""
        mri = mock.Mock()
        mri.notes = []
        mri.rhcommits = [1, 2, 3, 4, 5]
        self.maxDiff = None
        table = [['6cb6a4a3ede59f047febaeaa801f164954541ff0', ['Posted'],
                  webhook.commit_compare.Match.POSTED, ['1'], ''],
                 ['1508d8fb007a71c7342f1ccbfa15edd5c3d47ccf',
                  ['6cb6a4a2ede59f047febaeaa801f164954541ff0'],
                  webhook.commit_compare.Match.MERGECOMMIT, [], ''],
                 ['12345678aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                  ['abcdef01aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
                  webhook.commit_compare.Match.FULL, [], ''],
                 ['1cd738b13ae9b29e03d6149f0246c61f76e81fcf',
                  ['0c55f51a8c4bade7b2525047130f3925f1dd42bb'],
                  webhook.commit_compare.Match.DIFFS, ['2'], ''],
                 ['348742bd816fbbcda2f8243bf234bf1c91788082', ['RHELonly'],
                  webhook.commit_compare.Match.RHELONLY, ['3'], '']]
        mri.notes.append(webhook.commit_compare.posted_msg())
        mri.notes.append(webhook.commit_compare.diffs_msg())
        mri.notes.append(webhook.commit_compare.rhelonly_msg())
        mri.approved = True
        mri.commit_count = len(table)
        mri.status = webhook.defs.READY_SUFFIX
        report = webhook.commit_compare.print_text_report(mri, table)
        self.assertEqual(report, EXPECTED_REPORT)

    def test_print_text_report_empty(self):
        """See that we get a sane-looking report output table with empty rows."""
        mri = mock.Mock()
        mri.notes = []
        mri.rhcommits = [1, 2, 3, 4, 5]
        table = []
        mri.approved = True
        mri.commit_count = len(table)
        mri.status = webhook.defs.READY_SUFFIX
        report = webhook.commit_compare.print_text_report(mri, table)
        self.assertEqual(report, EXPECTED_REPORT_EMPTY)

    @mock.patch('webhook.commit_compare.print_gitlab_report')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_report_results(self, add_comment, gl_report):
        """Check on reporting functions."""
        mock_session = SessionRunner('commit_compare', [], webhook.commit_compare.HANDLERS)
        mri = mock.Mock()
        mri.merge_request = mock.Mock()
        mri.rhcommits = []
        mri.commit_count = 5
        mri.lab.user.username = "shadowman"
        gl_report.return_value = "Here's your report\n"
        table = [['6cb6a4a3ede59f047febaeaa801f164954541ff0', ['Posted'],
                  webhook.commit_compare.Match.POSTED, ['1'], ''],
                 ['1508d8fb007a71c7342f1ccbfa15edd5c3d47ccf',
                  ['6cb6a4a2ede59f047febaeaa801f164954541ff0'],
                  webhook.commit_compare.Match.MERGECOMMIT, [], ''],
                 ['12345678aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                  ['abcdef01aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
                  webhook.commit_compare.Match.FULL, [], ''],
                 ['1cd738b13ae9b29e03d6149f0246c61f76e81fcf',
                  ['0c55f51a8c4bade7b2525047130f3925f1dd42bb'],
                  webhook.commit_compare.Match.DIFFS, ['2'], ''],
                 ['348742bd816fbbcda2f8243bf234bf1c91788082', ['RHELonly'],
                  webhook.commit_compare.Match.RHELONLY, ['3'], '']]
        errors = "You screwed up SO bad here...\n"
        zcompare_notes = ""
        webhook.commit_compare.MRUCIDInstance.report_results(
            mri, mock_session, table, errors, zcompare_notes)
        add_comment.assert_called_once()
        add_comment.assert_called_with(mri.merge_request, "Here's your report\n" + errors,
                                       bot_name="shadowman",
                                       identifier="Upstream Commit ID Readiness")

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.cdlib.is_rhconfig_commit', mock.Mock(return_value=False))
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('webhook.commit_compare.process_unknown_commit')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.commit_compare.no_upstream_commit_data')
    def test_validate_commit_ids(self, no_ucid, get_udiff, ucom, get_sdiff):
        """Check that we properly flesh out RHCommit dataclass elements."""
        mri = mock.Mock()
        mri.notes = []
        project = mock.Mock()
        mri.project = project
        mri.linux_src = "/src/linux"
        mpcget = mock.MagicMock(id="1234", author_email="johndoe@redhat.com")
        mpcget.diff.return_value = PATCH_A
        mri.project.commits.get.return_value = mpcget
        mri.nids = {'kabi': 0, 'merge': 0, 'unknown': 0}
        c1 = mock.Mock(id="abcdef012345", author_email="jdoe@redhat.com", message="")
        c2 = mock.Mock(id="fedcba012345", author_email="jdoe@redhat.com", message="")
        c3 = mock.Mock(id="fedcba012345", author_email="jdoe@redhat.com", message="")
        c4 = mock.Mock(id="fedcba012346", author_email="jdoe@redhat.com", message="")
        c5 = mock.Mock(id="aaacba012346", author_email="jdoe@redhat.com", message="")
        c6 = mock.Mock(id="aaacba012346", author_email="jdoe@redhat.com", message="Touches kABI")
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c1))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c2))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c3))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c4))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c5))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c6))
        mri.rhcommits[0].ucids = ['RHELonly']
        mri.rhcommits[1].ucids = ['deadbeefdead']
        mri.rhcommits[2].ucids = ['Posted']
        mri.rhcommits[3].ucids = ['Merge']
        mri.rhcommits[4].ucids = ['-']
        mri.rhcommits[5].ucids = ['RHELonly']
        get_udiff.return_value = (None, None)
        get_sdiff.return_value = ("", [])
        webhook.commit_compare.validate_commit_ids(mock.Mock(), mri)
        self.assertEqual(no_ucid.call_count, 4)
        ucom.assert_called_once()
        get_udiff.assert_called_once()
        # Reset counters and run again w/an upstream diff returned
        no_ucid.call_count = 0
        get_udiff.call_count = 0
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c2))
        mri.rhcommits[0].ucids = ['deadbeefdead']
        get_udiff.return_value = (PATCH_A, 'johndoe@redhat.com')
        get_sdiff.return_value = (PATCH_A, ['include/linux/netdevice.h'])
        webhook.commit_compare.validate_commit_ids(mock.Mock(), mri)
        no_ucid.assert_not_called()
        ucom.assert_called_once()
        get_udiff.assert_called_once()

    @mock.patch('webhook.commit_compare.prep_temp_ark_branch', mock.Mock(return_value=('x', 'y')))
    @mock.patch('webhook.commit_compare.evaluate_config', mock.Mock(return_value=False))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.cdlib.is_rhconfig_commit', mock.Mock(return_value=False))
    @mock.patch('webhook.cdlib.is_rhdocs_commit', mock.Mock(return_value=False))
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('webhook.commit_compare.get_report_table')
    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    @mock.patch('webhook.common.extract_dependencies')
    def test_check_on_ucids_has_deps(self, ext_deps, f_labels, c_labels,
                                     c_table, sdiff, udiff):
        """Check that we break loop properly when we have deps."""
        self.maxDiff = None
        mri = mock.Mock(notes=[])
        mri.commit_count = 5
        merge_request = mock.MagicMock()
        merge_request.author = {'id': 1}
        merge_request.iid = 2
        project = mock.MagicMock()
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        commit = mock.Mock(id="4567", author_email="shadowman@redhat.com",
                           message="2\ncommit 12345678", parent_ids=["1234"])
        merge_request.diff_refs['start_sha'] = 'abbadabba'
        merge_request.labels = ['Dependencies::4567']
        merge_request.commits.return_value = [commit]
        mri.merge_request = merge_request
        mri.project = project
        mri.project.mergerequests.get.return_value = merge_request
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(commit))
        mri.nids = {'diffs': 0}
        mri.ucids = []
        mri.omitted = []
        mri.fixes = {}
        table = [['4321', ['8765'], webhook.commit_compare.Match.FULL, [], '']]
        c_table.return_value = (table, True)
        ext_deps.return_value = ['4567']
        f_labels.return_value = ([], [])
        t_out, _ = webhook.commit_compare.check_on_ucids(mock.Mock(), mri)
        self.assertEqual(t_out[0][0][2], webhook.commit_compare.Match.FULL)
        table = [['4321', ['8765'], webhook.commit_compare.Match.DIFFS, [], '']]
        c_table.return_value = (table, True)
        t_out, _ = webhook.commit_compare.check_on_ucids(mock.Mock(), mri)
        self.assertEqual(t_out[0][0][2], webhook.commit_compare.Match.DIFFS)
        table = [['4321', ['8765'], webhook.commit_compare.Match.RHELONLY, [], '']]
        c_table.return_value = (table, True)
        t_out, _ = webhook.commit_compare.check_on_ucids(mock.Mock(), mri)
        self.assertEqual(t_out[0][0][2], webhook.commit_compare.Match.RHELONLY)

    @mock.patch('webhook.commit_compare.prep_temp_ark_branch', mock.Mock(return_value=('x', 'y')))
    @mock.patch('webhook.commit_compare.evaluate_config', mock.Mock(return_value=False))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.cdlib.is_rhconfig_commit', mock.Mock(return_value=False))
    @mock.patch('webhook.cdlib.set_dependencies_label')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('webhook.commit_compare.get_report_table')
    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    @mock.patch('webhook.common.extract_dependencies')
    def test_check_on_ucids_rhdocs(self, ext_deps, f_labels, c_labels,
                                   c_table, sdiff, udiff, set_dep):
        """Check that a commit that belongs to docs are properly evaluated."""
        self.maxDiff = None
        mri = mock.Mock(notes=[])
        mri.commit_count = 3
        merge_request = mock.MagicMock()
        merge_request.author = {'id': 1}
        merge_request.iid = 2
        project = mock.MagicMock()
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        set_dep.return_value = 'Dependencies::4567'
        commit = mock.Mock(id="4567", author_email="shadowman@redhat.com",
                           message="1\n", parent_ids=["1234"])
        commit.diff = mock.Mock(return_value=[{'new_path': 'redhat/rhdocs/info/owners.yaml'}])
        merge_request.diff_refs['start_sha'] = 'abbadabba'
        merge_request.labels = ['Dependencies::4567']
        merge_request.commits.return_value = [commit]
        mri.merge_request = merge_request
        mri.project = project
        mri.project.mergerequests.get.return_value = merge_request
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(commit))
        mri.nids = {'rhelonly': 0}
        mri.ucids = []
        mri.omitted = []
        mri.fixes = {}
        table = [['4321', ['8765'], webhook.commit_compare.Match.FULL, [], '']]
        c_table.return_value = (table, True)
        ext_deps.return_value = []
        f_labels.return_value = ([], [])
        t_out, _ = webhook.commit_compare.check_on_ucids(mock.Mock(), mri)

    @mock.patch('webhook.common.get_commits_count')
    def test_check_on_ucids_no_commits(self, mock_gcc):
        mri = mock.Mock()
        mri.lab.user.username = 'botty'
        session = mock.Mock()
        mock_gcc.return_value = 0, 30
        webhook.commit_compare.MRUCIDInstance.run_ucid_validation(mri, session)
        errors = "**Upstream Commit ID Readiness Error(s)**  \n"
        errors += "*ERROR*: This Merge Request appears to have no commits for the commit "
        errors += "reference webhook to process."
        session.update_webhook_comment.assert_called_with(mri.merge_request, errors,
                                                          bot_name='botty',
                                                          identifier="Upstream Commit ID Readiness")
        self.assertEqual(mri.status, webhook.defs.NEEDS_REVIEW_SUFFIX)

    def test_check_on_ucids_excessive_commits(self):
        mri = mock.Mock()
        mri.commit_count = 2001
        mri.authlevel = 30
        output = webhook.commit_compare.check_on_ucids(mock.Mock(), mri)
        self.assertEqual("**Upstream Commit ID Readiness Error(s)**  \n"
                         "*ERROR*: This Merge Request has too many commits for the commit "
                         "reference webhook to process (2001) -- please make sure you have "
                         "targeted the correct branch.", output[1])

    @mock.patch('git.Repo')
    def test_get_upstream_diff_commit_not_found(self, mocked_repo):
        """Test the code that does not find the coomit ID."""
        mri = mock.Mock()
        ucid = "a1b2c3d4e5"
        filelist = []
        mocked_repo.return_value.commit.side_effect = Exception('Commit not found!')
        self.assertEqual(webhook.commit_compare.get_upstream_diff(mri, ucid, filelist), (None, ""))

    @mock.patch('git.Repo')
    def test_get_upstream_diff_commit_found_but_filelist_empty(self, mocked_repo):
        """Test the code that does not find the coomit ID."""
        mri = mock.Mock()
        ucid = "a1b2c3d4e5"
        filelist = []
        commit = mock.MagicMock()
        commit.author.email = "shadowman@redhat.com"
        mocked_repo.return_value = mock.MagicMock()
        mocked_repo.return_value.commit.return_value = commit
        mocked_repo.return_value.git.diff.return_value = PATCH_D
        self.assertEqual(webhook.commit_compare.get_upstream_diff(mri, ucid, filelist),
                         (PATCH_D, "shadowman@redhat.com"))

    @mock.patch('git.Repo')
    def test_get_upstream_diff_full(self, mocked_repo):
        """Test the code that does not find the coomit ID."""
        mri = mock.Mock()
        ucid = "a1b2c3d4e5"
        filelist = ['include/linux/netdevice.h']
        commit = mock.MagicMock()
        commit.author.email = "shadowman@redhat.com"
        mocked_repo.return_value = mock.MagicMock()
        mocked_repo.return_value.commit.return_value = commit
        mocked_repo.return_value.git.diff.return_value = PATCH_D
        self.assertEqual(webhook.commit_compare.get_upstream_diff(mri, ucid, filelist),
                         (PATCH_B, "shadowman@redhat.com"))

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_kabi_label(self, f_labels, c_labels):
        """Test the code that adds a kabi label."""
        mri = mock.Mock()
        merge_request = mock.MagicMock()
        merge_request.iid = 2
        mri.merge_request = merge_request
        mri.project = mock.Mock()
        f_labels.return_value = ([], [])
        table = [['4321', ['8765'], webhook.commit_compare.Match.KABI, [], '']]
        status = webhook.commit_compare.add_kabi_label(mri, table)
        self.assertTrue(status)
        table = [['abcd', ['dead'], webhook.commit_compare.Match.FULL, [], '']]
        status = webhook.commit_compare.add_kabi_label(mri, table)
        self.assertFalse(status)

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_config_mismatch_label(self, f_labels, c_labels):
        """Test the code that adds a config mismatch label."""
        mri = mock.Mock()
        merge_request = mock.MagicMock()
        merge_request.iid = 2
        mri.merge_request = merge_request
        mri.project = mock.Mock()
        f_labels.return_value = ([], [])
        table = [['4321', ['8765'], webhook.commit_compare.Match.CONFIG, [], '']]
        status = webhook.commit_compare.add_config_mismatch_label(mri, table)
        self.assertTrue(status)
        table = [['abcd', ['dead'], webhook.commit_compare.Match.FULL, [], '']]
        status = webhook.commit_compare.add_config_mismatch_label(mri, table)
        self.assertFalse(status)

    def test_no_upstream_commit_data(self):
        """Test function that maps to different no ucid types."""
        mri = mock.Mock(notes=[])
        mri.nids = {'noucid': 0, 'rhelonly': 0, 'posted': 0}
        rhcommit = mock.Mock(ucids=[], match=webhook.commit_compare.Match.FULL)
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.FULL)
        rhcommit.ucids = ["-"]
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.NOUCID)
        rhcommit.ucids = ["RHELonly"]
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.RHELONLY)
        rhcommit.ucids = ["Posted"]
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.POSTED)

    def test_no_ucid_commit_note(self):
        """Test that we get the expected data back for commits w/o an upstream commit ID."""
        mri = mock.Mock(notes=[])
        mri.nids = {'noucid': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_no_ucid_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.noucid_msg())
        self.assertEqual(mri.nids['noucid'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.NOUCID)

    def test_rhelonly_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[])
        mri.nids = {'rhelonly': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_rhel_only_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.rhelonly_msg())
        self.assertEqual(mri.nids['rhelonly'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.RHELONLY)

    def test_posted_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[])
        mri.nids = {'posted': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_posted_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.posted_msg())
        self.assertEqual(mri.nids['posted'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.POSTED)

    def test_merge_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[])
        mri.nids = {'merge': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_merge_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.merge_msg())
        self.assertEqual(mri.nids['merge'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.MERGECOMMIT)

    def test_unknown_commit_note(self):
        """Test that we get the expected data back for commits that are of unknown origin."""
        mri = mock.Mock(notes=[])
        mri.nids = {'unknown': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_unknown_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.unk_cid_msg())
        self.assertEqual(mri.nids['unknown'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.NOUCID)

    def test_kabi_commit_note(self):
        """Test that we get the expected data back for commits that flagged for kabi screening."""
        mri = mock.Mock(notes=[])
        mri.nids = {'kabi': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_kabi_patch(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.kabi_msg())
        self.assertEqual(mri.nids['kabi'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.KABI)

    def test_commit_with_diffs_note(self):
        """Test that we get the expected data back for commits with diffs from upstream."""
        mri = mock.Mock(notes=[])
        mri.nids = {'diffs': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_commit_with_diffs(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.diffs_msg())
        self.assertEqual(mri.nids['diffs'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.DIFFS)

    def test_partial_commit_note(self):
        """Test that we get the expected data back for partial backport commits."""
        mri = mock.Mock(notes=[])
        mri.nids = {'part': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_partial_backport(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.partial_msg())
        self.assertEqual(mri.nids['part'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.PARTIAL)

    def test_get_submitted_diff(self):
        """Test that we can properly extract patch content from a submitted diff."""
        commit = []
        path = {'old_path': 'include/linux/netdevice.h',
                'new_path': 'include/linux/netdevice.h',
                'new_file': False,
                'renamed_file': False,
                'deleted_file': False,
                'a_mode': '100644',
                'b_mode': '100644',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_A)
        self.assertEqual(output[1], ["include/linux/netdevice.h"])
        commit = []
        path['new_file'] = True
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_E)
        commit = []
        path['new_file'] = False
        path['deleted_file'] = True
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_F)
        commit = []
        path['new_file'] = False
        path['deleted_file'] = False
        path['renamed_file'] = True
        path['new_path'] = 'include/linux/foobar.h'
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_G)

    def test_partial_diff(self):
        """Check that we can match partial diffs."""
        filelist = []
        filelist.append("include/linux/netdevice.h")
        pdiff = webhook.cdlib.get_partial_diff(PATCH_D, filelist)
        output = webhook.cdlib.compare_commits(pdiff, PATCH_A)
        self.assertEqual(output, [])

    def test_has_upstream_commit_hash(self):
        """Check that we get expected return values from different match types."""
        m1 = webhook.commit_compare.Match.FULL
        m2 = webhook.commit_compare.Match.PARTIAL
        m3 = webhook.commit_compare.Match.DIFFS
        m4 = webhook.commit_compare.Match.NOUCID
        m5 = webhook.commit_compare.Match.RHELONLY
        m6 = webhook.commit_compare.Match.POSTED
        m7 = webhook.commit_compare.Match.MERGECOMMIT
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m1))
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m2))
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m3))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m4))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m5))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m6))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m7))

    def test_extract_ucid(self):
        mri = mock.Mock()
        mri.ucids = set()
        mri.omitted = set()
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2",
                       message="2\ncommit a012345",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="abcd", author_email="shadowman2021@redhat.com",
                       title="This is commit 3",
                       message="2\nUpstream Status: git://linux-nfs.org/~bfields/linux.git",
                       parent_ids=["5678"])
        c4 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 4",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678"
                               "\ncommit a234567890abcdef1234567890abcdef12345678"
                               "\ncommit b234567890abcdef1234567890abcdef12345678"
                               "\ncommit c234567890abcdef1234567890abcdef12345678"
                               "\ncommit d234567890abcdef1234567890abcdef12345678"
                               "\ncommit e234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c5 = mock.Mock(id="8765", author_email="developer@redhat.com",
                       title="This is commit 5",
                       message="5\ncommit  1234567890",
                       short_id="abcdefg1", parent_ids=["1234"])
        c6 = mock.Mock(id="9745", author_email="developer@redhat.com",
                       title="This is commit 6",
                       message="6\n (cherry picked from commit 1234567890abcdef)",
                       short_id="abcdefg2", parent_ids=["1235"])
        c7 = mock.Mock(id="9845", author_email="developer@redhat.com",
                       title="Merge: This is commit 7",
                       message="6\n (cherry picked from commit 1234567890abcdef)",
                       short_id="abc3efg2", parent_ids=["1357", "2468"])
        c8 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       message="1\n(cherry picked from commit "
                               "1234567890abcdef1234567890abcdef12345678)"
                               "\n (cherry picked from commit "
                               "2234567890abcdef1234567890abcdef12345678)"
                               "\n(cherry picked from commit   "
                               "3234567890abcdef1234567890abcdef12345678)",
                       parent_ids=["abcd"])
        c9 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678"
                               "\ncommit 2234567890abcdef1234567890abcdef12345678"
                               "\n commit 3234567890abcdef1234567890abcdef12345678"
                               "\ncommit 4234567890abcdef1234567890abcdef12345678"
                               "\ncommit  5234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c10 = mock.Mock(id="1234567890", author_email="jdoe@redhat.com",
                        title="This is a commit",
                        message="XYZ\nUpstream Status: https://github.com/torvalds/linux.git",
                        parent_ids=["abcdef0123"])
        c11 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                        title="This is commit 11",
                        message="1\n"
                                "Upstream Status: RHEL Only\n",
                        parent_ids=["abcd"])
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c1),
                         (["1234567890abcdef1234567890abcdef12345678"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c2), ([], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c3), (['Posted'], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c4), (['RHELonly'], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c5),
                         (['1234567890'],
                          '\n* Incorrect format git commit line in abcdefg1.  \n'
                          '`Expected:` `commit 1234567890`  \n'
                          '`Found   :` `commit  1234567890`  \n'
                          'Git hash only has 10 characters, expected 40.  \n'))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c6),
                         (['1234567890abcdef'],
                          '\n* Incorrect format git cherry-pick line in abcdefg2.  \n'
                          '`Expected:` `(cherry picked from commit 1234567890abcdef)`  \n'
                          '`Found   :` ` (cherry picked from commit 1234567890abcdef)`  \n'
                          'Git hash only has 16 characters, expected 40.  \n'))
        mri.project.commits.get.return_value = c7
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c7), (['Merge'], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c8),
                         (["1234567890abcdef1234567890abcdef12345678"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c9),
                         (["1234567890abcdef1234567890abcdef12345678",
                           "2234567890abcdef1234567890abcdef12345678",
                           "4234567890abcdef1234567890abcdef12345678"], ''))
        # This commit should NOT be considered Posted, as Linus' tree always needs a hash
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c10), ([], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c11), (['RHELonly'], ''))

    def _test_payload(self, result, payload):
        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}):
            self._test(result, payload, [])
        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'development'}):
            self._test(result, payload, [])

    # pylint: disable=too-many-arguments
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock())
    @mock.patch('webhook.common.add_label_to_merge_request', mock.Mock())
    def _test(self, result, payload, labels, assert_labels=None):

        # setup dummy gitlab data
        project = mock.Mock(path_with_namespace="foo/bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        merge_request.author = {'id': 1, 'username': 'jdoe'}
        merge_request.iid = 2
        merge_request.state = payload["state"]
        merge_request.description = payload["description"]
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1", parent_ids=['abcd'],
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2", parent_ids=['abce'],
                       message="2\ncommit a01234567890abcdef1234567890abcdef123456")
        c2.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com",
                       title="This is commit 3", parent_ids=['abcf'],
                       message="3\nUpstream Status: RHEL-only")
        c3.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c4 = mock.Mock(id="deadbeef", author_email="zzzzzz@redhat.com", message="4\n"
                       "(cherry picked from commit abcdef0123456789abcdef0123456789abcdef01)",
                       title="This is commit 4", parent_ids=['abc0'])
        c4.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c5 = mock.Mock(id="0ff0ff00", author_email="spam@redhat.com",
                       title="This is commit 5", parent_ids=['abc1'],
                       message="5\nUpstream-status: Posted")
        c5.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c6 = mock.Mock(id="f00df00d", author_email="spam@redhat.com",
                       title="This is commit 6", parent_ids=['abc2'],
                       message="6\nUpstream-status: Embargoed")
        c6.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c7 = mock.Mock(id="abba", author_email="jdoe@redhat.com",
                       title="This is commit 7", parent_ids=['abc3'],
                       message="7\n")
        c7.diff = mock.Mock(return_value=[{'new_path': 'redhat/rhdocs/info/owners.yaml'}])
        mock_gl_instance = mock.Mock()
        mock_gl_instance.projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        project.namespace = mock.MagicMock(id=1, full_path=webhook.defs.RHEL_KERNEL_NAMESPACE)
        project.archived = payload["project"]["archived"]
        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6]
        merge_request.pipeline = {'status': 'success'}
        mock_gl_instance.users.list.return_value = []
        rh_group = mock.Mock(id=10810393)
        mock_gl_instance.groups.list.return_value = [rh_group]
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "deadbeef": c4, "00f00f00": c5,
                           "f00df00d": c6}
        mri = mock.Mock()
        mri.ucids = set()
        mri.omitted = set()
        mri.authlevel = 50
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c1),
                         (["1234567890abcdef1234567890abcdef12345678"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c2),
                         (["a01234567890abcdef1234567890abcdef123456"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c3), (["RHELonly"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c4),
                         (["abcdef0123456789abcdef0123456789abcdef01"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c5), (["Posted"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c6), (["Posted"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c7), ([], ''))

        parser = webhook.common.get_arg_parser('COMMIT_COMPARE')
        parser.add_argument('--linux-src')
        args = parser.parse_args('--linux-src /src/linux'.split())
        args.disable_user_check = True

        mock_session = SessionRunner('commit_compare', args, webhook.commit_compare.HANDLERS)
        mock_session.gl_instance = mock_gl_instance
        mock_session.rh_projects = mock.Mock()
        event = create_event(mock_session, {'message-type': 'gitlab'}, payload)
        event.gl_mr = merge_request

        webhook.commit_compare.process_gl_event({}, mock_session, event)

        if result:
            mock_gl_instance.projects.get.assert_called_with('g/p')
            project.mergerequests.get.assert_called_with(2)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels), sorted(assert_labels))

    def test_create_worktree_timestamp(self):
        worktree_dir = '/tmp/worktree'
        with mock.patch('builtins.open', mock.mock_open()) as m:
            webhook.commit_compare.create_worktree_timestamp(worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'w', encoding='ascii')
            handle = m()
            handle.write.assert_called_once()

    def test_read_config_content(self):
        worktree_dir = '/tmp/worktree'
        filename = 'redhat/configs/common/generic/CONFIG_RV'

        with mock.patch('os.path.exists', return_value=True):
            with mock.patch('builtins.open', mock.mock_open()) as m:
                webhook.commit_compare.read_config_content(worktree_dir, filename)
                m.assert_called_once_with(f'{worktree_dir}/{filename}', 'r', encoding='ascii')
                handle = m()
                handle.read.assert_called_once()

    def test_read_config_content_that_no_exists(self):
        worktree_dir = '/foo/bar/baz'
        filename = 'redhat/configs/common/generic/CONFIG_RV'

        self.assertEqual(webhook.commit_compare.read_config_content(worktree_dir, filename), None)

    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.worktree_remove', mock.Mock(return_value=True))
    def test_clean_up_temp_ark_branch(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        merge_branch = 'temp-merge-branch'
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            webhook.commit_compare.clean_up_temp_ark_branch('mocked', merge_branch, worktree_dir)
            self.assertIn(f'Removed worktree {worktree_dir} and deleted branch {merge_branch}',
                          logs.output[-1])

    @mock.patch('webhook.commit_compare.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('os.path.exists', mock.Mock(return_value=False))
    @mock.patch('webhook.commit_compare.clean_up_temp_ark_branch')
    def test_handle_stale_worktree(self, clean_up):
        kernel_src = '/src/kernel'
        merge_branch = 'kernel-ark/os-build'
        worktree_dir = '/tmp/worktree'
        clean_up.return_value = True

        m_args = ['git', 'branch', '-D', f'{merge_branch}-save']
        self._add_run_result(m_args, 4, 'Uhhh yeah it exploded')
        with mock.patch('builtins.open', mock.mock_open(read_data='20200504112233')) as m:
            with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
                webhook.commit_compare.handle_stale_worktree(kernel_src, merge_branch, worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_called_once()

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
        clean_up.call_count = 0
        with mock.patch('builtins.open', mock.mock_open(read_data=timestamp)) as m2:
            with self.assertRaises(messagequeue.QuietNackException):
                webhook.commit_compare.handle_stale_worktree(kernel_src, merge_branch, worktree_dir)
            m2.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_not_called()

    def _test_validate_config(self, mocked_read, diffs, mri=mock.MagicMock()):
        """Check if a config matches with upstream repo."""
        ark = "kernel-ark"
        mri.notes = []
        mri.linux_src = "/src/linux"
        mri.nids = {'config': 0}
        c = mock.Mock(id="abcdef012345", author_email="jdoe@redhat.com", message="")
        c.diff.return_value = diffs
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c))

        m_args0 = ['git', 'fetch', f'{ark}']
        m_args1 = ['git', 'worktree', 'add', '-B', f'{ark}-config-check',
                   f'/src/{ark}-config-check/', f'{ark}/os-build']
        m_args2 = ['git', 'worktree', 'remove', '-f', f'/src/{ark}-config-check/']
        m_args3 = ['git', 'branch', '-D', f'{ark}-config-check']
        self._add_run_result(m_args0, 0)
        self._add_run_result(m_args1, 0)
        self._add_run_result(m_args2, 0)
        self._add_run_result(m_args3, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            webhook.commit_compare.validate_commit_ids(mock.Mock(), mri)
            table = webhook.commit_compare.get_report_table(mri)
            return webhook.commit_compare.add_config_mismatch_label(mri, table)

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_addition_match(self, read, add_label):
        """Check if a config matches with rhel configs in upstream repo."""
        read.return_value = "CONFIG_RV=y"
        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_addition_match_in_ark(self, read, add_label):
        """Check if a config matches with ark configs in upstream repo."""
        read.side_effect = [None, "CONFIG_RV=y"]
        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_change_match(self, read, add_label):
        """Check if a config matches with upstream repo."""
        read.return_value = "CONFIG_MEMTEST=y"
        diffs = [{'old_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': False,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -1 +1 @@\n"
                          "-# CONFIG_MEMTEST is not set\n"
                          "+CONFIG_MEMTEST=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    def test_validate_config_change_mismatch(self, read, add_label):
        """Check if a config matches with upstream repo."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'old_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': False,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -1 +1 @@\n"
                          "-# CONFIG_MEMTEST is not set\n"
                          "+CONFIG_MEMTEST=y\n"}]

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.assertTrue(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    def test_validate_config_addition_that_does_not_exist_in_both(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        read.side_effect = [None, None, None, None]
        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"},
                 {"new_path": "kernel/rh_messages.c",
                  "old_path": "kernel/rh_messages.c",
                  "a_mode": "100644",
                  "b_mode": "100644",
                  "new_file": False,
                  "renamed_file": False,
                  "deleted_file": False,
                  "generated_file": None,
                  "diff": "@@ -163,7 +163,7 @@ void mark_tech_preview(const char *msg, struct "
                          "module *mod)\n \tif (msg)\n \t\tstr = msg;\n #ifdef CONFIG_MODULES\n"
                          "-\telse if (mod && mod->name)\n+\telse if (mod)\n "
                          "\t\tstr = mod->name;\n #endif\n \n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
                self.assertTrue(self._test_validate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    def test_validate_config_addition_that_exists_but_not_enabled(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            # Skip staging test here just to cover a different branch
            self.assertTrue(self._test_validate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    def test_validate_config_addition_does_not_exist(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
                self.assertTrue(self._test_validate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_deletion_match(self, read, add_label):
        """Check if a config deletion matches with upstream repo."""
        read.return_value = None
        diffs = [{'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'old_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': False,
                  'renamed_file': False,
                  'deleted_file': True,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -1 +0,0 @@\n"
                          "-CONFIG_RV=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment', mock.Mock(return_value=None))
    def test_validate_config_deletion_mismatch(self, read, add_label):
        """Check if a config deletion mismatches with upstream repo."""
        read.return_value = 'CONFIG_RV=y'
        diffs = [{'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'old_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': False,
                  'renamed_file': False,
                  'deleted_file': True,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -1 +0,0 @@\n"
                          "-CONFIG_RV=y\n"}]

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.assertTrue(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_rename_match(self, read, add_label):
        """Check if a config renaming matches with upstream repo."""
        read.return_value = None
        diffs = [{'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'old_path': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'new_file': False,
                  'renamed_file': True,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': ""}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_documentation_skip(self, read, add_label):
        """Check if a config renaming matches with upstream repo."""
        read.return_value = None
        diffs = [{'new_path': 'Documentation/dev-tools/testing-overview.rst',
                  'old_path': 'Documentation/dev-tools/testing-overview.rst',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'generated_file': None,
                  'a_mode': '0',
                  'b_mode': '100644',
                  'diff': PATCH_H}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.remove_labels_from_merge_request')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_labels(self, read, add_label, rem_label):
        """Check if a config mismatch label is properly handled."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        self.assertTrue(self._test_validate_config(read, diffs))
        add_label.assert_called_once()

        mri = mock.MagicMock()
        mri.merge_request.labels = ["Config::ARK::Mismatch"]

        diffs = [{'old_path': '/dev/null',
                  'new_path': 'redhat/configs/common/generic/CONFIG_RV',
                  'new_file': True,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+# CONFIG_RV is not set\n"}]

        self.assertFalse(self._test_validate_config(read, diffs, mri))
        rem_label.assert_called_once()

    def test_validate_x86_64_file_map(self):
        """Check if a config inside a x86_64 directory in ARK match with downstream versions."""
        filename = "redhat/configs/rhel/generic/x86/x86_64/CONFIG_NVRAM"
        commonfilename_exp = "redhat/configs/common/generic/x86/CONFIG_NVRAM"
        arkfilename_exp = "redhat/configs/rhel/generic/x86/CONFIG_NVRAM"

        commonfilename, arkfilename = webhook.commit_compare.x86_filename_replacement(filename)
        self.assertEqual(commonfilename, commonfilename_exp)
        self.assertEqual(arkfilename, arkfilename_exp)

        filename = "redhat/configs/common/generic/x86/x86_64/CONFIG_GENERIC_CPU"
        commonfilename_exp = "redhat/configs/common/generic/x86/CONFIG_GENERIC_CPU"
        arkfilename_exp = "redhat/configs/rhel/generic/x86/CONFIG_GENERIC_CPU"

        commonfilename, arkfilename = webhook.commit_compare.x86_filename_replacement(filename)
        self.assertEqual(commonfilename, commonfilename_exp)
        self.assertEqual(arkfilename, arkfilename_exp)

        filename = "redhat/configs/common/generic/CONFIG_RV"
        rhelfilename = filename.replace('common', 'rhel')
        commonfilename, arkfilename = webhook.commit_compare.x86_filename_replacement(filename)
        # We don't expect any change
        self.assertEqual(commonfilename, filename)
        self.assertEqual(arkfilename, rhelfilename)

    @mock.patch('webhook.commit_compare.new_session')
    def test_main_entrypoint(self, mock_session):
        """Test main entrypoint."""
        args = '--linux-src /foo/bar/baz/'.split()
        webhook.commit_compare.main(args)
        mock_session.assert_called_with('commit_compare', mock.ANY, webhook.commit_compare.HANDLERS)
        self.assertFalse(mock_session.call_args[0][1].disable_inactive_branch_check)
        self.assertFalse(mock_session.call_args[0][1].disable_closed_status_check)
        self.assertEqual(mock_session.call_args[0][1].linux_src, args[1])
        self.assertEqual(webhook.commit_compare.HANDLERS, {
            GitlabObjectKind.NOTE: webhook.commit_compare.process_gl_event,
            GitlabObjectKind.MERGE_REQUEST: webhook.commit_compare.process_gl_event,
        })

    @mock.patch('webhook.commit_compare.new_session')
    def test_main_entrypoint_no_args(self, mock_session):
        """Test main entrypoint."""
        args = '--linux-src /foo/bar/baz/'.split()
        with mock.patch.dict('os.environ', {'LINUX_SRC': '/foo/bar/baz/'}):
            webhook.commit_compare.main([])
            mock_session.assert_called_with('commit_compare', mock.ANY,
                                            webhook.commit_compare.HANDLERS)
            self.assertFalse(mock_session.call_args[0][1].disable_inactive_branch_check)
            self.assertFalse(mock_session.call_args[0][1].disable_closed_status_check)
            self.assertEqual(mock_session.call_args[0][1].linux_src, args[1])

    @mock.patch('webhook.commit_compare.new_session')
    def test_main_entrypoint_no_nothing(self, mock_session):
        """Test main entrypoint."""
        with self.assertRaises(SystemExit) as ret:
            webhook.commit_compare.main([])

        self.assertTrue(isinstance(ret.exception, SystemExit))
        self.assertEqual(ret.exception.code, 2)
