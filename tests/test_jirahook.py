"""Tests for the jirahook."""
from copy import deepcopy
from datetime import datetime
from unittest import mock

from tests import fakes
from tests import fakes_bz
from tests import fakes_jira
from tests import fakes_jira_mrs as fakes_mrs
from tests.no_socket_test_case import NoSocketTestCase
from webhook import defs
from webhook import jirahook
from webhook.bug import Bug
from webhook.description import MRDescription
from webhook.jissue import JIssue
from webhook.jissue import _find_linked_issues_harder
from webhook.jissue import find_linked_issues
from webhook.rh_metadata import Projects
from webhook.session import SessionRunner

# expected assertEquals check results for fakes_mrs.MR410
MR410_EQUALS = {'mr_id': 410,
                'global_id': fakes_mrs.MR410_DICT['mr']['id'],
                'depends_mrs': [],
                'description': MRDescription(fakes_mrs.MR410_DICT['mr']['description'],
                                             namespace='group/centos-stream-9'),
                'all_jissue_ids': {f'{defs.JPFX}1234567', f'{defs.JPFX}2323232'},
                'first_dep_sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3'}

MR410_IS = {'project': None,
            'branch': None,
            'is_dependency': False,
            'state': defs.MrState.OPENED,
            'only_internal_files': False,
            'pipeline_finished': None,
            'is_draft': False,
            'has_internal': False,
            'has_untagged': False}

# expected assertEquals check results for fakes_mrs.MR410
MR404_EQUALS = {'mr_id': 404,
                'global_id': fakes_mrs.MR404_DICT['mr']['id'],
                'depends_mrs': [],
                'description': MRDescription(fakes_mrs.MR404_DICT['mr']['description'],
                                             namespace='group/centos-stream-9'),
                'pipeline_finished':
                    datetime.fromisoformat(
                        fakes_mrs.MR404_DICT['mr']['headPipeline']['finishedAt'][:19]
                ),
                'all_jissue_ids': {f'{defs.JPFX}2323232'},
                'first_dep_sha': ''}

MR404_IS = {'project': None,
            'is_dependency': True,
            'branch': None,
            'state': defs.MrState.OPENED,
            'only_internal_files': False,
            'is_draft': False,
            'has_internal': False,
            'has_untagged': False}

DEP_MR10 = {'iid': '10', 'state': 'opened', 'targetBranch': 'main',
            'description': f'JIRA: {defs.JIRA_SERVER}browse/{defs.JPFX}5556667\n'}

DEP_MR20 = {'iid': '20', 'state': 'merged', 'targetBranch': 'main',
            'description': f'JIRA: {defs.JIRA_SERVER}browse/{defs.JPFX}7778889\n'}

DEP_MR30 = {'iid': '30', 'state': 'opened', 'targetBranch': 'main',
            'description': f'JIRA: {defs.JIRA_SERVER}browse/{defs.JPFX}9990001\n'
            f'Depends: {defs.GITFORGE}/group/project/-/merge_requests/20\n'}


@mock.patch('cki_lib.gitlab.get_graphql_client', mock.Mock)
@mock.patch('webhook.jissue.fetch_issues', mock.Mock(return_value=[]))
@mock.patch('webhook.libbz.fetch_bugs', mock.Mock(return_value=[]))
class TestMR(NoSocketTestCase):
    """Tests for the MR dataclass."""

    def _test_mr(self, mr, equals_dict, is_dict):
        print(f'Testing MR {mr.mr_id}...')
        for key, value in equals_dict.items():
            print(f'{key} should be: {value}')
            self.assertEqual(getattr(mr, key), value)
        for key, value in is_dict.items():
            print(f'{key} should be: {value}')
            self.assertIs(getattr(mr, key), value)

    def test_mr_init(self):
        """Sets expected default attribute values."""
        namespace = 'group/project'
        mr_id = 123
        equals_dict = {'namespace': namespace,
                       'mr_id': mr_id,
                       'commits': {},
                       'global_id': '',
                       'depends_mrs': [],
                       'jissues': [],
                       # 'issues_with_scopes': [],
                       'cves': [],
                       # 'cves_with_scopes': [],
                       'all_jissue_ids': set(),
                       'all_descriptions': [],
                       'first_dep_sha': ''}
        is_dict = {'project': None,
                   'is_dependency': False,
                   'description': None,
                   'branch': None,
                   'state': defs.MrState.UNKNOWN,
                   'only_internal_files': False,
                   'pipeline_finished': None,
                   'is_draft': False,
                   'has_internal': False,
                   'has_untagged': False}
        # an empty regular MR
        mr = fakes_mrs.make_mr(namespace=namespace, mr_id=mr_id)
        self._test_mr(mr, equals_dict, is_dict)
        # a missing MR?
        mr = fakes_mrs.make_mr(namespace=namespace, mr_id=mr_id,
                               query_results_list=[{'mr': None}])
        self._test_mr(mr, equals_dict, is_dict)
        # an empty dependency MR
        mr = fakes_mrs.make_mr(namespace=namespace, mr_id=mr_id, is_dependency=True)
        is_dict['is_dependency'] = True
        self._test_mr(mr, equals_dict, is_dict)

    @mock.patch('webhook.jirahook.find_linked_issues')
    @mock.patch('webhook.jissue.is_jissue_ready')
    def test_mr_load_data_410(self, mock_is_ready, mock_linked):
        """Updates attributes with data from the query."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err

        mr = fakes_mrs.make_mr('group/centos-stream-9', 410,
                               query_results_list=[fakes_mrs.MR410_DICT])
        linked_mr = fakes_mrs.make_mr('group/centos-stream-9', 404,
                                      query_results_list=[fakes_mrs.MR404_DICT])
        mock_linked.return_value = [linked_mr]
        mr.description.depends_jiras.update({f'{defs.JPFX}2323232'})
        MR410_EQUALS['description'].depends_jiras.update({f'{defs.JPFX}2323232'})
        MR410_IS['project'] = mr.projects.projects[24152864]
        MR410_IS['branch'] = mr.projects.projects[24152864].branches[0]
        self._test_mr(mr, MR410_EQUALS, MR410_IS)
        self.assertCountEqual(mr.commits.keys(), [commit['sha'] for commit in
                                                  fakes_mrs.MR410_DICT['mr']['commits']['nodes']])
        self.assertEqual(len(mr.jissues), 2)
        self.assertEqual(len(mr.cves), 1)
        self.assertEqual(len(mr.all_descriptions),
                         len(fakes_mrs.MR410_DICT['mr']['commits']['nodes'])+1)
        self.assertEqual(len(mr.issues_with_scopes), 2)
        self.assertEqual(len(mr.linked_jissues), 1)
        self.assertEqual(len(mr.cves_with_scopes), 1)

    @mock.patch('webhook.jissue.is_jissue_ready')
    def test_mr_load_data_404(self, mock_is_ready):
        """Updates attributes with data from the query."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err

        mr = fakes_mrs.make_mr('group/centos-stream-9', 404,
                               query_results_list=[fakes_mrs.MR404_DICT], is_dependency=True)
        MR404_IS['project'] = mr.projects.projects[24152864]
        MR404_IS['branch'] = mr.projects.projects[24152864].branches[0]
        self._test_mr(mr, MR404_EQUALS, MR404_IS)
        self.assertCountEqual(mr.commits.keys(), [commit['sha'] for commit in
                                                  fakes_mrs.MR404_DICT['mr']['commits']['nodes']])
        self.assertEqual(len(mr.jissues), 1)
        self.assertEqual(len(mr.cves), 0)
        self.assertEqual(len(mr.all_descriptions),
                         len(fakes_mrs.MR404_DICT['mr']['commits']['nodes'])+1)
        self.assertEqual(len(mr.issues_with_scopes), 1)
        self.assertEqual(len(mr.cves_with_scopes), 0)

    def test_mr_has_only_internal_files(self):
        """Returns True if all the paths in the list startwith defs.INTERNAL_FILES."""
        path_list = ['include/net.h', 'redhat/scripts/check.sh']
        self.assertIs(jirahook.MR._mr_has_only_internal_files(path_list), False)
        path_list = ['.gitlab.yaml', 'redhat/scripts/check.sh']
        self.assertIs(jirahook.MR._mr_has_only_internal_files(path_list), True)


class TestIssueRow(NoSocketTestCase):
    """Tests for the IssueRow class."""

    def test_issuerow_populate_empty(self):
        """Updates the attribute values of a IssueRow."""
        test_issue = JIssue.new_missing(ji_id=f'{defs.JPFX}1234567', mrs=[])
        test_issuerow = jirahook.IssueRow()
        test_issuerow.populate(test_issue, '')
        exp = f'[{defs.JPFX}1234567]({defs.JIRA_SERVER}browse/{defs.JPFX}1234567) (UNKNOWN)'
        self.assertEqual(test_issuerow.JIRA_Issue, exp)
        self.assertEqual(test_issuerow.CVEs, 'None')
        self.assertEqual(test_issuerow.Commits, 'None')
        self.assertEqual(test_issuerow.Readiness, 'INVALID')
        self.assertEqual(test_issuerow.Policy_Check, 'Check not done: No JIRA Issue')
        self.assertEqual(test_issuerow.Notes, '-')

    @mock.patch('webhook.jissue.is_jissue_ready')
    def test_issuerow_populate(self, mock_is_ready):
        """Updates the attribute vales of a IssueRow."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err

        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        mr410.projects.projects[24152864].branches[0].__dict__['policy'] = [1]
        test_issue = JIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=[mr410])
        test_issue.scope = defs.MrScope.READY_FOR_QA

        test_issuerow = jirahook.IssueRow()
        test_issuerow.populate(test_issue, [1, 3, 5])

        exp = f'[{defs.JPFX}1234567]({defs.JIRA_SERVER}browse/{defs.JPFX}1234567) (IN_PROGRESS)'
        self.assertEqual(test_issuerow.JIRA_Issue, exp)
        self.assertEqual(test_issuerow.CVEs,
                         '[CVE-1235-13516](https://bugzilla.redhat.com/CVE-1235-13516)<br>')
        self.assertIn('0aa467549b4e997d023c29f4d481aee01b9e9471', test_issuerow.Commits)
        self.assertIn('e53eab9f887f784044ad32ef5c082695831d90d9', test_issuerow.Commits)
        self.assertIn('88cdd4035228dac16878eb907381afea6ceffeaa', test_issuerow.Commits)
        self.assertEqual(test_issuerow.Readiness, 'READY_FOR_QA')
        self.assertEqual(test_issuerow.Policy_Check, f'Failed:<br>{cpc_err}')
        self.assertEqual(test_issuerow.Notes, 'See 1<br>See 3<br>See 5')
        mr410.projects.projects[24152864].branches[0].__dict__['policy'] = []


@mock.patch('webhook.jissue.fetch_issues', mock.Mock(return_value=[]))
class TestCveRow(NoSocketTestCase):
    """Tests for the CveRow class."""

    def test_cverow_populate_empty(self):
        """Updates the attribute values of CveRow."""
        test_cve = Bug.new_missing('CVE-2101-21515', mrs=[])
        test_cverow = jirahook.CveRow()
        test_cverow.populate(test_cve, '')
        exp = '[CVE-2101-21515](https://bugzilla.redhat.com/CVE-2101-21515)<br>'
        self.assertEqual(test_cverow.CVEs, exp)
        self.assertEqual(test_cverow.Priority, 'Unknown')
        self.assertEqual(test_cverow.Commits, 'None')
        self.assertEqual(test_cverow.Clones, 'Unknown')
        self.assertEqual(test_cverow.Readiness, 'INVALID')
        self.assertEqual(test_cverow.Notes, '-')

    def test_cverow_populate(self):
        """Updates the attribute values of CveRow."""
        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        test_cve = Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=[mr410])

        test_cverow = jirahook.CveRow()
        test_cverow.populate(test_cve, [2, 3])

        exp = '[CVE-1235-13516](https://bugzilla.redhat.com/CVE-1235-13516)<br>'
        self.assertEqual(test_cverow.CVEs, exp)
        self.assertEqual(test_cverow.Priority, 'High')
        self.assertIn('0aa467549b4e997d023c29f4d481aee01b9e9471', test_cverow.Commits)
        self.assertIn('e53eab9f887f784044ad32ef5c082695831d90d9', test_cverow.Commits)
        self.assertIn('88cdd4035228dac16878eb907381afea6ceffeaa', test_cverow.Commits)
        self.assertEqual(test_cverow.Clones, 'None')
        self.assertEqual(test_cverow.Readiness, 'INVALID')
        self.assertEqual(test_cverow.Notes, 'See 2<br>See 3')

    def test_format_clones(self):
        """Returns a string describing the CVE clones, if any."""
        # No parent_mr, returns 'Unknown'
        mock_cve = mock.Mock(parent_mr=None)
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'Unknown')

        # Not a High Prio, returns 'N/A'
        mock_cve = mock.Mock(parent_mr=True, bz_priority=jirahook.defs.BZPriority.LOW)
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'N/A')

        # High Prio but RHEL-6, returns 'N/A'
        mock_mr = mock.Mock(project=mock.Mock())
        mock_mr.project.name = 'rhel-6'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.HIGH)
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'N/A')

        # No ji_depends_on, returns 'None'
        mock_mr = mock.Mock(project=mock.Mock())
        mock_mr.project.name = 'rhel-9'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.URGENT,
                             jira_clones=[])
        self.assertEqual(jirahook.CveRow._format_Clones(mock_cve), 'None')

        # No parent_clone, nothing is **bold**.
        mock_mr.branch = 2
        mock_issue = mock.Mock(ji_branch=mock.Mock(internal_target_release='9.1.0'),
                               ji_status=jirahook.defs.JIStatus.IN_PROGRESS,
                               ji_components={'kernel'}, ji_fix_version='rhel-9.1.0')
        mock_issue.id = f'{defs.JPFX}1234567'
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.URGENT,
                             jira_clones=[mock_issue])
        host = f'{defs.JIRA_SERVER}browse/'
        self.assertEqual(
            jirahook.CveRow._format_Clones(mock_cve),
            f'rhel-9.1.0 (kernel): [{defs.JPFX}1234567]({host}{defs.JPFX}1234567) (IN_PROGRESS)<br>'
        )

        # The parent_clone is the same branch as the MR, clone is **bold**.
        mock_mr.branch = mock_issue.ji_branch
        mock_cve = mock.Mock(parent_mr=mock_mr, bz_priority=jirahook.defs.BZPriority.URGENT,
                             jira_clones=[mock_issue])
        self.assertEqual(
            jirahook.CveRow._format_Clones(mock_cve),
            f'**rhel-9.1.0 (kernel): [{defs.JPFX}1234567]({host}{defs.JPFX}1234567)'
            ' (IN_PROGRESS)**<br>'
        )


class TestDepRow(NoSocketTestCase):
    """Tests for the DepRow class."""

    def test_deprow_populate_empty(self):
        """Updates the attribute values of a DepRow."""
        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410, projects=projects,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        mr404 = fakes_mrs.make_mr('group/centos-stream-9', 404, projects=projects,
                                  query_results_list=[fakes_mrs.MR404_DICT], is_dependency=True)
        test_dep = JIssue.new_missing(ji_id=f'{defs.JPFX}2323232', mrs=[mr410, mr404])

        test_deprow = jirahook.DepRow()
        test_deprow.populate(test_dep, '')

        self.assertEqual(test_deprow.MR, '!404 (main)')
        exp = f'[{defs.JPFX}2323232]({defs.JIRA_SERVER}browse/{defs.JPFX}2323232) (UNKNOWN)'
        self.assertEqual(test_deprow.JIRA_Issue, exp)
        self.assertEqual(test_deprow.CVEs, 'None')
        self.assertIn('ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3', test_deprow.Commits)
        self.assertIn('f77278fcd9cef99358adc7f5e077be795a54ffca', test_deprow.Commits)
        self.assertEqual(test_deprow.Readiness, 'INVALID')
        self.assertEqual(test_deprow.Policy_Check, 'Check not done: No JIRA Issue')
        self.assertEqual(test_deprow.Notes, '-')

    @mock.patch('webhook.jissue.is_jissue_ready')
    def test_deprow_populate(self, mock_is_ready):
        """Updates the attribute vales of a DepRow."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err

        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410, projects=projects,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        mr404 = fakes_mrs.make_mr('group/centos-stream-9', 404, projects=projects,
                                  query_results_list=[fakes_mrs.MR404_DICT], is_dependency=True)
        mr410.projects.projects[24152864].branches[0].__dict__['policy'] = [1]
        test_dep = JIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[mr410, mr404])
        test_dep.scope = defs.MrScope.READY_FOR_QA

        test_deprow = jirahook.DepRow()

        test_deprow.populate(test_dep, [1, 3, 5])
        self.assertEqual(test_deprow.MR, '!404 (main)')
        exp = f'[{defs.JPFX}2323232]({defs.JIRA_SERVER}browse/{defs.JPFX}2323232) (READY_FOR_QA)'
        self.assertEqual(test_deprow.JIRA_Issue, exp)
        self.assertEqual(test_deprow.CVEs, 'None')
        self.assertIn('ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3', test_deprow.Commits)
        self.assertIn('f77278fcd9cef99358adc7f5e077be795a54ffca', test_deprow.Commits)
        self.assertEqual(test_deprow.Readiness, 'READY_FOR_QA')
        self.assertEqual(test_deprow.Policy_Check, f'Failed:<br>{cpc_err}')
        mr410.projects.projects[24152864].branches[0].__dict__['policy'] = []


class TestLinkedIssueRow(NoSocketTestCase):
    """Tests for the LinkedIssueRow class."""

    @mock.patch('webhook.jissue.is_jissue_ready')
    def test_linkedissuerow_populate(self, mock_is_ready):
        """Updates the attribute vales of a LinkedIssueRow."""
        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        test_issue = JIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=[mr410])

        test_linkedissuerow = jirahook.LinkedIssueRow()
        test_linkedissuerow.populate(test_issue, '')

        exp = f'[{defs.JPFX}1234567]({defs.JIRA_SERVER}browse/{defs.JPFX}1234567) (IN_PROGRESS)'
        exp_cves = '[CVE-1235-13516](https://bugzilla.redhat.com/CVE-1235-13516)<br>'
        self.assertEqual(test_linkedissuerow.JIRA_Issue, exp)
        self.assertEqual(test_linkedissuerow.CVEs, exp_cves)
        self.assertEqual(test_linkedissuerow.Component, 'kernel')
        self.assertEqual(test_linkedissuerow.Notes, '-')


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestMisc(NoSocketTestCase):
    """Tests for misc functions."""

    def test_find_needed_footnotes(self):
        """Returns a dict of testnames and footnote messages from the list of tag items."""
        issue1 = JIssue.new_missing(ji_id=f'{defs.JPFX}1', mrs=[])
        issue2 = JIssue.new_missing(ji_id=f'{defs.JPFX}2', mrs=[])
        issue3 = JIssue.new_missing(ji_id=f'{defs.JPFX}3', mrs=[])
        issue4 = JIssue.new_missing(ji_id=f'{defs.JPFX}4', mrs=[])
        issue1.failed_tests = ['BranchMatches']
        issue2.failed_tests = ['JIisNotClosed', 'PrelimTestingPass']
        issue3.failed_tests = []
        issue4.failed_tests = ['PrelimTestingPass', 'CommitPolicyApproved']

        result = jirahook.find_needed_footnotes([issue1, issue2, issue3, issue4])
        self.assertEqual(len(result), 4)
        self.assertTrue('BranchMatches' in result)
        self.assertTrue('JIisNotClosed' in result)
        self.assertTrue('PrelimTestingPass' in result)
        self.assertTrue('CommitPolicyApproved' in result)

    def test_create_ji_table(self):
        """Creates a Table from the list of items with the given type of Row class."""
        issue1 = JIssue.new_from_ji(ji=fakes_jira.JI1234567)
        issue2 = JIssue.new_from_ji(ji=fakes_jira.JI2323232)
        issue3 = JIssue.new_from_ji(ji=fakes_jira.JI2345678)
        test_items = [issue1, issue2, issue3]
        results = jirahook.create_ji_table(jirahook.IssueRow, test_items)
        self.assertEqual(len(results), 3)

    @mock.patch('webhook.cpc.is_jissue_ready')
    @mock.patch('webhook.jissue.fetch_issues', mock.Mock(return_value=[]))
    def test_generate_comment(self, mock_is_ready):
        """Returns a string of markdown that will render the MR status and Tag tables."""
        cpc_err = 'release "?" == "+" and internal_target_release "9.1.0" == "9.1.0"'
        mock_is_ready.return_value = False, cpc_err
        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])

        issue_items = [JIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=[mr410])]
        issue_table = jirahook.create_ji_table(jirahook.IssueRow, issue_items)

        dep_items = [JIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[mr410]),
                     JIssue.new_from_ji(ji=fakes_jira.JI2345678, mrs=[mr410])]
        dep_table = jirahook.create_ji_table(jirahook.DepRow, dep_items)

        cve_items = [Bug.new_from_bz(bz=fakes_bz.BZ3456789, mrs=[mr410]),
                     Bug.new_from_bz(bz=fakes_bz.BZ4567890, mrs=[mr410])]
        cve_table = jirahook.create_ji_table(jirahook.CveRow, cve_items)
        linked_items = [JIssue.new_from_ji(ji=fakes_jira.JI2345678, mrs=[mr410])]
        linked_issue_table = jirahook.create_ji_table(jirahook.LinkedIssueRow, linked_items)

        target_branch = '9.1'
        mr_scope = defs.MrScope.READY_FOR_QA
        tables = (issue_table, dep_table, cve_table, linked_issue_table)
        comment = jirahook.generate_comment(target_branch, mr_scope, tables)
        self.assertIn(f'Branch: {target_branch}', comment)
        self.assertIn('**passes**', comment)
        self.assertIn('JIRA Issue tags', comment)
        self.assertIn('Depends tags', comment)
        self.assertIn('CVE tags', comment)
        self.assertIn('Linked JIRA Issues', comment)

    @mock.patch('webhook.common.add_merge_request_to_milestone')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.session.BaseSession.update_webhook_comment')
    def test_update_statuses_comment_with_labels(self, mock_update_comment,
                                                 mock_add_label, mock_milestone):
        """A comment with labels."""
        mock_session = SessionRunner('jirahook', [], jirahook.HANDLERS)
        namespace = 'group/project'
        mr_id = 123
        username = 'user1'
        comment = 'a good comment'
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(567, namespace)
        mock_mr = mock_project.add_mr(mr_id)
        mock_session.gl_instance = mock_gl
        mock_graphql = mock.Mock()
        mock_graphql.username = username
        mock_this_mr = mock.Mock(namespace=namespace, mr_id=mr_id, graphql=mock_graphql)
        mock_mr.namespace = namespace
        mock_mr.mr_id = mr_id
        mock_mr.graphql = mock_graphql
        mock_issues = mock.Mock()

        # Post Comment is True and we have labels.
        labels = ['JIRA::OK']
        mock_add_label.return_value = labels + ['readyForMerge']
        jirahook.update_statuses(mock_session, mock_this_mr, mock_issues, labels, comment)
        mock_update_comment.assert_called_once_with(mock_mr, comment,
                                                    bot_name=mock_this_mr.graphql.username,
                                                    identifier=jirahook.COMMENT_TITLE)
        mock_add_label.assert_called_once_with(mock_project, mr_id, labels)

    @mock.patch('webhook.jissue._find_linked_issues_harder')
    @mock.patch('webhook.jissue.fetch_issues')
    def test_find_linked_issues(self, mock_fetch, mock_find):
        """See that we match up fix versions in kernel variants correctly."""
        mock_find.return_value = []
        # kernel CVE issue
        issue1 = JIssue.new_from_ji(ji=fakes_jira.JI1234567)
        # kernel non-CVE issue
        issue2 = JIssue.new_from_ji(ji=fakes_jira.JI2323232)
        jissues = [issue1, issue2]
        mock_fetch.return_value = [fakes_jira.JI2345678]
        ret = find_linked_issues(jissues, 'rhel-9.1.0', ['CVE-2022-43210'], [])
        self.assertEqual(len(ret), 1)
        self.assertTrue(ret[0].is_linked_issue)
        self.assertEqual(ret[0].id, 'RHEL-2345678')

    def test_find_linked_issues_harder(self):
        """See what we can find using subject parsing instead of FV matching."""
        # kernel CVE jissue, FV set to rhel-9.1.0
        base_issue = JIssue.new_from_ji(ji=deepcopy(fakes_jira.JI1234567))
        # kernel-rt CVE issue, no FV set
        rt_issue_nofv = deepcopy(fakes_jira.JI2345678)
        rt_issue_nofv.fields.fixVersions = []

        # Base bug does not have a properly formatted summary for a CVE
        base_issue.ji.fields.summary = 'No CVE to see here, boss'
        with self.assertLogs('cki.webhook.jissue', level='DEBUG') as logs:
            ret = _find_linked_issues_harder([base_issue], [rt_issue_nofv, base_issue.ji], [])
            self.assertIn("Cannot find CVE or Dist from bug RHEL-1234567 summary", logs.output[-2])
            self.assertEqual(len(ret), 0)

        # Possible linked bug does not have a properly formatted summary for a CVE
        base_issue.ji.fields.summary = 'CVE-2022-43210 kernel: a different bug [rhel-9.1.0]'
        rt_issue_nofv.fields.summary = 'Someone must have edited this subject'
        with self.assertLogs('cki.webhook.jissue', level='DEBUG') as logs:
            ret = _find_linked_issues_harder([base_issue], [rt_issue_nofv, base_issue.ji], [])
            self.assertIn("Cannot find CVE or Dist from linked bug RHEL-2345678 summary",
                          ' '.join(logs.output))
            self.assertEqual(len(ret), 0)

        # Oops, FV is already set in this bug
        rt_issue_fv = deepcopy(fakes_jira.JI2345678)
        rt_issue_fv.fields.fixVersions = ['rhel-8.7.0.z']
        with self.assertLogs('cki.webhook.jissue', level='DEBUG') as logs:
            ret = _find_linked_issues_harder([base_issue], [rt_issue_fv, base_issue.ji], [])
            self.assertIn("RHEL-1234567 is the same bug, skipping", ' '.join(logs.output))
            self.assertIn("Skipping RHEL-2345678, bug already has a Fix Version",
                          ' '.join(logs.output))
            self.assertEqual(len(ret), 0)

        # Okay, we actually found a linked bug via subject
        rt_issue_nofv.fields.summary = 'CVE-2022-43210 kernel: a different bug [rhel-9.1.0]'
        ret = _find_linked_issues_harder([base_issue], [rt_issue_nofv, base_issue.ji], [])
        self.assertEqual(len(ret), 1)
        self.assertTrue(ret[0].is_linked_issue)
        self.assertEqual(ret[0].id, 'RHEL-2345678')


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'})
class TestProcessMR(NoSocketTestCase):
    """Tests for process_mr function."""

    @mock.patch('webhook.jirahook.MR')
    def test_process_mr_check_mr_state(self, mock_mr):
        """Does nothing since check_mr_state returns False."""
        mock_session = mock.Mock()
        mock_session.graphql = mock.Mock()
        mock_session.rh_projects = mock.Mock()
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')

        mock_session.graphql.check_mr_state.return_value = False
        jirahook.process_mr(mock_session, mr_url)
        mock_mr.assert_not_called()

    @mock.patch('webhook.jirahook.MR')
    def test_process_mr_skip_funny(self, mock_MR):
        """Skips MRs with nothing to do."""
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_session = mock.Mock()
        mock_session.graphql = mock.Mock()
        mock_session.rh_projects = mock.Mock()

        # Nothing to do with an MR with no Description.
        mock_mr = mock.Mock(state=jirahook.defs.MrState.OPENED, description=None, commits=True)
        mock_MR.return_value = mock_mr
        jirahook.process_mr(mock_session, mr_url)
        mock_MR.assert_called_with(mock_session.graphql, mock_session.rh_projects, namespace, mr_id)
        mock_mr.get_depends_mrs.assert_not_called()

        # Nothing to do if there are no commits.
        mock_mr = mock.Mock(state=jirahook.defs.MrState.OPENED, description=True, commits=[])
        mock_MR.return_value = mock_mr
        jirahook.process_mr(mock_session, mr_url)
        mock_MR.assert_called_with(mock_session.graphql, mock_session.rh_projects, namespace, mr_id)
        mock_mr.get_depends_mrs.assert_not_called()

    @mock.patch('webhook.jirahook.update_statuses')
    @mock.patch('webhook.jirahook.MR')
    def test_process_mr_bugzilla_only(self, mock_MR, mock_update):
        """Skips MRs with only bugzilla references."""
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        mock_session = mock.Mock()
        mock_session.graphql = mock.Mock()
        mock_session.rh_projects = mock.Mock()

        # If the MR has no jira issues in it, but has bugs, assume it's bugzilla-only and do nothing
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=True),
                            first_dep_sha='',
                            is_draft=False,
                            state=jirahook.defs.MrState.OPENED)
        mock_mr.jissues = []
        mock_mr.issues_with_scopes = mock_mr.jissues
        mock_mr.cves_with_scopes = []
        mock_mr.linked_jissues = []
        mock_mr.linked_issues_with_scopes = []
        mock_MR.return_value = mock_mr

        jirahook.process_mr(mock_session, mr_url)
        mock_update.assert_called_once()

    @mock.patch('webhook.libjira.add_gitlab_link_in_issues', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.move_issue_states_forward')
    @mock.patch('webhook.jirahook.update_statuses')
    @mock.patch('webhook.jirahook.MR')
    def test_process_mr(self, mock_MR, mock_update, mock_issue_forward):
        """Does not update JIRA Issues or MR labels but leaves a comment."""
        mock_session = SessionRunner('jirahook', [], jirahook.HANDLERS)
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        jissue_id = f'{defs.JPFX}1234567'

        mock_session.graphql = mock.Mock(username=username)
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=False, jissue={jissue_id}),
                            first_dep_sha='',
                            is_draft=False,
                            state=jirahook.defs.MrState.OPENED)
        mock_issue = mock.Mock(ji_cves=[],
                               policy_check_ok=(False, 'Nope'),
                               commits=[],
                               failed_tests=[],
                               mr=mock_mr,
                               scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                               ji_components=['kernel-rt'],
                               test_list=[])
        mock_issue.id = jissue_id
        mock_mr.jissues = [mock_issue]
        mock_mr.linked_jissues = []
        mock_mr.issues_with_scopes = mock_mr.jissues
        mock_mr.cves_with_scopes = []
        mock_mr.linked_issues_with_scopes = [mock_issue]
        mock_MR.return_value = mock_mr
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(45678, namespace)
        mock_project.add_mr(mr_id, actual_attributes={'description': 'hello',
                                                      'head_pipeline': {'id': 54321},
                                                      'path_with_namespace': namespace,
                                                      'target_branch': 'main'})
        mock_session.gl_instance = mock_gl
        mock_session.rh_projects = mock.Mock()

        jirahook.process_mr(mock_session, mr_url)
        mock_issue_forward.assert_called_once_with([mock_issue.ji])
        mock_mr.get_depends_mrs.assert_called_once_with()
        mock_update.assert_called_once_with(mock_session, mock_mr, mock.ANY, ['JIRA::OK'], mock.ANY)

    @mock.patch('webhook.libjira.move_issue_states_forward')
    @mock.patch('webhook.jirahook.update_statuses')
    @mock.patch('webhook.jirahook.MR')
    def test_process_mr_with_draft(self, mock_MR, mock_update, mock_issue_forward):
        """Does not update JIRA Issues or MR labels but leaves a comment."""
        mock_session = SessionRunner('jirahook', [], jirahook.HANDLERS)
        username = 'user1'
        namespace = 'group/project'
        mr_id = 123
        mr_url = defs.GitlabURL(f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}')
        jissue_id = f'{defs.JPFX}1234567'

        mock_session.graphql = mock.Mock(username=username)
        mock_mr = mock.Mock(commits=True,
                            description=mock.Mock(bugzilla=False, jissue={jissue_id}),
                            first_dep_sha='',
                            is_draft=True,
                            state=jirahook.defs.MrState.OPENED)
        mock_issue = mock.Mock(ji_cves=[],
                               policy_check_ok=(False, 'Nope'),
                               commits=[],
                               failed_tests=[],
                               mr=mock_mr,
                               scope=jirahook.defs.MrScope.READY_FOR_MERGE,
                               test_list=[])
        mock_issue.id = jissue_id
        mock_mr.jissues = [mock_issue]
        mock_mr.linked_jissues = []
        mock_mr.issues_with_scopes = mock_mr.jissues
        mock_mr.cves_with_scopes = []
        mock_mr.linked_issues_with_scopes = []
        mock_MR.return_value = mock_mr
        mock_session.rh_projects = mock.Mock()

        jirahook.process_mr(mock_session, mr_url)
        mock_issue_forward.assert_not_called()
        mock_mr.get_depends_mrs.assert_called_once_with()
        mock_update.assert_called_once_with(mock_session, mock_mr, mock.ANY, ['JIRA::OK'], mock.ANY)
