"""Process all of the Approvals that are associated with a merge request."""
from dataclasses import dataclass
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from gitlab.const import MAINTAINER_ACCESS
import prometheus_client as prometheus

from . import cdlib
from . import common
from . import defs
from .base_mr import BaseMR
from .base_mr_mixins import DiffsMixin
from .description import MRDescription
from .session import new_session

if typing.TYPE_CHECKING:
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest

    from .owners import Parser
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.ack_nack')

APPROVAL_RULE_ACKS = (' - Approval Rule "%s" requires at least %d ACK(s) (%d given) '
                      'from set (%s).  \n')
APPROVAL_RULE_OKAY = ' - Approval Rule "%s" already has %d ACK(s) (%d required).  \n'
APPROVAL_RULE_OPT = ' - Approval Rule "%s" requests optional ACK(s) from set (%s).  \n'
STALE_APPROVALS_SUMMARY = ('Note: Approvals from %s have been invalidated due to code changes in '
                           'this merge request. Please re-approve, if appropriate.')

CC_TAGGED_USERS_RULE_NAME = 'Cc tagged users'
MISSING_CC_EMAILS_NAME = 'MISSING'

METRIC_KWF_NONPROJECT_MEMBER = prometheus.Counter(
    'kwf_nonproject_member', 'Gitlab user not a member of the project',
    ['project_name', 'username']
)


def _save(session, gl_project, gl_mergerequest, status, subsys_scoped_labels, message):
    # pylint: disable=too-many-arguments
    acks_label = f'Acks::{status}'
    note = f'**ACK/NACK Summary:** ~"{acks_label}"\n\n{message}'
    LOGGER.info(note)

    subsys_scoped_labels.insert(0, acks_label)
    common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, subsys_scoped_labels)

    if not common.mr_is_closed(gl_mergerequest):
        session.update_webhook_comment(gl_mergerequest, note,
                                       bot_name=gl_project.manager.gitlab.user.username,
                                       identifier='**ACK/NACK Summary:')


def _reviewers_to_gl_user_ids(gl_project, reviewers):
    # Search GitLab for users with a given gitlab username.
    users = []
    for reviewer in reviewers:
        if user := common.get_pog_member(gl_project, reviewer):
            users.append(user.id)
            continue
        LOGGER.warning('%s was not found in the %s project!', reviewer, gl_project.name)
        METRIC_KWF_NONPROJECT_MEMBER.labels(project_name=gl_project.name, username=reviewer).inc()
    return users


def _ensure_base_approval_rule(gl_project, gl_mergerequest):
    """Ensure base approval rule exists and has right number of reviewers set."""
    if gl_mergerequest.state != "opened" or gl_mergerequest.draft:
        return

    proj_ar_list = gl_project.approvalrules.list(search='All Members')
    if len(proj_ar_list) < 1:
        LOGGER.warning("Project %s has no 'All Members' approval rule", gl_project.name)
        return

    proj_all_members = proj_ar_list[0]
    proj_rule_id = proj_all_members.id
    proj_rule_name = proj_all_members.name
    proj_reqs = proj_all_members.approvals_required
    if proj_reqs == 0:
        LOGGER.warning("Project %s isn't requiring any reviewers for MRs", gl_project.name)
        return

    reset_ar = False

    for approval_rule in gl_mergerequest.approval_rules.list(iterator=True):
        if approval_rule.name == 'All Members':
            if approval_rule.approvals_required == 0:
                # author access level, must be >= gitlab.MAINTAINER_ACCESS (40)
                aal = common.get_authlevel(gl_project, gl_mergerequest.author['id'])
                if gl_mergerequest.target_branch == "main":
                    LOGGER.warning("%s MR %d (branch: main) had approvals required set to 0",
                                   gl_project.name, gl_mergerequest.iid)
                    reset_ar = True
                elif aal < MAINTAINER_ACCESS:
                    LOGGER.warning("%s MR %d (branch: %s) had approvals required set to 0 by "
                                   "non-maintainer",
                                   gl_project.name, gl_mergerequest.iid,
                                   gl_mergerequest.target_branch)
                    reset_ar = True
                else:
                    LOGGER.debug("%s MR %d (branch: %s) has 'All Members' rule adjusted by "
                                 "maintainer",
                                 gl_project.name, gl_mergerequest.iid,
                                 gl_mergerequest.target_branch)
            else:
                LOGGER.debug("%s MR %d has 'All Members' rule set appropriately",
                             gl_project.name, gl_mergerequest.iid)

            if reset_ar:
                approval_rule.approvals_required = proj_reqs
                approval_rule.save()

            return

    LOGGER.warning("%s MR %d had no base approvals required",
                   gl_project.name, gl_mergerequest.iid)
    gl_mergerequest.approval_rules.create({'approval_project_rule_id': proj_rule_id,
                                           'name': proj_rule_name,
                                           'approvals_required': proj_reqs})


def _recheck_base_approval_rule(gl_mergerequest):
    branch = gl_mergerequest.target_branch

    for approval_rule in gl_mergerequest.approval_rules.list(search='All Members', iterator=True):
        if approval_rule.name == 'All Members':
            if approval_rule.approvals_required == 0 and branch == "main":
                gl_mergerequest.discussions.create({'body': '*ERROR*: MR has 0 required approvals'})
            return

    if not misc.is_production_or_staging() or gl_mergerequest.draft:
        return

    gl_mergerequest.discussions.create({'body': '*ERROR*: MR has no "All Members" approval rule'})


def _edit_approval_rule(gl_project, gl_mergerequest, subsystem, reviewers, num_req):
    """Add a GitLab Approval Rule named subsystem, with num_req required reviewers."""
    rule_exists = False
    rule = None
    for rule in gl_mergerequest.approval_rules.list(iterator=True):
        if rule.name == subsystem:
            rule_exists = True
            break

    if rule_exists:
        LOGGER.info("Approval rule for subsystem %s already exists", subsystem)
        return False

    LOGGER.info("Create rule for ss %s, with %d required approval(s) from user(s) %s",
                subsystem, num_req, reviewers)

    user_ids = _reviewers_to_gl_user_ids(gl_project, reviewers)
    LOGGER.debug("Approval Rule user ids: %s", user_ids)

    if not user_ids:
        return False

    if misc.is_production_or_staging():
        gl_mergerequest.approvals.set_approvers(num_req, approver_ids=user_ids,
                                                approver_group_ids=[], approval_rule_name=subsystem)
    return True


def _get_reviewers(gl_project, gl_mergerequest, changed_files, owners_parser):
    """Parse the owners.yaml file and return a set of email addresses and subsystem labels."""
    # pylint: disable=too-many-branches,too-many-locals,too-many-arguments,too-many-statements
    if not changed_files:
        return [], []

    opt_reviewers = {}
    req_reviewers = {}
    all_reviewers = {}
    ar_num_req = 0
    notify = False
    # A merge request can span multiple subsystems so get the reviewers for each subsystem.
    # Call owners parser individually for each file.
    for changed_file in changed_files:
        for subsystem in owners_parser.get_matching_subsystems([changed_file]):
            ss_label = subsystem.subsystem_label
            required_approvals = subsystem.required_approvals
            # Get all maintainers and reviewers
            for user in subsystem.maintainers + subsystem.reviewers:
                if not user:
                    continue
                # Ensure this user has a gitlab user name listed for it
                if 'gluser' not in user:
                    LOGGER.warning("User %s in subsystem %s does not have a gitlab username listed",
                                   user['name'], ss_label)
                    continue
                username = user['gluser']
                # Ensure user isn't listed in owners.yaml w/an unknown gitlab user name
                if username is None:
                    continue
                # Ensure that the merge request submitter doesn't show up in the reviewers list
                if gl_mergerequest.author['username'] == username:
                    continue

                if ss_label not in opt_reviewers:
                    opt_reviewers[ss_label] = set([])
                    if required_approvals:
                        req_reviewers[ss_label] = set([])

                if ss_label not in all_reviewers:
                    all_reviewers[ss_label] = set([])

                all_reviewers[ss_label].update([username])

                # Ensure that restricted users aren't listed as required reviewers
                if required_approvals and not user.get('restricted', False):
                    req_reviewers[ss_label].update([username])
                else:
                    opt_reviewers[ss_label].update([username])

    # Build a rule out of any Cc: entries in the MR Description.
    cc_reviewers_usernames, missing_emails = get_cc_reviewers(gl_mergerequest)
    if cc_reviewers_usernames:
        opt_reviewers[CC_TAGGED_USERS_RULE_NAME] = cc_reviewers_usernames

    LOGGER.debug('Subsystem optional reviewers: %s', opt_reviewers)
    LOGGER.debug('Subsystem required reviewers: %s', req_reviewers)

    notifications = []
    for subsystem, reviewers in opt_reviewers.items():
        if reviewers:
            notify = _edit_approval_rule(gl_project, gl_mergerequest, subsystem,
                                         reviewers, ar_num_req)
        if notify:
            notifications.append((subsystem, ar_num_req, reviewers))
            # If get_cc_reviewers above could not map all emails, wedge the missing list into the
            # notifications.
            if missing_emails:
                notifications.append((MISSING_CC_EMAILS_NAME, None, missing_emails))
                missing_emails = []
            notify = False

    required = []
    ar_num_req = 1
    if req_reviewers:
        amappreq = next((r.approvals_required
                         for r in gl_mergerequest.approval_rules.list(iterator=True)
                         if r.name == 'All Members'), 1)
        authal = common.get_authlevel(gl_project, gl_mergerequest.author['id'])
        tbranch = gl_mergerequest.target_branch
        LOGGER.debug("access level = %d, target branch = %s, approvals required = %d",
                     authal, tbranch, amappreq)
        if authal >= MAINTAINER_ACCESS and tbranch != "main" and amappreq == 0:
            ar_num_req = 0

    for subsystem, reviewers in req_reviewers.items():
        required.append((reviewers, subsystem))
        if reviewers:
            notify = _edit_approval_rule(gl_project, gl_mergerequest, subsystem,
                                         reviewers, ar_num_req)
        if notify:
            notifications.append((subsystem, ar_num_req, reviewers))
            notify = False

    LOGGER.debug("Required reviewers: %s, notifications:\n%s", required, notifications)
    return required, notifications


def get_cc_reviewers(gl_mr):
    """Return the set of usernames derived Cc: tag emails, as well as a list of missing emails."""
    cc_usernames = set()
    missing = []
    if cc_emails := {cc[1] for cc in MRDescription(gl_mr.description).cc}:
        cc_usernames, missing = _emails_to_gl_user_names(gl_mr.manager.gitlab, cc_emails)
        if cc_usernames:
            LOGGER.info('Mapped %s Cc email addresses to %s usernames: %s', len(cc_emails),
                        len(cc_usernames), cc_usernames)
        if missing:
            LOGGER.info('Cc email addresses which could not be mapped to usernames: %s', missing)
    return cc_usernames, missing


def _get_old_subsystems_from_labels(gl_mergerequest):
    subsys_list = []
    subsys_labels = []
    for label in gl_mergerequest.labels:
        if label.startswith('Acks::'):
            sslabel_parts = label.split("::")
            if len(sslabel_parts) == 3:
                subsys_list.append(sslabel_parts[1])
                subsys_labels.append(label)
    return subsys_list, subsys_labels


def _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels):
    stale_labels = []
    for subsystem in old_subsystems:
        if subsystem not in subsys_scoped_labels.keys():
            for label in old_labels:
                if label.startswith(f"Acks::{subsystem}::"):
                    stale_labels.append(label)
    LOGGER.debug("Stale labels: %s", stale_labels)
    return stale_labels


def _get_ar_approval_status(gl_mergerequest, rule):
    current_approvals = gl_mergerequest.approvals.get()
    name = rule.name
    eligible_approvers = rule.eligible_approvers
    LOGGER.debug("Rule: %s, Eligible: %s (required: %d)",
                 name, rule.eligible_approvers, rule.approvals_required)
    req = rule.approvals_required
    given = 0
    for approver in current_approvals.approved_by:
        for eligible in rule.eligible_approvers:
            if approver['user']['id'] == eligible['id']:
                given += 1
                break
    approvers = []
    for approver in eligible_approvers:
        approvers.append(approver['username'])
    LOGGER.debug("Rule: %s, requires %s more approval(s) from set (%s )", name, req, approvers)

    return given >= req


def _get_subsys_scoped_labels(gl_project, gl_mergerequest, req_reviewers):
    # pylint: disable=too-many-locals,too-many-arguments
    subsys_scoped_labels = {}
    (old_subsystems, old_labels) = _get_old_subsystems_from_labels(gl_mergerequest)

    if req_reviewers is not None:
        for _, subsystem_label in req_reviewers:
            LOGGER.info("Examining label %s", subsystem_label)
            subsystem_approved = False
            for rule in gl_mergerequest.approval_rules.list(iterator=True):
                if rule.name == subsystem_label:
                    LOGGER.info("Getting approval status for %s", rule.name)
                    subsystem_approved = _get_ar_approval_status(gl_mergerequest, rule)
                    break

            if not subsystem_approved:
                scoped_label = defs.NEEDS_REVIEW_SUFFIX
            else:
                scoped_label = defs.READY_SUFFIX

            if subsystem_label:
                subsys_scoped_labels[subsystem_label] = scoped_label

    stale_labels = _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
    if stale_labels:
        common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid, stale_labels)

    ret = [f'Acks::{x}::{y}' for x, y in subsys_scoped_labels.items()]
    ret.sort()  # Sort the subsystem scoped labels for the tests.

    return ret


def _get_ar_approvals(gl_mergerequest, ar_reviewers, summary, faked_reset):
    # pylint: disable=too-many-locals,too-many-branches
    needed = []
    optional = []
    provided = []

    if not faked_reset:
        current_approvals = gl_mergerequest.approvals.get()
        if current_approvals and len(current_approvals.approved_by) > 0:
            summary.append("Approved by:\n")
            for approver in current_approvals.approved_by:
                summary.append(f" - {approver['user']['name']} ({approver['user']['username']})\n")
        else:
            summary.append("\n")

    req_approvals = 0
    for subsystem, required, given, _, user_ids in ar_reviewers:
        if faked_reset:
            given = 0
        usernames = []
        for user_id in user_ids:
            usernames.append(f'{gl_mergerequest.manager.gitlab.users.get(user_id).username}')
        reviewers = list(usernames)
        reviewers.sort()
        reviewers = ', '.join(reviewers)
        if required > 0:
            if given >= required:
                provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))
            else:
                req_approvals += required - given
                needed.append(APPROVAL_RULE_ACKS % (subsystem, required, given, reviewers))
        elif given == 0:
            optional.append(APPROVAL_RULE_OPT % (subsystem, reviewers))
        else:
            provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))

    if needed:
        summary.append("\nRequired Approvals:  \n")
        summary += needed
    if optional:
        summary.append("\nOptional Approvals:  \n")
        summary += optional
    if provided:
        summary.append("\nSatisfied Approvals:  \n")
        summary += provided

    return summary, req_approvals


def _has_unsatisfied_blocking_approval_rule(gl_mergerequest):
    for rule in gl_mergerequest.approval_rules.list(iterator=True):
        block_rule = rule.name.startswith(defs.BLOCKED_BY_PREFIX)
        satisfied = _get_ar_approval_status(gl_mergerequest, rule)
        if block_rule and not satisfied:
            LOGGER.info("Rule: '%s' is not satisfied, set Acks::%s", rule.name, defs.BLOCKED_SUFFIX)
            return True

    return False


def _get_approval_summary(gl_project, gl_mergerequest, req_reviewers, ar_reviewers, faked_reset):
    summary = []
    labels = _get_subsys_scoped_labels(gl_project, gl_mergerequest, req_reviewers)

    summary, req_approvals = _get_ar_approvals(gl_mergerequest, ar_reviewers, summary, faked_reset)

    if gl_project.only_allow_merge_if_all_discussions_are_resolved and \
       not gl_mergerequest.changes()['blocking_discussions_resolved']:
        summary.append('\nAll discussions must be resolved.')
        return defs.BLOCKED_SUFFIX, labels, ''.join(summary)

    if req_approvals > 0:
        if _has_unsatisfied_blocking_approval_rule(gl_mergerequest):
            return defs.BLOCKED_SUFFIX, labels, ''.join(summary)

        summary.append(f'\nRequires {req_approvals} more Approval Rule Approval(s).')
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    approvals = gl_mergerequest.approvals.get()
    min_reviewers = approvals.approvals_required
    ack_count = min_reviewers - approvals.approvals_left
    if approvals.approvals_left and ack_count < min_reviewers:
        summary.append(f'\nRequires {min_reviewers - ack_count} more Approval(s).')
        return defs.NEEDS_REVIEW_SUFFIX, labels, ''.join(summary)

    summary.append('\nMerge Request has all necessary Approvals.')
    LOGGER.info("Summary is:\n%s", summary)
    return defs.READY_SUFFIX, labels, ''.join(summary)


def _emails_to_gl_user_names(gl_instance, emails):
    """Return a tuple with the mapped usernames set and list of emails that did not map."""
    users = set()
    missing = []
    for email in emails:
        if "@redhat.com" not in email and "@fedoraproject.org" not in email:
            continue
        if found_usernames := [x.username for x in gl_instance.users.list(search=email)]:
            users.update(found_usernames)
            continue
        missing.append(email)
    return users, missing


def _assign_reviewers(gl_mergerequest, user_list):
    """Use a quickaction to assign the given user as a reviewer of the given MR."""
    # The command requires an @ in front of the username.
    users = [user if user.startswith('@') else f'@{user}' for user in user_list]
    LOGGER.info('Assigning reviewers %s to MR %s', users, gl_mergerequest.iid)
    if misc.is_production_or_staging():
        cmd = f'/assign_reviewer {" ".join(users)}'
        gl_mergerequest.notes.create({'body': cmd})


def get_reviewers_from_approval_rules(gl_mergerequest):
    """Read the MR's approval rules for custom-added required reviewers."""
    current_approvals = gl_mergerequest.approvals.get()
    ar_reviewers = []
    for rule in gl_mergerequest.approval_rules.list(iterator=True):
        # skip our default All Members rules
        if rule.name == "All Members":
            continue
        name = rule.name
        eligible_approvers = rule.eligible_approvers
        LOGGER.debug("Rule: %s, Eligible: %s (required: %d)",
                     name, rule.eligible_approvers, rule.approvals_required)
        LOGGER.debug("Current approvals: %s", current_approvals.approved_by)
        req = rule.approvals_required
        given = 0
        for approver in current_approvals.approved_by:
            for eligible in rule.eligible_approvers:
                if approver['user']['id'] == eligible['id']:
                    given += 1
                    break
        approvers = []
        ids = []
        for approver in eligible_approvers:
            approvers.append(approver['username'])
            ids.append(approver['id'])
        LOGGER.debug("Rule: %s, requires %s more approval(s) from set (%s )", name, req, approvers)
        ar_reviewers.append([name, req, given, approvers, ids])

    return ar_reviewers


def reset_merge_request_approvals(session, gl_project, mr_id, interdiff):
    """Post a comment about approvals being reset and optionally reset approvals on the MR.

    If the project is not configured to auto reset approvals on push then this function uses
    an API call to do it.
    Return True if the project is configured to automatically reset approvals on push.
    Return False if the project is not configured to automatically reset approvals on push.
    """
    namespace = gl_project.path_with_namespace
    # If the project does not automatically reset approvals on push then assume we have
    # a private token and use it to get a new gl_instance and gl_project.
    proj_reset = gl_project.approvals.get().reset_approvals_on_push
    LOGGER.info('reset_approvals_on_push: %s', proj_reset)
    if not proj_reset:
        gl_instance = get_instance(url=f'{defs.GITFORGE}/{namespace}',
                                   env_name='GITLAB_KWF_BOT_API_TOKENS')
        gl_instance.auth()
        gl_project = gl_instance.projects.get(gl_project.id)
        if not gl_instance.private_token:
            raise ValueError(f'Project {namespace} approval resets disabled, but no token!')

    gl_mergerequest = gl_project.mergerequests.get(mr_id)
    revision = len(gl_mergerequest.diffs.list(get_all=True))
    LOGGER.info("Clearing Approvals on %s MR %s (Rev v%s)", namespace, mr_id, revision)

    # Assign people who have approved the MR whose approvals we're about to clear as Reviewers
    cleared_approvers = []
    current_approvals = gl_mergerequest.approvals.get()
    for approver in current_approvals.approved_by:
        cleared_approvers.append(f"@{approver['user']['username']}")

    notification = ''
    if cleared_approvers:
        notification += (f'Approval(s) from {" ".join(cleared_approvers)} removed due to code '
                         f'changes, fresh approvals required on v{revision}.\n\n')
        _assign_reviewers(gl_mergerequest, cleared_approvers)

    if interdiff:
        header = f'Code changes in revision v{revision} of this MR:  \n'
        notification += common.wrap_comment_table(header, interdiff, "", f'v{revision} interdiff')

    session.update_webhook_comment(gl_mergerequest, notification)

    if proj_reset:
        LOGGER.info('Expecting GL to reset approval rules, nothing else to do.')
        return True
    LOGGER.info('Resetting approval rules via API.')
    if misc.is_production_or_staging():
        gl_mergerequest.reset_approvals()
    return False


def notify_reviewers(session, gl_mergerequest, notifications):
    """Leave a bulk 'please review this mr' comment for reviewers for affected subsystems."""
    aff_ss = ''
    body = ''
    missing_cc_emails = None
    for subsystem, num_req, reviewers in notifications:
        # If this is the goofy notification for unmapped Cc: emails then stash the email address
        # list and move on.
        if subsystem == MISSING_CC_EMAILS_NAME:
            missing_cc_emails = reviewers
            continue
        aff_ss += f'{subsystem}, '
        usernames = ' '.join([f'@{x}' for x in reviewers])
        notification = (f'Requesting review of subsystem {subsystem} with {num_req} required '
                        f'approval(s) from user(s) {usernames}\n\n')
        body += notification

    if not aff_ss and not missing_cc_emails:
        return

    notification = ''
    if aff_ss:
        notification += 'Affected subsystem(s): ' + aff_ss.strip(', ') + '\n\n' + body
    if missing_cc_emails:
        notification += ('Notice: the following email address(es) found in the MR Description Cc:'
                         f' tags could not be mapped to Gitlab usernames: {missing_cc_emails}\n')
    session.update_webhook_comment(gl_mergerequest, notification)


def purge_stale_approval_rules(
        gl_mergerequest: 'ProjectMergeRequest',
        owners_parser: 'Parser',
        path_list: list
) -> None:
    """Purge any approval rules that are no longer valid on this MR."""
    if not path_list:
        return

    mr_subsystems = owners_parser.get_matching_subsystems(path_list)
    mr_subsystem_labels = [s.subsystem_label for s in mr_subsystems if s.subsystem_label]

    # We need all known subsystems to make sure the rule isn't a custom one
    all_subsystems = owners_parser.subsystems
    all_subsystem_labels = [s.subsystem_label for s in all_subsystems if s.subsystem_label]

    current_rules_iter = gl_mergerequest.approval_rules.list(iterator=True)
    for rule in current_rules_iter:
        # The second clause should allow custom rules to not be purged
        if rule.name not in mr_subsystem_labels and rule.name in all_subsystem_labels:
            LOGGER.info("Deleting stale approval rule for subsystem %s", rule.name)
            if misc.is_production_or_staging():
                gl_mergerequest.approval_rules.delete(rule.id)


@dataclass(repr=False)
class ApproveMR(DiffsMixin, BaseMR):
    """Represent the MR, its revisions and approvals."""


def process_merge_request(
    session: 'SessionRunner',
    approve_mr: 'ApproveMR'
) -> None:
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    """Process a merge request."""
    owners_path = session.args.owners_yaml
    rhkernel_src = session.args.rhkernel_src
    try:
        owners_parser = common.get_owners_parser(owners_path)
    except OSError:
        LOGGER.error('get_owners_parser failed to load %s', owners_path)
        raise

    gl_mergerequest = approve_mr.gl_mr
    gl_project = session.get_gl_project(gl_mergerequest.project_id)

    commit_count, authlevel = common.get_commits_count(gl_project, gl_mergerequest)
    if commit_count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
        error_note = '**ACK/NACK Summary: *ERROR* **\n\n'
        error_note += 'This Merge Request has too many commits for the approval webhook to '
        error_note += f'process ({commit_count}) -- please make sure you have targeted the '
        error_note += 'correct branch if you want reviewers automatically assigned.'
        LOGGER.info(error_note)
        session.update_webhook_comment(gl_mergerequest, error_note,
                                       bot_name=session.gl_user.username,
                                       identifier='**ACK/NACK Summary:')
        return

    dep_label = cdlib.set_dependencies_label(gl_project, gl_mergerequest)
    if dep_label not in gl_mergerequest.labels:
        gl_mergerequest = gl_project.mergerequests.get(gl_mergerequest.iid)
    if dep_label == f"Dependencies::{defs.READY_SUFFIX}":
        changed_files = [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    else:
        changed_files = cdlib.get_filtered_changed_files(gl_mergerequest)

    # Update the path_list to include corresponding Kconfig files
    changed_config_files, config_label = common.process_config_items(rhkernel_src, changed_files)
    changed_files.extend(changed_config_files)
    if config_label:
        common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, config_label)
    LOGGER.debug('changed_files: %s', changed_files)

    _ensure_base_approval_rule(gl_project, gl_mergerequest)
    _recheck_base_approval_rule(gl_mergerequest)

    purge_stale_approval_rules(gl_mergerequest, owners_parser, changed_files)

    required, notifications = _get_reviewers(gl_project, gl_mergerequest,
                                             changed_files, owners_parser)
    notify_reviewers(session, gl_mergerequest, notifications)

    # If CC_TAGGED_USERS_RULE_NAME is in the 'notifications' tuple then assign them as reviewers.
    if cc_usernames := next((ss[2] for ss in notifications if ss[0] == CC_TAGGED_USERS_RULE_NAME),
                            None):
        _assign_reviewers(gl_mergerequest, cc_usernames)

    ar_reviewers = get_reviewers_from_approval_rules(gl_mergerequest)
    LOGGER.debug("Approval Rules Reviewers: %s", ar_reviewers)

    new_rev_label = None
    rev_count = len(approve_mr.history)
    faked_reset = False
    if rev_count > 1:
        new_rev_label = f'{defs.CODE_CHANGED_PREFIX}v{rev_count}'
        new_rev_label = new_rev_label if new_rev_label not in gl_mergerequest.labels else None
        diff = approve_mr.code_changes()

        if new_rev_label and diff:
            interdiff = cdlib.assemble_interdiff_markdown(diff)
            faked_reset = \
                reset_merge_request_approvals(session, gl_project, gl_mergerequest.iid, interdiff)

    status, subsystems_acks_labels, message = _get_approval_summary(gl_project, gl_mergerequest,
                                                                    required, ar_reviewers,
                                                                    faked_reset)
    if new_rev_label:
        subsystems_acks_labels.append(new_rev_label)
    _save(session, gl_project, gl_mergerequest, status, subsystems_acks_labels, message)


def handle_block_action(session, gl_mergerequest, block_action, author, author_id):
    """Create or remove blocking approval for the note author."""
    # pylint: disable=too-many-arguments
    if block_action is None:
        return

    rule_name = f"{defs.BLOCKED_BY_PREFIX} {author}"
    current_rules_iter = gl_mergerequest.approval_rules.list(iterator=True)

    if gl_mergerequest.author['username'] == author:
        block_comment = (f"WARNING: @{author}, you cannot use block actions on your own Merge "
                         "Request, because this project does not allow self-approval of MRs. You "
                         "can move your MR back to a draft, or open a blocking thread instead.")
        session.update_webhook_comment(gl_mergerequest, block_comment,
                                       bot_name=session.gl_instance.user.username,
                                       identifier="*Block/Unblock Action:")
        LOGGER.warning("Attempt by %s to use a block action on their own MR", author)
        return

    if block_action == "block":
        block_comment = f"@{author} has blocked this Merge Request via a `/block` action."

        session.update_webhook_comment(gl_mergerequest, block_comment,
                                       bot_name=session.gl_instance.user.username,
                                       identifier="*Block/Unblock Action:")

        if any(rule for rule in current_rules_iter if rule.name == rule_name):
            LOGGER.info("Blocking approval for user %s already exists", author)
            return
        LOGGER.info("Create blocking approval for user %s", author)

        if not misc.is_production_or_staging():
            return

        gl_mergerequest.approvals.set_approvers(approvals_required=1, approver_ids=[author_id],
                                                approver_group_ids=[], approval_rule_name=rule_name)
        _assign_reviewers(gl_mergerequest, [f'@{author}'])

    elif block_action == "unblock":
        # Find the rule w/name matching subsystem, if any
        if not (rule := next((r for r in current_rules_iter if r.name == rule_name), None)):
            return

        LOGGER.info("Deleting blocking approval rule for user %s", author)
        if misc.is_production_or_staging():
            gl_mergerequest.approval_rules.delete(rule.id)


def get_approve_mr(session, mr_url):
    """Return a merge request instance for the webhook payload."""
    params = {'graphql': session.graphql,
              'gl_instance': session.gl_instance,
              'projects': session.rh_projects,
              'url': mr_url}
    return ApproveMR(**params)


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    if not (gl_mr := event.gl_mr):
        return

    if event.kind is defs.GitlabObjectKind.NOTE:
        block_action = None
        for match in event.match_note_text:
            if block_action := match.groupdict().get('action'):
                handle_block_action(session, gl_mr, block_action,
                                    event.user, event.body['user']['id'])
                continue
        if not block_action and not event.has_requested_evaluation:
            LOGGER.info('Note event does not show any relevant changes, ignoring.')
            return

    approve_mr = get_approve_mr(session, event.mr_url)
    process_merge_request(session, approve_mr)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    session = new_session('ack_nack', args, HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
