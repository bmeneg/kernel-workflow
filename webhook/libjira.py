"""Library for interacting with jira."""
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from enum import StrEnum
from enum import unique
from os import environ
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production_or_staging
from cki_lib.misc import only_log_exceptions
from cki_lib.session import RequestLogger
from jira import JIRA
from jira.exceptions import JIRAError
from jira.resources import Issue
from jira.resources import User

from webhook.defs import FixVersion
from webhook.defs import GITFORGE
from webhook.defs import JIRA_BOT_ACCOUNTS
from webhook.defs import JIRA_SERVER
from webhook.defs import JIStatus
from webhook.defs import JPFX
from webhook.defs import KWF_SUPPORTED_ISSUE_COMPONENTS
from webhook.defs import KWF_SUPPORTED_ISSUE_TYPES
from webhook.defs import READY_FOR_QA_LABEL

if typing.TYPE_CHECKING:
    from webhook.defs import Label
    from webhook.jissue import JIssue

LOGGER = get_logger('cki.webhook.libjira')


@unique
class JiraField(StrEnum):
    # pylint: disable=invalid-name
    """Map pretty names to backend field name values."""

    assignee = 'assignee'
    components = 'components'
    fixVersions = 'fixVersions'
    issuelinks = 'issuelinks'
    issuetype = 'issuetype'
    labels = 'labels'
    priority = 'priority'
    project = 'project'
    reporter = 'reporter'
    status = 'status'
    subtasks = 'subtasks'
    summary = 'summary'
    updated = 'updated'
    resolution = 'resolution'
    Preliminary_Testing = 'customfield_12321540'
    Testable_Builds = 'customfield_12321740'
    Release_Blocker = 'customfield_12319743'
    Internal_Target_Milestone = 'customfield_12321040'
    Dev_Target_Milestone = 'customfield_12318141'
    QA_Contact = 'customfield_12315948'
    Commit_Hashes = 'customfield_12324041'
    Pool_Team = 'customfield_12317259'
    Reset_Contacts = 'customfield_12322640'

    @classmethod
    def all(cls):
        """Return the list of all the field values."""
        return [field.value for field in cls]


def connect_jira(token_auth=environ.get('JIRA_TOKEN_AUTH'), host=None):
    """Connect to JIRA and return a JIRA connection object."""
    if not host:
        host = environ.get('JIRA_SERVER', JIRA_SERVER)
    jira_options = {'server': host}
    jiracon = JIRA(options=jira_options, token_auth=token_auth)
    with only_log_exceptions():
        RequestLogger(LOGGER).hook(jiracon._session)  # pylint: disable=protected-access
    return jiracon


def _clear_issue_cache(issue_cache):
    """Clear the issue_cache list."""
    LOGGER.info('Clearing %s issues from the cache.', len(issue_cache))
    issue_cache.clear()


def _update_issue_cache(new_issues, issue_cache):
    """Put the new_issues in the issue_cache."""
    for issue in new_issues:
        if issue.id in issue_cache:
            LOGGER.warning('JIRA Issue %s already in issue_cache.', issue.key)
        issue_cache[issue.id] = issue


def get_jira_field(issue, field_name, default=None):
    """Fetch a single field value out of the the jira issue's fields."""
    if issue is None:
        return default

    field_value = get_nested_key(issue.fields, f'{field_name}', default, lookup_attrs=True)
    return field_value


def filter_kwf_issues(
    issues: typing.Iterable[Issue],
    valid_components: tuple[str, ...] = KWF_SUPPORTED_ISSUE_COMPONENTS,
    valid_types: tuple[str, ...] = KWF_SUPPORTED_ISSUE_TYPES
) -> list[Issue]:
    """Return the list of Issues which are a valid issuetype."""
    invalid_issues = []
    for issue in issues:
        if get_jira_field(issue, 'issuetype/name') not in valid_types or \
           not any(comp.name.split(" / ")[0].strip() in valid_components for comp in
                   get_jira_field(issue, 'components', default=[])):
            invalid_issues.append(issue)
    if invalid_issues:
        LOGGER.info('Filtered out unsupported issues: %s', invalid_issues)
    return [issue for issue in issues if issue not in invalid_issues]


def _getissues(
    jira: JIRA,
    issues: typing.Optional[typing.Iterable[str]] = None,
    cves: typing.Optional[typing.Iterable[str]] = None,
    filter_kwf: bool = False
) -> list[Issue]:
    """Return jira Issues matching the given list of issue keys or CVE labels."""
    if not issues and not cves:
        raise ValueError('Search lists are empty.')
    labels_str = f"labels in ({', '.join(cves)})" if cves else ''
    issues_str = ' OR '.join(f'key={issue}' for issue in issues) if issues else ''
    joiner_str = ' OR ' if labels_str and issues_str else ''
    if filter_kwf:
        filter_str = \
            ('AND component in componentMatch("^(kernel|kernel-rt|kernel-automotive)($| / .*$)")'
             f" AND issuetype in ({', '.join(KWF_SUPPORTED_ISSUE_TYPES)})")
    else:
        filter_str = ''
    jql_str = f"project = {JPFX.rstrip('-')} {filter_str} AND "
    jql_str += f'({labels_str}{joiner_str}{issues_str})'
    LOGGER.info('Fetching Jira data for issues: %s, cves: %s, JQL: %s', issues, cves, jql_str)
    # This will catch unknown issues but not unknown CVEs.
    try:
        issues = jira.search_issues(jql_str, fields=JiraField.all())
    except JIRAError:
        LOGGER.warning("User specified invalid jira issue(s): %s", issues)
        issues = []
    return issues


def parse_search_list(search_list: typing.Iterable[str]) -> tuple[set, set]:
    """Split the given search_list items into a tuple of jira Issues and CVE names."""
    issues: set[str] = set()
    cves: set[str] = set()
    other = []
    for item in search_list:
        if isinstance(item, str):
            if item.startswith(JPFX):
                issues.add(item)
            elif item.startswith('CVE-'):
                cves.add(item)
        else:
            other.append(item)
    if other:
        raise ValueError(f'Unknown search term: {other}')
    return issues, cves


def update_issue_field(
    issue: Issue,
    jfield: JiraField,
    value: typing.Union[str, dict, User],
) -> None:
    """Update a given issue's given field with the given value."""
    if jfield in (JiraField.Internal_Target_Milestone,
                  JiraField.Dev_Target_Milestone,
                  JiraField.Preliminary_Testing):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        if is_production_or_staging():
            issue.update(fields={jfield: {'value': str(value)}})
        return

    if jfield in (JiraField.Testable_Builds):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, f'{value[:32]}...')
        if is_production_or_staging():
            issue.update(fields={jfield: value})
        return

    if jfield in (JiraField.assignee):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        if is_production_or_staging():
            issue.update(assignee={'name': value})
        return

    if jfield in (JiraField.fixVersions):
        LOGGER.info("Setting issue %s's field %s to %s", issue.key, jfield.name, value)
        if is_production_or_staging():
            issue.update(fields={jfield: [{'name': value}]})
        return

    LOGGER.warning("Attempt to use update_issue_field for an unknown field: %s", jfield.name)


@dataclass
class SyncField:
    """Simple class to hold a field name and content."""

    # Field name in Jira API
    name: str = field(default='', init=False)
    # Data present in the field
    data: int | str = field(init=False)

    def __init__(self, name):
        """Initialize the field name."""
        self.name = name


@dataclass
class SyncFields:
    """Simple class to hold the fields to sync to linked issues."""

    def __init__(self, issue):
        """Initialize the SyncField names."""
        # Internal Target Milestone (integer value)
        self.itm = SyncField(name=JiraField.Internal_Target_Milestone)
        self.itm.data = int(get_jira_field(issue, f'{self.itm.name}/value', default=0))
        # Dev Target Milestone (integer value)
        self.dtm = SyncField(name=JiraField.Dev_Target_Milestone)
        self.dtm.data = int(get_jira_field(issue, f'{self.dtm.name}/value', default=0))
        # Testable Builds (free-form text field)
        self.tbuilds = SyncField(name=JiraField.Testable_Builds)
        self.tbuilds.data = get_jira_field(issue, self.tbuilds.name, default='')
        # Assignee
        self.assignee = SyncField(name=JiraField.assignee)
        self.assignee.data = get_jira_field(issue, self.assignee.name, default=None)


def _sync_linked_jissue_fields(
    from_i: Issue,
    to_i: Issue,
    from_fields: SyncFields,
) -> None:
    """Sync the sync_fields from one issue to a linked issue."""
    to_fields = SyncFields(to_i)

    # Sync Internal Target Milestone
    if from_fields.itm.data and to_fields.itm.data < from_fields.itm.data:
        update_issue_field(to_i, to_fields.itm.name, from_fields.itm.data)
    else:
        LOGGER.debug("ITM value %s for %s and %s already match",
                     from_fields.itm.data, from_i.key, to_i.key)

    # Sync Dev Target Milestone
    if from_fields.dtm.data and to_fields.dtm.data < from_fields.dtm.data:
        update_issue_field(to_i, to_fields.dtm.name, from_fields.dtm.data)
    else:
        LOGGER.debug("DTM value %s for %s and %s already match",
                     from_fields.dtm.data, from_i.key, to_i.key)

    # Sync Testable Builds
    if from_fields.tbuilds.data and not to_fields.tbuilds.data:
        update_issue_field(to_i, to_fields.tbuilds.name, from_fields.tbuilds.data)
    else:
        LOGGER.debug("Testable Builds already populated for %s", to_i.key)

    # Sync Assignee
    if from_fields.assignee.data and from_fields.assignee.data != to_fields.assignee.data:
        update_issue_field(to_i, to_fields.assignee.name, from_fields.assignee.data.name)
    else:
        LOGGER.debug("Assignee field already matches")


def sync_jissue_fields(
    jissues: typing.Iterable['JIssue'],
    linked_jissues: typing.Iterable['JIssue'],
) -> None:
    """Make sure linked jira issues have necessary fields filled in."""
    for issue in jissues:
        # Don't run if there are no CVEs in the main issue
        if not issue.ji_cves:
            continue

        issue_fields = SyncFields(issue.ji)

        for linked_issue in linked_jissues:
            if issue.ji_cves != linked_issue.ji_cves:
                LOGGER.debug("Issue %s and %s CVE lists do not match, ignoring",
                             issue.id, linked_issue.id)
                continue
            _sync_linked_jissue_fields(issue.ji, linked_issue.ji, issue_fields)


def fetch_issues(
    search_list: typing.Iterable[str],
    jira: typing.Optional[JIRA] = None,
    filter_kwf: bool = False,
) -> list[Issue]:
    # pylint: disable=dangerous-default-value
    """Return a list of issue objects queried from JIRA."""
    if not search_list:
        raise ValueError('Search list is empty.')
    issues, cves = parse_search_list(search_list)
    if not jira:
        jira = connect_jira()
    return _getissues(jira, issues=issues, cves=cves, filter_kwf=filter_kwf)


def issues_with_lower_status(issue_list, status, min_status=JIStatus.NEW):
    """Return the list of issues that have a status lower than the input status."""
    return [issue for issue in issue_list
            if min_status <= JIStatus.from_str(issue.fields.status) < status]


def latest_testing_failed_timestamp(jira, issue):
    """Return most recent timestamp in history when Preliminary Testing field was set to Fail."""
    timestamp = None
    comments = reversed(jira.comments(issue))
    for comment in comments:
        c_detail = jira.comment(issue.id, comment.id)
        if "issued failed testing" in c_detail.body:
            timestamp = datetime.strptime(c_detail.created, '%Y%m%dT%H:%M:%S')
            LOGGER.debug('Preliminary Testing: Fail last added %s', timestamp)
            return timestamp
    LOGGER.warning('Did not find issue failed testing in comment history.')
    return None


def request_preliminary_testing(issue_list: list[Issue], labels: list['Label'],
                                reset_failed=False) -> None:
    """Set Preliminary Testing: Requested in a given issue, if not Pass/Requested already."""
    if READY_FOR_QA_LABEL not in labels:
        LOGGER.debug("Merge request not ready for QA")
        return

    for issue in issue_list:
        prelim_testing = getattr(issue.fields, JiraField.Preliminary_Testing)
        if prelim_testing and prelim_testing.value in ('Pass', 'Requested'):
            LOGGER.debug("Jira Issue %s PT already set to %s", issue.key, prelim_testing.value)
            continue
        if prelim_testing and prelim_testing.value == 'Fail':
            LOGGER.info("Jira Issue %s Failed QE Preliminary Testing", issue.key)
            if not reset_failed:
                continue
        update_issue_field(issue, JiraField.Preliminary_Testing, 'Requested')


def issues_to_move_to_in_progress(issue_list):
    """Return the issues from the input list that need to be moved to In Progress."""
    ji_to_update = issues_with_lower_status(issue_list, JIStatus.IN_PROGRESS)

    update_list = set(ji_to_update)
    for issue in ji_to_update:
        itm = getattr(issue.fields, JiraField.Internal_Target_Milestone)
        dtm = getattr(issue.fields, JiraField.Dev_Target_Milestone)
        fixver = issue.fields.fixVersions
        if not fixver:
            update_list.remove(issue)
            continue
        if not FixVersion(fixver[0].name).zstream and not (itm and dtm):
            update_list.remove(issue)

    return update_list


def add_gitlab_link_in_issues(session, issues, this_mr):
    """Add GitLab MR link to Issue Links field in JIRA Issues."""
    # pylint: disable=too-many-locals
    base_url = f'{GITFORGE}/{this_mr.namespace}'
    mr_title = f'{this_mr.project.name}!{this_mr.mr_id}'
    mr_link = f'{base_url}/-/merge_requests/{this_mr.mr_id}'
    branch_link = f'{base_url}/-/tree/{this_mr.branch.name}'
    title = f'Merge Request: {this_mr.title}'
    icon = (f'{GITFORGE}/assets/favicon-'
            '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
    gitlab_link = {'url': mr_link, 'title': title,
                   'icon': {'url16x16': icon, 'title': 'GitLab Merge Request'}}
    comment = (
        f"Merge request {session.comment.jira_link(mr_title, mr_link)} ({this_mr.title}) "
        f"targetting {session.comment.jira_link(this_mr.branch.name, branch_link)} "
        "linked to this issue.\n"
    ) + session.comment.jira_footer()

    jira = connect_jira()
    for issue in filter_kwf_issues(issues):
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s already linked in %s", this_mr.mr_id, issue.key)
        else:
            LOGGER.info("Linking [%s](%s) to issue %s", title, mr_link, issue.key)
            if is_production_or_staging():
                jira.add_simple_link(issue=issue, object=gitlab_link)
                jira.add_comment(issue.key, comment)


def remove_gitlab_link_comment_in_issue(jira, issue, mr_url):
    """Remove the comment we left in the issue pointing to the MR."""
    for comment in jira.comments(issue):
        c_detail = jira.comment(issue.id, comment.id)
        if mr_url in c_detail.body and c_detail.author.name in JIRA_BOT_ACCOUNTS:
            LOGGER.info("Removing %s comment pointing to %s", issue.key, mr_url)
            c_detail.delete()


def remove_gitlab_link_in_issues(mr_id, namespace, issue_list):
    """Remove GitLab MR link from Issue Links field in JIRA Issues."""
    if not issue_list:
        return

    mr_link = f'{GITFORGE}/{namespace}/-/merge_requests/{mr_id}'
    jira = connect_jira()
    link = {}
    issues = _getissues(jira, issues=issue_list)
    for issue in filter_kwf_issues(issues):
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s linked in %s, removing it", mr_id, issue.key)
            jira.delete_remote_link(issue=issue, internal_id=link.id)
            remove_gitlab_link_comment_in_issue(jira, issue, mr_link)


def update_testable_builds(issues, text, pipeline_urls):
    """Fill in Testable Builds field for the given JIRA Issues."""
    LOGGER.info("Filling in Testable Builds for issues: %s", issues)
    LOGGER.debug("Text:\n%s\n", text)
    for issue in filter_kwf_issues(issues):
        testable_builds = getattr(issue.fields, JiraField.Testable_Builds)
        if testable_builds and all(url in testable_builds for url in pipeline_urls):
            LOGGER.info("All downstream pipelines found in %s, not updating field.", issue.key)
            continue
        update_issue_field(issue, JiraField.Testable_Builds, text)


def transition_issue(
    jira: JIRA,
    jissue: typing.Union[str, int, Issue],
    jistatus: JIStatus,
    extra_transition_kwargs: typing.Optional[dict] = None,
) -> None:
    """Transition the issue to the given status."""
    status_str = jistatus.name.replace('_', ' ')
    if not is_production_or_staging():
        return
    jira.transition_issue(jissue, status_str, **extra_transition_kwargs)


def update_issue_status(issue_list, new_status, min_status=JIStatus.NEW):
    """Change the issue status to new_status if it is currently lower, otherwise do nothing."""
    issue_list = filter_kwf_issues(issue_list)
    # Do nothing if the current status is lower than min_status.
    # Returns the list of issue objects which have had their status changed.
    if not issue_list:
        LOGGER.info('No issues to update status for.')
        return []
    if new_status not in (JIStatus.PLANNING, JIStatus.IN_PROGRESS):
        LOGGER.warning("Unsupported transition status: %s", new_status.name)
        return []
    if not (ji_to_update := issues_with_lower_status(issue_list, new_status, min_status)):
        LOGGER.info('All issues have status of %s or higher.', new_status.name)
        return []
    ji_keys = [ji.key for ji in ji_to_update]
    LOGGER.info('Updating status to %s for these issues: %s', new_status.name, ji_keys)
    if not is_production_or_staging():
        for issue in ji_to_update:
            issue.status = new_status.name
        return ji_to_update
    status_updated = []
    transition_comment = f"GitLab kernel MR bot updated status to {new_status.name}"
    jira = connect_jira()
    for issue in ji_to_update:
        transition_issue(jira, issue, new_status, {'comment': transition_comment})
        issue.fields.status = new_status.name
        status_updated.append(issue)
    return status_updated


def move_issue_states_forward(issue_list):
    """Move the given JIRA issues states to Planning or In Progress."""
    to_in_progress = issues_to_move_to_in_progress(issue_list)
    LOGGER.debug("Issues to move to In Progress: %s", to_in_progress)
    update_issue_status(to_in_progress, JIStatus.IN_PROGRESS)
    to_planning = {issue for issue in issue_list if issue not in to_in_progress}
    LOGGER.debug("Issues to move to Planning: %s", list(to_planning))
    update_issue_status(to_planning, JIStatus.PLANNING)


def get_linked_mrs(jissue_id, namespace=None, jiracon=None):
    """Return the list of MR urls linked to the given jira ID."""
    if not jiracon:
        jiracon = connect_jira()

    try:
        links = jiracon.remote_links(jissue_id)
    except JIRAError as err:
        LOGGER.warning("Jira returned an error fetching remote_links for '%s': %s", jissue_id,
                       err.text)
        return []
    starts_with = f'{GITFORGE}/{namespace}/' if namespace else f'{GITFORGE}/'
    return [link.object.url for link in links if link.object.url.startswith(starts_with)
            and '/-/merge_requests/' in link.object.url]
