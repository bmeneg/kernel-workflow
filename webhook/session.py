"""A session object for webhooks."""
from argparse import Namespace
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
import json
from pathlib import Path
import typing

from cki_lib import footer
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.metrics import prometheus_init
from cki_lib.misc import deployment_environment
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production
from cki_lib.misc import is_production_or_staging
from cki_lib.misc import is_staging
from cki_lib.misc import sentry_init
from jira.exceptions import JIRAError
import sentry_sdk

from .common import add_label_to_merge_request as add_labels
from .common import parse_mr_url
from .common import remove_labels_from_merge_request as remove_labels
from .defs import GITFORGE
from .defs import GitlabObjectKind
from .defs import MessageType
from .defs import READY_LABELS
from .graphql import GitlabGraph
from .labels import LabelsCache
from .libjira import connect_jira
from .rh_metadata import Projects
from .rh_metadata import Webhook
from .session_events import create_event
from .session_events import special_label_changes

if typing.TYPE_CHECKING:
    from gitlab.v4.objects.groups import Group
    from gitlab.v4.objects.projects import Project

    from .session_events import AnyEvent

LOGGER = get_logger('cki.webhook.session')

SESSION = None


def new_session(webhook_name, parser_args=None, handlers=None, load_policies=False):
    """Return a new BaseSession or FullSession object."""
    if SESSION:
        raise RuntimeError(f'Session already exists! {SESSION}')
    if handlers:
        session = SessionRunner(webhook_name, parser_args, handlers, load_policies=load_policies)
    else:
        session = BaseSession(webhook_name, parser_args, load_policies=load_policies)
    globals()['SESSION'] = session
    return SESSION


def get_session():
    """Return the current Session object or blow up if it isn't set up."""
    if not SESSION:
        raise RuntimeError('No existing Session to get!')
    return SESSION


@dataclass
class BaseSession:
    """An object to hold basic session data."""

    webhook_name: InitVar[str]
    args: Namespace
    load_policies: InitVar[bool] = field(kw_only=True, default=False)
    webhook: Webhook | None = field(init=False, default=None)
    _gl_groups: typing.List['Group'] = field(init=False, default_factory=list)
    _gl_projects: typing.List['Project'] = field(init=False, default_factory=list)

    def __post_init__(
        self,
        webhook_name: str,
        load_policies: bool
    ) -> None:
        """Set up the instance."""
        if not (webhook := self.rh_projects.webhooks.get(webhook_name)):
            raise RuntimeError(
                (f"webhook_name '{webhook_name}' not found in Projects data webhooks:"
                 f" {list(self.rh_projects.webhooks.keys())}")
            )
        self.webhook: Webhook = webhook
        if load_policies:
            self.rh_projects.do_load_policies()
        self.comment = footer.Footer(webhook)
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Talk about yourself."""
        environment = self.environment.upper() if self.is_production else self.environment
        repr_str = f"hook: '{self.webhook.name}', env: {environment}"
        return f"<{self.__class__.__name__}: {repr_str}>"

    @cached_property
    def gl_hostname(self):
        """Return the hostname used for Gitlab API connections."""
        return GITFORGE

    @cached_property
    def gl_instance(self):
        """Return an authenticated GL REST instance."""
        instance = get_instance(self.gl_hostname)
        instance.auth()
        if not getattr(instance, 'user', None):
            raise RuntimeError('Not authenticated to Gitlab.')
        return instance

    @cached_property
    def graphql(self):
        """Return an authenticated GL graphql instance."""
        graph = GitlabGraph(get_user=True, hostname=self.gl_hostname)
        if not graph.user:
            raise RuntimeError('Not authenticated to Gitlab.')
        return graph

    @cached_property
    def jira(self):
        """Return an authenticated JIRA API instance."""
        jira = connect_jira()
        try:
            jira.myself()
        except JIRAError as err:
            raise RuntimeError('Not authenticated to Jira.') from err
        return jira

    @cached_property
    def rh_projects(self):
        """Return a Projects object."""
        return Projects()

    @cached_property
    def gl_labels(self) -> LabelsCache:
        """Return a LabelsCache object."""
        return LabelsCache(session=self)

    @property
    def gl_user(self):
        """Return the current GL user the Session is authenticaed as."""
        return self.gl_instance.user

    @cached_property
    def environment(self):
        """Return a string describing the environment."""
        return deployment_environment()

    @cached_property
    def is_production_or_staging(self):
        """Return True if we are running in production or staging, otherwise False."""
        return is_production_or_staging()

    @cached_property
    def is_production(self):
        """Return True if we are running in production, otherwise False."""
        return is_production()

    @cached_property
    def is_staging(self):
        """Return True if we are running in staging, otherwise False."""
        return is_staging()

    def update_webhook_comment(self, gl_mergerequest, text, *, bot_name=None, identifier=None):
        """Find the webhook's status comment so we can edit it in place, add one if none exists."""
        if bot_name and identifier:
            for discussion in gl_mergerequest.discussions.list(iterator=True):
                note = discussion.attributes['notes'][0]
                if note['author']['username'] == bot_name and identifier in note['body']:
                    body = text + self.comment.gitlab_footer('updated')
                    if self.is_production_or_staging:
                        LOGGER.debug('Overwriting existing webhook comment:\n%s', body)
                        comment = discussion.notes.get(note['id'])
                        comment.body = body
                        comment.save()
                    else:
                        LOGGER.debug('Would overwrite existing webhook comment:\n%s', body)
                    return

        body = text + self.comment.gitlab_footer()
        if self.is_production_or_staging:
            LOGGER.debug('Creating new webhook comment:\n%s', body)
            gl_mergerequest.notes.create({'body': body})
        else:
            LOGGER.debug('Would create new webhook comment:\n%s', body)

    def get_gl_group(self, group_id: typing.Union[str, int]) -> 'Group':
        """Return a gitlab module Group from the cache or get a new one."""
        for gl_group in self._gl_groups:
            if str(group_id) in (str(gl_group.id), gl_group.full_path):
                return gl_group
        self._gl_groups.append(self.gl_instance.groups.get(group_id))
        return self._gl_groups[-1]

    def get_gl_project(self, project_id: typing.Union[str, int]) -> 'Project':
        """Return a gitlab module Project from the cache or get a new one."""
        for gl_project in self._gl_projects:
            if str(project_id) in (str(gl_project.id), gl_project.path_with_namespace):
                return gl_project
        self._gl_projects.append(self.gl_instance.projects.get(project_id))
        return self._gl_projects[-1]


@dataclass
class SessionRunner(BaseSession):
    """A BaseSession with event handling."""

    handlers: dict

    def __repr__(self):
        """Talk about yourself."""
        repr_str = super().__repr__()
        handlers = [handler.name for handler in getattr(self, 'handlers').keys()]
        return f'{repr_str[:-1]}, handlers: {handlers}>'

    def run(self, **kwargs):
        """Run it."""
        if self.args.merge_request:
            func = self.process_cmdline_message
        elif self.args.json_message_file:
            func = self.process_json_message
        else:
            func = self.consume_messages
        return func(**kwargs)

    def get_handler(self, message_type_or_object_kind):
        """Return the matching Handler or None."""
        return self.handlers.get(message_type_or_object_kind)

    @staticmethod
    def build_cmdline_message(args):
        """Return a GitlabMsg object matching the command line parameters."""
        if not args.merge_request:
            raise ValueError("'args.merge_request' must be set.")
        if args.action:
            gl_kind = GitlabObjectKind.get(args.action)
        elif args.note:
            gl_kind = GitlabObjectKind.NOTE
        else:
            gl_kind = GitlabObjectKind.MERGE_REQUEST
        msg_dict = {'url': args.merge_request,
                    'object_kind': gl_kind,
                    'project_id': args.payload_project_id,
                    'target_branch': args.payload_target_branch,
                    'note': args.note}
        return GitlabMsg(**msg_dict)

    def process_cmdline_message(self, **kwargs):
        """Set up a fake payload from the given cmdline parameters and process the message."""
        headers = {'message-type': 'gitlab'}
        body = self.build_cmdline_message(self.args).as_payload()
        return self.process_one_message(routing_key='cmdline', headers=headers, body=body, **kwargs)

    def process_json_message(self, **kwargs):
        """Set up a message from input json and process it."""
        headers = {'message-type': self.args.json_message_type}
        file_name = self.args.json_message_file
        msg_json = Path(file_name).read_text(encoding='utf-8')
        body = json.loads(msg_json)
        return self.process_one_message(routing_key=file_name, headers=headers, body=body, **kwargs)

    def process_one_message(
        self,
        routing_key: typing.Optional[str] = None,
        headers: typing.Optional[dict] = None,
        body: typing.Optional[dict] = None,
        **kwargs: typing.Dict
    ) -> bool:
        """Match the event to a Handler and run it if we determine we should."""
        event = create_event(self, headers, body)
        LOGGER.info('Received message on %s of type %s:\n%s', routing_key,
                    event.kind.name if hasattr(event, 'kind') else event.type,
                    json.dumps(body, indent=None))
        if not (handler_func := self.get_handler(getattr(event, 'kind', event.type))):
            LOGGER.info('No handler for this message, nothing to do.')
            return False
        # Filter out messages which do not match the environment.
        if not event.matches_environment:
            LOGGER.debug('Event does not match environment, ignoring.')
            return False
        # Filter out unwanted messages.
        if not event.passes_filters:
            LOGGER.debug('Event does not pass filters, ignoring.')
            return False
        # If we passed the filters then take care of the readyForX and Blocked labels, etc...
        self.update_special_labels(event)
        # If the event doesn't match any of the trigger conditions then we ignore it.
        if not event.matches_trigger:
            LOGGER.debug('Event does not match any triggers, ignoring.')
            return False
        # Call the handler and away we go!
        LOGGER.debug('Invoking handler function %s …', handler_func)
        handler_func(body, self, event, **kwargs)
        return True

    @cached_property
    def queue(self):
        """Return a MessageQueue object."""
        mqueue = MessageQueue(host=self.args.rabbitmq_host,
                              port=self.args.rabbitmq_port,
                              user=self.args.rabbitmq_user,
                              password=self.args.rabbitmq_password)
        mqueue.msg_logging_env.add_hook(MessageType.AMQP, make_amqp_logging_extras)
        mqueue.msg_logging_env.add_hook(MessageType.UMB_BRIDGE, make_umb_bridge_logging_extras)
        mqueue.msg_logging_env.add_hook(MessageType.JIRA, make_jira_logging_extras)
        mqueue.msg_logging_env.add_hook(MessageType.DATAWAREHOUSE, make_dw_logging_extras)
        return mqueue

    def consume_messages(self, **kwargs):
        # pylint: disable=unnecessary-lambda
        """Get the MessageQueue and start processing messages."""
        prometheus_init()
        sentry_init(sentry_sdk, ca_certs=self.args.sentry_ca_certs)
        if not self.args.rabbitmq_routing_key:
            raise RuntimeError('--rabbitmq-routing-key must be set.')
        self.queue.consume_messages(
            exchange=self.args.rabbitmq_exchange,
            routing_keys=self.args.rabbitmq_routing_key.split(),
            callback=lambda **cbkwargs: self.process_one_message(**cbkwargs, **kwargs),
            queue_name=self.args.rabbitmq_queue_name
        )

    def update_special_labels(self, event: 'AnyEvent') -> None:
        """Update special labels as needed on GL events."""
        # Only the Webhook instance with 'manage_special_labels' set should be doing this.
        if not self.webhook.manage_special_labels or event.type is not MessageType.GITLAB:
            return
        to_add, to_remove = special_label_changes(event)
        ready_label_changed = \
            any(ready_label in getattr(event, 'changed_labels', []) for ready_label in READY_LABELS)
        if not to_add and not to_remove and not ready_label_changed:
            LOGGER.info('No special label changes.')
            return
        gl_project = self.get_gl_project(event.namespace)
        # If a readyForX label changed then any add/remove_labels call will recalculate it.
        if ready_label_changed:
            LOGGER.info('Event shows a readyForX label in changed labels %s, recalculating it...',
                        event.changed_labels)
        if to_add or not to_remove:
            add_labels(gl_project, event.mr_url.id, to_add)
        if to_remove:
            remove_labels(gl_project, event.mr_url.id, to_remove)


def make_amqp_logging_extras(_, body):
    """Create the dict of 'extras' logging info for an amqp_bridge event."""
    return {'bugzilla_id': get_nested_key(body, 'event/bug_id'),
            'bugzilla_user': get_nested_key(body, 'event/user/login')
            }


def make_jira_logging_extras(_, body: dict) -> dict:
    """Create the dict of 'extras' logging info for an event from jira."""
    return {'key': body['issue'].get('key'), 'user': body['issue'].get('user')} if \
        'issue' in body else {}


def make_umb_bridge_logging_extras(headers, body):
    """Create the dict of 'extras' logging info for an ampq_bridge event."""
    extras = {}
    if body.get('mrpath'):
        extras['mr_id'] = int(body['mrpath'].split('!')[-1])
        extras['path_with_namespace'] = body['mrpath'].split('!')[0]
    if headers.get('source'):
        extras['event_source'] = headers['source']
    return extras


def make_dw_logging_extras(_, body):
    """Create the dict of 'extras' logging info for an datawarehouse event."""
    return {
        'status': body.get('status'),
        'object_type': body.get('object_type'),
        'object_id': get_nested_key(body, 'object/id', default=None),
    }


@dataclass
class GitlabMsg:
    """Creates a gitlab webhook event message."""

    url: str
    object_kind: GitlabObjectKind = GitlabObjectKind.MERGE_REQUEST
    project_id: int = 123
    target_branch: str = 'main'
    note: str = ''

    def as_payload(self):
        """Return the payload as a dict."""
        payload = {'object_kind': self.object_kind.name.lower()}
        if self.object_kind is not GitlabObjectKind.BUILD:
            payload['project'] = self.project_dict
        if self.object_kind is not GitlabObjectKind.PUSH:
            payload['user'] = {'username': 'cli', 'id': 0}

        match self.object_kind:
            case GitlabObjectKind.MERGE_REQUEST:
                payload.update(
                    {'changes': {},
                     'labels': [],
                     'object_attributes': self.merge_request_dict}
                )
            case GitlabObjectKind.NOTE:
                payload.update(
                    {'merge_request': self.merge_request_dict,
                     'object_attributes': {'noteable_type': 'MergeRequest', 'note': self.note}}
                )
            case GitlabObjectKind.PIPELINE:
                payload.update(
                    {'merge_request': self.merge_request_dict}
                )
            case GitlabObjectKind.PUSH:
                payload.update(
                    {'ref': f'refs/head/{self.target_branch}',
                     'user_id': 0,
                     'user_username': 'cli'}
                )
        return payload

    @property
    def iid(self):
        """Return the MR ID as an int."""
        return parse_mr_url(self.url)[1]

    @property
    def path_with_namespace(self):
        """Return the path_with_namespace string."""
        return parse_mr_url(self.url)[0]

    @property
    def project_name(self):
        """Return the project name string."""
        return self.path_with_namespace.rsplit('/', 1)[-1]

    @property
    def merge_request_dict(self):
        """Return the merge_request dict."""
        mr_dict = {'iid': self.iid,
                   'labels': [],
                   'state': 'opened',
                   'target_branch': self.target_branch,
                   'work_in_progress': False,
                   'url': self.url
                   }
        if self.object_kind is GitlabObjectKind.MERGE_REQUEST:
            mr_dict.update({'action': 'open', 'head_pipeline_id': None})
        return mr_dict

    @property
    def project_dict(self):
        """Return the project dict."""
        scheme = self.url.split(':', 1)[0]
        netloc = self.url.removeprefix(f'{scheme}://').split('/', 1)[0]
        return {'id': self.project_id,
                'name': self.project_name,
                'description': 'A faux project',
                'web_url': f'{scheme}://{netloc}/{self.path_with_namespace}',
                'git_ssh_url': f'git@{netloc}:{self.path_with_namespace}.git',
                'git_http_url': f'{scheme}://{netloc}/{self.path_with_namespace}.git',
                'namespace': self.path_with_namespace,
                'visibility_level': 0,
                'path_with_namespace': self.path_with_namespace,
                'default_branch': 'main',
                "ci_config_path": ""
                }
