"""Export information about Koji."""
import os
import subprocess
import typing

from cki_lib.cronjob import CronJob
from cki_lib.logger import get_logger
import prometheus_client
import yaml

KOJI_CHANNELS = yaml.safe_load(os.environ.get('KOJI_CHANNELS', '{}'))
LOGGER = get_logger(__name__)
EXPECTED_ARCHES = ['aarch64', 's390x', 'x86_64', 'ppc64le']


def run(self, **_):
    """Update the metrics."""
    self.update_metric_pending_jobs()


def get_channel_data(profile_name, channel):
    """Get channel data using koji cli."""
    ret = []
    header_found = False

    cmd = ['koji', '-p', profile_name, 'list-hosts', '--channel', channel]
    result = subprocess.run(cmd, check=True, capture_output=True)
    stdout = result.stdout.decode('utf-8')

    for line in stdout.split('\n'):
        if line.startswith('-' * 10):
            header_found = True
            continue
        if not header_found:
            continue

        data = line.split()
        if not data or len(data) < 5:
            continue

        hostinfo = {
            'hostname': data[0],
            'enabled':  data[1],
            'ready': data[2],
            'load': (float)(data[3].split('/')[0]),
            'cap': (float)(data[3].split('/')[1]),
            'arch':  data[4],
        }
        ret.append(hostinfo)
    return ret


def is_brew_reporting_offline(profile_name):
    """Check if koji reports offline status."""
    cmd = ['koji', '-p', profile_name, 'list-tags', '--package=kernel', 'rhel*']
    result = subprocess.run(cmd, check=False, capture_output=True)
    output = result.stdout.decode('utf-8').lower()
    if result.stderr:
        output = output + result.stderr.decode('utf-8').lower()

    if 'offline' in output:
        return 1

    return 0


def is_channel_healthy(channel, channel_data):
    """Return True if channel appears healthy."""
    _ = channel  # reserved for future use

    arch_healthy = {}
    for arch in EXPECTED_ARCHES:
        arch_healthy[arch] = 0

    for rec in channel_data:
        if rec['enabled'] == 'Y':
            arches = rec['arch'].split(',')
            for arch in arches:
                if arch in EXPECTED_ARCHES:
                    arch_healthy[arch] = arch_healthy[arch] + 1

    for arch, hosts_avail in arch_healthy.items():
        if hosts_avail == 0:
            return False
    return True


class KojiMetrics(CronJob):
    """Calculate Koji metrics."""

    schedule = '*/10 * * * *'

    metric_koji_channel_healthy = prometheus_client.Gauge(
        'kwf_koji_channel_healthy',
        'koji channel builders are available for all arches',
        ['instance', 'channel'],
    )

    metric_koji_offline = prometheus_client.Gauge(
        'kwf_koji_offline',
        'koji is offline',
        ['instance'],
    )

    def _update_metric_koji_channel_healthy(self, profile_name, channel, channel_data) -> None:
        is_healthy = 1 if is_channel_healthy(channel, channel_data) else 0
        LOGGER.debug('koji instance: %s channel: %s healthy: %s', profile_name, channel, is_healthy)
        self.metric_koji_channel_healthy.labels(profile_name, channel).set(is_healthy)

    def run(self, **_: typing.Any) -> None:
        """Update the metrics."""
        for profile_name in KOJI_CHANNELS:
            is_koji_offline = is_brew_reporting_offline(profile_name)
            LOGGER.debug('koji instance: %s, offline: %s', profile_name, is_koji_offline)
            self.metric_koji_offline.labels(profile_name).set(is_koji_offline)
            if is_koji_offline:
                continue

            for channel in KOJI_CHANNELS[profile_name]:
                LOGGER.debug('getting channel data: %s', channel)
                channel_data = get_channel_data(profile_name, channel)
                for data in channel_data:
                    LOGGER.debug('channel:%s %s', channel, data)
                self._update_metric_koji_channel_healthy(profile_name, channel, channel_data)
