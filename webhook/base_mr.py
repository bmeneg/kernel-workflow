"""A basic MR class using Graphql."""
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
from textwrap import dedent

from cki_lib.logger import get_logger
from gitlab.client import Gitlab

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.base_mr_mixins import GraphMixin
from webhook.description import MRDescription
from webhook.rh_metadata import Projects
from webhook.users import User
from webhook.users import UserCache

LOGGER = get_logger('cki.webhook.base_mr')


@dataclass
class BaseMR(GraphMixin):
    # pylint: disable=too-many-instance-attributes
    """Hold basic MR data and provide basic functions."""

    MR_QUERY_BASE = dedent("""
    query mrData(
        $mr_id: String!,
        $namespace: ID!,
        $first: Boolean = true,
        $after: String = ""
        %(variable_addons)s) {
      ...CurrentUser @include(if: $first)
      project(fullPath: $namespace) {
        id @include(if: $first)
        userPermissions {
          pushCode @include(if: $first)
        }
    %(project_addons)s
        mr: mergeRequest(iid: $mr_id) {
          approved @include(if: $first)
          author {
            ...GlUser @include(if: $first)
          }
          commitCount @include(if: $first)
          description @include(if: $first)
          global_id: id @include(if: $first)
          ...MrLabelsPaged @include(if: $first)
          state @include(if: $first)
          draft @include(if: $first)
          ...MrFiles @include(if: $first)
          sourceBranch @include(if: $first)
          targetBranch @include(if: $first)
          title @include(if: $first)
          headPipeline {
            id @include(if: $first)
          }
    %(mr_addons)s
        }
      }
    }
    """)

    MR_QUERY = (MR_QUERY_BASE +
                fragments.CURRENT_USER +
                fragments.GL_USER +
                fragments.MR_LABELS_PAGED +
                fragments.MR_FILES)

    gl_instance: Gitlab
    projects: Projects
    url: str

    # populated by the query response
    approved: bool = field(default=False, init=False)
    author: User | None = field(default=None, init=False)
    author_can_push: bool = field(default=False, init=False)
    commit_count: int = field(default=0, init=False)
    current_user: User | None = field(default=None, init=False)
    description: MRDescription = field(default_factory=MRDescription, init=False)
    draft: bool = field(default=False, init=False)
    labels: list = field(default_factory=list, init=False)
    _files: list = field(default_factory=list, init=False)
    global_id: str = field(default='', init=False)
    head_pipeline_id: int = field(default=0, init=False)
    project_id: int = field(default=0, init=False)
    state: defs.MrState = field(default=defs.MrState.UNKNOWN, init=False)
    source_branch: str = field(default='', init=False)
    target_branch: str = field(default='', init=False)
    title: str = field(default='', init=False)

    '''
    Parameters for the query run on post_init that can be overridden on creation or by a subclass.

    query_string: A valid Graphql query in string format. It can have printf-style conversion
                  specifiers: https://docs.python.org/3/library/stdtypes.html#old-string-formatting
                  Defaults to MR_QUERY.
    query_addons: A dict of conversion specifiers and their values to be applied to query_string.
                  Defaults to {'variable_addons': '', 'project_addons': '', 'mr_addons': ''}.
    query_params_extra: A dict that can extend the default query parameters of namespace and iid.
                        Defaults to {}.
    query_func: A function that is passed the query results dict after being passed through
                _parse_base_query().
                Defaults to False.
    '''
    query_string: str = field(default=MR_QUERY, repr=False, init=False)
    query_addons: dict = field(repr=False, default_factory=lambda: {'variable_addons': '',
                                                                    'project_addons': '',
                                                                    'mr_addons': ''},
                               init=False)
    query_params_extra: dict = field(default_factory=dict, repr=False, init=False)
    query_func: callable = field(default=False, repr=False, init=False)

    def __repr__(self):
        """Describe yourself."""
        is_dependency = getattr(self, 'is_dependency', '???')
        commits = len(getattr(self, 'commits', {})) or self.commit_count
        repr_str = f'MR {self.namespace}!{self.iid}'
        repr_str += f', approved: {self.approved}'
        repr_str += f', commits: {commits}'
        repr_str += f', state: {self.state.name.lower()}'
        repr_str += f', draft: {self.draft}'
        repr_str += f", dependency: {is_dependency}"
        repr_str += f", project: {self.project.name if self.project else 'None'}"
        repr_str += f", branch: {self.branch.name if self.branch else 'None'}"
        return f'<{repr_str}>'

    def __post_init__(self):
        """Set up the object by running a query using self.query_* values."""
        query_string = self.query_string if not self.query_addons else \
            self.query_string % self.query_addons
        self.query(query_string, self.query_params_extra, process_function=self._parse_base_query)
        LOGGER.info('Created %s', self)

    def _parse_base_query(self, results):
        """Parse the MR_QUERY results into our attributes."""
        if not results:
            return
        self.project_id = int(results['project']['id'].split('/')[-1])
        self.approved = results['project']['mr']['approved']
        self.author = self.user_cache.get(results['project']['mr']['author'])
        self.author_can_push = results['project']['userPermissions']['pushCode']
        self.commit_count = results['project']['mr']['commitCount']
        self.current_user = self.user_cache.get(results['currentUser'])
        self.description = MRDescription(results['project']['mr']['description'], self.namespace,
                                         self.graphql)
        self.draft = results['project']['mr']['draft']
        self._files = [path['path'] for path in results['project']['mr']['files']]
        self.global_id = results['project']['mr']['global_id']
        if head_pipeline := results['project']['mr']['headPipeline']:
            self.head_pipeline_id = int(head_pipeline['id'].rsplit('/', 1)[-1])
        self.labels = self.graphql.get_all_mr_labels(self.namespace, self.iid,
                                                     results['project']['mr']['labels'])
        self.state = defs.MrState.from_str(results['project']['mr']['state'])
        self.source_branch = results['project']['mr']['sourceBranch']
        self.target_branch = results['project']['mr']['targetBranch']
        self.title = results['project']['mr']['title']
        # If self.query_func is set to the name of a function then try to call it.
        if self.query_func:
            self.query_func(results)

    @property
    def branch(self):
        """Return the matching Branch object, or None."""
        return self.project.get_branch_by_name(self.target_branch) if self.project else None

    @property
    def files(self):
        """Return the list of files affected by the MR."""
        return self._files

    @cached_property
    def gl_mr(self):
        """Return a gl_mergerequest object."""
        return self.gl_project.mergerequests.get(self.iid)

    @cached_property
    def gl_project(self):
        """Return a gl_project object."""
        return self.gl_instance.projects.get(self.namespace)

    @property
    def has_depends(self):
        """Return True if the MR description lists any dependencies, otherwise False."""
        return bool(self.description.depends or self.description.depends_mrs)

    @property
    def iid(self):
        """Return the MR internal ID as an int."""
        return common.parse_mr_url(self.url)[1]

    @property
    def namespace(self):
        """Return the MR namespace."""
        return common.parse_mr_url(self.url)[0]

    @property
    def project(self):
        """Return the matching Project object, or None."""
        return self.projects.get_project_by_id(self.project_id)

    @cached_property
    def user_cache(self):
        """Return a UserCache object."""
        return UserCache(self.graphql, self.namespace)

    def add_labels(self, to_add, remove_scoped=False):
        """Use common code to add the list of labels to the MR."""
        fresh_labels = common.add_label_to_merge_request(self.gl_project, self.iid, to_add,
                                                         remove_scoped=remove_scoped)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_add).issubset(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_add: {to_add}'))

    def remove_labels(self, to_remove):
        """Use common code to remove the list of labels from the MR."""
        fresh_labels = common.remove_labels_from_merge_request(self.gl_project, self.iid, to_remove)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_remove).isdisjoint(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_remove: {to_remove}'))

    def labels_with_prefix(self, prefix):
        """Return the list of scoped MR labels with the given prefix."""
        return [label for label in self.labels if label.scoped and label.prefix == prefix]

    def labels_with_scope(self, scope):
        """Return the list of scoped MR labels with the given MrScope."""
        return [label for label in self.labels if label.scoped and label.scope is scope]
