"""Session Event module."""
from dataclasses import dataclass
from datetime import datetime
from functools import cached_property
from os import environ
from re import fullmatch as re_fullmatch
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key

from .defs import BLOCKED_LABEL
from .defs import GitlabObjectKind
from .defs import GitlabURL
from .defs import JIRA_BOT_ACCOUNTS
from .defs import JiraKey
from .defs import Label
from .defs import MessageType
from .defs import READY_LABELS
from .libjira import get_linked_mrs

if typing.TYPE_CHECKING:
    from re import Match

    from gitlab.v4.objects.merge_requests import MergeRequest
    from gitlab.v4.objects.pipelines import ProjectPipeline
    from gitlab.v4.objects.projects import Project

    from .rh_metadata import Branch
    from .rh_metadata import Project as RH_Project
    from .session import BaseSession
    from .session import SessionRunner

LOGGER = get_logger('cki.webhook.session_event')


class DraftStatus(typing.NamedTuple):
    """The Draft status indicated in an GL webhook event."""

    is_draft: bool | None
    changed: bool | None

    @classmethod
    def create(cls, mr_payload: dict, changes_payload: dict | None) -> 'DraftStatus':
        """Create a DraftStatus from the given GL event data."""
        is_draft = mr_payload.get('draft')
        changed = 'draft' in changes_payload if isinstance(changes_payload, dict) else None
        return cls(is_draft, changed)

    @property
    def into(self) -> bool | None:
        """Return True if this DraftStatus indicates we just went into Draft."""
        if self.is_draft is None or self.changed is None:
            return None
        return self.is_draft is True and self.changed is True

    @property
    def out_of(self) -> bool | None:
        """Return True if this DraftStatus indicates we just came out of Draft."""
        if self.is_draft is None or self.changed is None:
            return None
        return self.is_draft is False and self.changed is True


# If any of the Event Filter functions return False the event should be ignored, unless the
# Webhook has skip_session_filter_check set in which case the event is not filtered.
#
# If any of the Event Trigger functions return True, or the Event has no trigger functions, or
# the Webhook has skip_session_trigger_check set, then the event should be passed to the handler.
# Otherwise the Event will be ignored.

BASE_EVENT_FILTERS = ('filter_bots',)
BASE_EVENT_TRIGGERS = tuple()

GITLAB_EVENT_FILTERS = BASE_EVENT_FILTERS + ('filter_drafts', 'filter_status')
GITLAB_EVENT_TRIGGERS = BASE_EVENT_TRIGGERS + ('has_missing_label', 'has_blocked_label_mismatch')

GITLAB_ISSUE_EVENT_FILTERS = GITLAB_EVENT_FILTERS + tuple()
GITLAB_ISSUE_EVENT_TRIGGERS = BASE_EVENT_TRIGGERS + \
    ('has_missing_label',
     'has_open_action',
     'has_changed_label',
     'has_changed_description',
     'has_closing_action',
     )

GITLAB_MR_EVENT_FILTERS = GITLAB_EVENT_FILTERS + tuple()
GITLAB_MR_EVENT_TRIGGERS = GITLAB_EVENT_TRIGGERS + \
    ('has_open_action',
     'has_changed_label',
     'has_changed_commits',
     'has_changed_description',
     'has_changed_to_draft',
     'has_changed_to_ready',
     'has_closing_action',
     )

GITLAB_NOTE_EVENT_FILTERS = GITLAB_EVENT_FILTERS + ('filter_note_type',)
GITLAB_NOTE_EVENT_TRIGGERS = GITLAB_EVENT_TRIGGERS + ('has_requested_evaluation', 'match_note_text')

# Build and Pipeline events are identical for now so they share the same triggers tuple.
GITLAB_BUILD_PIPELINE_EVENT_FILTERS = GITLAB_EVENT_FILTERS + ('filter_merge_requests',)
GITLAB_BUILD_PIPELINE_EVENT_TRIGGERS = ('has_downstream', 'has_upstream')

GITLAB_PUSH_EVENT_FILTERS = ('filter_branch',)
GITLAB_PUSH_EVENT_TRIGGERS = tuple()

JIRA_EVENT_FILTERS = BASE_EVENT_FILTERS + ('filter_project',)

AMQP_EVENT_FILTERS = BASE_EVENT_FILTERS + ('filter_bug',)

UMBBRIDGE_EVENT_FILTERS = BASE_EVENT_FILTERS + ('filter_target',)

DATAWAREHOUSE_EVENT_FILTERS = BASE_EVENT_FILTERS + ('filter_status', 'filter_object_type')


@dataclass
class BaseEvent:
    """The base Event class."""

    filters: typing.ClassVar[tuple] = BASE_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = BASE_EVENT_TRIGGERS

    session: 'BaseSession'
    headers: dict
    body: dict

    def __post_init__(self):
        """Say hi."""
        LOGGER.info('Created %s', self)

    def __repr__(self) -> str:
        """Say it out loud."""
        repr_str = f'filters: {list(self.filters)}, triggers: {list(self.triggers)}'
        return f'<{self.__class__.__name__}: {repr_str}>'

    @cached_property
    def type(self) -> MessageType:
        """Return the MessageType of the event."""
        return MessageType.get(self.headers['message-type'])

    @cached_property
    def gl_mr(self) -> typing.Union['MergeRequest', None]:
        """Return a gitlab MergeRequest instance if we have an mr_url, otherwise None."""
        return self.gl_project.mergerequests.get(self.mr_url.id) if \
            getattr(self, 'mr_url', None) else None

    @cached_property
    def gl_project(self) -> typing.Union['Project', None]:
        """Return a gitlab Project instance if we have a namespace, otherwise None."""
        return self.session.get_gl_project(self.namespace) if hasattr(self, 'namespace') else None

    @cached_property
    def rh_project(self) -> typing.Union['RH_Project', None]:
        """Return the rh_metadata.Project matching the event, or None if not determined."""
        if namespace := getattr(self, 'namespace', None):
            return self.session.rh_projects.get_project_by_namespace(namespace)
        return None

    @cached_property
    def bot_users(self) -> set[str]:
        """Return the set of the bot users to ignore, if any."""
        return set()

    @cached_property
    def matches_environment(self) -> bool:
        """Return True if the event is relevant to the matched Project and environment."""
        # Some webhooks don't get this check, and some event types don't reveal a namespace
        # so in these cases there is nothing to do and we return True.
        if not self.session.webhook.match_to_projects or not hasattr(self, 'namespace'):
            LOGGER.info('Skipping Project check for this Event.')
            return True
        # Filter out messages from unexpected Projects.
        if not (event_project := self.rh_project):
            LOGGER.info('Event is not associated with any known Project, ignoring.')
            return False
        LOGGER.info('Event associated with project %s (sandbox: %s)', event_project.namespace,
                    event_project.sandbox)
        # Filter out messages for hooks which are not active in this Project.
        if self.session.webhook.name not in self.rh_project.webhooks:
            LOGGER.info("'%s' is not an expected webhook for this event's project: %s",
                        self.session.webhook.name, self.rh_project)
            return False
        # Filter out sandbox-project messages in production, and non-sandbox-project messages
        # in staging.
        if not self.session.args.disable_environment_check:
            if self.session.is_production and self.rh_project.sandbox:
                LOGGER.info('Ignoring sandbox project event while in production environment.')
                return False
            if self.session.is_staging and not self.rh_project.sandbox:
                LOGGER.info('Ignoring production project event while in staging environment.')
                return False
        return True

    @cached_property
    def filter_bots(self) -> bool:
        """Return True if the event is *not* generated by a bot user, otherwise False."""
        if not (event_user := getattr(self, 'user', None)):
            return True
        if not self.session.args.disable_user_check and event_user in self.bot_users:
            LOGGER.info("Event is from ignorable user '%s', ignoring.", event_user)
            return False
        return True

    @cached_property
    def passes_filters(self) -> bool:
        """Return True if the event passes all the filter functions, otherwise False."""
        if self.session.webhook.skip_session_filter_check:
            LOGGER.info('Webhook skips filter checks.')
        else:
            # Run each filter method and if any return False then the event should not be processed.
            for filter_name in self.filters:
                filter_result = getattr(self, filter_name)
                LOGGER.debug("Filter '%s': %s.", filter_name,
                             'passed' if filter_result else 'failed')
                if not filter_result:
                    LOGGER.info("Event failed '%s' check, ignoring.", filter_name)
                    return False
        return True

    @cached_property
    def matches_trigger(self) -> bool:
        """Return True if the event matches any trigger function, otherwise False."""
        # If the Event does not require anything then we should process the event.
        if not self.triggers:
            LOGGER.info('No trigger checks for this event type, message should be processed.')
            return True
        if self.session.webhook.skip_session_trigger_check:
            LOGGER.info('Webhook disables trigger checks, message should be processed.')
            return True
        # Run each 'requires' method and if any return True then the event should be processed.
        for trigger_name in self.triggers:
            trigger_result = getattr(self, trigger_name)
            LOGGER.debug("Trigger check '%s': %s.", trigger_name, trigger_result)
            if trigger_result:
                LOGGER.info("Event matches '%s' check, message should be processed.", trigger_name)
                return True
        # We passed all the filters but none of the 'requires' methods returned True so the event
        # can be ignored.
        LOGGER.info('Event does not match any triggers, ignoring')
        return False


@dataclass(repr=False)
class GitlabEvent(BaseEvent):
    # pylint: disable=too-many-public-methods
    """Generic wrapper for Gitlab webhook events."""

    filters: typing.ClassVar[tuple] = GITLAB_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = GITLAB_EVENT_TRIGGERS

    @cached_property
    def created_at(self) -> typing.Union[datetime, None]:
        """Return the time the object was created, or None if not available."""
        if not (created_at := self.object_attributes.get('created_at')):
            return None
        return datetime.fromisoformat(created_at[:19])

    @cached_property
    def discussions_blocked(self) -> bool:
        """Return True if the event shows 'blocking_discussions_resolved' is False (lol)."""
        return not self.merge_request.get('blocking_discussions_resolved', True)

    @cached_property
    def draft_status(self) -> DraftStatus:
        """Return a tuple with the current Draft status and whether it just changed."""
        # First tuple member is the current draft state, second member is whether the event
        # indicates it just changed. None is returned for either if it cannot be determined.
        # MR, note, and some pipeline payloads allow us to know the draft state, but only the
        # 'merge_request' payload gives the 'changes'.
        if not self.merge_request:
            return DraftStatus(None, None)
        return DraftStatus.create(self.merge_request, self.body.get('changes', None))

    @cached_property
    def user(self) -> str:
        """Return the username of the user who generated this event."""
        return get_nested_key(self.body, 'user/username') \
            or self.body.get('user_username') or ''

    @cached_property
    def kind(self) -> GitlabObjectKind:
        """Return the GitlabObjectKind for the event."""
        return GitlabObjectKind.get(self.body['object_kind'])

    @cached_property
    def merge_request(self) -> dict:
        """Return the body's 'merge_request' key value, if any."""
        # For a MergeRequest event this is the object_attributes key.
        return self.object_attributes if self.kind is GitlabObjectKind.MERGE_REQUEST else \
            self.body.get('merge_request') or {}

    @cached_property
    def mr_inactive(self) -> bool | None:
        """Return a bool indicating whether the MR is already closed or merged."""
        if not self.merge_request:
            return None
        return self.merge_request['state'] in ('closed', 'merged')

    @cached_property
    def object_attributes(self) -> dict:
        """Return the body's 'object_attributes', if any."""
        return self.body.get('object_attributes', {})

    @cached_property
    def labels(self) -> list[Label] | None:
        """Return the list of MR labels from the event, or None if unknown."""
        if self.kind is GitlabObjectKind.MERGE_REQUEST:
            mr_labels = self.body.get('labels', [])
        else:
            mr_labels = get_nested_key(self.body, 'merge_request/labels')
        return None if mr_labels is None else [Label(label['title']) for label in mr_labels]

    @cached_property
    def project(self) -> dict:
        """Return the body's 'project' key value, if any."""
        return self.body.get('project', {})

    @cached_property
    def namespace(self) -> str:
        """Return the project path_with_namespace if known."""
        return self.project.get('path_with_namespace', '')

    @cached_property
    def mr_url(self) -> typing.Union[GitlabURL, None]:
        """Return the MR URL, or None if not present."""
        if not (mr_url := self.merge_request.get('url')):
            return None
        return GitlabURL(mr_url)

    @cached_property
    def url(self) -> typing.Union[GitlabURL, None]:
        """Return the object URL, or None if not present."""
        if not (url := self.object_attributes.get('url')):
            return None
        return GitlabURL(url)

    @cached_property
    def bot_users(self) -> set[str]:
        """Return the set with the username we are logged into the API as."""
        return set([self.session.gl_user.username])

    @cached_property
    def filter_drafts(self) -> bool:
        """Return True if this is not a draft, or is draft but has not changed, otherwise False."""
        # We pass this filter if the event shows the MR transitioning to draft even if
        # Webhook.run_on_drafts=False. The event will still have to match the has_changed_to_draft
        # trigger test to be passed to a hook.
        if not self.session.webhook.run_on_drafts and \
           (self.draft_status.is_draft and self.draft_status.changed is False):
            LOGGER.info('Event checks: ignoring draft MR events for this hook.')
            return False
        return True

    @cached_property
    def filter_status(self) -> bool:
        """Return True if the MR is not closed, or merged, otherwise False."""
        # If we can determine this is the 'closing' event then return True, the has_closing
        # test will handle that case.
        return not self.mr_inactive if not getattr(self, 'closing', False) else True

    @cached_property
    def has_missing_label(self) -> bool:
        """Return True if the event shows an MR with none of the given hook's expected labels."""
        # Skip this trigger if the event does not provide the MR labels.
        if self.labels is None:
            return False
        mr_prefixes = [lb.prefix for lb in self.labels if lb.scoped]
        return not all(prfx in mr_prefixes for prfx in self.session.webhook.required_label_prefixes)

    @cached_property
    def blocked_label_mismatch(self) -> bool:
        """Return True if the MR 'Blocked' label is does not match discussions_blocked."""
        # If the event doesn't have the necessary info then we just assume everything is alright.
        if not self.merge_request or self.labels is None:
            return False
        mr_has_blocked_label = BLOCKED_LABEL in self.labels
        return mr_has_blocked_label != self.discussions_blocked

    @cached_property
    def has_blocked_label_mismatch(self) -> bool:
        """Return True if the event blocking_discussions_resolved and 'Blocked' label mismatch."""
        return not self.blocked_label_mismatch if \
            self.session.webhook.run_on_blocking_mismatch else False

    @cached_property
    def rh_branch(self) -> typing.Union['Branch', None]:
        """Return the rh_metadata.Branch object associated with the event."""
        if not self.rh_project or not self.target_branch:
            return None
        return self.rh_project.get_branch_by_name(self.target_branch)

    @cached_property
    def target_branch(self) -> str:
        """Return the target branch name."""
        return self.merge_request.get('target_branch')


class GitlabMRIssueMixin:
    """Mixin for properties common to Gitlab Merge Request & Issue events."""

    @cached_property
    def action(self) -> str:
        """Return an MR or Issue event's 'action' value."""
        # Some messages have no action? Not sure why but a KeyError won't get us anywhere.
        # https://gitlab.com/gitlab-org/gitlab/-/issues/335136
        if not (action := self.object_attributes.get('action', '')):
            LOGGER.warning("Message body does not have an 'action'.")
        return action

    @cached_property
    def changed_labels(self) -> set:
        """Return the set of labels that the event indicates have changed."""
        if 'labels' not in self.changes:
            return set()
        prev_labels = {Label(label['title']) for label in self.changes['labels']['previous']}
        cur_labels = {Label(label['title']) for label in self.changes['labels']['current']}
        return prev_labels.difference(cur_labels) | cur_labels.difference(prev_labels)

    @cached_property
    def changes(self) -> dict:
        """Return the body's 'changes' key value, if any."""
        return self.body.get('changes', {})

    @cached_property
    def closing(self) -> bool:
        """Return True if this MR events indicates the MR has just been closed."""
        return self.action == 'close'

    @cached_property
    def has_open_action(self) -> bool:
        """Return True if the event is for a newly opened or reopened MR."""
        return self.action in ('open', 'reopen')

    @cached_property
    def has_changed_label(self) -> bool:
        """Return True if the event shows any labels associated with the given Webhook changing."""
        if webhook_labels := self.session.webhook.webhooks_labels(self.changed_labels):
            LOGGER.info('Event has relevant changed labels: %s', webhook_labels)
        else:
            LOGGER.debug('Event does not have any relevant label changes.')
        return bool(webhook_labels)

    @cached_property
    def has_changed_description(self) -> bool:
        """Return True if the Webhook wants this and the Event indicates it happened."""
        return self.session.webhook.run_on_changed_description and 'description' in self.changes

    @cached_property
    def has_closing_action(self) -> bool:
        """Return True if the Webhook wants this and the Event indicates it happened."""
        return self.session.webhook.run_on_closing and self.closing


@dataclass(repr=False)
class GitlabIssueEvent(GitlabMRIssueMixin, GitlabEvent):
    """Wrapper for ISSUE events."""

    filters: typing.ClassVar[tuple] = GITLAB_ISSUE_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = GITLAB_ISSUE_EVENT_TRIGGERS


@dataclass(repr=False)
class GitlabMREvent(GitlabMRIssueMixin, GitlabEvent):
    """Wrapper for MERGE_REQUEST events."""

    filters: typing.ClassVar[tuple] = GITLAB_MR_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = GITLAB_MR_EVENT_TRIGGERS

    @cached_property
    def commits_changed(self) -> bool:
        """Return True if the event indicates commits changes, otherwise False."""
        # If the action is 'update' and 'merge_status' changed then it looks like the target_branch
        # changed.
        if self.action == 'update' and self.changes.get('merge_status'):
            return True
        # If action is 'update' and 'oldrev' is set then it looks like the commits changed.
        if self.action == 'update' and 'oldrev' in self.merge_request:
            return True
        return False

    @cached_property
    def has_changed_commits(self) -> bool:
        """Return True if the Webhook wants this and the Event indicates it happened."""
        return self.session.webhook.run_on_changed_commits and self.commits_changed

    @cached_property
    def has_changed_to_draft(self) -> bool:
        """Return True if the Webhook wants this and the Event indicates it happened."""
        return self.session.webhook.run_on_changed_to_draft and self.draft_status.into

    @cached_property
    def has_changed_to_ready(self) -> bool:
        """Return True if the Webhook wants this and the Event indicates it happened."""
        return self.session.webhook.run_on_changed_to_ready and self.draft_status.out_of


@dataclass(repr=False)
class GitlabNoteEvent(GitlabEvent):
    """Wrapper for NOTE events."""

    filters: typing.ClassVar[tuple] = GITLAB_NOTE_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = GITLAB_NOTE_EVENT_TRIGGERS

    @cached_property
    def note_text(self) -> str:
        """Return the text of a Note event, or None for other types."""
        return self.body['object_attributes'].get('note', '')

    @cached_property
    def noteable_type(self) -> str:
        """Return the noteable type string of a Note event."""
        return self.body['object_attributes'].get('noteable_type', '')

    @cached_property
    def filter_note_type(self) -> bool:
        """Return True if the 'noteable_type' indicates this is an event from an MR."""
        return self.noteable_type == 'MergeRequest'

    @cached_property
    def has_requested_evaluation(self) -> bool:
        """Return True if a Note event requested evaluation and the webhook has triggers."""
        if not (re_strs := [f'request-{trigger}-evaluation' for
                            trigger in self.session.webhook.request_evaluation_triggers]):
            return False
        re_strs.append('request-evaluation')
        return any(self.note_text.startswith(re_str) for re_str in re_strs)

    @cached_property
    def match_note_text(self) -> list['Match']:
        """Return the list of Match objects from testing note_text against note_text_patterns."""
        matches = []
        for pattern_str in self.session.webhook.note_text_patterns:
            for line in self.note_text.splitlines():
                if match := re_fullmatch(pattern_str, line):
                    matches.append(match)
        return matches


@dataclass(repr=False)
class GitlabPipelineEvent(GitlabEvent):
    """Wrapper for Pipeline events."""

    filters: typing.ClassVar[tuple] = GITLAB_BUILD_PIPELINE_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = GITLAB_BUILD_PIPELINE_EVENT_TRIGGERS

    @cached_property
    def bot_users(self) -> set[str]:
        """Return an empty set for Build and Pipeline events."""
        # If ckihook has retriggered a pipeline then all the events related to it will show an
        # event_user which matches our session user. But we still want to process these events and
        # really there is no reason to ignore any build or pipeline event because of the username
        # so just don't ignore anyone in this case.
        return set()

    @cached_property
    def mr_url(self) -> typing.Union[GitlabURL, None]:
        """Return the MR url from the payload or pipeline vars 'mr_url', or None."""
        if super().mr_url:
            return super().mr_url
        if mr_url := self.variables.get('mr_url'):
            return GitlabURL(mr_url)
        return None

    @cached_property
    def namespace(self) -> str:
        """Return the namespace taken from the mr_url, or project dict."""
        return self.mr_url.namespace if self.mr_url else super().namespace

    @cached_property
    def downstream_project(self) -> typing.Union[dict, None]:
        """Return the body's 'project' dict if this is an event from downstream, otherwise None."""
        return self.body.get('project', {}) if self.is_downstream else None

    @cached_property
    def is_downstream(self) -> bool:
        """Return True if the event is from a downstream pipeline, otherwise False."""
        # If the body has a non-empty source_pipeline then we are downstream?
        return bool(self.body.get('source_pipeline'))

    @cached_property
    def project(self) -> dict:
        """Return the body's 'source_pipeline/project' dict or 'project' dict."""
        # Build (job) and Pipeline events from downstream should have a 'source_pipeline' key
        # which has a 'project' key with info from upstream which is more useful to us.
        return get_nested_key(self.body, 'source_pipeline/project') or self.body.get('project', {})

    @cached_property
    def gl_downstream_pipeline(self) -> typing.Union['ProjectPipeline', None]:
        """Return the downstream gitlab ProjectPipeline which triggered the event, or None."""
        if not self.is_downstream:
            return None
        pipe_id = self.body.get('pipeline_id') or get_nested_key(self.body, 'object_attributes/id')
        return self.session.get_gl_project(self.downstream_project['id']).pipelines.get(pipe_id)

    @cached_property
    def gl_pipeline(self) -> 'ProjectPipeline':
        """Return the upstream gitlab ProjectPipeline which triggered the event."""
        pipeline_id = get_nested_key(self.body, 'source_pipeline/pipeline_id') or \
            self.body.get('pipeline_id') or get_nested_key(self.body, 'object_attributes/id')
        return self.session.get_gl_project(self.project['id']).pipelines.get(pipeline_id)

    @cached_property
    def has_downstream(self) -> bool:
        """Return True if the webhook wants downstream builds/pipelines and this event is one."""
        kind = self.kind.name.lower()
        return getattr(self.session.webhook, f'run_on_{kind}_downstream') and self.is_downstream

    @cached_property
    def has_upstream(self) -> bool:
        """Return True if the webhook wants upstream builds/pipelines and this event is one."""
        kind = self.kind.name.lower()
        return getattr(self.session.webhook, f'run_on_{kind}_upstream') and not self.is_downstream

    @cached_property
    def filter_merge_requests(self) -> bool:
        """Return True if the event is associated with an MR, otherwise False."""
        return bool(self.mr_url) if self.session.webhook.match_to_projects else True

    @cached_property
    def variables(self) -> dict:
        """Return a dict of pipeline variables."""
        if event_vars := get_nested_key(self.body, 'object_attributes/variables'):
            return {var['key']: var['value'] for var in event_vars}
        if gl_downstream_pipeline := self.gl_downstream_pipeline:
            return {i.key: i.value for i in gl_downstream_pipeline.variables.list(get_all=True)}
        return {}


@dataclass(repr=False)
class GitlabBuildEvent(GitlabPipelineEvent):
    """Wrapper for Build (Job) events."""


@dataclass(repr=False)
class GitlabPushEvent(GitlabEvent):
    """Wrapper for Push events."""

    filters: typing.ClassVar[tuple] = GITLAB_PUSH_EVENT_FILTERS
    triggers: typing.ClassVar[tuple] = GITLAB_PUSH_EVENT_TRIGGERS

    @cached_property
    def target_branch(self) -> str:
        """Return the target branch name from the push event."""
        return self.body['ref'].rsplit('/', 1)[-1]

    @cached_property
    def filter_branch(self) -> bool:
        """Return True if the push event is associated with an active rh_metadata.Branch."""
        return self.rh_branch and not self.rh_branch.inactive


@dataclass(repr=False)
class JiraEvent(BaseEvent):
    """Wrapper for JIRA webhook events."""

    filters: typing.ClassVar[tuple] = JIRA_EVENT_FILTERS

    @cached_property
    def bot_users(self) -> set[str]:
        """Return the set with the username we are logged into the API as."""
        return set(JIRA_BOT_ACCOUNTS)

    @cached_property
    def key(self) -> JiraKey:
        """Get the Key the event is related to."""
        return JiraKey(self.body['issue']['key'])

    @cached_property
    def user(self) -> str:
        """Return the username of the user which generated the event."""
        return self.body['user']['name']

    @cached_property
    def namespace(self) -> str:
        """Return the 'path_with_namespace' value for the first linked 'redhat' MR, if known."""
        return self.mr_url.namespace if self.mr_url else ''

    @cached_property
    def mr_url(self) -> typing.Union[GitlabURL, None]:
        """Return the MR URL, or None if not present."""
        # A jira issue could be linked to multiple MRs but here we just return the first one 🤷.
        for mr_url in get_linked_mrs(self.key, namespace='redhat', jiracon=self.session.jira):
            return GitlabURL(mr_url)
        return None

    @cached_property
    def filter_project(self) -> bool:
        """Return True if the issue key is in the RHEL space, otherwise False."""
        return self.key.project == 'RHEL'


@dataclass(repr=False)
class UmbBridgeEvent(BaseEvent):
    """Wrapper for simple events generated by the umb_bridge webhook."""

    filters: typing.ClassVar[tuple] = UMBBRIDGE_EVENT_FILTERS

    @cached_property
    def mr_url(self) -> typing.Union[GitlabURL, None]:
        """Fake the MR URL."""
        namespace, mr_id = self.body['mrpath'].split('!')
        return GitlabURL(f'{self.session.gl_hostname}/{namespace}/-/merge_requests/{mr_id}')

    @cached_property
    def namespace(self) -> str:
        """Return the namespace string from the body 'mrpath'."""
        return self.body['mrpath'].split('!', 1)[0]

    @cached_property
    def filter_target(self) -> bool:
        """Return True if the 'event_target_webhook' matches the session webhook."""
        event_target = self.headers.get('event_target_webhook')
        LOGGER.info('event_target is %s, webhook is: %s', event_target, self.session.webhook.name)
        if event_target != self.session.webhook.name:
            LOGGER.info("Event target '%s' does not match environment.", event_target)
            return False
        return True


@dataclass(repr=False)
class AmqpEvent(BaseEvent):
    """Wrapper for events from the UMB (from bugzilla)."""

    filters: typing.ClassVar[tuple] = AMQP_EVENT_FILTERS

    @cached_property
    def bot_users(self) -> set[str]:
        """Return the set with the BUGZILLA_EMAIL from the environment."""
        return set([environ['BUGZILLA_EMAIL']])

    @cached_property
    def bug_id(self) -> int:
        """Return the bug ID from the event, using a value of 0 if not found."""
        return int(get_nested_key(self.body, 'event/bug_id', 0))

    @cached_property
    def user(self) -> str:
        """Return the username of the user which generated the event."""
        return get_nested_key(self.body, 'event/user/login', '')

    @cached_property
    def filter_bug(self) -> bool:
        """Return True if the event contains a BZ identifier, otherwise False."""
        return bool(self.bug_id)


@dataclass(repr=False)
class DataWarehouseEvent(BaseEvent):
    """Wrapper for events from the UMB (from DataWarehouse)."""

    filters: typing.ClassVar[tuple] = DATAWAREHOUSE_EVENT_FILTERS

    @cached_property
    def status(self) -> str:
        """Return the status label from the message, which represents its reason."""
        return self.body.get('status')

    @cached_property
    def object_type(self) -> typing.Literal['checkout', 'build', 'test', 'testresult']:
        """Return the object_type from the message."""
        return self.body.get('object_type')

    @cached_property
    def object(self) -> dict:
        """Return the object dict from the message, which contains a serialized KCIDB object."""
        return self.body.get("object")

    @cached_property
    def misc(self) -> dict:
        """Return the misc dictionary from the message, which contains extra data."""
        return self.body.get("misc")

    @cached_property
    def filter_status(self) -> bool:
        """Return True if event.status is handled, otherwise False."""
        return self.status in ["checkout_issueoccurrences_changed"]

    @cached_property
    def filter_object_type(self) -> bool:
        """Return True if event.object_type is handled, otherwise False."""
        return self.object_type in ["checkout"]


AnyEvent = typing.Union[
    GitlabMREvent, GitlabNoteEvent, GitlabBuildEvent, GitlabPipelineEvent, GitlabEvent,
    JiraEvent, AmqpEvent, UmbBridgeEvent, DataWarehouseEvent,
]


def create_event(
    session: 'SessionRunner',
    headers: dict,
    body: dict
) -> AnyEvent:
    # pylint: disable=too-many-return-statements
    """Return an instance of some BaseEvent subclass depending on the input."""
    message_type = MessageType.get(headers['message-type'])
    match message_type:
        case MessageType.GITLAB:
            kind = GitlabObjectKind.get(body['object_kind'])
            if kind is GitlabObjectKind.MERGE_REQUEST:
                return GitlabMREvent(session, headers, body)
            if kind is GitlabObjectKind.NOTE:
                return GitlabNoteEvent(session, headers, body)
            if kind is GitlabObjectKind.ISSUE:
                return GitlabIssueEvent(session, headers, body)
            if kind is GitlabObjectKind.BUILD:
                return GitlabBuildEvent(session, headers, body)
            if kind is GitlabObjectKind.PIPELINE:
                return GitlabPipelineEvent(session, headers, body)
            if kind is GitlabObjectKind.PUSH:
                return GitlabPushEvent(session, headers, body)
            return GitlabEvent(session, headers, body)
        case MessageType.JIRA:
            return JiraEvent(session, headers, body)
        case MessageType.AMQP:
            return AmqpEvent(session, headers, body)
        case MessageType.UMB_BRIDGE:
            return UmbBridgeEvent(session, headers, body)
        case MessageType.DATAWAREHOUSE:
            return DataWarehouseEvent(session, headers, body)
    raise RuntimeError(f'No Event type for {message_type}')


def special_label_changes(
    event: typing.Union[GitlabEvent, GitlabMREvent]
) -> tuple[list[Label], list[Label]]:
    """Return a tuple with the readyForX/Blocked Labels to add and remove."""
    to_add = []
    to_remove = []
    # Ensure Draft MRs do not have any readyFoxX label.
    if mr_ready_label := next((rl in event.labels or [] for rl in READY_LABELS), None) if \
       event.draft_status.is_draft else None:
        LOGGER.info('MR is draft, removing %s label.', mr_ready_label)
        to_remove.append(mr_ready_label)
    # Ensure the 'Blocked' label matches the MR's blocking_discussions_resolved property
    if event.blocked_label_mismatch:
        label_list = to_remove if BLOCKED_LABEL in event.labels else to_add
        LOGGER.info('Event indicates the %s label should be %s.', BLOCKED_LABEL,
                    'added' if label_list is to_add else 'removed')
        label_list.append(BLOCKED_LABEL)
    return to_add, to_remove
