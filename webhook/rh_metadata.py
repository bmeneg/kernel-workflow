"""Metadata for RHEL projects."""
import dataclasses
from functools import cached_property
import os
import pkgutil
from re import fullmatch as re_fullmatch
import typing

from cki_lib import footer
from cki_lib import misc
from cki_lib import yaml
from cki_lib.logger import get_logger

from webhook import defs
from webhook.cpc import get_policy_data
from webhook.pipelines import PipelineType
from webhook.temp_utils import load_yaml_data as load_yaml

if typing.TYPE_CHECKING:
    from webhook.cpc import Token

LOGGER = get_logger(__name__)

PROJECTS_SCHEMA = yaml.load(
    contents=pkgutil.get_data(__name__, 'schema_rh_projects.yaml')
)['schema']

WEBHOOKS_SCHEMA = yaml.load(
    contents=pkgutil.get_data(__name__, 'schema_rh_webhooks.yaml')
)['schema']


def check_data(this_dc: dataclasses.dataclass) -> None:
    """Check the dataclass object fields have the right types and are not empty values."""
    dc_type = type(this_dc)
    for dc_field in [field for field in dataclasses.fields(this_dc) if field.init]:
        fvalue = getattr(this_dc, dc_field.name)
        field_base_type = dc_field.type.__origin__ if \
            isinstance(dc_field.type, typing.GenericAlias) else dc_field.type
        # fields should hold data of the correct type
        if not isinstance(fvalue, field_base_type):
            raise TypeError(f'{dc_type}.{dc_field.name} must be {field_base_type}: {this_dc}')
        # do not test for empty values if a field is a bool or has an empty default value
        if isinstance(fvalue, bool) or (isinstance(fvalue, str) and dc_field.default == '') or \
           (isinstance(fvalue, list) and dc_field.default_factory is list):
            continue
        # fields with non-empty default values should not have empty values
        if not fvalue:
            raise ValueError(f'{dc_type}.{dc_field.name} cannot be empty: {this_dc}')


@dataclasses.dataclass(kw_only=True)
class Webhook(footer.Context):  # pylint: disable=too-many-instance-attributes
    """Webhook metadata."""

    name: str
    enabled_by_default: bool = False
    required: bool = True
    required_label_prefixes: list[str] = dataclasses.field(default_factory=list)
    extra_labels_regex: str = ''
    required_for_qa_scope: defs.MrScope = defs.MrScope.NEEDS_REVIEW
    on_draft: bool = True
    match_to_projects: bool = True
    request_evaluation_triggers: list[str] = dataclasses.field(default_factory=list)
    run_on_drafts: bool = True
    run_on_blocking_mismatch: bool = False
    run_on_changed_commits: bool = True
    run_on_changed_description: bool = True
    run_on_changed_to_draft: bool = False
    run_on_changed_to_ready: bool = True
    run_on_closing: bool = False
    run_on_build_downstream: bool = False
    run_on_build_upstream: bool = False
    run_on_pipeline_downstream: bool = False
    run_on_pipeline_upstream: bool = False
    note_text_patterns: list[str] = dataclasses.field(default_factory=list)
    skip_session_filter_check: bool = False
    skip_session_trigger_check: bool = False
    instance_manages_ready_labels: bool = False
    manage_special_labels: bool = False

    def __post_init__(self) -> None:
        """Fix the required_for_qa_scope and Check it."""
        if not isinstance(self.required_for_qa_scope, defs.MrScope):
            self.required_for_qa_scope = defs.MrScope.get(self.required_for_qa_scope)
        check_data(self)

    def status(self, mr_labels: typing.Iterable[typing.Union[str, defs.Label]]) -> defs.MrScope:
        """Return the defs.MrScope describing this hook's status for the given set of labels."""
        if not self.required:
            return defs.MrScope.READY_FOR_MERGE
        status = defs.MrScope.NEEDS_REVIEW
        if hook_mr_labels := [label for label in mr_labels if
                              label.scoped and label.prefix in self.required_label_prefixes]:
            if all(label.scope >= self.required_for_qa_scope for label in hook_mr_labels):
                status = defs.MrScope.READY_FOR_QA
            if all(label.scope >= defs.MrScope.OK for label in hook_mr_labels):
                status = defs.MrScope.READY_FOR_MERGE
        return status

    def webhooks_labels(
        self,
        mr_labels: typing.Iterable[typing.Union[str, defs.Label]]
    ) -> typing.List[defs.Label]:
        """Return the subset of input labels which are 'owned' by this webhook."""
        if (not self.required_label_prefixes and not self.extra_labels_regex):
            return []
        mr_labels = [defs.Label(label) for label in mr_labels]
        webhook_labels = []
        for label in mr_labels:
            if label.scoped and label.prefix in self.required_label_prefixes:
                webhook_labels.append(label)
            elif self.extra_labels_regex and re_fullmatch(self.extra_labels_regex, label):
                webhook_labels.append(label)
        LOGGER.debug("%s labels are: %s", self.name, webhook_labels)
        return webhook_labels


@dataclasses.dataclass(kw_only=True)
class Project:
    # pylint: disable=too-many-instance-attributes
    """Project metadata."""

    id: int  # pylint: disable=invalid-name
    group_id: int
    name: str
    product: str
    pipelines: list[PipelineType] = dataclasses.field(default_factory=list, repr=False)
    inactive: bool = False
    sandbox: bool = False
    confidential: bool = False
    group_labels: bool = True
    namespace: str = ''
    branches: list['Branch'] = dataclasses.field(default_factory=list, repr=False)
    webhooks: dict[str, Webhook] = dataclasses.field(default_factory=dict, repr=False)
    public_signoff_ok: bool = False
    default_branch: str = 'main'

    def __post_init__(self) -> None:
        """Run check_data."""
        check_data(self)

    @classmethod
    def new(
        cls,
        proj_data: typing.Dict,
        all_webhooks: typing.Dict[str, Webhook],
    ) -> 'Project':
        """Construct a new Project instance with."""
        fixed_data = proj_data.copy()
        # Delete the branches here and set them below after the Project is created.
        del fixed_data['branches']
        # Set up the pipelines.
        fixed_data['pipelines'] = [PipelineType.get(p) for p in proj_data['pipelines']]
        # If this project does not have any webhooks defined then take the defaults.
        if not proj_data.get('webhooks'):
            fixed_data['webhooks'] = {
                wh_name: wh for wh_name, wh in all_webhooks.items() if wh.enabled_by_default
            }
        else:
            # Otherwise, take the given setting for each webhook, or the default.
            fixed_data['webhooks'] = {
                webhook.name: webhook for webhook in all_webhooks.values() if
                proj_data['webhooks'].get(webhook.name, webhook.enabled_by_default)
            }
        new_project = cls(**fixed_data)
        new_project.branches = [Branch(**b, project=new_project) for b in proj_data['branches']]
        return new_project

    def get_branch_by_name(self, branch_name: str) -> typing.Union['Branch', None]:
        """Return the branch with the given name, or None."""
        return next((b for b in self.branches if b.name == branch_name), None)

    def get_branches_by_itr(self, itr: str) -> list['Branch']:
        """Return a list of active branches whose policy matches the given ITR."""
        return [b for b in self.branches if b.internal_target_release == itr and not b.inactive]

    def get_branches_by_ztr(self, ztr: str) -> list['Branch']:
        """Return a list of active branches whose policy matches the given ZTR."""
        return [b for b in self.branches if b.zstream_target_release == ztr and not b.inactive]

    def get_branches_by_fix_version(self, fixver: defs.FixVersion) -> list['Branch']:
        """Return a list of active branches whose policy matches the given fix_version."""
        return [b for b in self.branches if fixver in b.fix_versions and not b.inactive]

    def webhooks_status(
        self,
        mr_labels: typing.Iterable[typing.Union[str, defs.Label]]
    ) -> defs.MrScope:
        """Return the minimim MrScope of this project's webhook labels for the given mr_labels."""
        mr_labels = [defs.Label(label) for label in mr_labels]
        return min(webhook.status(mr_labels) for webhook in self.webhooks.values())


@dataclasses.dataclass(eq=True, kw_only=True)
class Branch:
    # pylint: disable=too-many-instance-attributes
    """Branch metadata."""

    name: str
    components: set
    distgit_ref: str
    sub_component: str = ''
    internal_target_release: str = ''
    zstream_target_release: str = ''
    milestone: str = ''
    inactive: bool = False
    pipelines: list[PipelineType] = dataclasses.field(default_factory=list, repr=False)
    policy: list['Token'] = dataclasses.field(default_factory=list, compare=False, repr=False)
    project: Project | None = dataclasses.field(default=None, repr=False)
    protected_approval_rules: bool = True
    fix_versions: list = dataclasses.field(default_factory=list)

    def __post_init__(self) -> None:
        """Set up pipelines and check data."""
        if self.pipelines:
            self.pipelines[:] = [PipelineType.get(pipe) if isinstance(pipe, str) else pipe for
                                 pipe in self.pipelines]
        if not self.pipelines:
            self.pipelines = self.project.pipelines
        if self.project.inactive:
            self.inactive = True
        self.components = set(self.components)
        self.fix_versions = [defs.FixVersion(fv_str) for fv_str in self.fix_versions]
        check_data(self)

    @cached_property
    def major(self) -> typing.Union[int, None]:
        """Return the Major version number, or None if not present."""
        return self.fix_versions[0].major if self.fix_versions else None

    @cached_property
    def minor(self) -> typing.Union[int, None]:
        """Return the Minor version number, or None if not present."""
        return self.fix_versions[0].minor if self.fix_versions else None

    @cached_property
    def ystream(self) -> typing.Union[bool, None]:
        """True if any of the fix_versions are ystream, or None if not present."""
        return any(not fv.zstream for fv in self.fix_versions) if self.fix_versions else None

    @cached_property
    def zstream(self) -> typing.Union[bool, None]:
        """True if any of the fix_versions are zstream, or None if not present."""
        return any(fv.zstream for fv in self.fix_versions) if self.fix_versions else None

    @cached_property
    def stage(self) -> defs.DevStage:
        """Return the DevStage representing the development stage of this Branch."""
        if self.fix_versions:
            if any(fv.cycle == 'alpha' for fv in self.fix_versions):
                return defs.DevStage.ALPHA
            if any(fv.cycle == 'beta' for fv in self.fix_versions):
                return defs.DevStage.BETA
            # During early zstream stage a branch will have both a ystream and zstream style
            # fix version.
            if self.ystream and self.zstream:
                return defs.DevStage.EARLY_ZSTREAM
            if self.ystream:
                return defs.DevStage.YSTREAM
            if self.zstream:
                return defs.DevStage.ZSTREAM
        return defs.DevStage.UNKNOWN

    @cached_property
    def version(self) -> typing.Union[str, None]:
        """Return a version string derived from fix_version, or None."""
        if not self.fix_versions:
            return None
        return f'{self.fix_versions[0].major}.{self.fix_versions[0].minor}'

    def __ge__(self, other: typing.Self) -> bool:
        """Return True if self is greather than or equal to other, otherwise False."""
        return self.__gt__(other) or self == other

    def __gt__(self, other: typing.Self) -> bool:
        """Return True if self is greater than other, otherwise False."""
        if other.stage is self.stage:
            return (self.major, self.minor) > (other.major, other.minor)
        return self.stage > other.stage

    def __le__(self, other: typing.Self) -> bool:
        """Return True if self is less than or equal to other, otherwise False."""
        return self.__lt__(other) or self == other

    def __lt__(self, other: typing.Self) -> bool:
        """Return True if self is less than other, otherwise False."""
        if other.stage is self.stage:
            return (self.major, self.minor) < (other.major, other.minor)
        return self.stage < other.stage


@dataclasses.dataclass
class Projects:
    """Projects metadata.

    load_policies: Bool to indicate whether to load dist-git policy data for each Branch.
                   Defaults to False.
    projects_yaml_path: Optional path to the rh_metadata.yaml to get Project data from.
                        If not given and env var RH_METADATA_YAML_PATH is set then use that.
                        If no path is given and RH_METADATA_YAML_PATH is not set then use
                        the path from defs.DEFAULT_RH_METADATA_PATH.
    webhooks_yaml_path: Optional path to the rh_webhooks.yaml to get Webhooks data from.
                        If not given and env var WEBHOOKS_YAML_PATH is set then use that.
                        If no path is given and WEBHOOKS_YAML_PATH is not set then use
                        the path from defs.DEFAULT_RH_WEBHOOKS_PATH.
    extra_projects_paths: Optional list of paths to load additional Project data from.
    """

    load_policies: dataclasses.InitVar[bool] = False
    projects_yaml_path: dataclasses.InitVar[str] = ''
    webhooks_yaml_path: dataclasses.InitVar[str] = ''
    extra_projects_paths: dataclasses.InitVar[typing.List[str]] = []
    projects: typing.Dict[int, Project] = dataclasses.field(default_factory=dict, init=False)
    webhooks: typing.Dict[str, Webhook] = dataclasses.field(default_factory=dict, init=False)

    def __post_init__(
        self,
        load_policies: bool,
        projects_yaml_path: str,
        webhooks_yaml_path: str,
        extra_projects_paths: typing.List[str]
    ) -> None:
        """Load yaml into Projects and check data."""
        # Try to load projects data from an rh_metadata.yaml file.
        projects_data = \
            load_yaml(projects_yaml_path, 'RH_METADATA_YAML_PATH', [defs.DEFAULT_RH_METADATA_PATH])
        if not misc.get_nested_key(projects_data, 'projects'):
            raise RuntimeError("No 'projects' found in yaml.")

        # If given extra paths, load them one by one and extend existing project_data.
        for path in extra_projects_paths or os.environ.get('RH_METADATA_EXTRA_PATHS', '').split():
            if not (extra_proj_data := misc.get_nested_key(load_yaml(path), 'projects', [])):
                raise RuntimeError(f"No 'projects' found in extra yaml: {path}")
            projects_data['projects'].extend(extra_proj_data)

        # Try to load webhooks data.
        webhooks_data = \
            load_yaml(webhooks_yaml_path, 'WEBHOOKS_YAML_PATH', [defs.DEFAULT_RH_WEBHOOKS_PATH])
        if not misc.get_nested_key(webhooks_data, 'webhooks'):
            raise RuntimeError("No 'webhooks' found in yaml.")

        # Validate the data with the schema.
        yaml.validate(projects_data, PROJECTS_SCHEMA)
        yaml.validate(webhooks_data, WEBHOOKS_SCHEMA)

        # Create the Webhook objects.
        self.webhooks = {name: Webhook(**wh) for name, wh in webhooks_data['webhooks'].items()}
        # Create the Project objects.
        self.projects = {p['id']: Project.new(p, self.webhooks) for p in projects_data['projects']}
        # Optionally do policy loading.
        if load_policies:
            self.do_load_policies()

    @staticmethod
    def _format_project_id(project_id: typing.Union[str, int]) -> int:
        """Return the project_id as an int."""
        # project_id can be a number or a Gitlab ID scalar type, ie. gid://gitlab/Project/1234567
        if isinstance(project_id, str) and project_id.startswith('gid://gitlab/Project/'):
            project_id = project_id.removeprefix('gid://gitlab/Project/')
        return int(project_id)

    def get_project_by_id(self, project_id: typing.Union[str, int]) -> typing.Union[Project, None]:
        """Return the project with the given project_id, or None."""
        project_id = self._format_project_id(project_id)
        return self.projects.get(project_id)

    def get_projects_by_environment(self, sandbox: typing.Optional[bool] = None) -> list[Project]:
        """Return the list of Projects that match the given or current environment."""
        is_sandbox = not misc.is_production() if sandbox is None else sandbox
        return [project for project in self.projects.values() if is_sandbox == project.sandbox]

    def get_projects_by_name(self, project_name: str) -> list[Project]:
        """Return the list of Projects with the given name."""
        return [project for project in self.projects.values() if project.name == project_name]

    def get_project_by_namespace(self, namespace: str) -> typing.Union[Project, None]:
        """Return the Project object with the matching namespace."""
        return next((project for project in self.projects.values() if
                     project.namespace == namespace), None)

    def get_target_branch(
        self,
        project_id: typing.Union[str, int],
        target_branch: str
    ) -> typing.Union[Branch, None]:
        """Return the Branch object matching the given project_id and target_branch."""
        project_id = self._format_project_id(project_id)
        if not (project := self.projects.get(project_id)):
            return None
        return next((branch for branch in project.branches if branch.name == target_branch), None)

    def do_load_policies(self) -> None:
        """Load policy tokens for each branch."""
        policies = get_policy_data(policy_regex=r'^(rhel-.*|c\d+s)$')
        for project in self.projects.values():
            for branch in project.branches:
                if policy := policies.get(branch.distgit_ref):
                    branch.policy = policy


def is_branch_active(
    projects: Projects,
    project_id: typing.Union[str, int],
    target_branch: str
) -> bool:
    """Return True if the target branch is active, otherwise False."""
    if project := projects.get_project_by_id(project_id):
        if branch := project.get_branch_by_name(target_branch):
            return not branch.inactive
    else:
        LOGGER.warning('Project %s is not present in rh_metadata.', project_id)
    return False
