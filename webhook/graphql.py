"""Common helpers for graphql."""
from functools import cached_property
import typing

from cki_lib import gitlab
from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production_or_staging

from webhook import fragments
from webhook.defs import GITFORGE
from webhook.defs import Label
from webhook.defs import MAX_COMMITS_PER_MR

LOGGER = get_logger('cki.webhook.graphql')

GET_USER_DETAILS_BASE = """
query userData {
  ...CurrentUser
}
"""

GET_USER_DETAILS_QUERY = GET_USER_DETAILS_BASE + fragments.CURRENT_USER + fragments.GL_USER

SET_MR_REVIEWERS_MUTATION_BASE = """
mutation setReviewers($input: MergeRequestSetReviewersInput!) {
  mergeRequestSetReviewers(input: $input) {
    mr: mergeRequest {
      reviewers {
        nodes {
          ...GlUser
        }
      }
    }
  }
}
"""

SET_MR_REVIEWERS_MUTATION = SET_MR_REVIEWERS_MUTATION_BASE + fragments.GL_USER

FIND_MEMBER_BASE = """
query mrData($namespace: ID!, $search_key: String!) {
  group(fullPath: $namespace) {
    groupMembers(search: $search_key) {
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
  project(fullPath: $namespace) {
    projectMembers(search: $search_key) {
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
}
"""

FIND_MEMBER_QUERY = FIND_MEMBER_BASE + fragments.GL_USER

GET_USER_BASE = """
query userQuery($username: String!) {
  user(username: $username) {
    ...GlUser
  }
}
"""

GET_USER_QUERY = GET_USER_BASE + fragments.GL_USER

GET_USER_BY_ID_BASE = """
query mrData($userid: UserID!) {
  user(id: $userid) {
    ...GlUser
  }
}
"""

GET_USER_BY_ID_QUERY = GET_USER_BY_ID_BASE + fragments.GL_USER

GET_MR_DESCRIPTIONS_QUERY = """
query mrData($namespace: ID!, $mr_ids: [String!]) {
  project(fullPath: $namespace) {
    mergeRequests(iids: $mr_ids) {
      nodes {
        iid
        description
      }
    }
  }
}
"""

MR_LABELS_QUERY_BASE = """
  query mrData($mr_id: String!, $namespace: ID!, $first: Boolean = true, $after: String = "") {
    project(fullPath: $namespace) {
      mr: mergeRequest(iid: $mr_id) {
        ...MrLabelsPaged @include(if: $first)
      }
    }
  }
"""

MR_LABELS_QUERY = MR_LABELS_QUERY_BASE + fragments.MR_LABELS_PAGED

ALL_MEMBERS_BASE = """
query data($namespace: ID!, $after: String = "") {
  %s(fullPath: $namespace) {
    %sMembers(after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        user {
          ...GlUser
        }
      }
    }
  }
}
"""

ALL_MEMBERS_QUERY = ALL_MEMBERS_BASE + fragments.GL_USER

ALL_PROJECT_ISSUES_QUERY = """
query data($namespace: ID!, $after: String = "") {
  project(fullPath: $namespace) {
    issues(after: $after, state: opened) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        title
        webUrl
      }
    }
  }
}
"""

CREATE_ISSUE_MUTATION = """
mutation createIssue($input: CreateIssueInput!) {
  createIssue(input: $input) {
    issue {
      iid
      webUrl
    }
  }
}
"""


CHECK_MR_STATE = """
query mr_state($mr_id: String!, $namespace: ID!) {
  project(fullPath: $namespace) {
    mr: mergeRequest(iid: $mr_id) {
      commitCount
      state
    }
  }
}
"""


class GitlabGraph:
    """A wrapper object for interacting with gitlab graphql."""

    @staticmethod
    def _check_user(results, check_user):
        """Return True if the query currentUser username matches the check_user."""
        return check_user == results['currentUser']['username']

    @staticmethod
    def _check_keys(results, check_keys):
        """Return True if all the keys are in the given query results."""
        return check_keys <= results.keys()

    @classmethod
    def check_query_results(cls, results, check_keys=None, check_user=None):
        """Perform some optional checks of query results."""
        # See GitlabGraph.execute_query().
        # Ignore our own messages (for bots). Query must include 'currentUser' field.
        if check_user and cls._check_user(results, check_user):
            LOGGER.info('Ignoring message from %s.', check_user)
            return None

        # Raise an error if not all expected keys are in the results.
        if check_keys and not cls._check_keys(results, check_keys):
            raise RuntimeError(f'Gitlab did not return all keys {check_keys} in {results}.')
        return results

    def __init__(self, get_user=False, hostname=None):
        """Set up the client."""
        hostname = hostname or GITFORGE
        self.client = gitlab.get_graphql_client(hostname)
        LOGGER.info('Connected to %s.', hostname)
        if get_user:
            LOGGER.info('Logged in as %s (%s).', self.username, self.user_id)

    @cached_property
    def user(self):
        """Return the details of the user we are logged in as."""
        return self.client.query(GET_USER_DETAILS_QUERY)['currentUser']

    @property
    def user_id(self):
        """Return the global user ID of the user we are logged in as as an int."""
        if not self.user:
            return None
        return int(self.user['gid'].rsplit('/')[-1])

    @property
    def username(self):
        """Return the username we are logged in as."""
        if not self.user:
            return None
        return self.user['username']

    def get_user(self, username):
        """Return the user details if found, otherwise None."""
        if not username:
            raise ValueError(f"username must be a valid non-zero length string, not '{username}'")
        results = self.client.query(GET_USER_QUERY, {'username': username})
        return get_nested_key(results, 'user')

    def get_mr_descriptions(self, namespace, mr_ids):
        """Return a dict of descriptions with the mr_id as key."""
        params = {'namespace': namespace, 'mr_ids': [str(mr_id) for mr_id in mr_ids]}
        results = self.client.query(GET_MR_DESCRIPTIONS_QUERY, params)
        mrs = get_nested_key(results, 'project/mergeRequests/nodes', [])
        return {int(mr['iid']): mr['description'] for mr in mrs}

    def get_user_by_id(self, userid):
        """Return the user with the given GID, or None."""
        if isinstance(userid, int):
            userid = f'gid://gitlab/User/{userid}'
        if not isinstance(userid, str) or not userid.startswith('gid://gitlab/User/'):
            raise ValueError('userid must be an int or valid GID string.')
        results = self.client.query(GET_USER_BY_ID_QUERY, {'userid': userid})
        return results.get('user') if results else None

    def get_all_issues(self, namespace):
        """Return the list of all open Issues of the given project. Returns None if no project."""
        paged_key = 'project/issues'
        results = self.client.query(ALL_PROJECT_ISSUES_QUERY, {'namespace': namespace}, paged_key)
        if results and results.get('project') is None:
            LOGGER.warning('get_all_issues: project namespace not found: %s', namespace)
            return None
        return get_nested_key(results, f'{paged_key}/nodes', [])

    def get_all_members(self, namespace, namespace_type):
        """Return a dict of all group or poject members. Returns None if the ns is not found."""
        if namespace_type not in ('group', 'project'):
            raise ValueError(f"namespace_type must be 'group' or 'project', not {namespace_type}")
        params = {'namespace': namespace}
        paged_key = f'{namespace_type}/{namespace_type}Members'
        results = self.client.query(ALL_MEMBERS_QUERY % (namespace_type, namespace_type), params,
                                    paged_key)
        if results and results.get(namespace_type) is None:
            LOGGER.warning('get_all_members: %s namespace not found: %s', namespace_type, namespace)
            return None
        return {user['user']['username']: user['user'] for user in
                get_nested_key(results, f'{paged_key}/nodes', [])}

    def get_all_mr_labels(
        self, namespace: str,
        mr_id: int,
        existing_results: typing.Optional[dict] = None
    ) -> list[Label]:
        """Return the complete list of Label objects for the given MR."""
        # If the optional existing_results dict is provided it should be in the following format:
        # {'pageInfo': {'hasNextPage': bool, 'endCursor': str}, 'nodes': list[dict]}
        # In other words, the 'labels' field contents of an existing query result.
        existing_results = existing_results or {}
        has_next_page: bool = get_nested_key(existing_results, 'pageInfo/hasNextPage', True)
        end_cursor: str = get_nested_key(existing_results, 'pageInfo/endCursor', '')
        existing_results_nodes: list = existing_results.get('nodes', [])

        if has_next_page:
            params = {'namespace': namespace, 'mr_id': str(mr_id), 'after': end_cursor}
            more_results = self.client.query(MR_LABELS_QUERY, variable_values=params,
                                             paged_key='project/mr/labels/')
            # If we didn't get back any labels nodes then we've been given bad input or maybe
            # don't have the right permissions. Blow up for now?
            if (
                label_nodes := get_nested_key(more_results, 'project/mr/labels/nodes', None)
            ) is None:
                raise ValueError(f"Unexpected missing labels nodes: {more_results}")
            existing_results_nodes.extend(label_nodes)
        return [Label(label['title']) for label in existing_results_nodes]

    def check_mr_state(self, namespace: str, mr_id: int) -> bool:
        """Return True if the MR is open and doesn't have a silly # of commits."""
        results = self.client.query(CHECK_MR_STATE, {'namespace': namespace, 'mr_id': str(mr_id)})
        state = get_nested_key(results, 'project/mr/state')
        commits = int(get_nested_key(results, 'project/mr/commitCount', 0))
        LOGGER.info('%s!%s: state: %s, commits: %s', namespace, mr_id, state, commits)
        return state == 'opened' and commits < MAX_COMMITS_PER_MR

    def create_project_issue(self, namespace, title, body, extra_input=None):
        """Create a new Issue with the given title and body on the given namespace."""
        params = {'input': {'projectPath': namespace, 'title': title, 'description': body}}
        if extra_input:
            params['input'].update(extra_input)
        if is_production_or_staging():
            results = self.client.query(CREATE_ISSUE_MUTATION, params)
            return results['createIssue']['issue']
        return {'iid': 0, 'webUrl': f'{GITFORGE}/{namespace}/-/issues/0'}

    def find_member(self, namespace, attribute, search_key):
        """Find the member of the given namespace with the matching attribute."""
        # This won't find anything if the search_key isn't in the first 100 results!
        if not attribute or not search_key:
            raise ValueError('attibute and search_key must be non-zero length strings.')
        params = {'namespace': namespace, 'search_key': search_key}
        results = self.client.query(FIND_MEMBER_QUERY, params)
        users = get_nested_key(results, 'group/groupMembers/nodes', []) or \
            get_nested_key(results, 'project/projectMembers/nodes', [])
        return next((user['user'] for user in users if
                     user['user'].get(attribute, None) == search_key), None)

    def find_member_by_email(self, namespace, email, username):
        """Find the member with the given email that has the given username."""
        # This won't find anything if the search_key isn't in the first 100 results!
        if not email or not username:
            raise ValueError('email and username must be non-zero length strings.')
        params = {'namespace': namespace, 'search_key': email}
        results = self.client.query(FIND_MEMBER_QUERY, params)
        users = get_nested_key(results, 'group/groupMembers/nodes', []) or \
            get_nested_key(results, 'project/projectMembers/nodes', [])
        return next((user['user'] for user in users if
                     user['user']['username'] == username), None)

    def set_mr_reviewers(self, namespace, mr_id, usernames, mode):
        """Perform a mergeRequestSetReviewers mutation and returns the new list of reviewers."""
        # https://docs.gitlab.com/ee/api/graphql/reference/#mutationmergerequestsetreviewers
        valid_modes = ('APPEND', 'REMOVE', 'REPLACE')
        if mode not in valid_modes:
            raise ValueError(f'mode {mode} is invalid. Must be one of: {valid_modes}')
        if not usernames:
            raise ValueError('usernames must be a non-empty iterable')
        if not is_production_or_staging():
            return [{'username': username} for username in usernames]

        params = {'input': {'projectPath': namespace, 'iid': str(mr_id),
                            'reviewerUsernames': list(usernames),
                            'operationMode': mode
                            }}
        LOGGER.debug('Running mergeRequestSetReviewers mutation with: %s', params)
        if not (results := self.client.query(SET_MR_REVIEWERS_MUTATION, params)):
            raise RuntimeError('Set reviewers mutation did not return expected results.')
        return get_nested_key(results, 'mergeRequestSetReviewers/mr/reviewers/nodes', [])
