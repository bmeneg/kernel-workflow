# pylint: disable=too-many-lines

"""Query MRs for upstream commit IDs to compare against submitted patches."""
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from datetime import timedelta
import enum
import os
import re
import subprocess
import sys
import typing

from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import misc
# GitPython
import git
from gitlab.const import MAINTAINER_ACCESS
from gitlab.exceptions import GitlabGetError

from webhook.rh_metadata import Projects

from . import cdlib
from . import common
from . import defs
from . import kgit
from .session import new_session

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.commit_compare')


def extract_ucid(mri, commit):
    """Extract upstream commit ID from the message."""
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    merge_title_prefixes = ["Merge: ", "Merge branch "]
    if any(commit.title.startswith(prefix) for prefix in merge_title_prefixes):
        commit_ref = mri.project.commits.get(commit.id)
        if len(commit_ref.parent_ids) > 1:
            return ["Merge"], ""
    message_lines = commit.message.split('\n')
    errors = ""
    gitref_list = []
    have_exact_match = False
    # Pattern for 'git show <commit>' (and git log) based backports
    gl_pattern = r'^\s*(?P<pfx>commit)[\s]+(?P<hash>[a-f0-9]{8,40})$'
    gitlog_re = re.compile(gl_pattern)
    # Pattern for 'git cherry-pick -x <commit>' based backports
    cp_pattern = r'^\s*\((?P<pfx>cherry picked from commit)[\s]+(?P<hash>[a-f0-9]{8,40})\)$'
    gitcherrypick_re = re.compile(cp_pattern)
    # Look for 'RHEL-only' patches
    rhelonly_re = re.compile(r'^Upstream[ -][Ss]tatus: RHEL[ -]*[0-9.Zz]*[ -][Oo]nly')
    # Look for 'Posted' patches, posted upstream but not in a git tree yet
    posted_re = re.compile('^Upstream[ -][Ss]tatus: Posted')
    # Look for 'Posted' patches that are under embargo
    embargo_re = re.compile('^Upstream[ -][Ss]tatus: Embargo')
    # Look for 'Posted' patches, which are in a git tree we don't track (yet?)
    upstream_status_re = re.compile('^Upstream[ -][Ss]tatus: ')
    tree_prefixes = ('git://anongit.freedesktop.org/',
                     'https://anongit.freedesktop.org/git/',
                     'https://git.kernel.org/pub/scm/',
                     'git://git.kernel.org/pub/scm/',
                     'git://git.infradead.org/',
                     'http://git.linux-nfs.org/',
                     'git://linux-nfs.org/',
                     'https://github.com/',
                     'https://git.samba.org/')
    linus_repos = ('https://github.com/torvalds/linux.git',
                   'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
                   'git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
                   'https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git')

    for line in message_lines:
        gitref = gitlog_re.match(line)
        if gitref:
            githash = gitref.group('hash')
            hash_prefix = gitref.group('pfx')
            if line == f"{hash_prefix} {githash}" and len(githash) == 40:
                if githash not in gitref_list:
                    gitref_list.append(githash)
                mri.ucids.add(githash)
                have_exact_match = True
            else:
                errors += f"\n* Incorrect format git commit line in {commit.short_id}.  \n"
            if have_exact_match and errors:
                errors = ""
                continue
            if githash not in gitref_list:
                gitref_list.append(githash)
            mri.ucids.add(githash)
            if line != f"{hash_prefix} {githash}":
                errors += f"`Expected:` `{hash_prefix} {githash}`  \n"
                errors += f"`Found   :` `{line}`  \n"
            if len(githash) < 40:
                errors += f"Git hash only has {len(githash)} characters, expected 40.  \n"
            continue
        gitref = gitcherrypick_re.match(line)
        if gitref:
            githash = gitref.group('hash')
            hash_prefix = gitref.group('pfx')
            if line == f"({hash_prefix} {githash})" and len(githash) == 40:
                if githash not in gitref_list:
                    gitref_list.append(githash)
                mri.ucids.add(githash)
                have_exact_match = True
            else:
                errors += f"\n* Incorrect format git cherry-pick line in {commit.short_id}.  \n"
            if have_exact_match and errors:
                errors = ""
                continue
            if githash not in gitref_list:
                gitref_list.append(githash)
            mri.ucids.add(githash)
            if line != f"({hash_prefix} {githash})":
                errors += f"`Expected:` `({hash_prefix} {githash})`  \n"
                errors += f"`Found   :` `{line}`  \n"
            if len(githash) < 40:
                errors += f"Git hash only has {len(githash)} characters, expected 40.  \n"
            continue
        if rhelonly_re.findall(line):
            gitref_list.append("RHELonly")
            continue
        if posted_re.findall(line):
            gitref_list.append("Posted")
            continue
        if embargo_re.findall(line):
            gitref_list.append("Posted")
            continue
        if upstream_status_re.findall(line):
            if any(prefix in line for prefix in tree_prefixes):
                LOGGER.debug("Found tree_prefix in: %s", line)
                if any(repo in line for repo in linus_repos):
                    LOGGER.warning("Patch contains Upstream Status: Linus ref w/o 40-char sha")
                    continue
                gitref_list.append("Posted")
    if "RHELonly" in gitref_list:
        LOGGER.debug("This commit is marked as RHELonly, ignore all other found commit refs")
        gitref_list = ["RHELonly"]
        errors = ""
    # We return empty arrays if no commit IDs are found
    LOGGER.debug("Found upstream refs: %s", gitref_list)
    if len(gitref_list) > 5:
        LOGGER.info("Excessive number of commits found: %d, call it RHEL-only", len(gitref_list))
        gitref_list = ["RHELonly"]
    if errors != "":
        LOGGER.warning("Upstream commit reference errors for submitted commit %s:\n%s",
                       commit.short_id, errors)
    return gitref_list, errors


def noucid_msg():
    """Return the message to use as a note for commits without an upstream commit ID."""
    msg = "No commit information found.  All commits are required to have a 'commit: "
    msg += "<upstream_commit_hash>' entry, or an 'Upstream Status: RHEL-only' entry "
    msg += "with an explanation why this change is not upstream. Alternatively, an "
    msg += "'Upstream Status: Posted' entry followed by a URL pointing to the posting "
    msg += "is also acceptable.  Please review this commit and appropriate metadata.  "
    msg += "Guidelines for these entries can be found in CommitRules,"
    msg += "https://red.ht/kwf_commit_rules."
    return msg


def posted_msg():
    """Return the message to use as note for commits that claim to be Posted."""
    msg = "This commit has Upstream Status as Posted, but we're not able to auto-compare it.  "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def merge_msg():
    """Return the message to use as note for commits that appear to be merge commits."""
    msg = "This commit looks like a merge commit, probably for dependencies in the series.  "
    msg += "Reviewers should evaluate these commits accordingly."
    return msg


def rhelonly_msg():
    """Return the message to use as note for commits that claim to be RHEL-only."""
    msg = "This commit has Upstream Status as RHEL-only and has no corresponding upstream commit.  "
    msg += "The author of this MR should verify if this commit has to be applied to "
    msg += "[future versions of RHEL](https://gitlab.com/cki-project/kernel-ark). "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def unk_cid_msg():
    """Return the message to use as note for commits that have an unknown commit ID."""
    msg = "No upstream source commit found.  This commit references an upstream commit ID, but "
    msg += "its source of origin is not recognized.  Please verify the upstream source tree."
    return msg


def diffs_msg():
    """Return the message to use as note for commits that differ from upstream."""
    msg = "This commit differs from the referenced upstream commit and should be evaluated "
    msg += "accordingly."
    return msg


def kabi_msg():
    """Return the message to use as note for commits that may contain kABI work-arounds."""
    msg = "This commit references a kABI work-around, and should be evaluated accordingly."
    return msg


def partial_msg():
    """Return the message to use as not for commits that match the part submitted."""
    msg = "This commit is a partial backport of the referenced upstream commit ID, and "
    msg += "the portions backported match upstream 100%."
    return msg


def config_msg():
    """Return the message to use for commits with non ARK reference."""
    msg = "A CONFIG change was detected and does not match ARK."
    return msg


class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4
    RHELONLY = 5
    POSTED = 6
    MERGECOMMIT = 7
    CONFIG = 8


def get_report_table(mri):
    """Create the table for the upstream commit ID report."""
    table = []
    for rhcommit in mri.rhcommits:
        table.append([rhcommit.commit.id, rhcommit.ucids, rhcommit.match, rhcommit.notes, ""])
    return table


def get_match_info(match):
    """Get info on match type."""
    mstr = "No UCID   "
    if match == Match.FULL:
        mstr = "100% match"
    elif match == Match.PARTIAL:
        mstr = "Partial   "
    elif match == Match.DIFFS:
        mstr = "Diffs     "
    elif match == Match.KABI:
        mstr = "kABI Diffs"
    elif match == Match.RHELONLY:
        mstr = "n/a       "
    elif match == Match.POSTED:
        mstr = "n/a       "
    elif match == Match.MERGECOMMIT:
        mstr = "Merge commit"
    elif match == Match.CONFIG:
        mstr = "Config    "
    return mstr


def has_upstream_commit_hash(entry):
    """Figure out if this patch has an upstream commit hash or not."""
    if entry == Match.NOUCID:
        return False
    if entry == Match.RHELONLY:
        return False
    if entry == Match.POSTED:
        return False
    if entry == Match.MERGECOMMIT:
        return False
    return True


def print_gitlab_report(mri, table):
    """Print an upstream commit ID mapping report for gitlab."""
    # pylint: disable=too-many-locals,too-many-branches
    kerneloscope = "http://kerneloscope.usersys.redhat.com"
    show_full_match_note = False
    evaluated = len(mri.rhcommits)
    pnum = 0
    report = f'<br>\n\n**Upstream Commit ID Readiness Report**: ~"CommitRefs::{mri.status}"\n\n'
    report += "This report indicates how backported commits compare to the upstream source " \
              "commit.  Matching (or not matching) is not a guarantee of correctness.  KABI, " \
              "missing or un-backportable dependencies, or existing RHEL differences against " \
              "upstream may lead to a difference in commits. As always, care should be taken " \
              "in the review to ensure code correctness.\n"
    table_header = "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n" \
                   "|:-------|:-------|:-------|:---------|:-------|\n"
    table_entries = ""
    for (rhcid, ucids, match, note, _) in reversed(table):
        pnum += 1
        if match == Match.FULL:
            show_full_match_note = True
            continue
        table_entries += f"|{str(pnum)}|{rhcid}|"
        if has_upstream_commit_hash(match):
            for ucid in ucids:
                if ucid in ('-', '(...)', 'Posted') and len(ucids) > 1:
                    continue
                table_entries += f"[{ucid[:8]}]({kerneloscope}/commit/{ucid})<br>"
        else:
            for ucid in ucids:
                table_entries += f"{ucid[:8]}<br>"
        table_entries += f"|{get_match_info(match)}|"
        table_entries += common.build_note_string(note)

    footnotes = common.print_notes(mri.notes)
    report += common.wrap_comment_table(table_header, table_entries, footnotes, "table")
    report += f"\n\nTotal number of commits analyzed: **{evaluated}**<br>"
    if mri.commit_count > evaluated:
        report += f"* Skipped dependency commits: **{mri.commit_count - evaluated}**<br>"
    if table_entries and show_full_match_note:
        report += "* Patches that match upstream 100% not shown in table<br>"
    if table_entries and mri.approved:
        report += "* Please note and evaluate differences from upstream in the table.  \n"
    if mri.approved:
        report += "\nMerge Request **passes** commit ID validation, references all present.  \n"
    else:
        report += "\nThis Merge Request contains commits that are missing upstream commit ID " \
                  "references. Please review the table. \n" \
                  " \n" \
                  "To request re-evalution after resolving any issues with the commits in the " \
                  "merge request, add a comment to this MR with only the text:  " \
                  "request-commit-id-evaluation \n"

    return report


def print_text_report(mri, table):
    """Print an upstream commit ID mapping report for the text console."""
    # pylint: disable=too-many-locals
    show_full_match_note = False
    evaluated = len(mri.rhcommits)
    pnum = 0
    report = "\n\nUpstream Commit ID Readiness Report\n\n"
    table_header = "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n"
    table_header += "|:-------|:-------|:-------|:---------|:-------|\n"
    table_entries = ""
    for (rhcid, ucids, match, note, logs) in reversed(table):
        pnum += 1
        if match == Match.FULL:
            show_full_match_note = True
            continue
        commits = []
        for ucid in ucids:
            if ucid not in ('-', 'Posted') or len(ucids) == 1:
                commits.append(ucid)
        commits = common.build_commits_for_row((rhcid, commits, match, note, logs))
        table_entries += f"|{str(pnum)}|{str(rhcid[:8])}"
        table_entries += f"|{' '.join(commits)}"
        table_entries += f"|{get_match_info(match)}|{', '.join(note)}|\n"
    if table_entries:
        report += table_header + table_entries
    else:
        report += "Patches all match upstream 100%\n\n"
    report += common.print_notes(mri.notes)
    report += f"Total number of commits analyzed: **{evaluated}**\n"
    if mri.commit_count > evaluated:
        report += f"* Skipped dependency commits: **{mri.commit_count - evaluated}**\n"
    if table_entries and mri.approved:
        report += "Please verify differences from upstream.\n"
    if table_entries and show_full_match_note:
        report += "* Patches that match upstream 100% not shown in table\n"
    status = "passes" if mri.approved and mri.status == defs.READY_SUFFIX else "fails"
    report += f"\nMerge Request {status} upstream commit ID validation.\n"
    return report


def get_upstream_diff(mri, ucid, filelist):
    """Extract diff for upstream commit ID, optionally limiting to filelist."""
    repo = git.Repo(mri.linux_src)

    try:
        commit = repo.commit(ucid)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.debug("Commit ID %s not found in upstream", ucid)
        return None, ""

    diff = repo.git.diff(f"{commit}^", commit)
    # author_name = commit.author.name
    author_email = commit.author.email

    if len(filelist) == 0:
        return diff, author_email

    LOGGER.debug("Getting partial diff for %s", ucid)
    part_diff = cdlib.get_partial_diff(diff, filelist)

    return part_diff, author_email


def find_kabi_hints(message, diff):
    """Check for hints this patch has kABI workarounds in it."""
    kabi_re = re.compile(r'kabi(?![a-zA-Z])', re.IGNORECASE)
    if kabi_re.findall(message):
        return True
    if kabi_re.findall(diff):
        return True
    for keyword in ("genksyms", "rh_reserved"):
        if keyword in message.lower():
            return True
        if keyword in diff.lower():
            return True
    return False


def x86_filename_replacement(filename):
    """Map and replace x86 configs to match ARK."""
    filename = filename.replace("x86/x86_64", "x86")
    arkfilename = filename.replace("common", "rhel")
    commonfilename = filename.replace("rhel", "common")

    return commonfilename, arkfilename


def read_config_content(workdir, filename):
    """Read the content of a config file if it exists."""
    file_content = os.path.join(workdir, filename)

    if not os.path.exists(file_content):
        return None

    with open(file_content, 'r', encoding='ascii') as cr_fp:
        return cr_fp.read().rstrip()


def fetch_file_content(ark_worktree, commonfilename, arkfilename):
    """Fetch the content of a config file inside ARK worktree."""
    common_output = None
    ark_output = None

    common_output = read_config_content(ark_worktree, commonfilename)

    if not common_output:
        ark_output = read_config_content(ark_worktree, arkfilename)

    return common_output, ark_output


def compare_config_diffs(commit, output, common_output, ark_output):
    """Check if the current repository has the same configs of ARK repo."""
    LOGGER.debug("Content in common '%s' and in ark '%s'. The expected "
                 "output is '%s'.", common_output, ark_output, output)

    if not (common_output or ark_output):
        # If there is no CONFIG in ARK, downstream should disable (None) either.
        if not output:
            LOGGER.debug("Matched config change to common directory in ARK")
            return ''

        msg = (f'A CONFIG change in RHEL commit {commit.id} (`{output}`) does not match '
               'ARK, no matching common or rhel config file found.')
        LOGGER.debug(msg)
        return msg

    if output:
        # Considering the CONFIG exists and was changed, we need to compare
        # with common or ark dirs.
        if output == common_output:
            LOGGER.debug("Matched config change to common directory in ARK")
            return ''

        if output == ark_output:
            LOGGER.debug("Matched config change to ark directory in ARK")
            return ''

    # Any other combination is considered a mismatch.

    msg = (f'A CONFIG change in RHEL commit {commit.id} (`{output}`) does not match ARK '
           f'configs for future RHEL releases (redhat/configs/common: `{common_output}`, '
           f'redhat/configs/rhel: `{ark_output}`).')
    LOGGER.debug(msg)
    return msg


def get_config_change(path):
    """Extract the specific config change from a diff."""
    config_setting = ''
    changed_file = None

    if path['deleted_file']:
        changed_file = path['old_path']

        config_setting = None
    elif path['renamed_file']:
        pass
    else:
        # File is new or it is a file change
        changed_file = path['new_path']

        config_setting = ''
        for line in path['diff'].split('\n'):
            if line.startswith('+') and 'CONFIG_' in line:
                config_setting += line[1:]

    if changed_file and not changed_file.startswith('redhat/configs/'):
        # Revert when it is not a CONFIG file.
        config_setting = ''
        changed_file = None

    return config_setting, changed_file


def evaluate_config(upstream_repository, commit):
    """Compare and evaluate all the config files in diff with upstream."""
    mapped_config_changes = {}
    config_mismatches = ''

    gl_diff = commit.diff()
    for path in gl_diff:
        config_setting, changed_file = get_config_change(path)

        if changed_file:
            mapped_config_changes[changed_file] = config_setting

    for filename, content in mapped_config_changes.items():
        commonfilename, arkfilename = x86_filename_replacement(filename)

        common_output, ark_output = fetch_file_content(upstream_repository,
                                                       commonfilename, arkfilename)

        mismatch = compare_config_diffs(commit, content, common_output, ark_output)
        if mismatch:
            config_mismatches += f'- {mismatch}  \n'

    LOGGER.debug(config_mismatches)

    return config_mismatches


def process_generic(mri, rhcommit, nid, match, msg_fn):
    """Process a generic commit."""
    mri.notes += [] if mri.nids[nid] else [msg_fn()]
    mri.nids[nid] = mri.nids[nid] if mri.nids[nid] else str(len(mri.notes))
    rhcommit.match = match
    rhcommit.notes.append(mri.nids[nid])


def process_no_ucid_commit(mri, rhcommit):
    """Process a commit w/no associated upstream commit ID."""
    process_generic(mri, rhcommit, 'noucid', Match.NOUCID, noucid_msg)


def process_rhel_only_commit(mri, rhcommit):
    """Process a commit listed as being RHEL-only."""
    process_generic(mri, rhcommit, 'rhelonly', Match.RHELONLY, rhelonly_msg)


def process_posted_commit(mri, rhcommit):
    """Process a commit listed as being Posted upstream."""
    process_generic(mri, rhcommit, 'posted', Match.POSTED, posted_msg)


def process_merge_commit(mri, rhcommit):
    """Process a commit that is a merge commit (probably for dependencies)."""
    process_generic(mri, rhcommit, 'merge', Match.MERGECOMMIT, merge_msg)


def process_unknown_commit(mri, rhcommit):
    """Process a commit with an unrecognized commit ID ref."""
    process_generic(mri, rhcommit, 'unknown', Match.NOUCID, unk_cid_msg)


def process_partial_backport(mri, rhcommit):
    """Process a commit that is a partial backport of an upstream commit."""
    process_generic(mri, rhcommit, 'part', Match.PARTIAL, partial_msg)


def process_kabi_patch(mri, rhcommit):
    """Process a commit that references kABI workarounds."""
    process_generic(mri, rhcommit, 'kabi', Match.KABI, kabi_msg)


def process_commit_with_diffs(mri, rhcommit):
    """Process a commit with differences from upstream."""
    process_generic(mri, rhcommit, 'diffs', Match.DIFFS, diffs_msg)


def process_config_commit(mri, rhcommit):
    """Process a commit that affect config changes."""
    process_generic(mri, rhcommit, 'config', Match.CONFIG, config_msg)


def no_upstream_commit_data(mri, rhcommit):
    """Handle a commit that has no upstream commit ID data."""
    if "-" in rhcommit.ucids:
        return process_no_ucid_commit(mri, rhcommit)

    if "RHELonly" in rhcommit.ucids:
        return process_rhel_only_commit(mri, rhcommit)

    if "Posted" in rhcommit.ucids:
        return process_posted_commit(mri, rhcommit)

    return None


def create_worktree_timestamp(worktree_dir):
    """Create a timestamp file in the worktree."""
    now = datetime.now()
    ts_file = f"{worktree_dir}/timestamp"
    with open(ts_file, 'w', encoding='ascii') as ts_fh:
        ts_fh.write(now.strftime('%Y%m%d%H%M%S'))
        ts_fh.close()


def clean_up_temp_ark_branch(kernel_src, ark_branch, worktree_dir):
    """Clean up the git worktree and branches from the upstream worktree."""
    kgit.worktree_remove(kernel_src, worktree_dir)
    kgit.branch_delete(kernel_src, ark_branch)
    LOGGER.debug("Removed worktree %s and deleted branch %s", worktree_dir, ark_branch)


def handle_stale_worktree(kernel_src, ark_branch, worktree_dir):
    """Ensure we're not stomping on another run, and clean it up if it's old."""
    LOGGER.warning("Worktree already exists at %s!", worktree_dir)
    ts_file = f"{worktree_dir}/timestamp"
    if not os.path.exists(ts_file):
        create_worktree_timestamp(worktree_dir)

    with open(ts_file, 'r', encoding='ascii') as ts_fh:
        old_ts = ts_fh.read()
        ts_fh.close()

    if old_ts < (datetime.now() - timedelta(hours=1)).strftime('%Y%m%d%H%M%S'):
        LOGGER.warning("Stale worktree at %s is over 1h old, purging it.", worktree_dir)
        clean_up_temp_ark_branch(kernel_src, ark_branch, worktree_dir)
        try:
            kgit.branch_delete(kernel_src, f"{ark_branch}-save")
        except subprocess.CalledProcessError:
            return
    else:
        raise messagequeue.QuietNackException("Worktree less than 1h old, trying back later.")


def prep_temp_ark_branch(kernel_src):
    """Set up the temporary branch/worktree to test upstream CONFIGs in it."""
    # kernel-ark remote is hard coded because this repo is the baseline.
    target_branch = "kernel-ark/os-build"
    ark_branch = "kernel-ark-config-check"
    worktree_dir = f"/{'/'.join(kernel_src.strip('/').split('/')[:-1])}/{ark_branch}/"

    # Ensure we don't have a stale/failed checkout lingering
    if os.path.isdir(worktree_dir):
        handle_stale_worktree(kernel_src, ark_branch, worktree_dir)

    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for upstream testing, please hold",
                worktree_dir, ark_branch)
    kgit.worktree_add(kernel_src, ark_branch, worktree_dir, target_branch)
    return ark_branch, worktree_dir


def validate_commit_ids(session, mri):
    """Iterate through the upstream commit IDs we found and compare w/our submitted commits."""
    # pylint: disable=too-many-branches,too-many-locals,too-many-statements
    commit_config_mismatches = ''

    # Kernel ARK is hard coded because it is our baseline
    kgit.worktree_fetch(mri.linux_src, "kernel-ark")
    ark_branch, worktree_dir = prep_temp_ark_branch(mri.linux_src)

    for rhcommit in mri.rhcommits:
        commit = rhcommit.commit
        no_ucid_invalid = True

        if "Merge" in rhcommit.ucids:
            process_merge_commit(mri, rhcommit)
            continue

        if any(ucid in ["-", "RHELonly", "Posted"] for ucid in rhcommit.ucids):
            no_upstream_commit_data(mri, rhcommit)
            mydiff, _ = cdlib.get_submitted_diff(commit.diff(per_page=100))
            if find_kabi_hints(commit.message, mydiff):
                process_kabi_patch(mri, rhcommit)
            no_ucid_invalid = False

        if cdlib.is_rhconfig_commit(rhcommit.commit) and mri.project.id != defs.ARK_PROJECT_ID:
            config_mismatches = evaluate_config(worktree_dir, rhcommit.commit)
            if config_mismatches:
                commit_config_mismatches += config_mismatches
                process_config_commit(mri, rhcommit)
                continue

        found_a_patch = False
        for ucid in rhcommit.ucids:
            if ucid in ("-", "RHELonly", "Posted"):
                continue
            udiff, _ = get_upstream_diff(mri, ucid, [])
            if udiff is None:
                if no_ucid_invalid and not found_a_patch:
                    process_unknown_commit(mri, rhcommit)
                continue
            found_a_patch = True
            mydiff, filelist = cdlib.get_submitted_diff(commit.diff(per_page=100))
            idiff = cdlib.compare_commits(udiff, mydiff)

            if len(idiff) == 0:
                rhcommit.match = Match.FULL
            else:
                diff = "```diff\n"
                diff += f"--- Upstream {ucid}\n"
                diff += f"+++ Backport {commit.id}\n"
                for entry in idiff[2:]:
                    diff += f"{entry}\n"
                diff += "```\n"
                LOGGER.debug("Patch diff:\n%s", diff)

                udiff_partial, _ = get_upstream_diff(mri, ucid, filelist)
                idiff = cdlib.compare_commits(udiff_partial, mydiff)
                if len(idiff) == 0:
                    process_partial_backport(mri, rhcommit)
                else:
                    process_commit_with_diffs(mri, rhcommit)
                    if find_kabi_hints(commit.message, mydiff):
                        process_kabi_patch(mri, rhcommit)

    if commit_config_mismatches:
        warning_msg = f'<br>\n\n**Upstream Config Differences**: ~"Config::ARK::{mri.status}"\n\n'
        warning_msg += f"@{mri.submitter_handle}, this warning does not imply that your CONFIG " \
            "change values are incorrect, but further review may be necessary. You " \
            "can resolve this thread if you feel your changes are correct for both " \
            "ARK and RHEL. If you see that you have changes to make (for example, " \
            "you may have to update your RHEL MR, or you may have to make further " \
            "changes in ARK) please make them and then resolve this thread.\n\n" \
            f"{commit_config_mismatches}\n"
        LOGGER.info(warning_msg)
        if misc.is_production_or_staging():
            session.update_webhook_comment(mri.merge_request, warning_msg,
                                           bot_name=mri.lab.user.username,
                                           identifier="Upstream Config Differences")

    clean_up_temp_ark_branch(mri.linux_src, ark_branch, worktree_dir)


def add_kabi_label(mri, table):
    """Add a kabi label if the MR containts a commit flagged as kabi-related."""
    for row in table:
        if row[2] == Match.KABI:
            cur_labels = common.add_label_to_merge_request(mri.project, mri.merge_request.iid,
                                                           ['KABI'])

            LOGGER.debug('Current MR labels: %s', cur_labels)
            return True
    return False


def add_config_mismatch_label(mri, table):
    """Add a config mismatch label if the MR containts a commit flagged as config change."""
    config_mismatch_label = 'Config::ARK::Mismatch'

    for row in table:
        if row[2] == Match.CONFIG:
            cur_labels = common.add_label_to_merge_request(mri.project, mri.merge_request.iid,
                                                           [config_mismatch_label])

            LOGGER.debug('Current MR labels: %s', cur_labels)
            return True

    if config_mismatch_label in mri.merge_request.labels:
        cur_labels = common.remove_labels_from_merge_request(mri.project,
                                                             mri.merge_request.iid,
                                                             [config_mismatch_label])

        LOGGER.debug('Current MR labels: %s', cur_labels)

    return False


def run_zstream_comparison_checks(mri):
    """Do some rudimentary validation of z-stream backports vs. their y-stream counterparts."""
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    target = str(mri.merge_request.target_branch)
    if target in ("main", "main-rt", "main-automotive", "os-build"):
        return ""

    projects = Projects()
    branch_data = projects.get_target_branch(mri.project.id, target)
    if not branch_data.zstream_target_release:
        return ""

    ycommit_pattern = r'^Y-Commit: (?P<hash>[a-f0-9]{8,40})$'
    ycommit_re = re.compile(ycommit_pattern, re.IGNORECASE)
    merge_title_prefixes = ["Merge: ", "Merge branch "]
    yrefs = {}
    zcompare_notes = ""

    (has_deps, dep_sha) = cdlib.get_dependencies_data(mri.merge_request)
    for commit in mri.merge_request.commits():
        # Exit the loop once we hit the first dependency commit
        if has_deps and cdlib.is_first_dep(commit, dep_sha):
            break

        if any(commit.title.startswith(prefix) for prefix in merge_title_prefixes):
            commit_ref = mri.project.commits.get(commit.id)
            if len(commit_ref.parent_ids) > 1:
                continue

        cmsg_lines = commit.message.split('\n')
        ycommit_found = False
        for line in cmsg_lines:
            gitref = ycommit_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                yrefs[githash] = commit
                ycommit_found = True
                break
        if not ycommit_found:
            LOGGER.debug("Z-Commit %s has no Y-Commit reference", commit.id)
            if mri.authlevel >= MAINTAINER_ACCESS:
                zcompare_notes += f"Z-Commit {commit.id} has no Y-Commit reference  \n"

    for ycid, zcid in yrefs.items():
        LOGGER.debug("Comparing Y-Commit: %s to Z-Commit: %s", ycid, zcid.id)
        zdiff, _ = cdlib.get_submitted_diff(zcid.diff(per_page=100))
        try:
            ycommit = mri.project.commits.get(ycid)
        except GitlabGetError:
            zcompare_notes += f"Y-Commit {ycid} listed for Z-Commit {zcid.id} is invalid  \n"
            continue
        ydiff, _ = cdlib.get_submitted_diff(ycommit.diff(per_page=100))
        idiff = cdlib.compare_commits(ydiff, zdiff, strict_header=True)
        if idiff:
            LOGGER.debug("Y-Commit %s and Z-Commit %s do not match", ycid, zcid.id)
            header = f"Y-Commit {ycommit.id} and Z-Commit {zcid.id} do not match:  \n"
            desc = "interdiff"
            diff = "```diff\n"
            diff += f"--- Y-Commit {ycommit.id}\n"
            diff += f"+++ Z-Commit {zcid.id}\n"
            for entry in idiff[2:]:
                diff += f"{entry}\n"
            diff += "```\n"
            zcompare_notes += common.wrap_comment_table(header, diff, "", desc)
        else:
            LOGGER.debug("Y-Commit %s and Z-Commit %s match perfectly", ycommit.id, zcid.id)
            zcompare_notes += f"Y-Commit {ycommit.id} and Z-Commit {zcid.id} match 100%  \n"

    return zcompare_notes


def check_on_ucids(session, mri):
    """Check upstream commit IDs."""
    error_notes = ""

    # do NOT run validation if the commit count is over 2000
    if mri.commit_count > defs.MAX_COMMITS_PER_MR and mri.authlevel < MAINTAINER_ACCESS:
        error_notes = "**Upstream Commit ID Readiness Error(s)**  \n"
        error_notes += "*ERROR*: This Merge Request has too many commits for the commit reference "
        error_notes += f"webhook to process ({mri.commit_count}) -- please make sure you have "
        error_notes += "targeted the correct branch."
        return [], error_notes

    dep_label = cdlib.set_dependencies_label(mri.project, mri.merge_request)
    # re-fetch MR to get updated Dependencies label, if needed
    if dep_label not in mri.merge_request.labels:
        mri.merge_request = mri.project.mergerequests.get(mri.merge_request.iid)
    (has_deps, dep_sha) = cdlib.get_dependencies_data(mri.merge_request)
    for commit in mri.merge_request.commits():
        # Exit the loop once we hit the first dependency commit
        if has_deps and cdlib.is_first_dep(commit, dep_sha):
            break
        mri.rhcommits.append(RHCommit(commit))

    for rhcommit in mri.rhcommits:
        try:
            found_refs, errors = extract_ucid(mri, rhcommit.commit)
        # pylint: disable=broad-except
        except Exception:
            found_refs = []
            errors = ""

        error_notes += errors

        if not found_refs and cdlib.is_rhdocs_commit(rhcommit.commit):
            found_refs = ["RHELonly"]

        rhcommit.ucids = found_refs

    LOGGER.debug('List of %d ucids: %s', len(mri.ucids), mri.ucids)
    validate_commit_ids(session, mri)
    table = get_report_table(mri)
    desc = "error details"
    error_notes = common.wrap_comment_table("", error_notes, "", desc)
    return table, error_notes


@dataclass
class RHCommit:
    """Per-MR-commit data class for storing comparison data."""

    commit: set
    ucids: set = field(default_factory=set, init=False)
    match: int = Match.NOUCID
    notes: list = field(default_factory=list)


# pylint: disable=too-many-instance-attributes,too-few-public-methods
class MRUCIDInstance:
    """Merge request Instance."""

    def __init__(self, lab, project, merge_request):
        """Initialize the commit ID comparison instance."""
        # pylint: disable=too-many-arguments
        self.linux_src = "/usr/src/linux"
        self.lab = lab
        self.approved = True
        self.status = defs.READY_SUFFIX
        self.project = project
        self.merge_request = merge_request
        self.commit_count = 0
        self.authlevel = 0
        self.submitter_handle = ""
        self.rhcommits = []
        self.notes = []
        self.nids = {'noucid': 0, 'rhelonly': 0, 'posted': 0, 'merge': 0, 'unknown': 0,
                     'diffs': 0, 'kabi': 0, 'part': 0, 'badmail': 0, 'config': 0}
        self.ucids = set()
        self.omitted = set()

    def check_commit_ref_matches(self):
        """Look at the match status of all commits to determine CommitRefs label scope."""
        for rhcommit in self.rhcommits:
            self.approved = self.approved and rhcommit.match != Match.NOUCID

    def report_results(self, session, table, errors, zcompare_notes):
        """Report the results of our examination."""
        # pylint: disable=too-many-branches
        if misc.is_production_or_staging():
            report = print_gitlab_report(self, table)
            report += errors + zcompare_notes
            session.update_webhook_comment(self.merge_request, report,
                                           bot_name=self.lab.user.username,
                                           identifier="Upstream Commit ID Readiness")
        else:
            LOGGER.info('Skipping adding report in non-production')
            report = print_text_report(self, table)
            report += errors + zcompare_notes
            LOGGER.info(report)
        if not self.approved:
            LOGGER.info("Some commits in MR %s require further manual inspection!",
                        self.merge_request.iid)

    def run_ucid_validation(self, session):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        LOGGER.info("Running upstream commit ID validation on MR %s", self.merge_request.iid)
        LOGGER.debug("Merge request description:\n%s", self.merge_request.description)
        count, authlevel = common.get_commits_count(self.project, self.merge_request)
        if not count:
            self.status = defs.NEEDS_REVIEW_SUFFIX
            errors = "**Upstream Commit ID Readiness Error(s)**  \n"
            errors += "*ERROR*: This Merge Request appears to have no commits for the commit "
            errors += "reference webhook to process."
            session.update_webhook_comment(self.merge_request, errors,
                                           bot_name=self.lab.user.username,
                                           identifier="Upstream Commit ID Readiness")
            return

        self.commit_count = count
        self.authlevel = authlevel
        (table, errors) = check_on_ucids(session, self)
        self.check_commit_ref_matches()
        # append quick label "Commitrefs" with scope "OK", "Missing" or "NeedsReview"
        if errors != "":
            self.status = defs.NEEDS_REVIEW_SUFFIX
        if not self.approved:
            self.status = defs.MISSING_SUFFIX
        common.add_label_to_merge_request(self.project, self.merge_request.iid,
                                          [f'CommitRefs::{self.status}'])

        if self.commit_count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
            session.update_webhook_comment(self.merge_request, errors,
                                           bot_name=self.lab.user.username,
                                           identifier="Upstream Commit ID Readiness")
            return

        zcompare_notes = run_zstream_comparison_checks(self)
        if zcompare_notes:
            if zcompare_notes.count('\n') > defs.TABLE_ENTRY_THRESHOLD:
                collapse_header = "<details><summary>Click to show/hide comparison</summary>\n\n"
                collapse_footer = "</details>\n\n"
                zcompare_notes = "\n\nZ-stream comparison report:  \n---\n" + collapse_header \
                                 + zcompare_notes + collapse_footer
            else:
                zcompare_notes = "\n\nZ-stream comparison report:  \n---\n" + zcompare_notes

        # append quick label "KABI" if we flagged any commit as possibly impacting kabi
        if add_kabi_label(self, table):
            LOGGER.debug("This MR (%s) impacts kABI", self.merge_request.iid)

        # append quick label "Config::ARK::Mismatch" if a CONFIG change inside the
        # commit deviates from ARK project
        if add_config_mismatch_label(self, table):
            LOGGER.debug("This MR (%s) impacts upstream CONFIG changes", self.merge_request.iid)

        self.report_results(session, table, errors, zcompare_notes)


def perform_mri_tasks(session, mri, linux_src):
    """Perform tasks for a mri."""
    mri.submitter_handle = mri.merge_request.author['username']
    mri.linux_src = linux_src
    mri.run_ucid_validation(session)


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    if not (gl_mr := event.gl_mr):
        return
    mri = MRUCIDInstance(session.gl_instance, session.get_gl_project(event.namespace), gl_mr)
    perform_mri_tasks(session, mri, session.args.linux_src)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('COMMIT_COMPARE')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    session = new_session('commit_compare', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
