"""Add kernel subsystem topics as labels to an MR."""
from dataclasses import dataclass
import os
import re
import sys
import typing

from cki_lib import logger
from cki_lib import owners

from . import common
from .base_mr import BaseMR
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import DependsMixin
from .base_mr_mixins import DiffsMixin
from .base_mr_mixins import OwnersMixin
from .defs import CODE_CHANGED_PREFIX
from .defs import GitlabObjectKind
from .defs import Label
from .defs import MrScope
from .defs import NOTIFICATION_HEADER
from .defs import NOTIFICATION_TEMPLATE
from .session import new_session

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.subsystems')

EXT_CI_LABEL_PREFIX = 'ExternalCI'  # without colons
SUBSYS_LABEL_PREFIX = 'Subsystem:'  # with its single colon


@dataclass(repr=False)
class SubsysMR(DiffsMixin, OwnersMixin, CommitsMixin, DependsMixin, BaseMR):
    """An MR object with owners."""

    @property
    def current_ss_names(self):
        """Return the set of Subsystem: names currently on the MR."""
        return {lbl.removeprefix(SUBSYS_LABEL_PREFIX) for lbl in self.labels if
                lbl.startswith(SUBSYS_LABEL_PREFIX) and lbl.count(':') == 1}

    @property
    def expected_ss_names(self):
        """Return the expected set of Subsystem: names for the MR."""
        return {s.subsystem_label for s in self.owners_subsystems if s.subsystem_label}

    @property
    def stale_ss_names(self):
        """Return the set of current_subsystem_names that are not in the expected set."""
        return self.current_ss_names - self.expected_ss_names

    @property
    def expected_ss_labels(self):
        """Return the list of expected Subsystem: labels for this MR."""
        return [Label(f'{SUBSYS_LABEL_PREFIX}{name}') for name in self.expected_ss_names]

    @property
    def current_ci_labels(self):
        """Return the set of ExternalCI labels currently on the MR."""
        # This excludes the overall single-scoped ExternalCI label.
        return {label for label in self.labels_with_prefix(EXT_CI_LABEL_PREFIX) if label.scoped > 1}

    @property
    def current_ci_names(self):
        """Return the set of ExternalCI names currently on the MR."""
        return {label.primary for label in self.current_ci_labels}

    @property
    def expected_ci_names(self):
        """Return the expected set of ExternalCI names for the MR."""
        if self.draft:
            return set()
        return {name for s in self.owners_subsystems for name in s.ready_for_merge_label_deps}

    @property
    def missing_ci_labels(self):
        """Return the set of expected ExternalCI labels which are not currently found on the MR."""
        return {MrScope.NEEDS_TESTING.label(f'{EXT_CI_LABEL_PREFIX}::{name}') for
                name in self.expected_ci_names - self.current_ci_names}

    @property
    def current_ci_scope(self):
        """Return the MrScope of the existing ExternalCI label, or None."""
        return next((label.scope for label in self.labels_with_prefix(EXT_CI_LABEL_PREFIX) if
                     label.scoped == 1), None)

    @property
    def expected_ci_scope(self):
        """Return the MrScope for the ExternalCI label."""
        scope = min((label.scope for label in self.expected_ci_labels), default=MrScope.OK)
        # If any of the expected ci labels are missing then we can't be better than NeedsTesting.
        if self.missing_ci_labels and scope > MrScope.NEEDS_TESTING:
            return MrScope.NEEDS_TESTING
        # Don't ever set a Waived overall scope.
        return scope if scope is not MrScope.WAIVED else MrScope.OK

    @property
    def expected_ci_labels(self):
        """Return the set of expected ExternalCI:: labels for this MR. For a Draft this is set()."""
        if self.draft:
            return []
        # If the code changed then 'reset' all ci labels to NEEDS_TESTING.
        # Only consider the second value in the returned tuple!
        new_rev_label = f'{CODE_CHANGED_PREFIX}v{len(self.history)}'
        new_rev_label = new_rev_label if new_rev_label not in self.gl_mr.labels else None
        diff = self.code_changes()
        if new_rev_label and diff:
            return {MrScope.NEEDS_TESTING.label(f'{EXT_CI_LABEL_PREFIX}::{name}') for
                    name in self.expected_ci_names | self.current_ci_names}
        return self.current_ci_labels | self.missing_ci_labels


def path_matches(search_key, map_path_list, regex=False):
    # pylint: disable=too-many-return-statements
    """Return True if the key matches any of the path_list items."""
    if not map_path_list:
        return False
    # N: Files and directories *Regex* patterns.
    #  N:   [^a-z]tegra     all files whose path contains tegra
    #                       (not including files like integrator)
    if regex:
        for pattern in map_path_list:
            if re.search(pattern, search_key):
                return True
        return False

    # F: Files and directories with wildcard patterns.
    #  A trailing slash includes all files and subdirectory files.
    #  F:	drivers/net/	all files in and below drivers/net
    #  F:	drivers/net/*	all files in drivers/net, but not below
    #  F:	*/net/*		all files in "any top level directory"/net
    for map_path in map_path_list:
        if owners.glob_match(search_key, map_path):
            return True
    return False


def user_wants_notification(user_data, path_list, target_branch):
    """Determine if the given user wants notification about any of the given files."""
    for path in path_list:
        if 'all' in user_data and path_matches(path, user_data['all']):
            return True
        if target_branch in user_data and path_matches(path, user_data[target_branch]):
            return True
    return False


def do_usermapping(target_branch, path_list, repo_path):
    """For the given branch and list of paths return a list of users to be notified."""
    # Every file in the repo 'users' directory should be the name of a GL user.
    users_path = os.path.join(repo_path, 'users')
    try:
        path_listing = os.listdir(users_path)
    except OSError:
        LOGGER.exception("Problem listing path: '%s'", users_path)
        return []

    user_list = []
    for username in path_listing:
        user_path = os.path.join(users_path, username)
        user_data = common.load_yaml_data(user_path)
        if not user_data:
            LOGGER.error("Error loading user data from path '%s'.", user_path)
            continue
        if user_wants_notification(user_data, path_list, target_branch):
            user_list.append(username)
    return user_list


def post_notifications(session, gl_instance, merge_request, user_list):
    """Post a note to the MR notifying the users in user_list."""
    participants = []
    for participant in merge_request.participants():
        participants.append(participant['username'])

    new_users = ['@' + user for user in user_list if user not in participants]
    if not new_users:
        LOGGER.info('No one new to notify.')
        return

    note_text = NOTIFICATION_TEMPLATE.format(header=NOTIFICATION_HEADER,
                                             users=' '.join(new_users),
                                             project=os.environ['KERNEL_WATCH_URL'])
    LOGGER.info('Posting notification on MR %d:\n%s', merge_request.iid, note_text)
    session.update_webhook_comment(merge_request, note_text,
                                   bot_name=gl_instance.user.username,
                                   identifier=NOTIFICATION_HEADER)


def update_mr(subsys_mr):
    """Process the SubsysMR."""
    LOGGER.info('Processing %s', subsys_mr.url)
    LOGGER.debug('All files affected: %s', subsys_mr.all_files)
    LOGGER.debug('Matching owners subsystems: %s', subsys_mr.owners_subsystems)
    LOGGER.info('Matching subsystems: %s', subsys_mr.expected_ss_names)

    mr_is_draft = subsys_mr.draft

    if stale_subsystems := subsys_mr.stale_ss_names:
        stale_ss_labels = [f'{SUBSYS_LABEL_PREFIX}{name}' for name in stale_subsystems]
        LOGGER.info('Removing stale subsystems labels: %s', stale_ss_labels)
        subsys_mr.remove_labels(stale_ss_labels)

    ss_labels = subsys_mr.expected_ss_labels
    ci_labels = list(subsys_mr.expected_ci_labels)  # This will always be empty for Draft MRs.
    LOGGER.info('Calculated subsystem labels: %s', ss_labels)
    if mr_is_draft:
        LOGGER.info('MR is in Draft state, not setting ExternalCI labels.')
    else:
        LOGGER.info('Calculated ExternalCI labels: %s', ci_labels)
        # Include the overall ExternalCI status label.
        ci_labels.append(subsys_mr.expected_ci_scope.label(EXT_CI_LABEL_PREFIX))
        LOGGER.info('Calculated overall ExternalCI label: %s', ci_labels[-1])
    if new_labels := set(ss_labels + ci_labels) - set(subsys_mr.labels):
        LOGGER.info('Labels to update: %s', new_labels)
        subsys_mr.add_labels(new_labels, remove_scoped=False)


def notify_users(session, gl_instance, gl_mr, path_list, target_branch, local_repo_path):
    # pylint: disable=too-many-arguments
    """Notify users via kernel-watch, if any."""
    if user_list := do_usermapping(target_branch, path_list, local_repo_path):
        post_notifications(session, gl_instance, gl_mr, user_list)
    else:
        LOGGER.info('No new kernel-watch users to notify.')


def get_subsys_mr(graphql, gl_instance, projects, mr_url, args):
    """Return a new SubsysMR."""
    params = {'graphql': graphql,
              'gl_instance': gl_instance,
              'projects': projects,
              'url': mr_url}
    if owners_path := args.owners_yaml:
        params['owners_path'] = owners_path
    if kernel_src := args.linus_src:
        params['kernel_src'] = kernel_src
    return SubsysMR(**params)


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Handle a GL event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    # Get the MR data and set all the Subsystem: and ExternalCI labels.
    subsys_mr = get_subsys_mr(session.graphql, session.gl_instance, session.rh_projects,
                              event.mr_url, session.args)
    update_mr(subsys_mr)

    # Do kernel-watch user notifications.
    target_branch = subsys_mr.target_branch if subsys_mr.target_branch != 'main' else \
        subsys_mr.project.name
    notify_users(session, session.gl_instance, subsys_mr.gl_mr, subsys_mr.all_files, target_branch,
                 session.args.local_repo_path)


HANDLERS = {
    GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SUBSYSTEMS')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--local-repo-path', **common.get_argparse_environ_opts('LOCAL_REPO_PATH'),
                        help='Local path where the kernel-watch repo is checked out')
    parser.add_argument('--linus-src', **common.get_argparse_environ_opts('LINUS_SRC'),
                        help='Directory where upstream will be checked out')
    args = parser.parse_args(args)

    LOGGER.info('Using owners file: %s, local repo path: %s, kernel-watch repo: %s.',
                args.owners_yaml, args.local_repo_path, os.environ['KERNEL_WATCH_URL'])

    session = new_session('subsystems', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
