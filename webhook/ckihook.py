"""Manage the CKI labels."""
from dataclasses import dataclass
import fnmatch
from os import environ
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_variables
from jinja2 import Environment
from jinja2 import FileSystemLoader
import prometheus_client as prometheus

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import PipelinesMixin
from webhook.defs import GitlabObjectKind
from webhook.defs import GitlabURL
from webhook.defs import Label
from webhook.defs import MessageType
from webhook.defs import MrScope
from webhook.defs import MrState
from webhook.pipelines import CKI_LABEL_PREFIXES
from webhook.pipelines import PipelineResult
from webhook.pipelines import PipelineStatus
from webhook.pipelines import PipelineType
from webhook.session import new_session

if typing.TYPE_CHECKING:
    from .graphql import GitlabGraph
    from .rh_metadata import Branch
    from .session import SessionRunner
    from .session_events import DataWarehouseEvent
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent
    from .session_events import GitlabPipelineEvent
    from .session_events import GitlabPushEvent

LOGGER = logger.get_logger('cki.webhook.ckihook')


METRIC_KWF_CKIHOOK_PIPELINES_RETRIED = prometheus.Counter(
    'kwf_ckihook_pipelines_retried',
    'Number of CKI pipelines retried by ckihook',
    ['bridge_name']
)

METRIC_KWF_CKIHOOK_RESULTS_RETRIED = prometheus.Counter(
    'kwf_ckihook_results_retried',
    'Number of CKI "check-kernel-results" retried by ckihook',
    ['project_name', 'reason']
)

REPORT_HEADER = '**CKI Pipelines Status:**'
INDENT = '    '


@dataclass(repr=False)
class PipeMR(PipelinesMixin, BaseMR):
    """An MR class for pipelines."""


OPEN_MRS_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mrs: mergeRequests(
      after: $after
      state: opened
      labels: ["CKI_RT::Warning"]
      targetBranches: $branches
    ) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        headPipeline {
          jobs {
            nodes {
              allowFailure
              id
              name
              createdAt
              pipeline {
                id
              }
              status
              downstreamPipeline {
                id
                project {
                  id
                }
                status
                stages {
                  nodes {
                    name
                    jobs {
                      nodes {
                        id
                        name
                        status
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
"""

RETRY_JOB_QUERY = """
mutation retryJob($job_gid: CiProcessableID!) {
  jobRetry(input: {id: $job_gid}) {
    job {
      id
    }
  }
}
"""

FIND_JOB_BY_NAME_QUERY = """
query jobByName($namespace: ID!, $pipeline_iid: ID!, $job_name: String!) {
  project(fullPath: $namespace) {
    pipeline(iid: $pipeline_iid) {
      job(name: $job_name) {
        id
        status
        retryable
      }
    }
  }
}
"""


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return any(common.has_label_prefix_changed(changes, f'{prefix}::') for
               prefix in list(CKI_LABEL_PREFIXES.values()) + ['CKI'])


def retry_pipeline(graphql, mr_id, pipeline):
    """Get a gitlab instance and retry the failed pipelines."""
    LOGGER.info('Retrying downstream pipeline %s for MR %s', pipeline, mr_id)
    if misc.is_production_or_staging():
        query_params = {'job_gid': pipeline.bridge_gid}
        graphql.client.query(RETRY_JOB_QUERY, query_params)
        METRIC_KWF_CKIHOOK_PIPELINES_RETRIED.labels(pipeline.bridge_name).inc()


def retry_check_kernel_results(session: 'SessionRunner', pipeline_url: str, reason: str) -> None:
    """Retry the failed check-kernel-results job to recompute the testing report."""
    job_name = "check-kernel-results"

    # Fetch Pipeline to handle its iid and status
    gl_url = GitlabURL(pipeline_url)
    gl_pipeline = (
        session.gl_instance.projects.get(gl_url.namespace, lazy=True).pipelines.get(gl_url.id)
    )

    # If the pipeline didn't finish, the job certainly didn't run, therefore can't retry
    if gl_pipeline.status not in ['failed', 'success']:
        LOGGER.info("Skipping %r retry on pipeline %d because it didn't finish (status=%r)",
                    job_name, gl_pipeline.id, gl_pipeline.status)
        return

    # Query job by name within the pipeline
    query_params = {
        'namespace': gl_url.namespace, 'pipeline_iid': gl_pipeline.iid, 'job_name': job_name,
    }
    query_result = session.graphql.client.query(FIND_JOB_BY_NAME_QUERY, query_params)
    if not (job_data := misc.get_nested_key(query_result, 'project/pipeline/job', None)):
        LOGGER.info("Skipping %r retry on pipeline %d because there's no such job",
                    job_name, gl_pipeline.id)
        return

    # Can't retry if the job didn't finish
    if (job_status := job_data['status']) not in ['FAILED', 'SUCCESS']:
        LOGGER.info("Skipping %r retry on pipeline %d because the job didn't finish (status=%r)",
                    job_name, gl_pipeline.id, job_status)
        return

    # Simply can't retry
    if not job_data['retryable']:
        LOGGER.info("Skipping %r retry on pipeline %d because the job was not retryable",
                    job_name, gl_pipeline.id)
        return

    # Send request to retry
    if misc.is_production_or_staging():
        query_params = {'job_gid': job_data['id']}
        session.graphql.client.query(RETRY_JOB_QUERY, query_params)

        METRIC_KWF_CKIHOOK_RESULTS_RETRIED.labels(
            project_name=gl_url.namespace, reason=reason).inc()
        LOGGER.info('Retrying %r on pipeline %d', job_name, gl_pipeline.id)
    else:
        LOGGER.info("Skipping %r retry on pipeline %d because we're not in production/staging",
                    job_name, gl_pipeline.id)


def failed_rt_mrs(graphql, namespace, branches):
    """Return a dict of namespace MRs open on the branches that failed CKI_RT with a warning."""
    query_params = {'namespace': namespace, 'branches': branches}
    result = graphql.client.query(OPEN_MRS_QUERY, query_params, paged_key='project/mrs')
    return {
        mr['iid']: misc.get_nested_key(mr, 'headPipeline/jobs/nodes')
        for mr in misc.get_nested_key(result, 'project/mrs/nodes', [])
    }


def retrigger_failed_pipelines(graphql: 'GitlabGraph', branch: 'Branch') -> None:
    """Retrigger any RT pipelines that failed on or before the MERGE stage."""
    if PipelineType.REALTIME not in branch.pipelines:
        LOGGER.info('This branch does not expect a realtime pipeline: %s', branch.pipelines)
        return
    branch_names = [branch.name, branch.name.removesuffix('-rt')]
    namespace = branch.project.namespace
    if not (mrs := failed_rt_mrs(graphql, namespace, branch_names)):
        LOGGER.info('No open MRs for %s on branches %s, nothing to do.', namespace, branch_names)
        return
    for mr_id, raw_bridge_jobs in mrs.items():
        pipelines = PipelineResult.prepare_pipelines(raw_bridge_jobs)
        if pipe := next((p for p in pipelines
                         if (stage := p.failed_stage) and (stage.name == 'merge')), None):
            retry_pipeline(graphql, mr_id, pipe)


def get_project_pipeline_var(gl_project, pipeline_id, key):
    """Return the value of the pipeline variable matching key, or None."""
    gl_pipeline = gl_project.pipelines.get(pipeline_id)
    return get_variables(gl_pipeline).get(key)


def get_pipeline_target_branch(gl_instance, project_id, pipeline_id):
    """Return the 'branch' pipeline variable value for the given project/pipeline."""
    gl_project = gl_instance.projects.get(project_id)
    return get_project_pipeline_var(gl_project, pipeline_id, 'branch')


def get_downstream_pipeline_branch(gl_instance, pipe_result):
    """Return the 'branch' var value if found, otherwise None."""
    ds_project_id = pipe_result.ds_project_id
    ds_pipeline_id = pipe_result.ds_pipeline_id

    if not (ds_branch := get_pipeline_target_branch(gl_instance, ds_project_id, ds_pipeline_id)):
        LOGGER.debug("Could not get 'branch' variable from downstream pipeline %s", ds_pipeline_id)
    return ds_branch


def branch_changed(payload):
    """Return True if it seems like the MR branch changed, otherwise False."""
    LOGGER.info('Checking if MR target branch matches latest pipeline target branch.')
    # If the MR doesn't have a head pipeline then we're done here.
    if not payload['object_attributes']['head_pipeline_id']:
        LOGGER.info('No head pipeline, skipping target branch check.')
        return False
    # This is the signature of an MR event when the target branch changes. Maybe?
    if 'merge_status' in payload['changes']:
        return True
    LOGGER.info("'changes' dict does not have 'merge_status' key, skipping target branch check.")
    return False


def process_possible_branch_change(session, pipe_mr, payload):
    """Confirm MR branch changed and if so, Return True and cancel/trigger pipelines."""
    if not pipe_mr.pipelines:
        LOGGER.info('MR has no pipelines, nothing to do.')
        return False
    ds_branch = get_downstream_pipeline_branch(pipe_mr.gl_instance, pipe_mr.pipelines[0])
    mr_branch = payload['object_attributes']['target_branch']
    if ds_branch and ds_branch != mr_branch:
        LOGGER.info('Target branch changed: MR %s != downstream pipeline %s, triggering...',
                    mr_branch, ds_branch)
        if not misc.is_production_or_staging():
            LOGGER.info('Not production, skipping work.')
            return True
        head_pipeline_id = pipe_mr.head_pipeline_id
        gl_mr = pipe_mr.gl_mr
        common.cancel_pipeline(pipe_mr.gl_project, head_pipeline_id)
        new_pipeline_id = common.create_mr_pipeline(gl_mr)
        new_pipeline_url = f'{pipe_mr.gl_project.web_url}/-/pipelines/{new_pipeline_id}'
        note_text = ("This MR's current target branch has changed since the last pipeline"
                     f" was triggered. The last pipeline {head_pipeline_id} has been canceled"
                     f" and a new pipeline has been triggered: {new_pipeline_url}  \n"
                     f"Last pipeline MR target branch: {ds_branch}  \n"
                     f"Current MR target branch: {mr_branch}  ")
        session.update_webhook_comment(gl_mr, note_text)
        return True
    LOGGER.info("MR current target branch '%s' matches latest pipeline.", mr_branch)
    return False


def pipeline_is_waived(pipeline, mr_labels):
    """Return True if the pipeline is currently waived, otherwise False."""
    # Only possible to waive RT and Automotive pipelines.
    waiveable = (PipelineType.REALTIME, PipelineType.AUTOMOTIVE)
    # You can't waive a pipeline that isn't failed.
    return pipeline.type in waiveable and f'{pipeline.type.prefix}::Waived' in mr_labels and \
        pipeline.status is PipelineStatus.FAILED


def context_status_dict(pipelines, mr_labels):
    """Return a dict of failed, canceled, running, success, and waived PipelineResults."""
    failed = [pipe for pipe in pipelines if not pipeline_is_waived(pipe, mr_labels) and
              pipe.status is PipelineStatus.FAILED]
    canceled = [pipe for pipe in pipelines if not pipeline_is_waived(pipe, mr_labels) and
                pipe.status is PipelineStatus.CANCELED]
    running = [pipe for pipe in pipelines if pipe.status is PipelineStatus.RUNNING]
    success = [pipe for pipe in pipelines if pipe.status is PipelineStatus.SUCCESS]
    waived = [pipe for pipe in pipelines if pipeline_is_waived(pipe, mr_labels)]
    return {'failed': failed, 'canceled': canceled, 'running': running, 'success': success,
            'waived': waived}


def context_dict(pipe_mr, pipelines, header_str, invalid_branch, protected_branches):
    """Return a dict with PipelineResult data ready for a jinja template."""
    context = {
        'header_str': header_str,
        'invalid_branch': invalid_branch,
        'protected_branches': protected_branches,
        'blocking': {},
        'nonblocking': {},
        'missing': [],
        'dw_url': environ.get('DATAWAREHOUSE_URL'),
        'is_draft': pipe_mr.draft,
    }

    blocking = [pipe for pipe in pipelines.values() if pipe and not pipe.allow_failure]
    nonblocking = [pipe for pipe in pipelines.values() if pipe and pipe.allow_failure]

    context['blocking'] = context_status_dict(blocking, pipe_mr.labels)
    context['nonblocking'] = context_status_dict(nonblocking, pipe_mr.labels)
    context['missing'] = [ptype for ptype, pipeline in pipelines.items() if pipeline is None]
    return context


def generate_comment(pipe_mr, pipelines, cki_status_label, invalid_branch, protected_branches):
    """Generate a markdown string for the given MR & pipelines."""
    jinja_env = Environment(loader=FileSystemLoader("templates/"),
                            trim_blocks=True, lstrip_blocks=True)
    template = jinja_env.get_template('ckihook.j2')

    header_str = f'{REPORT_HEADER} ~"{cki_status_label}"'
    context = context_dict(pipe_mr, pipelines, header_str, invalid_branch, protected_branches)

    comment_string = template.render(context)
    return '\n'.join([f'{line}  ' for line in comment_string.splitlines()])


def generate_pipeline_labels(mr_cki_labels: typing.List[Label], pipelines) -> typing.Set[Label]:
    """Return the set of CKI labels derived from the given dict of PipelineResults."""
    cki_labels = set()
    for pipeline_type, pipeline_result in pipelines.items():
        if pipeline_result is None:
            LOGGER.info('Missing result for %s', repr(pipeline_type))
            cki_labels.add(Label(f'{pipeline_type.prefix}::Missing'))
            continue
        if not pipeline_result.label:
            LOGGER.info('%s has no label, ignoring result.', pipeline_result)
            continue
        if pipeline_is_waived(pipeline_result, mr_cki_labels):
            LOGGER.info('MR has Waived label for %s, ignoring result.', pipeline_result)
            # Include the waived label in the list we return since this is the list of labels
            # we expect to be present on the MR.
            cki_labels.add(Label(f'{pipeline_result.label.prefix}::Waived'))
            continue
        cki_labels.add(pipeline_result.label)
    LOGGER.info('Calculated CKI labels: %s', cki_labels)
    return cki_labels


def generate_status_label(pipe_mr, pipelines, invalid_branch) -> Label:
    """Return a Label string representing the overall status of all required pipelines."""
    default_label = PipelineStatus.INVALID if invalid_branch else PipelineStatus.MISSING
    # Get the lowest PipelineStatus of expected results which are not allowed to fail & not waived.
    lowest_status = min((pipe.status for pipe in pipelines.values() if
                         pipe is not None and
                         not pipe.allow_failure and
                         not pipeline_is_waived(pipe, pipe_mr.labels)),
                        default=default_label)
    return Label(f'CKI::{lowest_status.title}')


def update_mr(session, pipe_mr, pipelines, invalid_branch, protected_branches):
    """Update the given MR with a comment and set CKI labels."""
    current_mr_cki_labels = set(session.webhook.webhooks_labels(pipe_mr.labels))
    new_cki_status_label = generate_status_label(pipe_mr, pipelines, invalid_branch)
    new_cki_pipeline_labels = generate_pipeline_labels(current_mr_cki_labels, pipelines)
    # Only include an individual pipeline's label if its status is not OK.
    expected_cki_labels = {new_cki_status_label}
    expected_cki_labels.update(label for label in new_cki_pipeline_labels if
                               label.scope is not MrScope.READY_FOR_MERGE)
    to_add = expected_cki_labels - current_mr_cki_labels
    to_remove = current_mr_cki_labels - expected_cki_labels
    LOGGER.info('Overall status label: %s', new_cki_status_label)
    LOGGER.debug('to_add: %s', to_add)
    LOGGER.debug('to_remove: %s', to_remove)
    # Update the MR labels if needed.
    if to_add:
        pipe_mr.add_labels(to_add)
    if to_remove:
        pipe_mr.remove_labels(to_remove)
    if not to_add and not to_remove:
        LOGGER.info('MR already has all the expected labels, nothing to do.')

    # Update the report comment.
    report_text = generate_comment(pipe_mr, pipelines, new_cki_status_label,
                                   invalid_branch, protected_branches)
    LOGGER.info('Leaving comment: \n%s', report_text)
    gl_mr = pipe_mr.gl_mr
    session.update_webhook_comment(gl_mr, report_text,
                                   bot_name=pipe_mr.current_user.username,
                                   identifier=REPORT_HEADER)


def process_pipe_mr(session, pipe_mr: PipeMR) -> None:
    """Populate a sorted dict of the MR's pipelines and call update_mr()."""
    # If the MR state is not 'opened' then don't even bother.
    if pipe_mr.state is not MrState.OPENED:
        LOGGER.info('MR state is %s, nothing to do.', pipe_mr.state.name)
        return
    # If the MR does not have a recognized branch then don't even bother.
    if not pipe_mr.branch:
        LOGGER.info("MR target branch '%s' is not recognized, nothing to do.",
                    pipe_mr.target_branch)
        return
    if pipe_mr.pipelines:
        protected_branches = []
        invalid_branch = None
    else:
        protected_branches = [b.name for b in pipe_mr.gl_project.protectedbranches.list(all=True)]
        invalid_branch = pipe_mr.source_branch if any(
            fnmatch.fnmatchcase(pipe_mr.source_branch, b) for b in protected_branches
        ) else None
    # Create a mapping of PipelineType:PipelineResult pairs.
    pipelines = {}
    for pipeline in pipe_mr.pipelines:
        if pipeline.type in pipelines:
            raise ValueError(f'Multiple pipelines have the same type: {pipeline.type}')
        if pipeline.type == PipelineType.INVALID:
            continue
        pipelines[pipeline.type] = pipeline
    # Insert placeholders keys for any missing pipelines.
    for ptype in pipe_mr.branch.pipelines:
        if ptype not in pipelines:
            pipelines[ptype] = None
    # Sort the pipelines so we process them in a consistent order.
    update_mr(session, pipe_mr, dict(sorted(pipelines.items())), invalid_branch, protected_branches)


def fix_possible_mismatching_status(session: 'SessionRunner', event: 'GitlabPipelineEvent',
                                    pipe_mr: PipeMR):
    """Fix mismatching status between upstream trigger job and downstream pipeline.

    This is a workaround for the bug, usually with the upstream failing and downstream passing,
    currently tracked at: https://gitlab.com/gitlab-org/gitlab/-/issues/340064
    """
    reason = "fix_possible_mismatching_status"

    # Only try to fix when downstream pipeline has passed
    downstream_status = PipelineStatus.get(event.object_attributes["status"])
    if downstream_status is not PipelineStatus.OK:
        LOGGER.debug("Skipping %r on pipeline %r because the downstream pipeline didn't succeed",
                     reason, event.url)
        return

    # Warn if we can't find the upstream trigger job
    trigger_job_name = common.get_pipeline_variable(event.body, "trigger_job_name")
    find_status = (p.bridge_status for p in pipe_mr.pipelines if p.bridge_name == trigger_job_name)
    upstream_status = next(find_status, None)
    if upstream_status is None:
        LOGGER.warning("Skipping %r on pipeline %r because failed to find the trigger job in %r",
                       reason, event.url, pipe_mr.pipelines)
        return

    # Only try to fix when trigger job has failed
    if upstream_status is not PipelineStatus.FAILED:
        LOGGER.debug("Skipping %r on pipeline %r because the trigger job didn't fail (status=%r)",
                     reason, event.url, upstream_status.name)
        return

    # status of the upstream trigger job doesn't match downstream's pipeline
    LOGGER.error(
        ("Mismatching status (%r != %r) between upstream trigger job and"
         " downstream pipeline: %s, tentatively retrying an idempotent job."),
        upstream_status.name, downstream_status.name, event.url,
    )
    # Retrying a job is usually enough to fix it
    retry_check_kernel_results(session, pipeline_url=event.url, reason=reason)


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent', 'GitlabPipelineEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    pipe_mr = PipeMR(session.graphql, session.gl_instance, session.rh_projects, event.mr_url)
    if event.kind is GitlabObjectKind.MERGE_REQUEST and branch_changed(event.body):
        if process_possible_branch_change(session, pipe_mr, event.body):
            return

    if event.kind == GitlabObjectKind.PIPELINE and event.is_downstream:
        fix_possible_mismatching_status(session, event, pipe_mr)

    process_pipe_mr(session, pipe_mr)


def process_push_event(
    _: dict,
    session: 'SessionRunner',
    event: 'GitlabPushEvent',
    **__: typing.Any
) -> None:
    """Process a push event message."""
    LOGGER.info('Processing push event for %s on branch %s.',
                event.rh_branch.project.namespace, event.rh_branch)
    if not event.rh_branch.name.endswith('-rt'):
        LOGGER.info('Ignoring push to non-rt branch.')
        return
    retrigger_failed_pipelines(session.graphql, event.rh_branch)


def process_datawarehouse_event(
    _: dict,
    session: 'SessionRunner',
    event: 'DataWarehouseEvent',
    **__: typing.Any
) -> None:
    """Process an event triggered by a message from DataWarehouse."""
    LOGGER.info('Processing %s event: %r', event.type.name, event)

    # DataWarehouseEvent should've filtered event status and object_type at this point
    provenances = misc.get_nested_key(event.object, "misc/provenance", default=[])
    urls = (p["url"] for p in provenances if p.get("service_name") == "gitlab")
    if pipeline_url := next(urls, None):
        retry_check_kernel_results(session, pipeline_url, reason=event.status)
    else:
        LOGGER.info("Nothing to be done to event %r. Checkout has no pipeline url", event)


HANDLERS = {
    GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    GitlabObjectKind.NOTE: process_gl_event,
    GitlabObjectKind.PIPELINE: process_gl_event,
    GitlabObjectKind.PUSH: process_push_event,
    MessageType.DATAWAREHOUSE: process_datawarehouse_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    session = new_session('ckihook', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
