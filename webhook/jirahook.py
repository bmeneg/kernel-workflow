# pylint: disable=invalid-name
"""Query JIRA for MRs."""
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from functools import cached_property
import re
import sys
import typing

from cki_lib.logger import get_logger

from webhook import common
from webhook import defs
from webhook import fragments
from webhook import libjira
from webhook import table
from webhook.bughook import make_bugs
from webhook.description import Description
from webhook.description import MRDescription
from webhook.graphql import GitlabGraph
from webhook.jissue import JIssue
from webhook.jissue import find_linked_issues
from webhook.jissue import make_jissues
from webhook.rh_metadata import Branch
from webhook.rh_metadata import Projects
from webhook.session import new_session

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent
    from .session_events import UmbBridgeEvent

LOGGER = get_logger('cki.webhook.jirahook')

COMMENT_TITLE = '**JIRA Hook Readiness Report**'
COMMENT_FOOTER = ("Guidelines for these entries can be found in CommitRules: "
                  "https://red.ht/kwf_commit_rules.  \nTo request re-evalution either remove the "
                  "JIRA label from the MR or add a comment with only the text: "
                  "request-jirahook-evaluation.")


@dataclass
class MR:
    # pylint: disable=too-many-instance-attributes
    """Store for MR data."""

    # required parameters
    graphql: GitlabGraph
    projects: Projects = field(repr=False)
    namespace: str
    mr_id: int
    is_dependency: bool = False

    # set by do_jirahook_query()
    commits: dict = field(default_factory=dict, init=False, repr=False)
    title: str = field(default='', init=False)
    description: MRDescription | None = field(default=None, init=False)
    global_id: str = field(default='', init=False)
    project_id: str = field(default='', init=False)
    branch: Branch = field(default=None, init=False, repr=False)
    state: defs.MrState = field(default=defs.MrState.UNKNOWN, init=False)
    only_internal_files: bool = False  # set if the MR only touches INTERNAL_FILES
    depends_mrs: list = field(default_factory=list, init=False, repr=False)
    labels: list = field(default_factory=list, init=False)
    pipeline_finished: datetime = field(default=None, init=False)
    is_draft: bool = field(default=False, init=False)

    def __post_init__(self):
        """Initialize the object."""
        super().__init__()
        self._get_mr_data()
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Return a string represantion of the MR."""
        repr_str = f'MR {self.namespace}!{self.mr_id}'
        repr_str += f', commits: {len(self.commits)}'
        repr_str += f', state: {self.state.name.lower()}'
        repr_str += f', is_draft: {self.is_draft}'
        repr_str += f", is_dependency: {self.is_dependency}"
        repr_str += f", project: {self.project.name if self.project else 'None'}"
        repr_str += f", branch: {self.branch.name if self.branch else 'None'}"
        return f'<{repr_str}>'

    def _get_mr_data(self):
        """Query the MR and return an MR object."""
        query_params = {'namespace': self.namespace, 'mr_id': str(self.mr_id)}
        results = self.graphql.check_query_results(self.graphql.client.query(
            JIRAHOOK_QUERY, variable_values=query_params, paged_key='project/mr/commits'),
            check_keys={'currentUser', 'project'})
        if not results:
            LOGGER.warning('No query results??')
            return
        if not results['project']['mr']:
            LOGGER.warning('Merge request does not exist?')
            return
        self._populate_attributes(results)

    def _populate_attributes(self, results):
        """Parse JIRAHOOK_QUERY results into attributes."""
        raw_commits = results['project']['mr']['commits']['nodes']
        self.commits = {commit['sha']: Description(commit['description']) for commit in raw_commits}
        self.global_id = results['project']['mr']['id']
        self.project_id = results['project']['id']
        self.title = results['project']['mr']['title']
        self.description = MRDescription(results['project']['mr']['description'], self.namespace,
                                         self.graphql)
        mr_file_list = [path['path'] for path in results['project']['mr']['files']]
        self.only_internal_files = self._mr_has_only_internal_files(mr_file_list)
        self.state = defs.MrState.from_str(results['project']['mr']['state'])
        self.branch = self.projects.get_target_branch(self.project_id,
                                                      results['project']['mr']['targetBranch'])
        self.labels = [defs.Label(label['title']) for
                       label in results['project']['mr']['labels']['nodes']]
        if results['project']['mr']['headPipeline'] and \
           results['project']['mr']['headPipeline']['finishedAt']:
            pipeline_timestamp = results['project']['mr']['headPipeline']['finishedAt']
            self.pipeline_finished = datetime.fromisoformat(pipeline_timestamp[:19])
        self.is_draft = results['project']['mr']['draft']

    @staticmethod
    def _mr_has_only_internal_files(path_list):
        """Return True if all the file paths begin with allowed paths for INTERNAL issues."""
        return all(path.startswith(defs.INTERNAL_FILES) for path in path_list)

    @property
    def all_jissue_ids(self):
        """Return the set of all the JIssue IDs referenced in the MR."""
        if not self.description:
            return set()
        all_jiras = set()
        for desc in self.all_descriptions:
            all_jiras.update(desc.jissue)
            if hasattr(desc, 'depends_jiras'):
                all_jiras.update(desc.depends_jiras)
        return all_jiras

    @property
    def all_descriptions(self):
        """Return  a list of all the Description objects from the MR."""
        if not self.description:
            return []
        return [self.description] + list(self.commits.values())

    @cached_property
    def jissues(self):
        """Return the list of all JIssue objects derived from this MR."""
        # This should be any JIRA Issue called out in a JIRA: or Depends: tag in all the MRs
        # plus possibly the faux INTERNAL and/or UNTAGGED JIssue.
        all_mrs = [self] + self.depends_mrs
        jissues = make_jissues(self.all_jissue_ids, mrs=all_mrs)
        if self.has_internal:
            jissues.append(JIssue.new_internal(mrs=all_mrs))
        if self.has_untagged:
            jissues.append(JIssue.new_untagged(mrs=all_mrs))
        return jissues

    @property
    def issues_with_scopes(self):
        """Return the list of JIssue objects after doing tests."""
        jissues = self.jissues
        for jissue in jissues:
            jissue.set_scope()
        return jissues

    @cached_property
    def linked_jissues(self):
        """Return the list of all linked JIssue objects derived from this MR."""
        # This should be any JIRA Issue(s) *linked* to a called out JIRA tag in the MR
        # but only if the jira is a valid KWF one (Bug or Story, kernel* component)
        jissues = [issue for issue in self.jissues if issue.kwf_ready]
        return find_linked_issues(jissues, self.branch.fix_versions, self.description.cve, [self])

    @property
    def linked_issues_with_scopes(self):
        """Return the list of JIssue objects after doing tests."""
        jissues = self.linked_jissues
        for jissue in jissues:
            jissue.set_scope()
        return jissues

    @cached_property
    def cves(self):
        """Return the list of BZ Cve objects derived from this MR's description only."""
        # In other words, CVE tags in the descriptions of dependency MRs are not considered.
        return make_bugs(self.description.cve, mrs=[self] + self.depends_mrs) if \
            self.description else []

    @property
    def cves_with_scopes(self):
        """Return the list of Cves objects after doing tests."""
        cves = self.cves
        for cve in cves:
            cve.set_scope()
        return cves

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose JIRA tag refers to a Depends."""
        if not self.description or not self.description.depends_jiras:
            return ''
        return next((sha for sha, commit in self.commits.items() for ji_id in
                     self.description.depends_jiras if ji_id in commit.jissue), '')

    @property
    def has_internal(self):
        """Return True if the MR description or any commits are marked INTERNAL."""
        return any(description.marked_internal for description in self.all_descriptions)

    @property
    def has_untagged(self):
        """Return True if any commits are UNTAGGED."""
        return not all(description.jissue or description.bugzilla or description.marked_internal for
                       description in self.commits.values())

    def new_depends_mr(self, mr_id):
        """Return a new MR object with is_dependency set to True."""
        return MR(graphql=self.graphql, projects=self.projects, namespace=self.namespace,
                  mr_id=mr_id, is_dependency=True)

    @property
    def project(self):
        """Return the project associated with the MR, if any."""
        return self.branch.project if self.branch else None

    def get_depends_mrs(self):
        # pylint: disable=protected-access
        """Populate depends_mrs with MR objects derived from this MR's Description.depends_mrs."""
        for mr_id in self.description.depends_mrs:
            new_mr = self.new_depends_mr(mr_id)
            self.depends_mrs.append(new_mr)


class TagRow(table.TableRow):
    # pylint: disable=too-few-public-methods
    """Generic functions for all Tag TableRow objects."""

    @staticmethod
    def _format_cve(cve_name):
        """Return the cve_name as a markdown link."""
        def make_cve_link(cve_match):
            """Return the CVE in the match object as a markdown link."""
            return f'[{cve_match.group()}](https://bugzilla.redhat.com/{cve_match.group()})'

        return re.sub(r'CVE-\d{4}-\d{4,7}', make_cve_link, cve_name)

    @staticmethod
    def _format_JIRA_Issues(jissue):
        """Format the JIRA Issue field."""
        status_str = jissue.ji_status.name
        if jissue.ji_status is defs.JIStatus.CLOSED:
            status_str += f': {jissue.ji_resolution.name}'
        return f'{jissue.id} ({status_str})' if isinstance(jissue.id, str) else jissue.id

    def _format_CVEs(self, cves):
        """Format the CVEs field."""
        if not cves:
            cve_str = 'None'
        else:
            cve_str = ''
            for cve in cves:
                cve_str += f'{self._format_cve(cve)}<br>'
        return cve_str

    @staticmethod
    def _format_Commits(commits):
        """Format the list of commits."""
        if len(commits) > defs.MAX_COMMITS_PER_COMMENT_ROW:
            commits = list(commits)[:defs.MAX_COMMITS_PER_COMMENT_ROW]
            commits.append('(...)')
        return commits if commits else 'None'

    @staticmethod
    def _format_Notes(notes):
        """Format the Notes field."""
        return '<br>'.join(f'See {note}' for note in notes) if notes else '-'

    @staticmethod
    def _format_Policy_Check(issue):
        """Create a string for the Policy Check field."""
        check_passed, check_msg = issue.policy_check_ok
        if check_passed is True:
            policy_str = 'Passed'
        elif check_passed is False:
            policy_str = 'Failed:<br>' + check_msg
        elif check_passed is None:
            policy_str = check_msg
        return policy_str


class IssueRow(TagRow):
    """Format a TableRow for JIssue objects."""

    def __init__(self):
        """Init."""
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Commits = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    def populate(self, tag, footnotes):
        """Populate the row with data from a JIssue tag."""
        jtag = f'[{tag.id}]({defs.JIRA_SERVER}browse/{tag.id}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Commits', tag.commits)
        self.set_value('Readiness', tag.scope.name)
        self.set_value('Policy_Check', tag)
        self.set_value('Notes', footnotes)
        return self


class CveRow(TagRow):
    """Format a TableRow for Cve objects."""

    def __init__(self):
        """Init."""
        self.CVEs = ''
        self.Priority = ''
        self.Commits = ''
        self.Clones = ''
        self.Readiness = ''
        self.Notes = ''

    @staticmethod
    def _format_Clones(cve):
        """Format the Clones field."""
        # If no parent_mr we can't proceed.
        if not cve.parent_mr:
            return 'Unknown'
        # Skip over low prio CVEs and rhel-6 (it has no zstreams)
        if cve.bz_priority < defs.BZPriority.HIGH or cve.parent_mr.project.name == 'rhel-6':
            return 'N/A'
        if not (clones := cve.jira_clones):
            return 'None'
        parent_clone = next((issue for issue in clones if issue.ji_branch == cve.parent_mr.branch),
                            None)
        clones_str = ''
        for clone in clones:
            target = clone.ji_fix_version
            # There should only be one?
            component = next((comp for comp in clone.ji_components), 'Unknown')
            status = clone.ji_status.name
            if clone.ji_status is defs.JIStatus.CLOSED:
                status += f' {clone.ji_resolution.name}'
            host = 'https://bugzilla.redhat.com' if clone.id.isdigit() else \
                f'{defs.JIRA_SERVER}browse'
            clone_url = f'[{clone.id}]({host}/{clone.id})'
            clone_str = f'{target} ({component}): {clone_url} ({status})'
            clones_str += f'{clone_str}<br>' if parent_clone != clone else f'**{clone_str}**<br>'
        return clones_str

    def populate(self, cve, footnotes):
        """Populate the row with data from a CVE JIRA Issue."""
        self.set_value('CVEs', cve.cve_ids)
        self.set_value('Priority', cve.bz_priority.name.capitalize())
        self.set_value('Commits', cve.commits)
        self.set_value('Clones', cve)
        self.set_value('Readiness', cve.scope.name)
        self.set_value('Notes', footnotes)
        return self


class DepRow(TagRow):
    """Format a TableRow for JIssue objects that are dependencies."""

    def __init__(self):
        """Init."""
        self.MR = ''
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Commits = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    @staticmethod
    def _format_MR(jissue):
        """Set the MR field."""
        return f'!{jissue.mr.mr_id} ({jissue.mr.branch.name})' if jissue.mr else '?'

    def populate(self, tag, footnotes):
        """Populate the row with data from a JIssue tag."""
        jtag = f'[{tag.id}]({defs.JIRA_SERVER}browse/{tag.id}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('MR', tag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Commits', tag.commits)
        self.set_value('Notes', footnotes)
        if tag.mr.state is not defs.MrState.MERGED:
            self.set_value('Readiness', tag.scope.name)
            self.set_value('Policy_Check', tag)
        return self


class LinkedIssueRow(TagRow):
    """Format a TableRow for JIssue objects."""

    def __init__(self):
        """Init."""
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Component = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    def populate(self, tag, footnotes):
        """Populate the row with data from a JIssue tag."""
        jtag = f'[{tag.id}]({defs.JIRA_SERVER}browse/{tag.id}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Component', tag.ji_components)
        self.set_value('Readiness', tag.scope.name)
        self.set_value('Policy_Check', tag)
        self.set_value('Notes', footnotes)
        return self


# All the (non-jira) data we need to process the MR in one shot. Maybe.
JIRAHOOK_QUERY_BASE = """
query mrData($mr_id: String!, $namespace: ID!, $first: Boolean = true, $after: String = "") {
  ...CurrentUser @include(if: $first)
  project(fullPath: $namespace) {
    id @include(if: $first)
    mr: mergeRequest(iid: $mr_id) {
      ...MrCommits
      ...MrFiles @include(if: $first)
      title @include(if: $first)
      description @include(if: $first)
      id @include(if: $first)
      ...MrLabels @include(if: $first)
      state @include(if: $first)
      draft @include(if: $first)
      targetBranch @include(if: $first)
      headPipeline @include(if: $first) {
        finishedAt
      }
    }
  }
}
"""

JIRAHOOK_QUERY = (JIRAHOOK_QUERY_BASE +
                  fragments.CURRENT_USER +
                  fragments.MR_COMMITS +
                  fragments.MR_FILES +
                  fragments.MR_LABELS +
                  fragments.GL_USER)


def find_needed_footnotes(jissue_list):
    """Return a dict of test_name: test.notes needed for the given list of Issues."""
    return {test_name: next((test.note for test in jissue.test_list if test.__name__ == test_name))
            for jissue in jissue_list for test_name in jissue.failed_tests}


def create_ji_table(row_class, jissue_list):
    """Return a Table object populated with the given row class."""
    footnotes = find_needed_footnotes(jissue_list) if jissue_list else {}
    tag_table = table.Table(footnote_list=footnotes.values())
    for jissue in jissue_list:
        note_idx = sorted([list(footnotes.keys()).index(test) + 1 for test in jissue.failed_tests])
        row = row_class().populate(jissue, note_idx)
        tag_table.add_row(row)
    return tag_table


def generate_comment(target_branch, mr_scope, tables):
    """Generate the comment string."""
    status = 'fails' if mr_scope < defs.MrScope.READY_FOR_QA else 'passes'
    label = mr_scope.label("JIRA")
    total_rows = len(tables[0]) + len(tables[1]) + len(tables[2])
    issue_table = str(tables[0])
    dep_table = str(tables[1])
    cve_table = str(tables[2])
    links_table = str(tables[3])

    post = COMMENT_TITLE + '\n\n'

    post += f'Target Branch: {target_branch}  \n\n'
    post += (f'This merge request **{status}** jirahook validation: '
             f'~"{label}"  \n\n')

    if total_rows > 10:
        post += '<details><summary>Click to expand</summary>  \n\n'
    if issue_table:
        post += '##### JIRA Issue tags:  \n' + issue_table + '\n'
    if dep_table:
        post += '##### Depends tags:  \n' + dep_table + '\n'
    if cve_table:
        post += '##### CVE tags:  \n' + cve_table + '\n'
    if links_table:
        post += '##### Linked JIRA Issues:  \n' + links_table + '\n'
    if total_rows > 10:
        post += '</details>  \n'

    post += '\n' + COMMENT_FOOTER
    return post


def update_statuses(session, this_mr, all_issues, labels, comment):
    """Wrap old API stuff to set the labels, check dependencies and set testing requested."""
    # pylint: disable=too-many-arguments
    gl_project = session.get_gl_project(this_mr.namespace)
    gl_mergerequest = gl_project.mergerequests.get(this_mr.mr_id)

    LOGGER.info('Posting comment using REST:\n%s', comment)
    session.update_webhook_comment(gl_mergerequest, comment,
                                   bot_name=this_mr.graphql.username,
                                   identifier=COMMENT_TITLE)

    cur_labels = common.add_label_to_merge_request(gl_project, this_mr.mr_id, labels)
    LOGGER.debug('Current MR labels: %s', cur_labels)
    common.add_merge_request_to_milestone(this_mr.branch, gl_project, gl_mergerequest)
    libjira.request_preliminary_testing(all_issues, cur_labels)


def generate_dependencies_label(first_dep_sha):
    """Generate the Dependencies label from the given sha, if any."""
    scope = first_dep_sha[:12] if first_dep_sha else defs.READY_SUFFIX
    return f'Dependencies::{scope}'


def get_lowest_scope(issues):
    """Return the lowest scope of all the JIssues/CVEs."""
    scope = defs.MrScope.READY_FOR_MERGE
    for issue in issues:
        scope = min(scope, issue.scope)
    return scope


def process_mr(session, mr_url):
    # pylint: disable=too-many-arguments,too-many-locals
    """Process the given MR."""
    session.rh_projects.do_load_policies()

    # Fetch and parse MR data.
    if not session.graphql.check_mr_state(mr_url.namespace, mr_url.id):
        LOGGER.info('This MR is not open or has too many commits, ignoring.')
        return
    this_mr = MR(session.graphql, session.rh_projects, mr_url.namespace, mr_url.id)

    # Skip funny MRs.
    if not this_mr.description:
        LOGGER.info('This MR has no description, nothing to do.')
        return
    if not this_mr.commits:
        LOGGER.info('This MR has no commits, nothing to do.')
        return

    # Get data for Depends: MRs before we do anything else.
    this_mr.get_depends_mrs()

    # Hack to not run when there are bugzilla refs but no jira issues.
    # mr_issues for our purposes here should only be the issues that correspond to a JIRA tag
    # in the MR Description.
    mr_issues = [jissue.ji for jissue in this_mr.jissues if
                 jissue.ji and jissue.id in this_mr.description.jissue]
    all_issues = mr_issues + [issue.ji for issue in this_mr.linked_jissues]
    bugs = this_mr.description.bugzilla
    if bugs and not all_issues:
        LOGGER.info("No jira issues provided, but we have bugzillas: %s", bugs)
        comment = COMMENT_TITLE + '\n\n'
        comment += "Nothing to report, this MR is using bugzilla instead of jira."
        update_statuses(session, this_mr, [], [f'JIRA::{defs.READY_SUFFIX}'], comment)
        return

    if all_issues and not this_mr.is_draft:
        # Move all_issues to Planning or In Progress.
        libjira.move_issue_states_forward(all_issues)
        # Add cross-reference to GitLab MR in JIRA Issue
        libjira.add_gitlab_link_in_issues(session, all_issues, this_mr)
    else:
        LOGGER.info('MR is in draft state, not moving JIRA Issue states forward.')

    # Get the JIRA issue lists with their scopes set
    issues = this_mr.issues_with_scopes
    for issue in issues:
        LOGGER.info("Issue: %s", issue)
    cves = this_mr.cves_with_scopes
    # This call must be before getting linked_issues_with_scopes to reflect ITM/DTM updates
    libjira.sync_jissue_fields(issues, this_mr.linked_jissues)
    linked_issues = this_mr.linked_issues_with_scopes

    # Generate a tuple of results tables: Issues, Deps, and CVEs.
    tables = (create_ji_table(IssueRow, [issue for issue in issues if issue.mr is this_mr]),
              create_ji_table(DepRow, [issue for issue in issues if issue.mr is not this_mr]),
              create_ji_table(CveRow, cves),
              create_ji_table(LinkedIssueRow, linked_issues)
              )

    mr_scope = get_lowest_scope(issues + cves + linked_issues)
    comment = generate_comment(this_mr.branch.name, mr_scope, tables)

    # Update the MR labels and comment
    # dependencies_label = generate_dependencies_label(this_mr.first_dep_sha)
    update_statuses(session, this_mr, all_issues, [mr_scope.label('JIRA')], comment)


def process_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent', 'UmbBridgeEvent'],
    **__: typing.Any
) -> None:
    """Process an event."""
    LOGGER.info('Processing %s event for %s', getattr(event, 'kind', event.type).name, event.mr_url)
    process_mr(session, event.mr_url)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_event,
    defs.GitlabObjectKind.NOTE: process_event,
    defs.MessageType.UMB_BRIDGE: process_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('JIRAHOOK')
    args = parser.parse_args(args)
    session = new_session('jirahook', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
