"""Ensure MR commits have the necessary DCO signoff."""
from dataclasses import dataclass
import sys
import typing

from cki_lib.logger import get_logger

from webhook import common
from webhook import defs
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.session import new_session
from webhook.table import Table
from webhook.table import TableRow

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = get_logger('cki.webhook.signoff')


REPORT_HEADER = '**DCO Signoff Check Report**'
REPORT_STATUS = 'The DCO Signoff Check for all commits and the MR description has **%s**.\n\n'
REPORT_HELP = ("Some issues were found when validating the DCO Signoff of this MR's"
               " description and its commits.  \n"
               "A valid DCO Signoff is given in the format: `Signed-off-by: Name <email>`\n.  "
               "The Signoff Name and email must match the author name and email of the commit.\n\n"
               "Validation failures often occur due to syntax so please ensure the Signoff:\n"
               "- is on a line by itself **with no leading whitespace**\n"
               "- does not contain markdown syntax\n"
               "- has <> brackets around the email address (Gitlab does not render these)\n\n"
               "If a Signoff string was copy/pasted from the Gitlab UI it may contain"
               " extraneous characters or be missing brackets.\n\n")
REPORT_NO_COMMITS = ('No commits were found for this MR. If this is an error please retry the'
                     ' check by removing the Signoff label from this MR.  Contact the Webhooks'
                     ' team if the problem persists\n\n.')
DCO_URL = "https://developercertificate.org"
REPORT_FOOTER = ("This project requires developers add a Merge Request description and per-commit "
                 f"acknowlegement of the [Developer Certificate of Origin]({DCO_URL}), also known "
                 "as the DCO. This can be accomplished by adding an explicit 'Signed-off-by:' tag "
                 "to your MR description and each commit.\n\n"
                 "**This Merge Request's commits will not be considered for inclusion into this "
                 "project until these problems are resolved. After making the required changes "
                 "please resubmit your merge request for review.**")

DCO_STRING = 'Signed-off-by: %s <%s>'


@dataclass(repr=False)
class MR(CommitsMixin, BaseMR):
    """Represent the MR."""


def update_mr(session, gl_project, mr_id, report_text):
    """Update the MR with a note of the results and possibly set the label scope."""
    gl_mr = gl_project.mergerequests.get(mr_id)

    current_scope = next((lbl.rsplit('::', 1)[-1] for lbl in gl_mr.labels if
                          lbl.startswith('Signoff::')), None)
    new_scope = defs.READY_SUFFIX if REPORT_STATUS % 'PASSED' in report_text else \
        defs.NEEDS_REVIEW_SUFFIX
    LOGGER.info("current scope: '%s', new scope: '%s'", current_scope, new_scope)

    label = f'Signoff::{new_scope}'
    if current_scope != new_scope:
        common.add_label_to_merge_request(gl_project, mr_id, [label])
    report_text = REPORT_HEADER + f' ~"{label}"\n\n' + report_text
    LOGGER.info('Leaving comment: \n%s', report_text)
    session.update_webhook_comment(gl_mr, report_text,
                                   bot_name=gl_project.manager.gitlab.user.username,
                                   identifier=REPORT_HEADER)


class SignoffRow(TableRow):
    # pylint: disable=invalid-name,too-few-public-methods
    """A TableRow for Signoffs."""

    def __init__(self, commit, needed_footnotes):
        """Define the Row columns."""
        short_dco = DCO_STRING.removeprefix('Signed-off-by: ')
        footnote_index = needed_footnotes.index(commit.dco_state.footnote) + 1
        self.Commit = commit.sha if len(commit.sha) != 40 else commit.short_sha
        self.Check_Result = f'**{commit.dco_state.title}**<sup>{footnote_index}</sup>'
        self.Expected_Signoff = f'`{short_dco % commit.expected_signoff}`'
        signoffs = [f'`{short_dco % dco}`' for dco in commit.description.signoff]
        self.Found_Signoffs = '<br>'.join(signoffs) if signoffs else 'None'


def generate_report(commits: list, public_signoff_ok: bool):
    """Return a markdown formatted string with the results from the list of commits."""
    report_str = ''
    # If there are no real commits then say so and be done.
    if len(commits) == 0 or (len(commits) == 1 and commits[0].sha == 'MR Description'):
        report_str += REPORT_STATUS % 'FAILED' + REPORT_NO_COMMITS
        return report_str

    # DCOStates we consider OK. Include OK_NOT_REDHAT if public_signoff_ok.
    ok_dco_states = (defs.DCOState.OK, defs.DCOState.OK_NOT_REDHAT) if public_signoff_ok else \
        (defs.DCOState.OK,)

    # If everything is in ok_dco_states then don't bother generating a table and be done.
    if not (states := {commit.dco_state for commit in commits if
                       commit.dco_state not in ok_dco_states}):
        report_str += REPORT_STATUS % 'PASSED'
        return report_str

    # We have commits and some have a bad state so build a Table.
    needed_footnotes = [state.footnote for state in states]
    report_table = Table(footnote_list=needed_footnotes)
    for commit in commits:
        if commit.dco_state in ok_dco_states:
            continue
        row = SignoffRow(commit, needed_footnotes)
        report_table.add_row(row)

    # Generate report text
    report_str += REPORT_STATUS % 'FAILED' + REPORT_HELP
    if len(report_table) > 10:
        report_str += '<details><summary>Click to expand</summary>  \n\n'
    report_str += str(report_table)
    if len(report_table) > 10:
        report_str += '</details>  \n'
    report_str += REPORT_FOOTER
    return report_str


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    signoff_mr = MR(session.graphql, session.gl_instance, session.rh_projects, event.mr_url)
    report_text = generate_report(signoff_mr.all_commits, signoff_mr.project.public_signoff_ok)
    update_mr(session, signoff_mr.gl_project, signoff_mr.iid, report_text)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SIGNOFF')
    args = parser.parse_args(args)
    session = new_session('signoff', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
