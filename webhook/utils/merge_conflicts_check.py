#!/usr/bin/env python
"""Look for MRs that cannot be merged and have stale Merge::OK labels."""

import argparse
import os
import subprocess

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.misc import sentry_init
import sentry_sdk

from webhook import defs
from webhook import fragments
from webhook import kgit
from webhook.graphql import GitlabGraph
from webhook.mergehook import build_mr_conflict_string
from webhook.mergehook import check_for_merge_conflicts
from webhook.mergehook import format_conflict_info
from webhook.rh_metadata import Projects
from webhook.session import new_session

LOGGER = logger.get_logger('webhook.utils.merge_conflicts_check')

# Get all open MRs.
MR_QUERY_BASE = """
query mrData($namespace: ID!, $first: Boolean = true, $after: String = "") {
  %s(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after) {
      pageInfo {hasNextPage endCursor}
      nodes {iid author{username} title targetBranch project{fullPath} ...MrLabels}
    }
  }
}
"""

MR_QUERY = MR_QUERY_BASE + fragments.MR_LABELS


def get_open_mrs(graphql, namespace, namespace_type):
    """Return a list of the MR IDs with the Acks::OK label."""
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY % namespace_type, variable_values={'namespace': namespace},
                             paged_key=f'{namespace_type}/mergeRequests'),
        check_keys={namespace_type})
    if result and result[namespace_type] is None:
        raise RuntimeError(f"Namespace '{namespace}' is not visible!")
    mrs = {}
    # Sort MRs into namespace/target_branch buckets
    for mreq in misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes'):
        namespace = mreq['project']['fullPath']
        target_branch = mreq['targetBranch']
        ns_tb = f'{namespace}_{target_branch}'
        if ns_tb in mrs:
            mrs[ns_tb].append(mreq)
            continue
        mrs[ns_tb] = [mreq]

    LOGGER.debug('%s MRs: found %s', namespace_type.title(), mrs)
    return mrs


def prep_branch_for_merges(rhkernel_src, mr_params):
    """Prepare a git branch/worktree for doing bulk merges."""
    # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
    # where everything is an additional remote on top of kernel-ark, w/remote name == project name
    proj_target_branch = f"{mr_params['gl_project'].name}/{mr_params['target_branch']}"
    merge_branch = f"{mr_params['gl_project'].name}-{mr_params['target_branch']}-megamerge"
    worktree_dir = f"/{'/'.join(rhkernel_src.strip('/').split('/')[:-1])}/{merge_branch}/"
    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for mergeability testing, please hold",
                worktree_dir, merge_branch)
    kgit.worktree_add(rhkernel_src, merge_branch, worktree_dir, proj_target_branch)
    return worktree_dir


def get_reset_branch_name(merge_dir):
    """Extract reset branch name from merge_dir."""
    merge_base = merge_dir.strip('/').split('/')[-1]
    reset_branch = f"{merge_base}-reset"
    return reset_branch


def try_merging_all(mrs, mr_params):
    """Try merging all MRs together both directions to identify MRs with conflicts."""
    conflict_mrs = []
    pname = mr_params['gl_project'].name
    merge_dir = mr_params['merge_dir']
    reset_branch = get_reset_branch_name(merge_dir)
    interim_branch = f"{reset_branch}-interim"

    for mreq in list(reversed(mrs)):
        kgit.branch_copy(merge_dir, interim_branch)
        mr_id = int(mreq['iid'])
        try:
            kgit.merge(merge_dir, f'{pname}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError:
            kgit.hard_reset(merge_dir, interim_branch)
            if mreq not in conflict_mrs:
                conflict_mrs.append(mreq)
        kgit.branch_delete(merge_dir, interim_branch)

    kgit.hard_reset(merge_dir, reset_branch)
    for mreq in mrs:
        kgit.branch_copy(merge_dir, interim_branch)
        mr_id = int(mreq['iid'])
        try:
            kgit.merge(merge_dir, f'{pname}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError:
            kgit.hard_reset(merge_dir, interim_branch)
            if mreq not in conflict_mrs:
                conflict_mrs.append(mreq)
        kgit.branch_delete(merge_dir, interim_branch)

    kgit.hard_reset(merge_dir, reset_branch)

    return conflict_mrs


def check_pending_conflicts(this_mr, remote_name, conflict_mrs, merge_dir, reset_branch):
    """Check for direct conflicts with other MRs with merge issues."""
    LOGGER.debug("Checking other pending %s MRs for conflicts with MR %s",
                 this_mr['targetBranch'], this_mr['iid'])

    save_branch = f"{reset_branch}-{this_mr['iid']}"
    kgit.branch_copy(merge_dir, save_branch)

    conflicts = []
    target_branch = f"{remote_name}/{this_mr['targetBranch']}"
    for conflict_mr in conflict_mrs:
        mr_id = int(conflict_mr['iid'])
        if mr_id == this_mr['iid']:
            continue

        # First, make sure this MR can actually be merged by itself
        if not kgit.branch_mergeable(merge_dir, target_branch,
                                     f'{remote_name}/merge-requests/{mr_id}'):
            LOGGER.debug("MR %d can't be merged by itself, skipping conflict check", mr_id)
            continue

        # Now, try to merge it on top of this MR, so we can see if it has any conflicts
        kgit.hard_reset(merge_dir, save_branch)
        try:
            kgit.merge(merge_dir, f'{remote_name}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError as err:
            conflicts.append(build_mr_conflict_string(conflict_mr))
            conflicts.append(err.output)

    kgit.branch_delete(merge_dir, save_branch)
    return conflicts


def find_direct_conflicts(session, args, conflict_mrs, mr_params, user):
    """Find which MRs directly conflict with others."""
    gl_project = mr_params['gl_project']
    rh_project = Projects().get_project_by_id(gl_project.id)
    remote_name = gl_project.name if not rh_project.confidential else f'{gl_project.name}-private'
    reset_branch = get_reset_branch_name(mr_params['merge_dir'])

    for mreq in conflict_mrs:
        gl_mergerequest = gl_project.mergerequests.get(mreq['iid'])
        mr_labels = mreq['labels']['nodes']
        LOGGER.debug('Checking for merge conflicts on %s MR %s', mr_params['proj_tb'], mreq['iid'])

        kgit.hard_reset(mr_params['merge_dir'], reset_branch)
        conflict_info = check_for_merge_conflicts(remote_name, gl_mergerequest,
                                                  mr_params['merge_dir'])
        if conflict_info:
            merge_label = defs.MERGE_CONFLICT_LABEL
            note = f'**Mergeability Summary:** ~"{merge_label}"\n\n'
            note += format_conflict_info(conflict_info, merge_label)
            LOGGER.debug('Target branch merge conflicts in %s/%s MR %s:\n%s',
                         remote_name, mreq['targetBranch'], mreq['iid'], note)
            if not args.testing and {'title': merge_label} not in mr_labels:
                gl_mergerequest.notes.create({'body': f'/label {merge_label}'})
                session.update_webhook_comment(gl_mergerequest, note,
                                               bot_name=user,
                                               identifier='**Mergeability Summary:')
            continue

        conflict_info = check_pending_conflicts(mreq, remote_name, conflict_mrs,
                                                mr_params['merge_dir'], reset_branch)
        if conflict_info:
            merge_label = defs.MERGE_WARNING_LABEL
            note = f'**Mergeability Summary:** ~"{merge_label}"\n\n'
            note += format_conflict_info(conflict_info, merge_label)
            LOGGER.debug('Other pending MR merge conflicts found on %s/%s MR %s:\n%s',
                         remote_name, mreq['targetBranch'], mreq['iid'], note)
            if not args.testing and {'title': merge_label} not in mr_labels:
                gl_mergerequest.notes.create({'body': f'/label {merge_label}'})
                session.update_webhook_comment(gl_mergerequest, note,
                                               bot_name=user,
                                               identifier='**Mergeability Summary:')
            continue

        LOGGER.warning('%s/%s MR %s had errors earlier, and now does not?!?',
                       remote_name, mreq['targetBranch'], mreq['iid'])

    kgit.branch_delete(mr_params['merge_dir'], reset_branch)


def clean_up_temp_merge_branches(args, merge_dirs):
    """Clean up all our temp merge branches."""
    for _, merge_dir in merge_dirs.items():
        merge_base = merge_dir.strip('/').split('/')[-1]
        kgit.clean_up_temp_merge_branch(args.rhkernel_src, merge_base, merge_dir)


def set_merge_ok_labels(args, mrs, conflict_mrs, mr_params):
    """Set the Merge::OK label on any MR that doesn't have conflicts if not preset."""
    gl_project = mr_params['gl_project']
    for mreq in mrs:
        if mreq in conflict_mrs:
            continue
        LOGGER.debug('Merge OK for %s/%s MR %s from %s',
                     gl_project.name, mr_params['target_branch'], mreq['iid'],
                     mreq['author']['username'])
        mr_labels = mreq['labels']['nodes']
        gl_mergerequest = gl_project.mergerequests.get(mreq['iid'])
        if not args.testing and {'title': f'Merge::{defs.READY_SUFFIX}'} not in mr_labels:
            gl_mergerequest.notes.create({'body': f'/label Merge::{defs.READY_SUFFIX}'})


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check open MRs for merge conflicts')

    # Global options
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-s', '--since', default=24,
                        help='check if target branch changed since X hours ago.',
                        type=int, required=False)
    parser.add_argument('-r', '--rhkernel-src', default=os.environ.get('RHKERNEL_SRC', ''),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode: do not update gitlab labels")
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(session, args):
    """Find open MRs and check for merge conflicts."""
    if not args.rhkernel_src:
        LOGGER.warning("No path to RH Kernel source git found, aborting!")
        return

    LOGGER.info('Fetching from git remotes to ensure we have the latest data needed')
    kgit.fetch_all(args.rhkernel_src)

    LOGGER.info('Finding open MRs for groups %s and projects %s', args.groups, args.projects)
    graphql = GitlabGraph(get_user=True)
    gl_instance = get_instance(defs.GITFORGE)

    mrs_to_conflict_check = {}
    for group in args.groups:
        mrs_to_conflict_check.update(get_open_mrs(graphql, group, 'group'))
    for project in args.projects:
        mrs_to_conflict_check.update(get_open_mrs(graphql, project, 'project'))

    LOGGER.debug("MR lists: %s", mrs_to_conflict_check)
    if not mrs_to_conflict_check:
        LOGGER.info('No open MRs to process.')
        return

    gl_projects = {}
    merge_dirs = {}
    # Build per-project/target_branch lists of MRs
    for mr_ns_tb, mrs in mrs_to_conflict_check.items():
        mr_params = {}
        mr_params['namespace'] = mr_ns_tb.split('_')[0]
        if not (gl_project := gl_projects.get(mr_params['namespace'])):
            gl_project = gl_instance.projects.get(mr_params['namespace'])
            gl_projects[mr_params['namespace']] = gl_project
        mr_params['gl_project'] = gl_project
        mr_params['target_branch'] = mr_ns_tb.split('_')[1]
        mr_params['proj_tb'] = f"{mr_params['gl_project'].name}_{mr_params['target_branch']}"
        if not (merge_dir := merge_dirs.get(mr_params['proj_tb'])):
            merge_dir = prep_branch_for_merges(args.rhkernel_src, mr_params)
            merge_dirs[mr_params['proj_tb']] = merge_dir

        mr_params['merge_dir'] = merge_dir
        kgit.branch_copy(merge_dir, get_reset_branch_name(merge_dir))

        conflict_mrs = try_merging_all(mrs, mr_params)

        set_merge_ok_labels(args, mrs, conflict_mrs, mr_params)

        find_direct_conflicts(session, args, conflict_mrs, mr_params, graphql.username)

    clean_up_temp_merge_branches(args, merge_dirs)


if __name__ == '__main__':
    pargs = _get_parser_args()
    sentry_init(sentry_sdk, ca_certs=pargs.sentry_ca_certs)
    main(new_session('mergehook', pargs), pargs)
