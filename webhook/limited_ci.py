"""Event handlers."""
import copy
import sys
from time import sleep
import typing

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib.logger import get_logger
import cki_lib.misc
import gitlab
from yaml import Loader
from yaml import load

from . import common
from .defs import GitlabObjectKind
from .session import new_session
from .session_events import create_event

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabPipelineEvent

LOGGER = get_logger('cki.webhook.limited_ci')
CI_CONFIG = {}

EXTERNAL_LABEL_NAME = 'External Contribution'
LIMITED_LABEL_NAME = 'Limited CI'


EMOJI_MAP = {
    'created': ':hourglass_flowing_sand:',
    'pending': ':hourglass_flowing_sand:',
    'running': ':hourglass_flowing_sand:',
    'canceled': ':grey_exclamation:',
    'skipped': ':grey_exclamation:',
    'success': ':heavy_check_mark:',
    'failed': ':sob:',
    'manual': ':no_entry:'
}

WELCOME_MESSAGE = """Hi! This is the friendly CKI test bot.

It appears that you are not a member of {group}. This means that the automatic
CI pipeline on your MR will fail. As getting testing is important, I'll be
responsible for testing your changes. After every MR change, I'll start a
small testing pipeline and link it here so you can follow the results.

**Reviewers can start a full CI run by going into the "Pipelines" tab on this
MR and clicking the "Run pipeline" button.** A passing full run is required in
order to integrate your changes.

You can find more details about the contributor setup, permission issues and
how to resolve them in the [CKI documentation](https://cki-project.org/devel-faq/).
"""


PIPELINE_MESSAGE = """Limited testing pipeline:

&nbsp;&nbsp;{link} - {status} {emoji}


"""


def already_commented(merge_request, bot_name, members_of):
    """Check if bot_name user already added a welcome comment on the MR.

    Args:
        merge_request: MR object as returned by the gitlab module
        bot_name:      Username of the bot account to check.
        members_of:    Group or project to fill into the message template.

    Returns:
        True if a note added by the bot exists on the MR
        False if there is no note added by the bot on the MR
    """
    return any(note.attributes['author']['username'] == bot_name and
               note.attributes['body'].strip() == WELCOME_MESSAGE.format(group=members_of).strip()
               for note in merge_request.notes.list(iterator=True))


def get_vars_from_hook(var_list):
    """Get a dictionary of variables from pipeline webhook.

    The webhook data contains a list of dictionaries which is less fun to deal
    with than a regular key/value dictionary.
    """
    return {v['key']: v['value'] for v in var_list}


def wait_for_attribute(instance, attribute, getter, getter_kwargs):
    """Return the instance once the attribute is available and not null, or blow up."""
    instance_name = instance.__module__
    loop_count = 0
    while not getattr(instance, attribute, None):
        if loop_count >= 5:
            raise ValueError(f'Giving up waiting for {instance_name}.{attribute}. Sleep longer?')
        sleep(2)
        loop_count += 1
        instance = getter(**getter_kwargs)
    LOGGER.info('Found %s.%s after %s loop%s', instance_name, attribute, loop_count,
                '' if loop_count == 1 else 's')
    return instance


def handle_mr(session, gitlab_instance, project_config, mr_data):
    """Trigger pipelines for given MR."""
    mr_id = mr_data['object_attributes']['iid']
    path_with_ns = mr_data['project']['path_with_namespace']
    labels = []

    project = gitlab_instance.projects.get(path_with_ns)
    if not (merge_request := common.get_mr(project, mr_id)):
        return

    # We need diff_refs but we might have to wait for it.
    # https://docs.gitlab.com/ee/api/merge_requests.html#empty-diff_refs-for-new-merge-requests
    merge_request = wait_for_attribute(merge_request, 'diff_refs', project.mergerequests.get,
                                       {'id': mr_id})

    # Get extra data
    extra_variables = {
        'git_url': mr_data['object_attributes']['target']['git_http_url'],
        'branch': mr_data['object_attributes']['target_branch'],
        'commit_hash': mr_data['object_attributes']['last_commit']['id'],
        'title': f"{path_with_ns}: MR {mr_id}",
        'mr_id': mr_id,
        'origin_path': path_with_ns,
        'mr_project_id': mr_data['project']['id'],
        'mr_url': mr_data['object_attributes']['url'],
    }

    if not already_commented(merge_request,
                             gitlab_instance.user.username,
                             project_config['.members_of']):
        # If this is the first time we see the MR that means there is no
        # label assigned to it yet.
        labels.append(EXTERNAL_LABEL_NAME)
        session.update_webhook_comment(merge_request,
                                       WELCOME_MESSAGE.format(group=project_config['.members_of']))

    trigger = copy.deepcopy(project_config)
    trigger.update(extra_variables)
    trigger = config_tree.clean_config(trigger)

    status_message = 'Testing pipeline status:\n'

    if '.cki_external_pipeline_branch' not in project_config or \
            '.cki_external_pipeline_project' not in project_config:
        LOGGER.info('No pipeline project or branch configured for %s', path_with_ns)
        common.add_label_to_merge_request(project, mr_id, labels)
        return

    trigger['cki_pipeline_branch'] = project_config['.cki_external_pipeline_branch']
    trigger['cki_project'] = project_config['.cki_external_pipeline_project']
    external_pipeline = cki_pipeline.trigger_multiple(gitlab_instance, [trigger])[0]

    status_message = ''.join([
        status_message,
        PIPELINE_MESSAGE.format(
            link=external_pipeline.attributes['web_url'],
            status='created',
            emoji=EMOJI_MAP['created'])
    ])
    labels.append(LIMITED_LABEL_NAME + '::running')
    common.add_label_to_merge_request(project, mr_id, labels, remove_scoped=True)

    session.update_webhook_comment(merge_request, status_message)


def get_stage_suffix(job_info_list):
    """Get the ::{stage} suffix of the non-passing job of a pipeline (or '')."""
    failed_job = next((job for job in job_info_list if job['status'] == 'failed'), None)
    return f'::{failed_job["stage"]}' if failed_job else ''


def process_pipeline_message(
    _: dict,
    session: 'SessionRunner',
    event: 'GitlabPipelineEvent',
    **__: typing.Any
) -> None:
    # pylint: disable=too-many-locals,too-many-branches
    """Update existing MR with current pipeline status."""
    LOGGER.info('Processing PIPELINE event for %s', event.mr_url)
    body = event.body
    pipeline_id = str(body['object_attributes']['id'])
    pipeline_vars = get_vars_from_hook(
        body['object_attributes']['variables']
    )

    # Is this a pipeline we care about?
    project_path = pipeline_vars.get('origin_path')
    if not CI_CONFIG.get(project_path):
        LOGGER.info('No matching MR project for %s, found %s',
                    body['commit']['url'],
                    project_path)
        return
    project = session.gl_instance.projects.get(project_path)

    pipeline_status = body['object_attributes']['status']
    is_retriggered = pipeline_vars.get('CKI_DEPLOYMENT_ENVIRONMENT', 'production') != 'production'
    if is_retriggered:
        LOGGER.info('Handling a retriggered pipeline, not posting comments')
        LOGGER.info('STATUS:\n%s', PIPELINE_MESSAGE.format(
            link=f"{body['project']['web_url']}/pipelines/{pipeline_id}",
            status=pipeline_status,
            emoji=EMOJI_MAP[pipeline_status]
        ))
        return

    mr_id = pipeline_vars['mr_id']
    if not (merge_request := common.get_mr(project, mr_id)):
        return
    bot_name = session.gl_instance.user.username

    # Find the correct comment to edit. Bot only creates simple notes so we
    # don't need to dig through every comment in discussions and it's enough to
    # check the top comments.
    bot_discussions = [discussion for discussion in merge_request.discussions.list(iterator=True)
                       if discussion.attributes['notes'][0]['author']['username'] == bot_name]
    for index, discussion in enumerate(bot_discussions):
        first_comment_data = discussion.attributes['notes'][0]
        if pipeline_id in first_comment_data['body']:
            if index + 1 == len(bot_discussions):
                # Only update the labels if this is the newest change set

                # The label should say OK instead of success to be consistent with other labels.
                label_status = 'OK' if pipeline_status == 'success' else pipeline_status
                details = label_status + get_stage_suffix(body['builds'])

                common.add_label_to_merge_request(
                    project, mr_id, [f'{LIMITED_LABEL_NAME}::{details}'], remove_scoped=True
                )
            comment = discussion.notes.get(first_comment_data['id'])
            break
    else:
        # Did someone delete our original comment?
        LOGGER.error('Unable to find comment for %s in MR %s from %s',
                     body['commit']['url'],
                     mr_id,
                     project_path)
        return

    # Edit and update the comment
    body_lines = comment.body.splitlines()
    for index, line in enumerate(body_lines):
        if pipeline_id in line:
            new_line = line.rsplit('-', maxsplit=1)[0] + \
                f' - {pipeline_status} {EMOJI_MAP[pipeline_status]}'
            body_lines[index] = new_line
            break
    if cki_lib.misc.is_production_or_staging():
        comment.body = '\n'.join(body_lines)
        comment.save()
    else:
        LOGGER.info('Not a production deployment, not posting comments')
        LOGGER.info('UPDATED COMMENT:\n%s', '\n'.join(body_lines))


def load_config(config_path):
    """Load webhook configuration.

    Read the config yaml, parse out the actual definitions and populate
    CI_CONFIG.
    """
    # Calm down pylint, there's no better way to pass along global settings if
    # process_message doesn't take extra args
    global CI_CONFIG  # pylint: disable=global-statement
    with open(config_path, encoding='utf-8') as config_file:
        unparsed_config = load(config_file, Loader=Loader)
    CI_CONFIG = config_tree.process_config_tree(unparsed_config)


def get_manual_hook_data(gitlab_instance, mr_object, project_path):
    """Retrieve MR data that mimic the webhook so handler can be reused."""
    project = gitlab_instance.projects.get(project_path)

    return {
        'object_kind': 'pipeline',
        'object_attributes': {
            'target': {'git_http_url': project.attributes['http_url_to_repo']},
            'target_branch': mr_object.attributes['target_branch'],
            'iid': mr_object.attributes['iid'],
            'last_commit': {'id': mr_object.attributes['sha']},
            'url': mr_object.web_url
        },
        'project': {'name': project.attributes['name'],
                    'path_with_namespace': project_path},
        'user': {'username': mr_object.attributes['author']['username']}
    }


def process_mr_message(
    _: dict,
    session: 'SessionRunner',
    event: 'GitlabMREvent',
    **__: typing.Any
) -> None:
    """Check everything about the MR message and call the handler."""
    LOGGER.info('Processing MERGE_REQUEST event for %s', event.mr_url)
    body = event.body
    path_with_ns = body['project']['path_with_namespace']
    if not (project_config := CI_CONFIG.get(path_with_ns)):
        LOGGER.error('Missing config for %s', path_with_ns)
        return

    author = body['user']['username']
    try:
        group = session.gl_instance.groups.get(project_config['.members_of'])
    except gitlab.GitlabGetError:
        group = session.gl_instance.projects.get(project_config['.members_of'])

    if common.get_pog_member(group, author):
        LOGGER.info('Found internal contributor, not triggering')
        return

    if not common.mr_action_affects_commits(body):
        LOGGER.info('Not a code change, ignoring')
        return

    # If we got this far we should trigger the pipeline(s)
    handle_mr(session, session.gl_instance, project_config, body)


HANDLERS = {
    GitlabObjectKind.MERGE_REQUEST: process_mr_message,
    GitlabObjectKind.PIPELINE: process_pipeline_message,
}


def main(args):
    """Set up and start consuming messages or handle manual requests."""
    load_config(cki_lib.misc.get_env_var_or_raise('CONFIG_PATH'))

    parser = common.get_arg_parser('LIMITED_CI')
    args = parser.parse_args(args)
    session = new_session('limited_ci', args, HANDLERS)

    if args.merge_request:
        namespace, mr_id = common.parse_mr_url(args.merge_request)
        mr_object = session.gl_instance.projects.get(namespace, lazy=True).mergerequests.get(mr_id)
        manual_hook_data = get_manual_hook_data(session.gl_instance, mr_object, namespace)
        faux_event = create_event(session, {'message-type': 'gitlab'}, manual_hook_data)
        process_mr_message(manual_hook_data, session, faux_event)
        return

    # Always run with the bot user check disabled
    args.disable_user_check = True
    # Disable kernel branch checks as we often operate on non-kernel projects.
    args.disable_inactive_branch_check = True
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
